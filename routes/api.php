<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});
Route::get('addDefaultRolePermissions', 'Api\GeneralController@addDefaultRolePermissions');

Route::group(['namespace' => 'Api\v1','prefix' => 'v1'], function ($router) {

	Route::post('isEmailExists', 'AuthController@isEmailExists');
	Route::post('isPhoneExists', 'AuthController@isPhoneExists');
	Route::post('sendVerificationCode', 'AuthController@sendVerificationCode');
	Route::post('emailVerification', 'AuthController@emailVerification');
	Route::post('signup', 'AuthController@signup');
	Route::post('login', 'AuthController@login');

	/** seller forgot password **/
	// Route::post('seller/forgotPassword', 'SellerPasswordResetController@forgotPassword');
	// Route::post('seller/matchOtp', 'SellerPasswordResetController@matchOtp');
	// Route::post('seller/resetPassword', 'SellerPasswordResetController@resetPassword');

	/** dealer forgot password **/
	// Route::post('dealer/forgotPassword', 'DealerPasswordResetController@forgotPassword');
	// Route::post('dealer/matchOtp', 'DealerPasswordResetController@matchOtp');
	// Route::post('dealer/resetPassword', 'DealerPasswordResetController@resetPassword');

	Route::get('getVehicleList', 'VehicleController@getVehicleList');
	Route::post('addVehicle', 'VehicleController@addVehicle');

	Route::get('refresh', 'AuthController@refresh');
	Route::get('getCurListingDetail','SellerVehicleController@getCurListingDetail');

	/** forgot password **/
	Route::post('forgotPassword', 'PasswordResetController@forgotPassword');
	Route::post('matchOtp', 'PasswordResetController@matchOtp');
	Route::post('resetPassword', 'PasswordResetController@resetPassword');
	Route::group(['middleware' => 'jwt-auth'], function ($router) {

		Route::post('logout', 'AuthController@logout');

		Route::group(['middleware' => ['auth:api'],'prefix' => 'seller'], function(){
			Route::post('isVinExists','SellerVehicleController@isVinExists');
			Route::post('getAvailableDealersCount','SellerVehicleController@getAvailableDealersCount');
			Route::post('addVehicle','SellerVehicleController@addVehicle');
			Route::post('addVehicleStep1','SellerVehicleController@addVehicleStep1');
			Route::post('addVehicleStep2','SellerVehicleController@addVehicleStep2');
			Route::post('addVehicleStep3','SellerVehicleController@addVehicleStep3');
			Route::post('addVehicleStep4','SellerVehicleController@addVehicleStep4');
			Route::post('addVehicleStep5','SellerVehicleController@addVehicleStep5');
			Route::post('addVehicleStep6','SellerVehicleController@addVehicleStep6');
			Route::post('addVehicleStep7','SellerVehicleController@addVehicleStep7');
			Route::post('addVehicleStep8','SellerVehicleController@addVehicleStep8');
			Route::get('getVehicleInfo','SellerVehicleController@getVehicleInfo');
			Route::get('getVehicleDetail','SellerVehicleController@getVehicleDetail');
			Route::post('deleteVehicle','SellerVehicleController@deleteVehicle');
			Route::get('clearBedge','SellerVehicleController@clearBedge');

			Route::post('getVehicleExteriorImage','SellerVehicleController@getVehicleExteriorImage');
			Route::post('getVehicleInteriorImage','SellerVehicleController@getVehicleInteriorImage');
			Route::post('getVehicleDamageImage','SellerVehicleController@getVehicleDamageImage');

			Route::post('updateProfile','SellerProfileController@updateProfile');
			Route::get('getProfile','SellerProfileController@getProfile');
			Route::post('changePassword','SellerProfileController@changePassword');

			Route::post('savePaymentDetail','PaymentController@savePaymentDetail');
			Route::post('repostVehicle','PaymentController@repostVehicle');
			Route::post('repostVehicleAfter15Days','PaymentController@repostVehicleAfter15Days');
			Route::get('paymentHistory','PaymentController@paymentHistory');
		});

		Route::group(['middleware' => ['auth:api,api-dealer']], function(){
			Route::get('getCurrentListingDetail','SellerVehicleController@getCurrentListingDetail');
			// Route::get('getCurListingDetail','SellerVehicleController@getCurListingDetail');
			Route::post('reportIssue','DealerBidController@reportIssue');
		});

		Route::group(['middleware' => ['auth:api-dealer'],'prefix' => 'dealer'], function(){
			Route::get('getGeneralInfo','DealerBidController@getGeneralInfo');
			Route::get('prviouslyWonsBids','DealerBidController@prviouslyWonsBids');
			Route::get('lifetimeWonsBids','DealerBidController@lifetimeWonsBids');
			Route::post('submitABid','DealerBidController@submitABid');
			Route::post('getListingDetail','DealerBidController@getListingDetail');
			Route::post('updateDealershipInfo','DealerProfileController@updateDealershipInfo');
			Route::post('updateProfile','DealerProfileController@updateProfile');
			Route::get('getProfile','DealerProfileController@getProfile');
			Route::post('changePassword','DealerProfileController@changePassword');
			Route::get('getNotifications','NotificationController@getDealerNotifications');
			Route::get('current_listing','DealerBidController@current_listing');
			Route::post('current_listing_submitted_bids','DealerBidController@current_listing_submitted_bids');
			Route::get('clearBedge','DealerBidController@clearBedge');
			Route::post('reportVehicle','DealerBidController@reportVehicle');

			
		});

	});

});