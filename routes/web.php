<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
// 	return view('welcome');
// });

Auth::routes();

Route::get('/', 'Website\HomeController@index')->name('home');

Route::group(['prefix'=> 'admin', 'namespace'=>'Admin\Auth', 'as'=>'admin.'],function(){

	Route::get('login', 'LoginController@showLoginForm')->name('login');
	Route::post('login', 'LoginController@login');
	Route::post('logout', 'LoginController@logout')->name('logout');

	//password reset routes
	Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
	Route::post('password/reset', 'ResetPasswordController@reset');
	Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');

});
Route::group(['prefix'=> 'admin','middleware'=>'auth:admin', 'namespace'=>'Admin', 'as'=>'admin.'],function(){
	
	//dashboards
	Route::get('/', 'AdminController@index')->name('dashboard');
	Route::get('/changePassword', 'AdminController@changePassword')->name('changePassword');
	Route::post('/update_password', 'AdminController@update_password')->name('update_password');

	//seller
	Route::resource('seller', 'SellerController');
	Route::get('getSellerList', 'SellerController@getSellerList')->name('getSellerList');
	Route::get('repo/{id}', 'SellerController@repo')->name('seller.repo');
	Route::put('updaterepo', 'SellerController@updaterepo')->name('seller.updaterepo');

	Route::get('seller/current_listing/{id}', 'SellerController@current_listing')->name('seller.current_listing');
	Route::get('seller/past_listing/{id}', 'SellerController@past_listing')->name('seller.past_listing');
	Route::get('seller/vehicle_detail/{id}', 'SellerController@vehicle_detail')->name('seller.vehicle_detail');
	Route::get('reports', 'AdminController@reports')->name('reports');
	Route::get('getreports', 'AdminController@getreports')->name('getreports');
	Route::get('report/{id}', 'AdminController@report')->name('report');

	//dealers	
	Route::resource('dealer', 'DealerController');	
	Route::post('dealer/update_profile/{id}', 'DealerController@update_profile')->name('dealer.update_profile');

	Route::get('dealer/edit_profile/{id}', 'DealerController@edit_profile')->name('dealer.edit_profile');
	Route::get('getDealerList', 'DealerController@getDealerList')->name('getDealerList');
	Route::get('dealer/dashboard/{id}', 'DealerController@dashboard')->name('dealer.dashboard');
	Route::get('dealer/submitted_bids/{id}', 'DealerController@submitted_bids')->name('dealer.submitted_bids');
	Route::get('dealer/won_bids/{id}', 'DealerController@won_bids')->name('dealer.won_bids');
	Route::get('dealer/payment_detail/{id}', 'DealerController@payment_detail')->name('dealer.payment_detail');
	Route::get('dealer/cancel_subscription/{id}', 'DealerController@cancel_subscription')->name('dealer.cancel_subscription');
	Route::get('dealer/login_as_dealer/{id}', 'DealerController@login_as_dealer')->name('dealer.login_as_dealer');
	Route::post('dealer/is_approved', 'DealerController@is_approved')->name('dealer.is_approved');
	Route::get('dealer/reported_post/{id}', 'DealerController@reported_post')->name('dealer.reported_post');
	Route::get('/view_bid_detail/{dealer_id}/{id}', 'DealerController@view_bid_detail')->name('dealer.view_bid_detail');
	Route::delete('/removeListing/{dealer_id}/{id}', 'DealerController@removeListing')->name('dealer.removeListing');

	//vehicles
	Route::resource('vehicles', 'VehicleController');
	Route::get('getVehiclesList', 'VehicleController@getVehiclesList')->name('getVehiclesList');	
	Route::get('total_earnings', 'PaymentHistoryController@index')->name('totalEarnings');	
	Route::get('seller_orders', 'PaymentHistoryController@seller_orders')->name('sellerOrders');	
	Route::post('seller_orders_list', 'PaymentHistoryController@seller_orders_list')->name('seller_orders_list');	

	/** site settings **/
	Route::get('/settings','SettingController@index')->name('settings.index');
	Route::post('/updateSettings','SettingController@updateSettings')->name('settings.update');

	Route::resource('makes', 'MakeController');
	Route::get('makeList', 'MakeController@makeList')->name('makes.makeList');

});


// Route::get('/', 'Website\HomeController@index')->name('home');
Route::get('/dealer', 'Website\HomeController@dealer_home')->name('dealer_home');

Route::get('/about_us', 'Website\HomeController@about_us')->name('about_us');
Route::get('/pricing', 'Website\HomeController@pricing')->name('pricing');
Route::get('/contact_us', 'Website\HomeController@contact_us')->name('contact_us');
Route::post('/contactus/sendmail', 'Website\HomeController@contactusMail')->name('contactus.mail');
Route::get('/feedback/{token}', 'Website\HomeController@feedback')->name('feedback');
Route::post('/submitFeedback', 'Website\HomeController@submitFeedback')->name('submitFeedback');

Route::group(['namespace'=>'Website\Auth', 'as'=>'website.dealer.'],function(){

	Route::get('signup', 'RegisterController@showRegistrationForm')->name('showRegistrationForm');
	Route::post('signup', 'RegisterController@register')->name('signup');
	Route::get('thankYou', 'RegisterController@thankYou')->name('thankYou');
	Route::get('login', 'LoginController@showLoginForm')->name('login');
	Route::post('login', 'LoginController@login');
	Route::post('logout', 'LoginController@logout')->name('logout');

	//password reset routes
	Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
	Route::post('password/reset', 'ResetPasswordController@reset')->name('password.update');
	Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
	// Route::group(['middleware'=>['signupnotCompleted:dealer']],function(){

	Route::get('verify_email', 'RegisterController@verify_email')->name('verify_email');

	Route::post('emailVerification', 'RegisterController@emailVerification')->name('emailVerification');
	Route::post('resendEmailOtp', 'RegisterController@resendEmailOtp')->name('resendEmailOtp');

	Route::get('membership_plan','MembershipPlanController@membership_plan')->name('membership_plan');
	Route::post('addMembershipPlan', 'MembershipPlanController@addMembershipPlan')->name('addMembershipPlan');
	Route::get('payment_info','MembershipPlanController@payment_info')->name('payment_info');
	Route::post('save_card','MembershipPlanController@save_card')->name('save_card');
	Route::get('not_approved','MembershipPlanController@not_approved')->name('not_approved');
	Route::get('plan_status','MembershipPlanController@plan_status')->name('plan_status');
	// });
});
Route::post('/transaction', 'Website\Auth\RegisterController@transaction')->name('transaction');

Route::get('/vehicle_detail/{token}', 'Website\DealerController@vehicle_detail')->name('dealer.vehicle_detail');
Route::group(['prefix'=> 'dealer','namespace'=>'Website', 'as'=>'dealer.','middleware'=>['auth:dealer','verified','signupnotCompleted:dealer']],function(){

	Route::get('/dashboard', ['middleware' => ['permission:dashboard'],'uses'=>'DealerController@index'])->name('dashboard');
	Route::get('/benefits', 'DealerController@benefits')->name('benefits');
	Route::get('/account_setting', 'DealerController@account_setting')->name('account_setting');
	Route::get('/current_listing', ['middleware' => ['permission:dashboard'],'uses'=>'CurrentListingController@index'])->name('current_listing');
	Route::post('/shareVehicle', ['middleware' => ['permission:dashboard'],'uses'=>'DealerController@shareVehicle'])->name('shareVehicle');
	Route::get('/submitted_bids', ['middleware' => ['permission:submitted_bids'],'uses'=>'DealerBidsController@submitted_bids'])->name('submitted_bids');
	Route::post('/submitABid', ['middleware' => ['permission:submit_bid'],'uses'=>'DealerBidsController@submitABid'])->name('submitABid');
	Route::get('/won_bids', ['middleware' => ['permission:won_bids'],'uses'=>'DealerBidsController@won_bids'])->name('won_bids');
	Route::get('/view_bid_detail/{id}', 'DealerBidsController@view_bid_detail')->name('view_bid_detail');
	Route::get('/downloadPdf/{id}', 'DealerBidsController@downloadPdf')->name('downloadPdf');
	Route::get('/edit_profile', 'DealerController@edit_profile')->name('edit_profile');
	Route::post('/update_profile', 'DealerController@update_profile')->name('update_profile');
	Route::post('/update_password', 'DealerController@update_password')->name('update_password');
	Route::get('/notifications', 'NotificationController@index')->name('getNotifications');
	Route::get('/subscription', ['middleware' => ['permission:subscription_management'],'uses'=>'SubscriptionManagementController@index'])->name('subscription');
	Route::post('/update_card', ['middleware' => ['permission:subscription_management'],'uses'=>'SubscriptionManagementController@update_card'])->name('update_card');
	Route::get('/delete_card',['middleware' => ['permission:subscription_management'],'uses'=>'SubscriptionManagementController@delete_card'])->name('delete_card');
	Route::post('/cancel_subscription', ['middleware' => ['permission:subscription_management'],'uses'=>'SubscriptionManagementController@cancel_subscription'])->name('cancel_subscription');
	Route::post('/update_subscription_profile', ['middleware' => ['permission:subscription_management'],'uses'=>'SubscriptionManagementController@update_subscription_profile'])->name('update_subscription_profile');
	Route::resource('users', 'UserController',['middleware' => ['role:Dealer']]);	
	Route::get('/getUsersList','UserController@getUsersList',['middleware' => ['role:Dealer']])->name('users.getUsersList');
});
