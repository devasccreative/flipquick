<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $class = get_class($exception);
        // echo  $class;die();
        switch($class) {
        case 'Illuminate\Foundation\Http\Exceptions\MaintenanceModeException':
            if($this->isApiRequest($request)){
                return response()->json(['status' => false,
                    'code' => 503,
                    'message' => 'App is under maintanace']);
             }else{
                return parent::render($request, $exception);
            }
        case 'Illuminate\Auth\AuthenticationException':
            $guard = array_get($exception->guards(), 0);
            switch ($guard) {
                case 'admin':
                $login = 'admin.login';
                break;
                case 'dealer':
                $login = 'website.dealer.login';
                break;
                case 'api-dealer':
                return response()->json(['status' => false,
                    'code' => 4011,
                    'message' => 'Unauthorized']);
                break;
                case 'api':
                return response()->json(['status' => false,
                    'code' => 401,
                    'message' => 'Unauthorized']);
                break;
                default:
                $login = 'website.dealer.login';
                break;
            }
            return redirect()->route($login);
        }
        return parent::render($request, $exception);
    }

    private function isApiRequest($request)
    {
        return ($request->is('api/*'));
    }

}
