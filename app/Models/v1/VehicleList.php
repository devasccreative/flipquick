<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class VehicleList extends Model
{
	protected $fillable = [
		'year',
		'make',
		'model',
		'trim',
		'milage',
		'style',
		'body_shape',
		'exterior_color',
		'interior_color',
		'doors',
		'passangers',
		'drive_train',
		'air_conditioning',
		'navigation_system',
		'leather_seats',
		'sun_roof',
		'panoramic_roof',
		'dvd',
		'heated_seats',
		'ac_seats',
		'360_camera',
		'after_market_equipment',
		'original_owner',
		'still_making_monthly_payment',
		'still_own',
		'have_any_accident_record',
		'how_much_records',
		'smoked_in',
		'vehicle_condition',
		'front_tires_condition',
		'back_tires_condition',
		// 'warning_lights_in_cluster',
		'warning_lights',
		'have_mechanical_issues',
		'mechanical_issues',
		'transmission'
	];

	public $timestamps = false;

}
