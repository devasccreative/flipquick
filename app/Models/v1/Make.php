<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class Make extends Model
{
    protected $fillable = [
		'make'
	];
}
