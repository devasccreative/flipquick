<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class Maintenance extends Model
{
    protected $fillable = [
		'android_version','ios_version','maintenance_mode'
	];
}
