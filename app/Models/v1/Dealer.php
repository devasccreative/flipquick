<?php

namespace App\Models\v1;

use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\DealerResetPasswordNotification;
use Illuminate\Database\Eloquent\SoftDeletes;
// use Spatie\Permission\Contracts\Role;

class Dealer extends Authenticatable implements JWTSubject //,Role
{
    use Notifiable,SoftDeletes,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password','email_verified_at','country_code','phone','firebase_android_id','firebase_ios_id','timezone_id','latitude','longitude','address','dealership_name','amvic_no','bambora_id','membership_plan_id','transaction_response','deleted_at','is_approved','parent_id','dealer_no','bedge','make_id','plan_status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new DealerResetPasswordNotification($token));

    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function bids()
    {
        return $this->hasMany('App\Models\v1\VehicleBid');
    }

    public function makes(){
        return $this->belongsToMany('App\Models\v1\Make','dealer_make')->withTimestamps();
    }
    public function shared_vehicle()
    {
        return $this->hasMany('App\Models\v1\ShareVehicle');
    }

    public function sub_dealers()
    {
        return $this->hasMany('App\Models\v1\Dealer','parent_id');
    }

    public function main_dealers()
    {
        return $this->belongsTo('App\Models\v1\Dealer','parent_id');
    }

    public function wonBids()
    {
        return $this->bids()->filter(function ($wonBid) {
                             return $wonBid->status == 1;
                         });
    }

    public function time_zone(){
        return $this->belongsTo('App\Models\v1\TimeZone','timezone_id');
    }

    public function membership_plan(){
        return $this->belongsTo('App\Models\v1\MembershipPlan','membership_plan_id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\v1\DealerTransaction','dealer_id');
    }
}
