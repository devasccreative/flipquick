<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class VehicleDamagePhoto extends Model
{
    protected $fillable = [
    	'vehicle_id',
    	'image'
    ];

    public function vehicle()
	{
		return $this->belongsTo('App\Models\v1\Vehicle');
	}

	public function getImageAttribute($value)
	{
		if(file_exists(storage_path('app/public/damage_photos/'.$this->vehicle_id.'/'.$value)) && $value){
            return asset('/storage/damage_photos').'/'.$this->vehicle_id.'/'.$value;     
        }else{
            return "";
        }
	}

}
