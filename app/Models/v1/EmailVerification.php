<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class EmailVerification extends Model
{
    use Notifiable;

	protected $fillable = [
		'email','verification_code',
	];
}
