<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class EmailVerificationDealer extends Model
{
	use Notifiable;

	protected $fillable = [
		'email','verification_code',
	];
}
