<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class SellerFeedback extends Model
{
    protected $fillable = [
		'vehicle_id',
		'dealer_id',
		'seller_id',
		'experience',
		'token',
		'dealership',
		'feedback'
	];

	public function vehicle()
	{
		return $this->belongsTo('App\Models\v1\Vehicle');
	}

	public function dealer()
	{
		return $this->belongsTo('App\Models\v1\Dealer');
	}

	public function seller()
	{
		return $this->belongsTo('App\Models\v1\Seller');
	}
}
