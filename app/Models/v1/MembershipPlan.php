<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class MembershipPlan extends Model
{
	protected $fillable = [
		'amount','plan_name','billing_cycle','billing_cycle_name','billing_cycle_unit','discount'
	];
}
