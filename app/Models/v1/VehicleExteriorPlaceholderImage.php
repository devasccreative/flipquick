<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class VehicleExteriorPlaceholderImage extends Model
{
    protected $fillable = [
    	'vehicle_type',
    	'image',
    	'layer_image',
        'required',
        'vehicle_key'
    ];

    protected $table = 'vehicle_exterior_images';


 //    public function vehicle()
	// {
	// 	return $this->belongsTo('App\Models\v1\Vehicle');
	// }

	// public function getImageAttribute($value)
	// {
	// 	if(file_exists(storage_path('app/public/exterior_photos/'.$this->vehicle_id.'/'.$value)) && $value){
 //            return asset('/storage/exterior_photos').'/'.$this->vehicle_id.'/'.$value;     
 //        }else{
 //            return "";
 //        }

	// }
	public function getLayerImageAttribute($value){
		return asset('assets/images/'.$value);
	}
}
