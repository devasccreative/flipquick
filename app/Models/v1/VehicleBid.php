<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleBid extends Model
{
    use SoftDeletes;
	protected $fillable = [
		'vehicle_id',
		'dealer_id',
		'amount',
		'status',
		'deleted_at'
	];

	public function vehicle()
	{
		return $this->belongsTo('App\Models\v1\Vehicle');
	}

	public function dealer()
	{
		return $this->belongsTo('App\Models\v1\Dealer');
	}

	public function getStatusAttribute($value)
	{
		$highestBid = $this->vehicle->bids()
						->select('id','vehicle_id','amount')
						->orderBy('amount', 'desc')
						->first();

		$status = 0;
		if($highestBid->id == $this->id){
			$status = 1;
		}
		return $status;
	}
}
