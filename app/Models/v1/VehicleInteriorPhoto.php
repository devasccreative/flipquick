<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class VehicleInteriorPhoto extends Model
{
    protected $fillable = [
    	'vehicle_id',
    	'vehicle_interior_type',
    	'image'
    ];

    public function vehicle()
	{
		return $this->belongsTo('App\Models\v1\Vehicle');
	}

	public function getImageAttribute($value)
	{
		if(file_exists(storage_path('app/public/interior_photos/'.$this->vehicle_id.'/'.$value)) && $value){
            return asset('/storage/interior_photos').'/'.$this->vehicle_id.'/'.$value;     
        }else{
            return "";
        }

	}
}
