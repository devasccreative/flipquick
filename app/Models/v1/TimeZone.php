<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class TimeZone extends Model
{
    protected $fillable = [
		'timezone_name','timezone'
	];
}
