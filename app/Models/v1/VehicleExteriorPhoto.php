<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class VehicleExteriorPhoto extends Model
{
    protected $fillable = [
    	'vehicle_id',
    	'vehicle_exterior_type',
    	'image',
        'orderid'
    ];

    public function vehicle()
	{
		return $this->belongsTo('App\Models\v1\Vehicle');
	}

	public function getImageAttribute($value)
	{
		if(file_exists(storage_path('app/public/exterior_photos/'.$this->vehicle_id.'/'.$value)) && $value){
            return asset('/storage/exterior_photos').'/'.$this->vehicle_id.'/'.$value;     
        }else{
            return "";
        }

	}
}
