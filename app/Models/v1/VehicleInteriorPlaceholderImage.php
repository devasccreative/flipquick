<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class VehicleInteriorPlaceholderImage extends Model
{
    protected $fillable = [
    	
        'vehicle_type',
        'image',
        'required',
        'vehicle_key',
        'layer_image'
    ];
   protected $table = 'vehicle_interior_images';
 //    public function vehicle()
	// {
	// 	return $this->belongsTo('App\Models\v1\Vehicle');
	// }

	// public function getImageAttribute($value)
	// {
	// 	if(file_exists(storage_path('app/public/interior_photos/'.$this->vehicle_id.'/'.$value)) && $value){
 //            return asset('/storage/interior_photos').'/'.$this->vehicle_id.'/'.$value;     
 //        }else{
 //            return "";
 //        }

	// }
   public function getLayerImageAttribute($value){
        return asset('assets/images/'.$value);
    }
}
