<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class AdminStatistic extends Model
{
    protected $fillable = ['id','name','value'];
}
