<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class SellerTransaction extends Model
{
	protected $fillable = [
		'seller_id','vehicle_id', 'order_id', 'approved', 'transaction_date','amount'
	];

	// protected $appends = ['vin_no'];

	public function seller()
	{
		return $this->belongsTo('App\Models\v1\Seller','seller_id');
	}

	public function vehicle()
	{
		return $this->belongsTo('App\Models\v1\Vehicle','vehicle_id');
	}

	// public function getVinNoAttribute($value){
	// 	return $this->vehicle->vin_no;
	// }
}
