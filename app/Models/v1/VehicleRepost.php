<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class VehicleRepost extends Model
{
	protected $fillable = [
		'vehicle_id',
		'repost_at'
	];

	public function vehicle()
	{
		return $this->belongsTo('App\Models\v1\Vehicle');
	}
}
