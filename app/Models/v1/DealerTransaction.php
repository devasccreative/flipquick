<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class DealerTransaction extends Model
{
	protected $fillable = [
		'dealer_id','recurring_account_id', 'transaction_id', 'approved', 'transaction_date','amount','billing_period_start','billing_period_end','vehicle_id'
	];

	// protected $appends = ['vin_no'];

	public function dealer()
	{
		return $this->belongsTo('App\Models\v1\Dealer','dealer_id');
	}
}
