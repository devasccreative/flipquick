<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ShareVehicle extends Model
{
	use Notifiable;
    protected $fillable = [
		'vehicle_id',
		'dealer_id',
		'email',
		'token',
		'type'
	];

	public function vehicle()
	{
		return $this->belongsTo('App\Models\v1\Vehicle');
	}

	public function dealer()
	{
		return $this->belongsTo('App\Models\v1\Dealer');
	}
}
