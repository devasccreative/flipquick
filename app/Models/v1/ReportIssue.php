<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;

class ReportIssue extends Model
{
    protected $fillable = ['dealer_id','seller_id','report','title','image','description'];

    public function vehicle(){
        return $this->belongsTo('App\Models\v1\Vehicle','vehicle_id');
    }

    public function dealer(){
        return $this->belongsTo('App\Models\v1\Dealer');
    }
    public function getImageAttribute($value)
	{
		if(file_exists(storage_path('app/public/reports/'.$value)) && $value){
            return asset('/storage/reports').'/'.$value;     
        }else{
            return "";
        }

	}
}
