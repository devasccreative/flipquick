<?php

namespace App\Models\v1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model
{
    use SoftDeletes;
    protected $fillable = [
		'seller_id',
		'vin_no',
		'year',
		'make',
		'model',
		'trim',
		'milage',
		'style',
		'body_shape',
		'exterior_color',
		'interior_color',
		'doors',
		'passengers',
		'drive_train',
		'air_conditioning',
		'navigation_system',
		'leather_seats',
		'sun_roof',
		'panoramic_roof',
		'dvd',
		'heated_seats',
		'ac_seats',
		'360_camera',
		'after_market_equipment',
		'original_owner',
		'still_making_monthly_payment',
		'still_own',
		'have_any_accident_record',
		'how_much_records',
		'smoked_in',
		'vehicle_condition',
		'front_tires_condition',
		'back_tires_condition',
		'warning_lights_in_cluster',
		'warning_lights',
		'have_mechanical_issues',
		'mechanical_issues',
		'latitude',
		'longitude',
		'starts_at',
		'expired_at',
		'repost_at',
		'steps_submitted',
		'transmission',
		'engine',
		'made_in',
		'modified_exhaust',
		'wantto_sell_or_tradein',
		'vehicle_status',
		// 'is_active',
		// 'salvage',
		// 'rebuilt',
		'out_of_province',
		'post_type',
		'deleted_at'
	];

	public function seller()
	{
		return $this->belongsTo('App\Models\v1\Seller','seller_id');
	}

	public function repost()
	{
		return $this->hasMany('App\Models\v1\VehicleRepost');
	}

	public function reports()
	{
		return $this->hasMany('App\Models\v1\ReportVehicle');
	}

	public function makes(){
		return $this->belongsToMany('App\Models\v1\Make','vehicle_make')->withTimestamps();
	}

	public function reportissues()
	{
		return $this->hasMany('App\Models\v1\ReportIssue');
	}

	public function vehicle_damage_photos()
	{
		return $this->hasMany('App\Models\v1\VehicleDamagePhoto');
	}

	public function vehicle_exterior_photos()
	{
		return $this->hasMany('App\Models\v1\VehicleExteriorPhoto')->orderBy('orderid','asc');
	}

	public function vehicle_exterior_photo()
	{
		return $this->hasOne('App\Models\v1\VehicleExteriorPhoto')->orderBy('orderid','asc');
	}

	public function vehicle_interior_photos()
	{
		return $this->hasMany('App\Models\v1\VehicleInteriorPhoto');
	}

	public function shared_vehicle()
	{
		return $this->hasMany('App\Models\v1\ShareVehicle');
	}

	public function bids()
	{
		return $this->hasMany('App\Models\v1\VehicleBid');
	}

	function vehicle_bids()
	{
		return $this->hasManyThrough('App\Models\v1\Dealer', 'App\Models\v1\VehicleBid');
	}

	// public function highestBid()
	// {
	// 	return $this->hasOne('App\Models\v1\VehicleBid')->max('amount');
	// }

	public function latest_bid()
	{
		return $this->hasOne('App\Models\v1\VehicleBid')->latest();
	}
	public function getEngineAttribute($value){
		return ($value != '') ? $value.' Cylinder' : '' ;
	}
}
