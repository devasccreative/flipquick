<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Notification;
use App\Models\v1\SellerFeedback;
use App\Notifications\SellerFeedbackNotification;
use App\Helpers\NotificationHelper;

class SellerFeedbacks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twiceDaily:sellerFeedback {--timezone=}';
    public $timezone;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notification send to all seller after 48 hours of vehicle list expired.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $timezone       = $this->option('timezone');
        $current_time   = Carbon::now($timezone);
        $time_1      = Carbon::now($timezone)->setTimeFromTimeString('10:00:00');
        if($current_time->lt($time_1)){
            $ends_at = Carbon::now($timezone)->setTimeFromTimeString('09:00:00');
        }else{
            $ends_at = Carbon::now($timezone)->setTimeFromTimeString('16:00:00');
        }

        $ends_at      =  Carbon::parse($ends_at,$timezone)->subDays('2')->setTimeZone('UTC')->toDateTimeString();

        $vehicles   = Vehicle::where('seller_id',4)
                                ->limit(3);
                                ->get();

         foreach ($vehicles as $key => $vehicle) {
            $highest_bid  = $vehicle->bids()
                        ->select('id','dealer_id','vehicle_id','amount')
                        ->with('dealer:id,first_name,last_name,phone,country_code,email,address,latitude,longitude,dealership_name')
                        ->orderBy('amount', 'desc')
                        ->first();

            if($vehicle->bids->count() > 0){
             

                 $token = md5(time());
                $feedback = SellerFeedback::create(['seller_id'=>$vehicle->seller->id,'vehicle_id'=>$vehicle->id,'dealer_id'=>$highest_bid->dealer_id,'token'=>$token,'type'=>'highest_bid']);
                $username = $vehicle->seller->first_name.' '.$vehicle->seller->last_name;
                $vehicle->seller->notify(new SellerFeedbackNotification($feedback,$username));
                $vehicle->seller->email = 'developer.foremost@gmail.com';
                $vehicle->seller->notify(new SellerFeedbackNotification($feedback,$username));
                $vehicle->seller->increment('bedge');
            }
            // $vehicle->seller->notify((new SellerListingExpiredNotification($vehicle,$timezone)));
        }
         echo $ends_at.' '.$timezone;die();

    }
}
