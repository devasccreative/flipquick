<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\v1\Vehicle;
use App\Models\v1\Dealer;
use App\Models\v1\Admin;
use Carbon\Carbon;
use Notification;
use App\Notifications\WonsbidNotification;
use App\Notifications\PaymentFailedNotification;
use App\Notifications\PlanExpired;
use App\Helpers\NotificationHelper;
use DB;

class Wonbids extends Command
{
   /**
     * The name and signature of the console command.
     *
     * @var string
     */
   protected $signature = 'twiceDaily:youWonsBid {--timezone=}';
   public $timezone;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notification send to all dealer at every day current listing start and expired time for new listing available.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $timezone       = $this->option('timezone');
        $current_time   = Carbon::now($timezone);
        $time_1      = Carbon::now($timezone)->setTimeFromTimeString('10:00:00');
        if($current_time->lt($time_1)){
            $ends_at = Carbon::now($timezone)->setTimeFromTimeString('09:00:00');
        }else{
            $ends_at = Carbon::now($timezone)->setTimeFromTimeString('16:00:00');
        }

        $ends_at      =  Carbon::parse($ends_at,$timezone)->setTimeZone('UTC')->toDateTimeString();

        $dealers        = Dealer::whereHas('time_zone',function($query) use ($timezone){
                                     $query->where('timezone',$timezone);
                                 })
                                ->where('bambora_id','!=','')
                                ->get();


        foreach ($dealers as $key => $dealer) {

            $lat  = $dealer->latitude;
            $long = $dealer->longitude;

            $current_available_listing  = Vehicle::where('expired_at',$ends_at)
                                      ->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
                                      ->whereHas('seller',function($query) use ($timezone){
                                          $query->whereHas('time_zone',function($query) use ($timezone){
                                              $query->where('timezone',$timezone);
                                          });
                                      })
                                      ->whereHas('bids',function($query) use ($dealer){
                                          $query->where('dealer_id',$dealer->id);
                                      })
                                      ->with(['bids'=>function($query) use ($dealer){
                                          $query->where('dealer_id',$dealer->id);
                                      }])
                                      ->with(['vehicle_exterior_photos'=>function($query){
                                            $query->where('vehicle_exterior_type','front_driver_corner')->select('id','vehicle_id','vehicle_exterior_type','image');
                                        }])
                                      ->with('seller:id,first_name,last_name,phone,country_code')
                                      ->get();

            foreach ($current_available_listing as $key => $vehicle) {
                echo $vehicle->id;
                print_r($dealer);
                 
                if($vehicle->bids[0]->status == 1){
                    $vehicle->bid_ammount = $vehicle->bids[0]->amount;
                    $dealer->increment('bedge');
                    if($dealer->parent_id == 0){
                        $token = md5(time());
                        $dealer->shared_vehicle()->create(['vehicle_id'=>$vehicle->id,'email'=>$dealer->email,'token'=>$token]);
                        $vehicle->link = route('dealer.vehicle_detail',$token);
                        $dealer->notify((new WonsbidNotification($vehicle,$timezone)));
                        $additionalData['vehicle'] = $vehicle->toArray();
                        NotificationHelper::sendPushNotification($dealer,'wonAbid',$additionalData);
                        // Notification::send($dealer->sub_dealers, new WonsbidNotification($vehicle,$timezone));
                        // NotificationHelper::sendPushNotification($dealer->sub_dealers,'wonAbid',$additionalData,true);
                    }else{
                       $token = md5(time());
                       $dealer->shared_vehicle()->create(['vehicle_id'=>$vehicle->id,'email'=>$dealer->email,'token'=>$token]);
                       $vehicle->link = route('dealer.vehicle_detail',$token);
                       $dealer->notify((new WonsbidNotification($vehicle,$timezone)));
                       $additionalData['vehicle'] = $vehicle->toArray();
                       NotificationHelper::sendPushNotification($dealer,'wonAbid',$additionalData);
                       // NotificationHelper::sendPushNotification($dealer->main_dealers,'wonAbid',$additionalData);
                       // Notification::send($dealer->main_dealers->sub_dealers, new WonsbidNotification($vehicle,$timezone));
                       // NotificationHelper::sendPushNotification($dealer->main_dealers->sub_dealers,'wonAbid',$additionalData,true);
                   }
                  
                   
                   
               }     
                try{
                         $merchant_id  = config('services.bambora.merchant_id'); 
                         $api_key    = config('services.bambora.api_key');
                         $api_version  = config('services.bambora.api_version');
                         $platform     = config('services.bambora.platform'); 
                         $payment_passcode   = config('services.bambora.payment_passcode'); 
                      if($vehicle->seller->repo == 1){
                        $beanstream = new \Beanstream\Gateway($merchant_id, $api_key, $platform, $api_version); 
                          $amount = $vehicle->seller->repo_charge;
                          $pymentData = array(
                            'amount'=> $amount,
                            'payment_method'=> 'payment_profile',
                            'payment_profile' => array(
                              'customer_code' => $vehicle->seller->bambora_id,
                              'card_id' => 1,
                              'complete' => true
                            )
                          );

                          $curl = curl_init();
                          curl_setopt_array($curl, array(
                            CURLOPT_URL => "https://api.na.bambora.com/v1/payments",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => json_encode($pymentData),
                            CURLOPT_HTTPHEADER => array(
                              "authorization: Passcode ".base64_encode($merchant_id.':'.$payment_passcode),
                              "Content-Type: application/json"
                            ),
                          ));
                          $response = curl_exec($curl);
                          $err = curl_error($curl);
                          curl_close($curl);

                          $transactionResponse = json_decode($response);
                          if($transactionResponse->approved == '1'){

                            $vehicle->seller->transactions()->create(['order_id'=>$transactionResponse->id, 'approved'=>$transactionResponse->approved, 'transaction_date'=>$transactionResponse->created,'amount'=>$amount,'vehicle_id'=>$request->get('vehicle_id')]);
                            return response()->json(['status'=>true,'message'=>'Vehicle Reposted successfully']);
                          }

                      }
                      if($dealer->membership_plan_id == 2){


                         //Create Beanstream Gateway
                         $beanstream = new \Beanstream\Gateway($merchant_id, $api_key, $platform, $api_version); 
                         $amount = ($vehicle->seller->repo == 1) ? 100 : $dealer->membership_plan->amount;
                         $pymentData = array(
                          'amount'=> $amount,
                          'payment_method'=> 'payment_profile',
                          'payment_profile' => array(
                             'customer_code' => $dealer->bambora_id,
                             'card_id' => 1,
                             'complete' => true
                          )
                         );

                         $curl = curl_init();
                         curl_setopt_array($curl, array(
                          CURLOPT_URL => "https://api.na.bambora.com/v1/payments",
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_ENCODING => "",
                          CURLOPT_MAXREDIRS => 10,
                          CURLOPT_TIMEOUT => 30,
                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                          CURLOPT_CUSTOMREQUEST => "POST",
                          CURLOPT_POSTFIELDS => json_encode($pymentData),
                          CURLOPT_HTTPHEADER => array(
                             "authorization: Passcode ".base64_encode($merchant_id.':'.$payment_passcode),
                             "Content-Type: application/json"
                          ),
                         ));
                         $response = curl_exec($curl);
                         $err = curl_error($curl);
                         curl_close($curl);

                         $transactionResponse = json_decode($response);
                         if($transactionResponse->approved == '1'){
                          
                          $dealer->transactions()->create(['recurring_account_id'=>$dealer->bambora_id,'transaction_id'=>$transactionResponse->id, 'approved'=>$transactionResponse->approved, 'transaction_date'=>$transactionResponse->created,'amount'=>$amount,'vehicle_id'=>$vehicle->id,'billing_period_start'=>date('Y-m-d h:i:s'),'billing_period_end'=>date('Y-m-d h:i:s')]);
                         }
                      }
                  } catch (\Beanstream\Exception $e) {
                      $admin = Admin::find(1);
                      $message = $e->getMessage();
                      $admin->email = 'developer.foremost@gmail.com';
                      $admin->notify((new PaymentFailedNotification($vehicle,$message)));
                      $dealer->update(['plan_status'=>$transactionResponse['trnApproved']]);
                      $dealer->notify(new PlanExpired());

                      echo $e->getCode().'--'.$e->getMessage(); die();
                  }
           }


       }
       echo $ends_at.' '.$timezone;die();
   }
}
