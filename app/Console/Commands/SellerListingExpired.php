<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Notifications\SellerListingExpiredNotification;
use App\Helpers\NotificationHelper;
use App\Models\v1\Seller;
use App\Models\v1\Vehicle;
use Carbon\Carbon;
use Notification;

class SellerListingExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twiceDaily:SellerListingExpired {--timezone=}';
    public $timezone;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notification send to all seller whose vehicle removed from list.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $timezone       = $this->option('timezone');
        $current_time   = Carbon::now($timezone);
        $time_1      = Carbon::now($timezone)->setTimeFromTimeString('10:00:00');
        if($current_time < $time_1){
            $ends_at   = Carbon::now($timezone)->setTimeFromTimeString('9:00:00');
        }else{
            $ends_at   = Carbon::now($timezone)->setTimeFromTimeString('16:00:00');
        }
        $vehicles   = Vehicle::where('expired_at',$ends_at->setTimezone('UTC'))
                                ->whereHas('seller',function($query) use ($timezone){
                                    $query->whereHas('time_zone',function($query) use ($timezone){
                                        $query->where('timezone',$timezone);
                                    });
                                })
                                ->with(['vehicle_exterior_photos'=>function($query){
                                            $query->where('vehicle_exterior_type','front_driver_corner')->select('id','vehicle_id','vehicle_exterior_type','image');
                                        }])
                                ->get();

        foreach ($vehicles as $key => $vehicle) {
            print_r($vehicle->seller);
            $highest_bid  = $vehicle->bids()
                        ->select('id','dealer_id','vehicle_id','amount')
                        ->with('dealer:id,first_name,last_name,phone,country_code,email,address,latitude,longitude,dealership_name')
                        ->orderBy('amount', 'desc')
                        ->first();

            if($highest_bid){
                $token = md5(time());
                $highest_bid->dealer->shared_vehicle()->create(['vehicle_id'=>$vehicle->id,'email'=>$vehicle->seller->email,'token'=>$token,'type'=>'highest_bid']);
                $vehicle->link = route('dealer.vehicle_detail',$token);
                $vehicle->seller->notify((new SellerListingExpiredNotification($vehicle,$highest_bid,'Asia/Kolkata')));
                $vehicle->seller->increment('bedge');
                NotificationHelper::sendPushNotification($vehicle->seller,'gotHighestBid',$highest_bid->toArray());
            }
            // $vehicle->seller->notify((new SellerListingExpiredNotification($vehicle,$timezone)));
        }
         echo $ends_at.' '.$timezone;die();
    }
}
