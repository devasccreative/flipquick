<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\v1\Vehicle;
use App\Models\v1\Dealer;
use Carbon\Carbon;
use Notification;
use App\Notifications\SellerListingPosted;
use App\Notifications\CurrentListingPostedNotification;
use App\Helpers\NotificationHelper;
use DB;

class CurrentListingPosted extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twiceDaily:currentListingPosted {--timezone=}';
    public $timezone;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notification send to all dealer at every day current listing start and expired time for new listing available.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $timezone       = $this->option('timezone');
        $current_time   = Carbon::now($timezone);
        $time_1      = Carbon::now($timezone)->setTimeFromTimeString('10:00:00');
        if($current_time->lt($time_1)){
            $starts_at = Carbon::now($timezone)->setTimeFromTimeString('9:00:00');
        }else{
            $starts_at = Carbon::now($timezone)->setTimeFromTimeString('16:00:00');
        }
        // echo $starts_at;
        // die('current_listing_posted');

        $starts_at      =  Carbon::parse($starts_at,$timezone)->setTimeZone('UTC')->toDateTimeString();

        $dealers        = Dealer::whereHas('time_zone',function($query) use ($timezone){
                                        $query->where('timezone',$timezone);
                                    })
                                    ->where('bambora_id','!=','')
                                    ->get();

        foreach ($dealers as $key => $dealer) {

            $lat  = $dealer->latitude;
            $long = $dealer->longitude;

            $current_available_listing  = Vehicle::where('starts_at',$starts_at)
                                                    ->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
                                                    ->whereHas('seller',function($query) use ($timezone){
                                                        $query->whereHas('time_zone',function($query) use ($timezone){
                                                            $query->where('timezone',$timezone);
                                                        });
                                                    })
                                                    ->count();

            if($current_available_listing > 0){
                print_r($dealer);
                $dealer->notify((new CurrentListingPostedNotification($current_available_listing,$timezone)));
                $dealer->increment('bedge');

                $additionalData['availableVehicle'] = $current_available_listing;
                NotificationHelper::sendPushNotification($dealer,'currentListingPosted',$additionalData);
            }     

        }

        $current_available_listing  = Vehicle::where('starts_at',$starts_at)
                                                    ->whereHas('seller',function($query) use ($timezone){
                                                        $query->whereHas('time_zone',function($query) use ($timezone){
                                                            $query->where('timezone',$timezone);
                                                        });
                                                    })
                                                    ->get();

        foreach ($current_available_listing as $key => $current_available) {

            $vehicle->seller->notify(new SellerListingPosted());
            $vehicle->seller->increment('bedge');
            NotificationHelper::sendPushNotification($seller,'sellerListingPosted');
        }
        echo $starts_at.' '.$timezone;die();
    }
}
