<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $timezones = DB::table('time_zones')->get();

        foreach ($timezones as $key => $timezone) {

            // $schedule->command('twiceDaily:currentListingPosted',['--timezone'=>$timezone->timezone])->twiceDaily(9,16)->timezone($timezone->timezone)->appendOutputTo(storage_path('logs/currentListingPosted.log'));
            // $schedule->command('twiceDaily:youWonsBid',['--timezone'=>$timezone->timezone])->twiceDaily(9,16)->timezone($timezone->timezone)->appendOutputTo(storage_path('logs/wonsAbid.log'));
            // $schedule->command('twiceDaily:SellerListingExpired',['--timezone'=>$timezone->timezone])->twiceDaily(9,16)->timezone($timezone->timezone)->appendOutputTo(storage_path('logs/sellerListingExpired.log'));
            // $schedule->command('twiceDaily:sellerFeedback',['--timezone'=>$timezone->timezone])->twiceDaily(9,16)->timezone($timezone->timezone)->appendOutputTo(storage_path('logs/test.log'));
            $schedule->command('twiceDaily:currentListingPosted',['--timezone'=>$timezone->timezone])->twiceDaily(9,16)->timezone($timezone->timezone)->appendOutputTo(storage_path('logs/currentListingPosted.log'));
            $schedule->command('twiceDaily:youWonsBid',['--timezone'=>$timezone->timezone])->twiceDaily(9,16)->timezone($timezone->timezone)->appendOutputTo(storage_path('logs/wonsAbid.log'));
            $schedule->command('twiceDaily:SellerListingExpired',['--timezone'=>$timezone->timezone])->twiceDaily(9,16)->timezone($timezone->timezone)->appendOutputTo(storage_path('logs/sellerListingExpired.log'));
            $schedule->command('twiceDaily:sellerFeedback',['--timezone'=>$timezone->timezone])->twiceDaily(9,16)->timezone($timezone->timezone)->appendOutputTo(storage_path('logs/sellerFeedback.log'));

            // $schedule->command('twiceDaily:currentListingPosted',['--timezone'=>$timezone->timezone])->everyMinute()->appendOutputTo(storage_path('logs/currentListingPosted9pm.log'));
        }

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
