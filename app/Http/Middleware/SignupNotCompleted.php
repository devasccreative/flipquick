<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class SignupNotCompleted
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if($guard == "dealer"){
            $dealer = Auth::guard('dealer')->user();
            // if($dealer->is_approved == 1){
            //     return redirect()->route('dealer.dashboard');
            // }
            $trnResponse = explode('&',$dealer->transaction_response);
            if(!$dealer->plan_status){
                return redirect()->route('website.dealer.plan_status');
            }
            if(!$dealer->email_verified_at){
                // die('1');
                return redirect()->route('website.dealer.verify_email');
            }else if(($dealer->membership_plan_id == '' && $dealer->parent_id == 0) || ($dealer->transaction_response != '' && explode('=',$trnResponse[0])[1] == '0')){
                // die('2');
                return redirect()->route('website.dealer.membership_plan');
            }else if($dealer->bambora_id != '' && $dealer->membership_plan->plan_name == 'Free' && (Carbon::now()->diffInDays($dealer->created_at)) >= 30){
                    // die('4');
                return redirect()->route('website.dealer.membership_plan')->with('message', 'Your trial is expired. Please select membership plan to continue.');
            }else if(($dealer->bambora_id == '' && $dealer->parent_id == 0) || ($dealer->transaction_response != '' && explode('=',$trnResponse[0])[1] == '0')){
                // die('3');
                return redirect()->route('website.dealer.payment_info');
            }else if($dealer->is_approved == 0){
                // die('5');
                return redirect()->route('website.dealer.not_approved');
            }
            // return redirect()->route('dealer.dashboard');
        }
        return $next($request);
    }
}
