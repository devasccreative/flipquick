<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v1\SellerTransaction;
use App\Models\v1\DealerTransaction;
use Carbon\Carbon;
use DataTables;

class PaymentHistoryController extends Controller
{
	public function index()
	{
		$earning_amount = SellerTransaction::where('approved','1')->sum('amount');
		return view('admin.payments.index',compact('earning_amount'));
	}

	public function seller_orders(){
		$earning_amount_seller = SellerTransaction::where('approved','1')->sum('amount');
		$earning_amount_dealer = DealerTransaction::where('approved','1')->sum('amount');     

		return view('admin.payments.seller_payments',compact('earning_amount_seller','earning_amount_dealer'));   
	}

	public function seller_orders_list(Request $request){
		$skip = (int)$request->get('start');
		$take = (int)$request->get('length');
		$search = $request->get('search')['value'];
		$transactions = SellerTransaction::select('*');

		if($request->get('start_date') != ''){
			$datefilter = explode(' - ',$request->get('start_date'));
            $start=Carbon::createFromFormat('d/m/Y',$datefilter[0])->format('Y-m-d 00:00:00');
            $end=Carbon::createFromFormat('d/m/Y',$datefilter[1])->format('Y-m-d 23:59:59');      
            $transactions->whereBetween('created_at',[$start,$end]);
        }
		if($search != ''){
			$transactions->where(function($query) use ($search){
				$query->where('order_id','like','%'.$search.'%')
				->orWhere('amount','like','%'.$search.'%')
				->orWhereHas('seller',function($query){
					$query->where(function($query) use ($search){
						$query->where('first_name','like','%'.$search.'%')
						->orWhere('last_name','like','%'.$search.'%');
					});
				})
				->orWhereHas('vehicle',function($query) use ($search){
					$query->where('vin_no','like','%'.$search.'%');
				});
			});
		}

		$count = $transactions->count();
		$transactions = $transactions->orderBy('created_at','DESC')->skip($skip)->take($take)->get();
            // return $transactions;  
		return DataTables::of($transactions)
					->addIndexColumn()
					->editColumn('seller_name', function($transaction){
						return $transaction->seller->first_name.' '.$transaction->seller->last_name;
					})
					->editColumn('vin_no', function($transaction){
						return $transaction->vehicle->vin_no;
					})
					->editColumn('amount', function($transaction){
						return '$'.$transaction->amount;
					})
					->editColumn('created_at', function($transaction){
						return Carbon::parse($transaction->created_at)->format('d-M-Y @ h:i A');
					})
					->addColumn('action', function($transaction){
						$data = '';
						$data =  '<a href="'.route('admin.seller.show',$transaction->id).'">View</a>';
					            // $data .=  '<a href="javascript:void(0)" class="delete_company" data-action="delete" data-placement="left" data-id="'.$transaction->id.'"  title="Delete" data-toggle="tooltip"><i class="fa fa-trash iconsetaddbox"></i>
					            //     </a>';

						return $data;
					})
					->rawColumns(['action'])
					->order(function ($query) {
						if (request()->has('created_at')) {
							$query->orderBy('created_at', 'DESC');
						}
					})
					->setTotalRecords($count)
					->setFilteredRecords($count)
					->skipPaging()
					->make(true); 
	}
}
