<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v1\Make;
use Carbon\Carbon;
use DataTables;
use Validator;

class MakeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('admin.makes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $validator = Validator::make($request->all(),[
                   'make' => 'required|unique:makes',
               ]);

        if($validator->fails()){
           return response()->json(['status'=>false,'message'=>$validator->errors()->first()]);     
        }    

        $inputs = $request->all();
        
        $make = new Make($inputs);
        $make->save(); 
        return response()->json(['status'=>true,'message'=>'Make added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $make = Make::where('id',$id)->first();
        // $view = view('admin.makes.edit',compact('make'))->render();
        return response()->json(['status'=>true,'data'=>$make]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //
        $inputs = $request->all();
        $make = Make::where('id',$id)->first();
        $make->update($inputs);
        return response()->json(['status'=>true,'message'=>'Make updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $make = Make::where('id',$id)->delete();
        return response()->json(['status'=>true,'message'=>'Make deleted successfully']);
    }

    public function makeList()
    {
        $makes = Make::orderBy('created_at','ASC')->get();

        return DataTables::of($makes)
        ->addIndexColumn()  
        ->editColumn('created_at',function($make){
            return Carbon::parse($make->created_at)->format('d M, Y');
        })
        ->addColumn('action',function($make){
            return '<a href="javascript:void(0)" data-toggle="tooltip" title="Edit" onclick="EditMake('.$make->id.');"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
        })  
        // ->order(function ($query) {
        //             if (request()->has('created_at')) {
        //                 $query->orderBy('created_at', 'DESC');
        //             }
                    
        //         })       
        // ->skipPaging()
        ->make(true);
    }

}
