<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v1\TimeZone;
use App\Models\v1\Seller;
use App\Models\v1\Vehicle;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Validator;
use DataTables;

class SellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.sellers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $seller = Seller::where('id',$id)->first();
        $seller->vehicles->map(function($vehicle) use ($seller){
                                   $seller->earning_amount = $vehicle->bids()
                                                                    ->get()
                                                                    ->filter(function ($wonBid) {
                                                                        return $wonBid->status == 1;
                                                                    })
                                                                    ->sum('amount');

                                   $seller->selling_vehicle = $vehicle->bids()
                                                                    ->get()
                                                                    ->filter(function ($wonBid) {
                                                                        return $wonBid->status == 1;
                                                                    })
                                                                    ->count();
                                   return $vehicle;
                                });
                              
        return view('admin.sellers.dashboard',compact('seller','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seller = Seller::where('id',$id)->first();
        $timezones = Timezone::all();

        return view('admin.sellers.edit_profile',compact('seller','timezones','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $seller = Seller::where('id',$id)->first();
        $rules =  [
            'first_name'    => 'required | string',
            'last_name'     => 'required | string',
            'email'         => 'required | string | email | unique:dealers,email,'.$seller->id,
            'phone'         => 'required | unique:dealers,phone,'.$seller->id,
            'timezone_id'   => 'required'
        ];
   
        $validator  = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }

        $inputs = $request->all(); 
        unset($inputs['_token']);
        $seller->update($inputs);

        return response()->json(['status' => true,'message' => 'Profile updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


     public function getSellerList(Request $request){
        $skip = (int)$request->get('start');
        $take = (int)$request->get('length');
        $search = $request->get('search')['value'];
        $sellers = Seller::select('id','first_name','last_name','email','country_code','phone','repo','created_at');

        if($search != ''){
            $sellers->where(function($query) use ($search){
                $query->where('first_name','like','%'.$search.'%')
                ->orWhere('last_name','like','%'.$search.'%')
                ->orWhere('phone','like','%'.$search.'%')
                ->orWhere('email','like','%'.$search.'%');
            });
        }

        $sellers = $sellers->orderBy('created_at','DESC')->skip($skip)->take($take)->get();
        $count = Seller::count();
        if($search != ''){
            $count = Seller::where(function($query) use ($search){
                $query->where('first_name','like','%'.$search.'%')
                ->orWhere('last_name','like','%'.$search.'%')
                ->orWhere('phone','like','%'.$search.'%')
                ->orWhere('email','like','%'.$search.'%');
            })->count();
        }

        return DataTables::of($sellers)
        ->addIndexColumn()
        ->editColumn('created_at', function($seller){
            return Carbon::parse($seller->created_at)->format('d-M-Y @ h:i A');
        })
        ->editColumn('name', function($seller){
            return '<a href="'.route('admin.seller.show',$seller->id).'">'.$seller->first_name.' '.$seller->last_name.'</a>';
        })
        ->editColumn('phone', function($seller){
            return $seller->country_code.'-'.$seller->phone;
        })
        ->editColumn('repo', function($seller){
            if($seller->repo == 1){
                return 'Yes';
            }
            return 'No';
        })
        ->addColumn('action', function($seller){
            $data = '';
            $data =  '<a href="javascript:void(0);" onclick="changeRepo('.$seller->id.');">Edit Repo</a> | <a href="'.route('admin.seller.show',$seller->id).'">View</a>';
            // $data .=  '<a href="javascript:void(0)" class="delete_company" data-action="delete" data-placement="left" data-id="'.$seller->id.'"  title="Delete" data-togg/mnt/volume_nyc3_02/flipquick/public_html/apple="tooltip"><i class="fa fa-trash iconsetaddbox"></i>
            //     </a>';

            return $data;
        })
        ->rawColumns(['name','action'])
        ->order(function ($query) {
            if (request()->has('created_at')) {
                $query->orderBy('created_at', 'DESC');
            }
        })
        ->setTotalRecords($count)
        ->setFilteredRecords($count)
        ->skipPaging()
        ->make(true);
    }

    public function current_listing(Request $request,$id){
        $page      = $request->get('page');
        $seller    = Seller::where('id',$id)->first();
        $appTiming = AppHelper::getCurrentSlotTiming($seller->time_zone->timezone);

        $data['timezone']       = $seller->time_zone->timezone;
        $data['timezone_name']  = $seller->time_zone->timezone_name;

        $data['starts_at']      = $appTiming['starts_at_timezone']->format('h:i A');
        $data['ends_at']        = $appTiming['ends_at_timezone']->format('h:i A');

        $data['time_remaining']  = $appTiming['time_remaining'];
        $data['current_listing'] = Vehicle:://where('starts_at','<',$appTiming['ends_at'])
                                            // ->where('expired_at','>=',$appTiming['ends_at'])
                                            where('seller_id',$id)
                                            ->with(['vehicle_exterior_photos'=>function($query){
                                                $query->where('vehicle_exterior_type','front')->select('vehicle_id','vehicle_exterior_type','image');
                                            }])
                                            // ->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
                                            ->withCount('bids')
                                            ->paginate(6);

        $past_listing = Vehicle:://where('starts_at','<',$appTiming['ends_at'])
                                            // ->where('expired_at','>=',$appTiming['ends_at'])
                                            where('seller_id',$id)
                                            ->with(['vehicle_exterior_photos'=>function($query){
                                                $query->where('vehicle_exterior_type','front')->select('vehicle_id','vehicle_exterior_type','image');
                                            }])
                                            // ->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
                                            ->withCount('bids')
                                            ->paginate(6);
        $past_listing->getCollection()->transform(function($vehicle) use ($data){
                                                $vehicle->expired_at = Carbon::parse($vehicle->expired_at)->setTimeZone($data['timezone']);
                                                $vehicle->is_selling = ($vehicle->bids->where('status',1)->count() >= 1) ? true : false;
                                                $vehicle->sell_amount = ($vehicle->bids->where('status',1)->count() >= 1) ? $vehicle->bids->where('status',1)->first()->amount : 0;

                                                return $vehicle;
                                            });
        $data['past_listing']   = $past_listing;
        
        if ($request->ajax()) {
            $view = view('admin.sellers.vehicle_list_results',compact('data'))->render();
            return response()->json(['html'=>$view]);
        }
        $lastPage   = $data['current_listing']->lastPage();        
        $total      = $data['current_listing']->total(); 
        $pastlastPage   = $past_listing->lastPage();        
        $pastTotal      = $past_listing->total(); 

        return view('admin.sellers.current_listing',compact('data','id','lastPage','total','pastTotal','pastlastPage'));
    }   

    public function past_listing(Request $request,$id){
        $page           = $request->get('page');
        $seller = Seller::where('id',$id)->first();
        $appTiming = AppHelper::getCurrentSlotTiming($seller->time_zone->timezone);
    
        $data['timezone']       = $seller->time_zone->timezone;
        $data['timezone_name']  = $seller->time_zone->timezone_name;

        $past_listing = Vehicle:://where('starts_at','<',$appTiming['ends_at'])
                                            // ->where('expired_at','>=',$appTiming['ends_at'])
                                            where('seller_id',$id)
                                            ->with(['vehicle_exterior_photos'=>function($query){
                                                $query->where('vehicle_exterior_type','front')->select('vehicle_id','vehicle_exterior_type','image');
                                            }])
                                            // ->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
                                            ->withCount('bids')
                                            ->paginate(6);
        $past_listing->getCollection()->transform(function($vehicle) use ($data){
                                                $vehicle->expired_at = Carbon::parse($vehicle->expired_at)->setTimeZone($data['timezone']);
                                                $vehicle->is_selling = ($vehicle->bids->where('status',1)->count() >= 1) ? true : false;
                                                $vehicle->sell_amount = ($vehicle->bids->where('status',1)->count() >= 1) ? $vehicle->bids->where('status',1)->first()->amount : 0;

                                                return $vehicle;
                                            });
        
        if ($request->ajax()) {
            $view = view('admin.sellers.pastvehicle_list_results',compact('past_listing'))->render();
            return response()->json(['html'=>$view]);
        }
        $lastPage = $past_listing->lastPage();        
        $total = $past_listing->total(); 

        return view('admin.sellers.past_listing',compact('past_listing','id','lastPage','total'));
    }   

    public function vehicle_detail($id){
        $vehicle = Vehicle::where('id',$id)->first();
        return view('admin.seller_vehicle_detail',compact('vehicle'));
    }

    public function repo($id)
    {
        $seller = Seller::where('id',$id)->first();

         return view('admin.sellers.repo',compact('seller','id'));
        // return response()->json(['status'=>true,'data'=>$html]);
    }

    public function updaterepo(Request $request){
        $seller = Seller::where('id',$request->s_id)->first();
        $rules =  [
            'repo'    => 'required',
            'repo_charged'     => 'required_if:repo,"=",1',
            'repo_charged'     => 'required_if:repo,"=",charged'
        ];
   
        $validator  = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }

        $inputs = $request->all(); 
        unset($inputs['_token']);
        $seller->update($inputs);

        return response()->json(['status' => true,'message' => 'Repo updated successfully']);
    }
}
