<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v1\Maintenance;
use App\Models\v1\AdminStatistic;
use Validator;
use Carbon\Carbon;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $maintenance     = Maintenance::first();
        $adminStatistics = AdminStatistic::all();
        return view('admin.settings.index',compact('maintenance','adminStatistics'));
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateSettings(Request $request)
    {
    	$validator = Validator::make($request->all(),[
                        'android_version'  => 'required',
                        'ios_version'      => 'required',
                        'maintenance_mode' => 'required',
                    ]);
        
        if ($validator->fails())
        {
            $message = $validator->messages()->first();
            return response()->json(['status' => false,'message' => $message]);
        }

        $maintenace = Maintenance::first();
        
        $maintanance['android_version']  = $request->get('android_version');
        $maintanance['ios_version']      = $request->get('ios_version');
        $maintanance['maintenance_mode'] = (int)$request->get('maintenance_mode');
        $maintenaceArr = [];
        if($maintenace){
            $maintenaceArr = ['id'=>$maintenace->id];
        }
        Maintenance::UpdateOrCreate($maintenaceArr,$maintanance);

        $adminStatistics = AdminStatistic::first();
        $adminStat['per_visit_charge_daylight']  = $request->get('per_visit_charge_daylight');
        $adminStat['per_visit_charge_midnight']  = $request->get('per_visit_charge_midnight');
        $adminStat['daylight_time']              = $request->get('daylight_time');
        $adminStat['midnight_time']              = $request->get('midnight_time');

        $adminStat['tax_cent']                   = $request->get('tax_cent');
        $adminStatArr = [];
        if($adminStatistics){
            $adminStatArr = ['id'=>$adminStatistics->id];
        }
        AdminStatistic::UpdateOrCreate($adminStatArr,$adminStat);

    	return response()->json(['status'=>true,'message'=>'Setting has been successfully updated.']);            
    }
}
