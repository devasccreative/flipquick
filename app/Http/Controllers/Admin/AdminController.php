<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\v1\Vehicle;
use App\Models\v1\Dealer;
use App\Models\v1\Seller;
use App\Models\v1\SellerTransaction;
use App\Models\v1\DealerTransaction;
use App\Models\v1\ReportIssue;
use Auth;
use Carbon\Carbon;
use DB;
use App\Http\Controllers\Controller;
use App\Helpers\AppHelper;
use Validator;
use Hash;
use DataTables;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        $page            = $request->get('page');
        $current_time    = Carbon::now();
        $current_listing = Vehicle::with(['vehicle_exterior_photos'=>function($query){
                                                $query->where('vehicle_exterior_type','front')->select('vehicle_id','vehicle_exterior_type','image');
                                            },'bids'=>function($query){
                                                // $query->where('dealer_id',$dealer->id);
                                            }])
                                            ->where('expired_at','>',$current_time)
                                            ->withCount('bids')
                                            ->paginate(6);
                                            
        $current_listing->getCollection()->transform(function($vehicle){
                                                $seller = $vehicle->seller;
                                                $appTiming = AppHelper::getCurrentSlotTiming($seller->time_zone->timezone);
                                                $vehicle->timezone       = $seller->time_zone->timezone;
                                                $vehicle->timezone_name  = $seller->time_zone->timezone_name;

                                                $vehicle->starts_at      = $appTiming['starts_at_timezone']->format('h:i A');
                                                $vehicle->time_remaining = $appTiming['time_remaining'];
                                                return $vehicle;
                                            });
        if ($request->ajax()) {
            $view = view('admin.vehicle_list_results',compact('current_listing'))->render();
            return response()->json(['html'=>$view]);
        }
        $lastPage = $current_listing->lastPage();        
        $total = $current_listing->total();      
        $total_vehicles =  Vehicle::where('steps_submitted',8)->count();
        $total_dealers  =  Dealer::where('parent_id',0)->count();     
        $total_sellers  =  Seller::count();     
        $total_income   =  SellerTransaction::where('approved','1')->sum('amount') + DealerTransaction::where('approved','1')->sum('amount');     

       return view('admin.admin-home',compact('current_listing','lastPage','total','total_vehicles','total_dealers','total_sellers','total_income'));
    }

    /** change password view **/
    public function changePassword(){
       return view('admin.change_password');
    }

    /** update password **/
    public function update_password(Request $request){

        $validator = Validator::make($request->all(), [
            'password'      => 'required',
            'newPassword'   => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }

        $profile = auth('admin')->user();

        if($request->get('password') != $request->get('newPassword')) {

            if (Hash::check($request->get('password'), $profile->password)) {
                $secret = bcrypt($request->newPassword);
                $updatePassword = $profile->update(['password'=>$secret]);

                if($updatePassword) {
                    return response()->json(['status'=>true,'message'=>'Password change successfully.']);
                }

                return response()->json(['status'=>false,'message'=>'Something went wrong.']);
            }

            return response()->json(['status'=>false,'message'=>'Old Password Does not match!']);
        }

        return response()->json(['status'=>false,'message'=>'Both Password are Same!']);
    }

    public function reports(){
       return view('admin.reports');
    }

    public function getreports(Request $request){
        $skip = (int)$request->get('start');
        $take = (int)$request->get('length');
        $search = $request->get('search')['value'];
        $reports = ReportIssue::select('id','title','created_at');

        if($search != ''){
            $reports->where(function($query) use ($search){
                $query->where('title','like','%'.$search.'%')
                ->orWhere('created_at','like','%'.$search.'%');
            });
        }

        $reports = $reports->orderBy('created_at','DESC')->skip($skip)->take($take)->get();
        $count = ReportIssue::count();
        if($search != ''){
            $count = ReportIssue::where(function($query) use ($search){
                $query->where('name','like','%'.$search.'%')
                ->orWhere('created_at','like','%'.$search.'%');
            })->count();
        }

        return DataTables::of($reports)
        ->addIndexColumn()
        ->editColumn('created_at', function($seller){
            return Carbon::parse($seller->created_at)->format('d-M-Y @ h:i A');
        })
        ->editColumn('subject', function($seller){
            return $seller->title;
        })
        ->addColumn('action', function($seller){
            $data = '';
            $data =  '<a href="'.route('admin.report',$seller->id).'">View</a>';
            // $data .=  '<a href="javascript:void(0)" class="delete_company" data-action="delete" data-placement="left" data-id="'.$seller->id.'"  title="Delete" data-toggle="tooltip"><i class="fa fa-trash iconsetaddbox"></i>
            //     </a>';

            return $data;
        })
        ->rawColumns(['action'])
        ->order(function ($query) {
            if (request()->has('created_at')) {
                $query->orderBy('created_at', 'DESC');
            }
        })
        ->setTotalRecords($count)
        ->setFilteredRecords($count)
        ->skipPaging()
        ->make(true);
    }

    public function report($id){
        $report = ReportIssue::find($id);
        return view('admin.report',compact('report'));
    }


}
