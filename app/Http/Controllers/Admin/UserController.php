<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('admin.users.profile',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //GET USER
    public function getUsers(Request $request)
    {
        // die('test');
        return DataTables::of(User::orderBy('created_at', 'desc')
                                    ->get())
        ->addIndexColumn()
        ->addColumn('name', function($user){
            return $user->first_name.' '.$user->last_name;
        })
        ->addColumn('verifiedInfo', function($user){
            $verifiedInfo='<a href="javascript:void(0)"><img src="'.asset('admin_assets/svg/users-icon1.svg').'"></a>';
            $verifiedInfo .= ($user->phone) ? '<a href="javascript:void(0)"><img src="'.asset('admin_assets/svg/users-icon2.svg').'"></a>' : '';
             $verifiedInfo .= ($user->business['is_publish']) ? '<a href="javascript:void(0)"><img src="'.asset('admin_assets/svg/users-icon3.svg').'"></a>' : '';
            return $verifiedInfo;
        })
        ->addColumn('more', function($user){
            $more='<a href="'.route("admin.users.show",$user->id).'" title="View" data-id="'.$user->id.'">View Profile</a>';
            return $more;
        })
        ->rawColumns(['name','verifiedInfo','more'])
        ->make(true);
    }
}
