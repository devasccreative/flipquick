<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\v1\DealerTransaction;
use App\Models\v1\VehicleBid;
use App\Models\v1\ReportVehicle;
use App\Models\v1\Vehicle;
use App\Models\v1\Dealer;
use App\Models\v1\TimeZone;
use App\Helpers\AppHelper;
use App\Models\v1\Make;
use Carbon\Carbon;
use Notification;
use App\Notifications\ApproveDealerAccountNotification;
use Auth;
use DB;
use Validator;

class DealerController extends Controller
{
    public function __construct()
    {
        $this->merchant_id  = config('services.bambora.merchant_id'); 
        $this->api_key      = config('services.bambora.api_key');
        $this->api_version  = config('services.bambora.api_version');
        $this->platform         = config('services.bambora.platform'); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dealers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehicle = Vehicle::where('id',$id)->first();
        return view('admin.vehicle_details',compact('vehicle'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function is_approved(Request $request){
        $status = $request->get('is_approved');
        $id     = $request->get('id');
        $dealer = Dealer::find($id);
        if($status == 'Active'){
            $dealer->is_approved = 1;
            $dealer->notify(new ApproveDealerAccountNotification());
            $message = 'Dealer Approved successfully';
        }else{
            $dealer->is_approved = 0;
            $message = 'Dealer Approved successfully';
        }
        $dealer->save();
        return response()->json(['status'=>true,'message'=>$message]);
    }

    public function getDealerList(Request $request){
        $skip   = (int)$request->get('start');
        $take   = (int)$request->get('length');
        $search = $request->get('search')['value'];
        $dealers = Dealer::select('id','first_name','last_name','email','dealership_name','dealer_no','amvic_no','is_approved','parent_id','created_at')->where('parent_id',0);

        if($search != ''){
            $dealers->where(function($query) use ($search){
                $query->where('first_name','like','%'.$search.'%')
                ->orWhere('last_name','like','%'.$search.'%')
                ->orWhere('dealership_name','like','%'.$search.'%')
                ->orWhere('amvic_no','like','%'.$search.'%')
                ->orWhere('dealer_no','like','%'.$search.'%')
                ->orWhere('email','like','%'.$search.'%');
            });
        }

        if($request->get('is_approved') == '0'){
            $dealers->where('is_approved',0);
        }

        $count = $dealers->count();
        $dealers = $dealers->orderBy('created_at','DESC')->skip($skip)->take($take)->get();
        if($search != ''){
            $count = Dealer::where(function($query) use ($search){
                $query->where('first_name','like','%'.$search.'%')
                ->orWhere('last_name','like','%'.$search.'%')
                ->orWhere('dealer_no','like','%'.$search.'%')
                ->orWhere('email','like','%'.$search.'%');
            })->count();
        }

        return DataTables::of($dealers)
                    ->addIndexColumn()
                    ->editColumn('created_at', function($dealer){
                        return Carbon::parse($dealer->created_at)->format('d-M-Y @ h:i A');
                    })
                    ->editColumn('name', function($dealer){
                        return '<a href="'.route('admin.dealer.dashboard',$dealer->id).'">'.$dealer->first_name.' '.$dealer->last_name.'</a>';
                    })
                    // ->editColumn('phone', function($dealer){
                    //     return $dealer->country_code.'-'.$dealer->phone;
                    // })
                     ->editColumn('is_approved',function($dealer){
                        $checked ="";
                        $activated="";
                        if ($dealer->is_approved == 1) {
                            $checked = "checked";
                            $activated = 'Active';
                        }else{
                            $activated = 'Deactive';
                        }
                        return '<label class="switch">
                        <input type="checkbox" class="this-switch" name="is_approved" onchange="is_approvedUpdate(this)" id="is_approved_'.$dealer->id.'" data-id="'.$dealer->id.'" value="'.$activated.'" '.$checked.'>
                        <span class="slider round"></span>
                        </label>';
                    })
                    ->addColumn('action', function($dealer){
                        $data =  '<a href="'.route('admin.dealer.dashboard',$dealer->id).'">View</a>';
                        return $data;
                    })
                    ->rawColumns(['name','is_approved','action'])
                    ->order(function ($query) {
                        if (request()->has('created_at')) {
                            $query->orderBy('created_at', 'DESC');
                        }
                    })
                    ->setTotalRecords($count)
                    ->setFilteredRecords($count)
                    ->skipPaging()
                    ->make(true);
    }

    public function dashboard(Request $request,$id){
        $page   = $request->get('page9am');
        $data['dealer'] = $dealer = Dealer::find($id);
        $dealer_id[]  = $dealer->id;
        if($dealer->parent_id == 0){
            $dealer_ids =  $dealer->sub_dealers()->pluck('id')->toArray();
        }else{
            $dealer_idmain[] =  $dealer->parent_id;
            $dealer_ids =  $dealer->main_dealers->sub_dealers()->pluck('id')->toArray();
            $dealer_ids = array_merge($dealer_idmain,$dealer_ids);
        }
        $dealer_id = array_merge($dealer_id,$dealer_ids);
        $dealerMakes = $dealer->makes()->pluck('make_id')->toArray();

        $data['timezone']       = $dealer->time_zone->timezone;
        $data['timezone_name']  = $dealer->time_zone->timezone_name;
        $appTiming = AppHelper::both_current_slot_time($dealer->time_zone->timezone);
        $appTimingCurrent = AppHelper::getCurrentSlotTiming($dealer->time_zone->timezone);

        // echo "<pre>";
        // print_r($appTiming);die();
        if($dealer->membership_plan && $dealer->membership_plan->plan_name == 'Free' && (Carbon::now()->diffInDays($dealer->created_at)) >= 30){
            return redirect()->route('website.dealer.membership_plan')->with('message', 'Your trial is expired. Please select membership plan to continue.');
        }

        $data['current_listing_9am_slot_starts_at']     = $appTiming['nine_am_slot_starts_at']->format('h:i A');
        $data['current_listing_9am_slot_ends_at']       = $appTiming['nine_am_slot_expired_at']->format('h:i A');
        $data['four_pm_slot_starts_at']     = $appTiming['nine_am_slot_starts_at']->format('h:i A');
        $data['four_pm_slot_expired_at']    = $appTiming['nine_am_slot_expired_at']->format('h:i A');
        
        $last_slot_time                      = $appTimingCurrent['last_slot_start'];
        $data['nine_am_slot_time_remaining'] = $appTiming['nine_am_slot_time_remaining'];
        $data['four_pm_slot_time_remaining'] = $appTiming['four_pm_slot_time_remaining'];

        $lat  = $dealer->latitude;
        $long = $dealer->longitude;
        $bids = VehicleBid::whereIn('dealer_id',$dealer_id);
        $data['total_bids']  = $bids->count();                                
        $data['lifetime_wons_bids']  = $bids
                                        ->whereHas('vehicle',function($query){
                                            $query->where('expired_at','<=',Carbon::now());
                                        })
                                        ->get()
                                        ->filter(function ($wonBid) {
                                                return $wonBid->status == 1;
                                            })
                                        ->count();
        $data['last_wons_bids'] = $dealer->bids()
                                        ->whereHas('vehicle',function($query) use ($last_slot_time){
                                            $query->where('starts_at',$last_slot_time);
                                        })
                                        ->get()
                                        ->filter(function ($wonBid) {
                                            return $wonBid->status == 1;
                                        })
                                        ->count();

        $data['current_listing_9am_slot'] = Vehicle::where('expired_at',$appTiming['expired_at_9am_utc'])
                                                ->where('steps_submitted',8)
                                                ->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
                                                ->with(['vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type as type,image','vehicle_interior_photos:id,vehicle_id,vehicle_interior_type as type,image','vehicle_damage_photos:id,vehicle_id,image'])
                                                ->whereDoesntHave('bids',function($query) use ($dealer_id){
                                                    $query->whereIn('dealer_id',$dealer_id);
                                                })
                                                ->where(function($query) use ($dealerMakes){
                                                    $query->where('post_type','!=','trade for new')
                                                        ->orWhereHas('makes',function($query) use ($dealerMakes){
                                                            $query->where('makes.id',1)
                                                                ->orWhereIn('makes.id',$dealerMakes);
                                                        });
                                                })
                                                ->withCount('bids')
                                                ->whereDoesntHave('seller', function ($query) {
                                                                $query->where('repo', 1);
                                                            })
                                                // ->groupBy('expired_at')
                                                ->paginate(6, ['*'], 'page', $page);

        $page4pm   = $request->get('page4pm');
        $data['current_listing_4pm_slot'] = Vehicle::where('expired_at',$appTiming['expired_at_4pm_utc'])                                       
                                                ->where('steps_submitted',8)
                                                ->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
                                                ->with(['vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type as type,image','vehicle_interior_photos:id,vehicle_id,vehicle_interior_type as type,image','vehicle_damage_photos:id,vehicle_id,image'])
                                                ->whereDoesntHave('bids',function($query) use ($dealer_id){
                                                    $query->whereIn('dealer_id',$dealer_id);
                                                })
                                                ->where(function($query) use ($dealerMakes){
                                                    $query->where('post_type','!=','trade for new')
                                                        ->orWhereHas('makes',function($query) use ($dealerMakes){
                                                            $query->where('makes.id',1)
                                                                ->orWhereIn('makes.id',$dealerMakes);
                                                        });
                                                })
                                                ->whereDoesntHave('seller', function ($query) {
                                                                $query->where('repo', 1);
                                                            })
                                                ->withCount('bids')
                                                ->paginate(6, ['*'], 'page', $page4pm);

        $pagerepo   = $request->get('pagerepo');

 $data['current_listing_repo_slot'] = Vehicle::where('expired_at',$appTiming['expired_at_9am_utc'])
                                                ->where('steps_submitted',8)
                                                ->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
                                                ->with(['vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type as type,image','vehicle_interior_photos:id,vehicle_id,vehicle_interior_type as type,image','vehicle_damage_photos:id,vehicle_id,image'])
                                                ->whereDoesntHave('bids',function($query) use ($dealer_id){
                                                    $query->whereIn('dealer_id',$dealer_id);
                                                })
                                                ->where(function($query) use ($dealerMakes){
                                                    $query->where('post_type','!=','trade for new')
                                                        ->orWhereHas('makes',function($query) use ($dealerMakes){
                                                            $query->where('makes.id',1)
                                                                ->orWhereIn('makes.id',$dealerMakes);
                                                        });
                                                })
                                                ->withCount('bids')
                                                ->whereHas('seller', function ($query) {
                                                                $query->where('repo', 1);
                                                            })
                                                // ->groupBy('expired_at')
                                                ->paginate(6, ['*'], 'page', $pagerepo);
        if ($request->ajax() && $request->has('page9am')) {
            $view = view('admin.dealers.vehicle_list_9amslot_results',compact('data'))->render();
            return response()->json(['html'=>$view]);
        }
        if ($request->ajax() && $request->has('page4pm')) {
            $view = view('admin.dealers.vehicle_list_4pmslot_results',compact('data'))->render();
            return response()->json(['html'=>$view]);
        }
        if ($request->ajax() && $request->has('pagerepo')) {
            $view = view('admin.dealers.vehicle_list_repo_results',compact('data'))->render();
            return response()->json(['html'=>$view]);
        }
        $lastPage_9am_slot  = $data['current_listing_9am_slot']->lastPage();        
        $total_9am_slot     = $data['current_listing_9am_slot']->total(); 
        $lastPage_4pm_slot  = $data['current_listing_4pm_slot']->lastPage();        
        $total_4pm_slot     = $data['current_listing_4pm_slot']->total(); 
         $lastPage_repo_slot  = $data['current_listing_repo_slot']->lastPage();        
        $total_repo_slot     = $data['current_listing_repo_slot']->total(); 
        $data['total_income'] =  DealerTransaction::where('approved','1')->sum('amount');     

        return view('admin.dealers.dashboard',compact('data','id','lastPage_9am_slot','total_9am_slot','lastPage_4pm_slot','total_4pm_slot','lastPage_repo_slot','total_repo_slot'));




        $page            = $request->get('page');
        $data['dealer'] = $dealer = Dealer::find($id);
        if($dealer->membership_plan && $dealer->membership_plan->plan_name == 'Free' && (Carbon::now()->diffInDays($dealer->created_at)) >= 30){
            return redirect()->route('website.dealer.membership_plan')->with('message', 'Your trial is expired. Please select membership plan to continue.');
        }
        $dealer_id[]  = $dealer->id;
        if($dealer->parent_id == 0){
            $dealer_ids =  $dealer->sub_dealers()->pluck('id')->toArray();
        }else{
            $dealer_idmain[] =  $dealer->parent_id;
            $dealer_ids =  $dealer->main_dealers->sub_dealers()->pluck('id')->toArray();
            $dealer_ids = array_merge($dealer_idmain,$dealer_ids);
        }
        $dealer_id = array_merge($dealer_id,$dealer_ids);
        $appTiming = AppHelper::getCurrentSlotTiming($dealer->time_zone->timezone);
        
        $current_time   = Carbon::now($dealer->time_zone->timezone);
        $data['timezone']       = $dealer->time_zone->timezone;
        $data['timezone_name']  = $dealer->time_zone->timezone_name;

        $data['starts_at']      = $appTiming['starts_at_timezone']->format('h:i A');
        $data['ends_at']        = $appTiming['ends_at_timezone']->format('h:i A');

        $last_slot_time         = $appTiming['last_slot_start'];
        $data['time_remaining']  = $appTiming['time_remaining'];
        
        $lat  = $dealer->latitude;
        $long = $dealer->longitude;

        $bids = VehicleBid::whereIn('dealer_id',$dealer_id);


         $data['total_bids']  = $bids->count();                                
         $data['lifetime_wons_bids']  = $bids
                                        ->whereHas('vehicle',function($query){
                                            $query->where('expired_at','<=',Carbon::now());
                                        })
                                        ->get()
                                        ->filter(function ($wonBid) {
                                                return $wonBid->status == 1;
                                            })
                                        ->count();

        $data['last_wons_bids']  = VehicleBid::whereIn('dealer_id',$dealer_id)
                                        ->whereHas('vehicle',function($query) use ($last_slot_time){
                                                $query->where('starts_at',$last_slot_time);
                                            })
                                        ->get()
                                        ->filter(function ($wonBid) {
                                                return $wonBid->status == 1;
                                            })
                                        ->count();

        $data['current_listing'] = Vehicle:://where('starts_at','<',$appTiming['ends_at'])
                                            // ->where('expired_at','>=',$appTiming['ends_at'])
                                            with(['vehicle_exterior_photos'=>function($query){
                                                $query->where('vehicle_exterior_type','front')->select('vehicle_id','vehicle_exterior_type','image');
                                            },'bids'])
                                            ->whereDoesntHave('bids',function($query) use ($dealer_id){
                                                $query->whereIn('dealer_id',$dealer_id);
                                            })
                                            // ->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
                                            ->withCount('bids')
                                            ->paginate(6);

        if ($request->ajax()) {
            $view = view('admin.sellers.vehicle_list_results',compact('data'))->render();
            return response()->json(['html'=>$view]);
        }
        $lastPage = $data['current_listing']->lastPage();        
        $total = $data['current_listing']->total();
        $data['total_income'] =  DealerTransaction::where('approved','1')->sum('amount');     
        return view('admin.dealers.dashboard',compact('data','id','lastPage','total'));
    }

    public function submitted_bids(Request $request,$id){
        $page   = $request->get('page');
        $dealer = Dealer::where('id',$id)->first();
        $dealer_id[]  = $dealer->id;
        if($dealer->parent_id == 0){
            $dealer_ids =  $dealer->sub_dealers()->pluck('id')->toArray();
        }else{
            $dealer_idmain[] =  $dealer->parent_id;
            $dealer_ids =  $dealer->main_dealers->sub_dealers()->pluck('id')->toArray();
            $dealer_ids = array_merge($dealer_idmain,$dealer_ids);
        }
        $dealer_id = array_merge($dealer_id,$dealer_ids);
        
        $bids = VehicleBid::whereIn('dealer_id',$dealer_id)->paginate(6);
        
        $lastPage = $bids->lastPage(); 
        if ($request->ajax()) {
            $view = view('admin.dealers.bids_list_results',compact('bids'))->render();
            return response()->json(['html'=>$view]);
        }
        $total =  $bids->total();
        return view('admin.dealers.bids_list',compact('dealer','bids','lastPage','total','id'));
    }


    public function won_bids(Request $request,$id){
        $page       = $request->get('page');
        $skip       = 0;
        $take = 2;
        if($page > 1){
            $skip = ($page-1) * $take;
        }
        $dealer = Dealer::where('id',$id)->first();
        $dealer_id[]  = $dealer->id;
        if($dealer->parent_id == 0){
            $dealer_ids =  $dealer->sub_dealers()->pluck('id')->toArray();
        }else{
            $dealer_idmain[] =  $dealer->parent_id;
            $dealer_ids =  $dealer->main_dealers->sub_dealers()->pluck('id')->toArray();
            $dealer_ids = array_merge($dealer_idmain,$dealer_ids);
        }
        $dealer_id = array_merge($dealer_id,$dealer_ids);
        $bids    = VehicleBid::whereIn('dealer_id',$dealer_id)
                                    ->whereHas('vehicle',function($query){
                                        $query->where('expired_at','<=',Carbon::now());
                                    });

        $lastPage   = ceil($bids->count() / $take); //$bids->lastPage(); 
        $total      = $bids->get()
                            ->filter(function ($wonBid) {
                                return $wonBid->status == 1;
                            })
                            ->count(); //$bids->total(); 

        $bids    = VehicleBid::whereIn('dealer_id',$dealer_id)
                                    ->whereHas('vehicle',function($query){
                                        $query->where('expired_at','<=',Carbon::now());
                                    })//->skip($skip)
                                    //->take($take)
                                    // ->paginate(6);
                                    ->get()
                                    ->filter(function ($wonBid) {
                                            return $wonBid->status == 1;
                                        });
        
        $bids->map(function($wonBids){
                            $wonBids->created_date = Carbon::parse($wonBids->created_at)->format('d M Y, @ H:i a');
                            return $wonBids;
                        });

       if ($request->ajax()) {
            $view = view('admin.dealers.wonbids_list_results',compact('bids'))->render();
            return response()->json(['html'=>$view]);
        }
        return view('admin.dealers.won_bids_list',compact('dealer','bids','lastPage','id','total'));
                        // ->whereHas('bids',function($query){
                        //     $query->where('status','1');
                        // })
                        // ->with(['bids'=>function($query){
                        //     $query->where('status','1')
                        //     ->with(['vehicle:id,style,year,make,model,trim,drive_train,milage,exterior_color,interior_color,body_shape,doors,passengers,air_conditioning,navigation_system,leather_seats,sun_roof,panoramic_roof,dvd,heated_seats,ac_seats,360_camera,after_market_equipment,original_owner,still_making_monthly_payment,still_own,have_any_accident_record,how_much_records,smoked_in,vehicle_condition,front_tires_condition,back_tires_condition,warning_lights,mechanical_issues','vehicle.vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type,image']);
                        // }])
                        /*->with([
                                'bids.vehicle:id,style,year,make,model,trim,drive_train,milage,exterior_color,interior_color,body_shape,doors,passengers',
                                'bids.vehicle.vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type,image'
                            ])*/
                        // ->first();
        return view('admin.dealers.bids_list',compact('dealer','id'));   
    }

     public function reported_post(Request $request,$id){
        $page       = $request->get('page');
        $skip       = 0;
        $take = 2;
        if($page > 1){
            $skip = ($page-1) * $take;
        }
        $dealer = Dealer::where('id',$id)->first();
        $dealer_id[]  = $dealer->id;
        if($dealer->parent_id == 0){
            $dealer_ids =  $dealer->sub_dealers()->pluck('id')->toArray();
        }else{
            $dealer_idmain[] =  $dealer->parent_id;
            $dealer_ids =  $dealer->main_dealers->sub_dealers()->pluck('id')->toArray();
            $dealer_ids = array_merge($dealer_idmain,$dealer_ids);
        }
        $dealer_id = array_merge($dealer_id,$dealer_ids);
        $bids    = ReportVehicle::whereIn('dealer_id',$dealer_id)->whereHas('vehicle');

        $lastPage   = ceil($bids->count() / $take); //$bids->lastPage(); 
        $total      = $bids->get()
                            ->count(); //$bids->total(); 

        $bids    = ReportVehicle::whereIn('dealer_id',$dealer_id)->whereHas('vehicle')
                                    ->get();
        
        $bids->map(function($wonBids){
                            $wonBids->created_date = Carbon::parse($wonBids->created_at)->format('d M Y, @ H:i a');
                            return $wonBids;
                        });

       if ($request->ajax()) {
            $view = view('admin.dealers.reported_post_list_results',compact('bids'))->render();
            return response()->json(['html'=>$view]);
        }
        return view('admin.dealers.reported_post_list',compact('dealer','bids','lastPage','id','total'));
                        // ->whereHas('bids',function($query){
                        //     $query->where('status','1');
                        // })
                        // ->with(['bids'=>function($query){
                        //     $query->where('status','1')
                        //     ->with(['vehicle:id,style,year,make,model,trim,drive_train,milage,exterior_color,interior_color,body_shape,doors,passengers,air_conditioning,navigation_system,leather_seats,sun_roof,panoramic_roof,dvd,heated_seats,ac_seats,360_camera,after_market_equipment,original_owner,still_making_monthly_payment,still_own,have_any_accident_record,how_much_records,smoked_in,vehicle_condition,front_tires_condition,back_tires_condition,warning_lights,mechanical_issues','vehicle.vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type,image']);
                        // }])
                        /*->with([
                                'bids.vehicle:id,style,year,make,model,trim,drive_train,milage,exterior_color,interior_color,body_shape,doors,passengers',
                                'bids.vehicle.vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type,image'
                            ])*/
                        // ->first();
        return view('admin.dealers.reported_post_list',compact('dealer','id'));   
    }

    public function view_bid_detail($dealer_id,$id){
        $vehicle = Vehicle::where('id',$id)->first();
        $id = $dealer_id;
        return view('website.vehicle_details',compact('id','vehicle'));
    }

    public function payment_detail($id){
        $dealer = Dealer::find($id);
        //Create Beanstream Gateway
        $profile = [];
        if($dealer->bambora_id){
            $beanstream = new \Beanstream\Gateway($this->merchant_id, $this->api_key, $this->platform, $this->api_version);
            $profile = $beanstream->profiles()->getProfile($dealer->bambora_id);
        }
        $membership_plan = $dealer->membership_plan;
        // if($membership_plan){
        //     if($membership_plan->plan_name == 'Free'){
        //         $membership_plan->expiry_date = $dealer->created_at->addDays(30);
        //     } else if($membership_plan->plan_name == '$100'){
        //         $membership_plan->expiry_date = $dealer->created_at->addMonth();
        //     } else{
        //         $membership_plan->expiry_date = $dealer->created_at->addYear();
        //     }
        //     $membership_plan->expiry_date = Carbon::parse($membership_plan->expiry_date)->format('d M, Y');
        // }
        $transaction = $dealer->transactions()->orderBy('created_at','desc')->first();
        $membership_plan->expiry_date = Carbon::parse($transaction->billing_period_end)->format('d M, Y');
        return view('admin.dealers.payment_detail',compact('profile','membership_plan','id','dealer'));   
    }

    /** cancel subscription **/
    public function cancel_subscription(Request $request,$id){

        $dealer = Dealer::find($id);
        if($dealer->membership_plan_id != 1){
            $merchant_id    = config('services.bambora.merchant_id'); 
            $recurring_passcode     = config('services.bambora.recurring_passcode'); 
            $transaction_response   = explode('&',$dealer->transaction_response);
            $rbAccountId            = explode('=',$transaction_response[11])[1];
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://web.na.bambora.com/scripts/recurring_billing.asp?serviceVersion=1.0&operationType=C&merchantId=".$merchant_id."&passCode=".$recurring_passcode."&rbAccountId=".$rbAccountId,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                    "Content-Length: 0"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $trnResponse = explode('&',$response);
            if ($err) {
                return response()->json(['status'=>false,'message'=>$err]);
            } 
            
            // if($trnResponse[0] == '1'){
                $dealer->deleted_response = $response;
                $dealer->save();
            // }
            if($trnResponse[0] == '0'){
                return response()->json(['status'=>false,'message'=>explode('&',$trnResponse[0])[1]]);
            }
            $dealer->delete();
            return response()->json(['status'=>true,'message'=>"Membership plan cancelled successfully."]);

        }else{
            $dealer->delete();
            return response()->json(['status'=>true,'message'=>"Membership plan cancelled successfully."]);
        }
    }

    public function login_as_dealer($id){
        Auth::guard('dealer')->logout();
        $dealer = Dealer::find($id);
        if(count($dealer) > 0){
            Auth::guard('dealer')->loginUsingId($dealer->id);
            return redirect()->route('dealer.dashboard');
        }else{
            return redirect()->route('admin.vendorDetail',base64_encode($vendor_id));
        }
    }

    /** edit profile page **/
    public function edit_profile($id){
        $dealer = Dealer::find($id);
        $timezones = Timezone::all();
        $makes = Make::all();
        $dealerMakes = $dealer->makes()->pluck('make_id')->toArray();
        // print_r($dealerMakes);die();
        return view('admin.dealers.edit_profile',compact('dealer','timezones','makes','dealerMakes','id'));
    }

    /** update profile info **/
    public function update_profile(Request $request,$id){

        $dealer = Dealer::find($id); 
        $rules =  [
            'first_name'    => 'required | string',
            'last_name'     => 'required | string',
            'email'         => 'required | string | email | unique:dealers,email,'.$dealer->id,
            'phone'         => 'required | unique:dealers,phone,'.$dealer->id,
            'makes'         => 'required'
        ];
        if($dealer->parent_id == 0){
            $rules['dealership_name'] = 'required';
            $rules['amvic_no'] = 'required';
            $rules['dealer_no'] = 'required';
            $rules['latitude'] = 'required';
            $rules['longitude'] = 'required';
            $rules['address'] = 'required';
        }
        $validator  = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
        }

        $inputs = $request->all(); 
        if(isset($inputs['amvic_no']) && $inputs['amvic_no'] != $dealer->amvic_no){
            $inputs['is_approved'] = 0;
        }

        unset($inputs['_token']);
        $dealer->update($inputs);
        if($request->has('makes')){
                $dealerMakes = $dealer->makes()->pluck('make_id')->toArray();
            for($i=0;$i<count($inputs['makes']);$i++){
                if(!in_array($inputs['makes'][$i], $dealerMakes)){

                    $dealer->makes()->attach($inputs['makes'][$i]);
                }

            }
            for($i=0;$i<count($dealerMakes);$i++){
                if(!in_array($dealerMakes[$i],$inputs['makes'])){

                    $dealer->makes()->detach($dealerMakes[$i]);
                }
                
            }
        }
        return response()->json(['status' => true,'message' => 'Profile updated successfully']);
    }

    public function removeListing($dealer_id,$id){
        $vehicle = Vehicle::where('id',$id)->first();
        foreach ($vehicle->bids as $key => $bid) {
            // $bid->delete();
        }
        // $vehicle->delete();
        $id = $dealer_id;
        return response()->json(['status' => true,'message' => 'Vehicle removed successfully']);
    }
}
