<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\v1\Vehicle;
use Carbon\Carbon;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.vehicles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehicle = Vehicle::where('id',$id)
                            ->with([
                                    'seller:id,first_name,last_name',
                                    'bids:vehicle_id,dealer_id,amount,status',
                                    'vehicle_damage_photos:id,vehicle_id,image',
                                    'vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type,image',
                                    'vehicle_interior_photos:id,vehicle_id,vehicle_interior_type,image'
                                ])
                            ->first();
        
        return view('admin.vehicles.details',compact('vehicle'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getVehiclesList(Request $request){
        $skip = (int)$request->get('start');
        $take = (int)$request->get('length');
        $search = $request->get('search')['value'];
        $vehicle = Vehicle::select('id','seller_id','vin_no','year','make','model','starts_at','expired_at','created_at');

        if($search != ''){
            $vehicle->where(function($query) use ($search){
                $query->where('vin_no','like','%'.$search.'%')
                ->orWhere('year','like','%'.$search.'%')
                ->orWhere('model','like','%'.$search.'%')
                ->orWhere('starts_at','like','%'.$search.'%')
                ->orWhere('expired_at','like','%'.$search.'%');
            });
        }

        $vehicle = $vehicle->orderBy('created_at','DESC')->skip($skip)->take($take)->get();
        $count = Vehicle::count();
        if($search != ''){
            $count = Vehicle::where(function($query) use ($search){
                $query->where('vin_no','like','%'.$search.'%')
                ->orWhere('year','like','%'.$search.'%')
                ->orWhere('model','like','%'.$search.'%')
                ->orWhere('starts_at','like','%'.$search.'%')
                ->orWhere('expired_at','like','%'.$search.'%');
            })->count();
        }

        return DataTables::of($vehicle)
        ->addIndexColumn()
        ->addColumn('seller_name', function($vehicle){
            return $vehicle->seller->first_name.' '.$vehicle->seller->last_name;
        })
        ->editColumn('starts_at', function($vehicle){
            if($vehicle->starts_at){
                return date('d-M-Y',strtotime($vehicle->starts_at));
            }
            return '---';
        })
        ->editColumn('expired_at', function($vehicle){
            if($vehicle->expired_at){
                return date('d-M-Y',strtotime($vehicle->expired_at));
            }
            return '---';
        })
        ->addColumn('action', function($vehicle){
            $data = '<a href="'.route('admin.vehicles.show',$vehicle->id).'">View</a>';
            //$data .=  '<a href="javascript:void(0)" class="delete_company" data-action="delete" data-placement="left" data-id="'.$vehicle->id.'"  title="Delete" data-toggle="tooltip"><i class="fa fa-trash iconsetaddbox"></i></a>';

            return $data;
        })
        ->rawColumns(['action'])
        ->order(function ($query) {
            if (request()->has('created_at')) {
                $query->orderBy('created_at', 'DESC');
            }
        })
        ->setTotalRecords($count)
        ->setFilteredRecords($count)
        ->skipPaging()
        ->make(true);
    }
}
