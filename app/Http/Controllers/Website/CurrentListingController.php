<?php

namespace App\Http\Controllers\Website;

use App\Helpers\AppHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v1\Vehicle;
use Carbon\Carbon;
use DB;

class CurrentListingController extends Controller
{
	/** current listing page **/
    public function index(){

		$dealer = auth('dealer')->user();  
		$data['timezone'] 		= $dealer->time_zone->timezone;
		$data['timezone_name'] 	= $dealer->time_zone->timezone_name;
		$appTiming = AppHelper::getCurrentSlotTiming($data['timezone']);

		$data['starts_at'] 		= $appTiming['starts_at_timezone']->format('h:i A');
		$data['ends_at'] 		= $appTiming['ends_at_timezone']->format('h:i A');

        $last_slot_time         = $appTiming['last_slot_start'];
		$data['time_remaining']  = $appTiming['time_remaining'];

        $lat  = $dealer->latitude;
		$long = $dealer->longitude;
		$data['last_wons_bids'] = $dealer->bids()
									// ->where('status',1)
									->whereHas('vehicle',function($query) use ($last_slot_time){
										// $query->where('starts_at',$last_slot_time);
									})
									->get()
									->filter(function ($wonBid) {
										return $wonBid->status == 1;
									})
									->count();

        $data['current_listing'] = Vehicle:://where('starts_at','<',$appTiming['ends_at'])
											// ->where('expired_at','>=',$appTiming['ends_at'])
											with(['vehicle_exterior_photos'=>function($query){
												$query->where('vehicle_exterior_type','front')->select('vehicle_id','vehicle_exterior_type','image');
											},'bids'=>function($query) use ($dealer){
												$query->where('dealer_id',$dealer->id);
											}])
											// ->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
											->withCount('bids')
											->get();

		return view('website.current_listing',compact('data'));
	}
}
