<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v1\Vehicle;
use Notification;
use App\Models\v1\SellerFeedback;
use App\Notifications\SellerFeedbackNotification;
use Mail;
class HomeController extends Controller
{
	/** website home page **/
	public function index(){
	   // echo request()->ip();die();
	   return redirect('https://flipquick.ca');
	    if(request()->ip() == '75.158.175.188' || request()->ip() == '103.251.215.170' || request()->ip() == '27.61.146.240'){
    		return redirect('https://flipquick.ca');
    	}else{
    		return view('welcome');
    	}
	}

	/** website dealer home page **/
	public function dealer_home(){
		$vehicle   = Vehicle::where('id','215')
                                ->first();

            // if($vehicle->bids->count() > 0){
                $token = md5(time());
                $feedback = SellerFeedback::create(['seller_id'=>$vehicle->seller->id,'token'=>$token,'type'=>'highest_bid']);
                $username = $vehicle->seller->first_name.' '.$vehicle->seller->last_name;
                $vehicle->seller->email = 'developer.foremost@gmail.com';
                $vehicle->seller->notify(new SellerFeedbackNotification($feedback,$username));
            // }
            die();
		return view('website.dealer_home');
	}

	/** website aboutus page **/
	public function about_us(){
		return view('website.about_us');
	}

	/** website pricing page **/
	public function pricing(){
		return view('website.pricing');
	}

	/** website pricing page **/
	public function contact_us(){
		return view('website.contact_us');
	}
	/** sending contact us mail **/
	public function contactusMail(Request $request){
		
		Mail::send([], [], function($message) use($request){
		        $message->to(['contact@flipquick.ca'], 'FlipQuick')->subject
		           ('flipquick enquiry');
		        $message->from('contact@flipquick.ca','FlipQuick');
		        $message->setBody('<table>
		        					 <tr>
		        					 	<th>Name</th>
		        					 	<th>'.$request->name.'</th>
		        					 </tr>
		        					  <tr>
		        					 	<th>Phone Number</th>
		        					 	<th>'.$request->phone.'</th>
		        					 </tr>
		        					  <tr>
		        					 	<th>Mail ID</th>
		        					 	<th>'.$request->email.'</th>
		        					 </tr>
		        					 <tr>
		        					 	<th>Subject</th>
		        					 	<th>'.$request->subject.'</th>
		        					 </tr> 
		        					 <tr>
		        					 	<th>Help:</th>
		        					 	<th>'.$request->help.'</th>
		        					 </tr>
		        	             </table>', 'text/html'); 
		     });
		return response()->json(['status' => true,'message' => 'Mail Sent Successfully']);
	}

	/** website pricing page **/
	public function feedback($token){
		if(!$token){

		}
		$feedback = SellerFeedback::where('token',$token)->first();
		return view('website.feedback',compact('feedback'));
	}

	/** website pricing page **/
	public function submitFeedback(Request $request){
		
		$feedback = SellerFeedback::where('token',$request->token)->first();
		$inputs=$request->all();
		unset($inputs['_token']);
		$feedback->update($inputs);
		return view('website.feedback',compact('feedback'));
	}



}
