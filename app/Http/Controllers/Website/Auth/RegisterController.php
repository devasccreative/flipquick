<?php

namespace App\Http\Controllers\Website\Auth;

use App\Models\v1\Dealer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\v1\DealerTransaction;
use App\Models\v1\EmailVerificationDealer;
use App\Models\v1\TimeZone;
use App\Models\v1\Make;
use Notification;
use App\Notifications\VerificationCode;
use App\Notifications\PlanExpired;
use Carbon\Carbon;
use Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/verify_email';
    protected $email;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest:dealer');
    }

    public function showRegistrationForm()
    {
      $timezones = Timezone::all();
      $makes = Make::all();

      return view('website.auth.signup',compact('timezones','makes'));
    }

    public function transaction(Request $request){
      $transactionResponse = $request->all();
      $dealer->deleted_response = json_encode($deleted_response);
      \Mail::raw($dealer->deleted_response, function($message) {
       $message->subject('webhook response')->to('developer.foremost@gmail.com');
     });
      // $dealer->save();
      $dealer = DealerTransaction::where('recurring_account_id',$transactionResponse['billingId'])->first();
      $daeler = DealerTransaction::create(['dealer_id' => $dealer->dealer->id,
                                'recurring_account_id' => $transactionResponse['billingId'],
                                'transaction_id' => $transactionResponse['trnId'],
                                'approved' => $transactionResponse['trnApproved'],
                                'transaction_date' => Carbon::now(),
                                'amount' => $transactionResponse['billingAmount'],
                                'billing_period_start'=>Carbon::createFromFormat('m/d/Y',$transactionResponse['periodFrom']),
                                'billing_period_end'=>Carbon::createFromFormat('m/d/Y',$transactionResponse['periodTo'])
                              ]);
      Dealer::where('id',$dealer->dealer->id)->update(['plan_status'=>$transactionResponse['trnApproved']])
      $dealer->dealer->notify(new PlanExpired());
      return response()->json(['status' => true,'message' => 'Webhook call successfully']);
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
      $dealer = Dealer::where('email',$data['emailregister'])->withTrashed()->whereNotNull('deleted_at')->first();
      $dealerReg = Dealer::where('email',$data['emailregister'])->first();
      $rules  = [
        'first_name'    => 'required | string',
        'last_name'     => 'required | string',
        // 'emailregister' => 'required | string | email | unique:dealers,email',
        'phone'         => 'required | unique:dealers,phone',
        'password'      => 'required | string | min:6',
        'amvic_no'      => 'required',
        'dealership_name'   => 'required',
        'amvic_no'      => 'required',
        'latitude'      => 'required | numeric | not_in:0',
        'longitude'     => 'required | numeric | not_in:0',
        'address'       => 'required',
        'timezone_id'       => 'required',
        'makes'         => 'required|array'
      ];
      $rules['emailregister'] =  'required | string | email | unique:dealers,email';
      if($dealer && !$dealerReg){
        $rules['emailregister'] =  'required | string | email | unique:dealers,email,'.$dealer->id;
      }
      // print_r($dealer);die();
      return Validator::make($data, $rules,[
        'emailregister.unique'=>'Email already exist !'
      ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
      $this->email = $data['emailregister'];

      $dealer = Dealer::create([
        'first_name' => $data['first_name'],
        'last_name'  => $data['last_name'],
        'email'      => $data['emailregister'],
        'phone'      => $data['phone'],
        'country_code'    => '+1',
        'dealership_name' => $data['dealership_name'],
        'latitude'   => $data['latitude'],
        'longitude'  => $data['longitude'],
        'address'    => $data['address'],
        'amvic_no'   => $data['amvic_no'],
        'dealer_no'  => $data['dealer_no'],
        'password'   => Hash::make($data['password']),
        'timezone_id'     => $data['timezone_id'],
      ]);
      $dealer->assignRole('Dealer');
      for($i=0;$i<count($data['makes']);$i++){
        $dealerMakes = $dealer->makes()->pluck('make_id')->toArray();
        if(!in_array($data['makes'][$i], $dealerMakes)){

          $dealer->makes()->attach($data['makes'][$i]);
        }
      }
      $dealer->givePermissionTo(['dashboard', 'submitted_bids','won_bids','submit_bid','subscription_management','notifications','users']);
      $vcode = rand(1000,9999);
      $EmailVerification = EmailVerificationDealer::UpdateOrCreate(['email' => $data['emailregister']],['verification_code' => $vcode]);

      $EmailVerification->notify(new VerificationCode($vcode));
      return $dealer;
    }

    public function resendEmailOtp(Request $request){
      $vcode = rand(1000,9999);
      $EmailVerification = EmailVerificationDealer::UpdateOrCreate(['email' => $request['email']],['verification_code' => $vcode]);
      $EmailVerification->notify(new VerificationCode($vcode));

      return response()->json(['status' => true,'message' => 'Otp has been sent to your email address']);
    }

    /** guard **/
    protected function guard()
    {
      return Auth::guard('dealer');
    }

    /** verify email **/
    public function verify_email(){

      $user = Auth::guard('dealer')->user();
      if($user->email_verified_at){
        return redirect()->route('dealer.dashboard');
      }
      $email = $this->email;
      return view('website.auth.verify_email',compact('email'));

    }

    /** email verification **/
    public function emailVerification(Request $request)
    {
      $user = Auth::guard('dealer')->user();
      $input = $request->all();
      $input['otp'] = $input['otp1'].''.$input['otp2'].''.$input['otp3'].''.$input['otp4'];
      $rules = [
        'email' => 'required ',
        'otp'   => 'required'
      ];


      $validator = Validator::make($input,$rules);

      if ($validator->fails()) {
        return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
      }

      $email        = $request->get('email');
      $vcode        = $input['otp'];

      $checkEmail = EmailVerificationDealer::where(['email'=>$email,'verification_code'=>(int)$vcode])->first();

      if ($checkEmail == "") {
        return response()->json(['status'=>false,'message'=>"Please provide the valid OTP."]);
      }
      $user->email_verified_at = Carbon::now();
      $user->save();

      return response()->json(['status'=>true,'message'=>"Congrats! Thank you for Verification."]);
    }
  }
