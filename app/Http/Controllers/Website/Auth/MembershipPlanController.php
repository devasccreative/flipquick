<?php

namespace App\Http\Controllers\Website\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v1\MembershipPlan;
use App\Models\v1\Admin;
use App\Models\v1\DealerTransaction;
use Carbon\Carbon;
use Validator;
use Auth;
use Notification;
use App\Notifications\ApproveDealer;
use App\Notifications\PlanExpired;

class MembershipPlanController extends Controller
{
	public function __construct()
	{
		$this->merchant_id 	= config('services.bambora.merchant_id'); 
		$this->api_key 		= config('services.bambora.api_key');
		$this->api_version 	= config('services.bambora.api_version');
		$this->platform 		= config('services.bambora.platform'); 
	}
	/** membership plan **/
	public function membership_plan(){
		$dealer = Auth::guard('dealer')->user();

		// if($dealer->membership_plan_id != '' && ($dealer->membership_plan->plan_name != 'Free' || ($dealer->membership_plan->plan_name == 'Free' && Carbon::now()->diffInDays($dealer->created_at) < 30) || $dealer->bambora_id != '' || $dealer->membership_plan->plan_name != '$900')){
		// 	return redirect()->route('website.dealer.payment_info');
		// }
		$message = session('message');
		$membership_plans = ($dealer->membership_plan && Carbon::now()->diffInDays($dealer->membership_plan->created_at) >= 30) ? MembershipPlan::where('id','!=',1)->get() : MembershipPlan::all();

		return view('website.auth.membership_plan',compact('membership_plans'))->with('message',$message);

	}

	/** add membership plan **/
	public function addMembershipPlan(Request $request){

		$dealer = Auth::guard('dealer')->user();
		if($request->get('membership_plan_id') == '1'){
			$dealer->membership_plan_id = $request->membership_plan_id;
			$dealer->save();
			return response()->json(['status'=>true,'message'=>"Membership plan selected.",'url'=>route('website.dealer.payment_info')]);
		}
		// if((((Carbon::now()->diffInDays($dealer->membership_plan->created_at)) >= 30 && $dealer->membership_plan) || $request->has('change_plan') ) && $request->get('membership_plan_id') == 1){
		if(!$dealer->bambora_id){
			$admin = Admin::find(1);
			$admin->email = 'team@flipquick.ca';
			$admin->notify(new ApproveDealer($dealer));
		}
		if($request->get('membership_plan_id') == 1){
			$merchant_id 	= config('services.bambora.merchant_id'); 
			$payment_passcode 	= config('services.bambora.payment_passcode'); 
			$curl = curl_init();
			$transactionAmnt = $dealer->membership_plan->amount;
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://web.na.bambora.com/scripts/process_transaction.asp?requestType=BACKEND&merchant_Id=".$merchant_id."&passcode=".$payment_passcode."&trnType=P&customerCode=".$dealer->bambora_id."&trnAmount=".$transactionAmnt."&trnRecurring=1&rbBillingPeriod=".$dealer->membership_plan->billing_cycle_unit."&rbBillingIncrement=".$dealer->membership_plan->billing_cycle,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_HTTPHEADER => array(
					"Content-Type: application/json",
					"Content-Length: 0"
				),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
			$trnResponse = explode('&',$response);

			if ($err) {
				return response()->json(['status'=>false,'message'=>$err]);
			} 
			
			// if($trnResponse[0] == '1'){
			$dealer->transaction_response = $response;
			$dealer->save();
			// }
			if($dealer->membership_plan->billing_cycle_unit == 'M'){
				$bllingend = Carbon::now()->addDays(30)->format('Y-m-d');
			}else{
				$bllingend = Carbon::now()->addYear()->format('Y-m-d');
			}
			DealerTransaction::create(['dealer_id' => $dealer->id,
				'recurring_account_id' => explode('=',$trnResponse[11])[1],
				'transaction_id' => explode('=',$trnResponse[1])[1],
				'approved' => explode('=',$trnResponse[0])[1],
				'transaction_date' => Carbon::now(),
				'amount' => $transactionAmnt,
				'billing_period_start'=>Carbon::now()->format('Y-m-d'),
				'billing_period_end'=>$bllingend
			]);
			if(explode('=',$trnResponse[0])[1] == '0'){
				return response()->json(['status'=>false,'message'=>explode('&',$trnResponse[0])[1]]);
			}
			$dealer->membership_plan_id = $request->membership_plan_id;
			$dealer->save();
			return response()->json(['status'=>true,'message'=>"Membership plan selected.",'url'=>route('dealer.dashboard')]);

		}else{
		    $merchant_id 	= config('services.bambora.merchant_id'); 
			$api_key 		= config('services.bambora.api_key');
			$api_version 	= config('services.bambora.api_version');
			$platform 		= config('services.bambora.platform'); 
			$payment_passcode 	= config('services.bambora.payment_passcode'); 
			$beanstream = new \Beanstream\Gateway($merchant_id, $api_key, $platform, $api_version); 
// 			$amount = AppHelper::repost_charge();
			if($dealer->bambora_id == ''){
				$profile_create = array(
					'billing' => array(
						'name' => $dealer->first_name.' '.$dealer->last_name,
						'email_address' => $dealer->email,
						'phone_number' => $dealer->phone,
						'address_line1' => 'Canada'
					),
					'card' => array(
						'name' => $request->get('name'),
						'number' => $request->get('number'),
						'expiry_month' => $request->get('expiry_month'),
						'expiry_year' => $request->get('expiry_year'),
						'cvd' => $request->get('cvd')
					)
				);

				$result = $beanstream->profiles()->createProfile($profile_create);
				$dealer->bambora_id = $result;
				$dealer->save();

			} else {
				$profile = $beanstream->profiles()->getProfile($dealer->bambora_id);
				$card_data['card'] =  array(
					'name' => $request->get('name'),
					'expiry_month' => $request->get('expiry_month'),
					'expiry_year' => $request->get('expiry_year'),
					'cvd' => $request->get('cvd')
				);
				if($profile['card']['number'] != ''){
					$beanstream->profiles()->updateCard($dealer->bambora_id, 1, $card_data);
				}else{
					$beanstream->profiles()->addCard($dealer->bambora_id, $card_data);
				}
			}
		}
		$dealer->membership_plan_id = $request->membership_plan_id;
		$dealer->save();
		return response()->json(['status'=>true,'message'=>"Membership plan selected.",'url'=>route('website.dealer.payment_info')]);
	}

	/** payment info **/
	public function payment_info(){
		$dealer = Auth::guard('dealer')->user();

		if($dealer->bambora_id != ''){
			return redirect()->route('dealer.subscription');
		}
		return view('website.auth.payment_info');

	}

	/** add payment info **/
	public function save_card(Request $request){

		$validator = Validator::make($request->all(),[
			'name' 			=> 'required',
			'number'		=> 'required',
			'expiry_date' 	=> 'required|date_format:m/y',
			'cvd'		  	=> 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$dealer = Auth::guard('dealer')->user();
		$merchant_id 	= config('services.bambora.merchant_id'); 
		$api_key 		= config('services.bambora.api_key');
		$api_version 	= config('services.bambora.api_version');
		$platform 		= config('services.bambora.platform'); 
		$payment_passcode 	= config('services.bambora.payment_passcode'); 

		$expiry_date    = explode('/',$request->get('expiry_date'));

		//Create Beanstream Gateway
		$beanstream = new \Beanstream\Gateway($merchant_id, $api_key, $platform, $api_version);

		//Try to submit a ProfileInfo Payment
		try {

			if($dealer->bambora_id == ''){
				$address = explode(',', $dealer->address);
				$profile_create = array(
					'billing' => array(
						'name' => $dealer->first_name.' '.$dealer->last_name,
						'email_address' => $dealer->email,
						'phone_number' => $dealer->phone,
						'address_line1' => $address[count($address)-1]
					),
					'card' => array(
						'name' => $request->get('name'),
						'number' => str_replace(' ', '', $request->get('number')),
						'expiry_month' => $expiry_date[0],
						'expiry_year' => $expiry_date[1],
						'cvd' => $request->get('cvd')
					)
				);

				$result = $beanstream->profiles()->createProfile($profile_create);
				// print_r($result);die();
				$dealer->bambora_id = $result;
				// $passcode_encoded = base64_encode($merchant_id.":".$api_key);
				// $curl = curl_init();
				// curl_setopt_array($curl, array(
				// 	CURLOPT_URL => "https://api.na.bambora.com/v1/profiles",
				// 	CURLOPT_RETURNTRANSFER => true,
				// 	CURLOPT_ENCODING => "",
				// 	CURLOPT_MAXREDIRS => 10,
				// 	CURLOPT_TIMEOUT => 30,
				// 	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				// 	CURLOPT_CUSTOMREQUEST => "POST",
				// 	CURLOPT_POSTFIELDS => json_encode($profile_create),
				// 	CURLOPT_HTTPHEADER => array(
				// 		"Content-Type: application/json",
				// 		"cache-control: no-cache",
				// 		"Authorization: Passcode ".$passcode_encoded,
				// 		"Content-Length: 0"
				// 	),
				// ));

				// $response = curl_exec($curl);
				// $err = curl_error($curl);
				// curl_close($curl);
				$dealer->save();
				// print_r($response);die();

			}
			
			if($dealer->membership_plan->plan_name != 'Free'){
				
				$curl = curl_init();
				$transactionAmnt = $dealer->membership_plan->amount;
				curl_setopt_array($curl, array(
					CURLOPT_URL => "https://web.na.bambora.com/scripts/process_transaction.asp?requestType=BACKEND&merchant_Id=".$merchant_id."&passcode=".$payment_passcode."&trnType=P&customerCode=".$dealer->bambora_id."&trnAmount=".$transactionAmnt."&trnRecurring=1&rbBillingPeriod=".$dealer->membership_plan->billing_cycle_unit."&rbBillingIncrement=".$dealer->membership_plan->billing_cycle,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_HTTPHEADER => array(
						"Content-Type: application/json",
						"Content-Length: 0"
					),
				));

				$response = curl_exec($curl);
				$err = curl_error($curl);
				curl_close($curl);
				$trnResponse = explode('&',$response);

				if ($err) {
					return response()->json(['status'=>false,'message'=>$err]);
				} 
				// if($trnResponse[0] == '1'){
				$dealer->transaction_response = $response;
				$dealer->save();
				// }
				if(explode('=',$trnResponse[0])[1] == '0'){
					return response()->json(['status'=>false,'message'=>explode('=',$trnResponse[0])[1]]);
				}
				if($dealer->membership_plan->billing_cycle_unit == 'M'){
					$bllingend = Carbon::now()->addDays(30)->format('Y-m-d');
				}else{
					$bllingend = Carbon::now()->addYear()->format('Y-m-d');
				}
				DealerTransaction::create(['dealer_id' => $dealer->id,
					'recurring_account_id' => explode('=',$trnResponse[11])[1],
					'transaction_id' => explode('=',$trnResponse[1])[1],
					'approved' => explode('=',$trnResponse[0])[1],
					'transaction_date' => Carbon::now(),
					'amount' => $transactionAmnt,
					'billing_period_start'=>Carbon::now()->format('Y-m-d'),
					'billing_period_end'=>$bllingend
				]);
			}

			return response()->json(['status'=>true,'message'=>'Payment Information added successfully']);

		} catch (\Beanstream\Exception $e) {
			return response()->json(['status'=>false,'message'=>$e->getCode().'--'.$e->getMessage()]);
		}
	}

	/** after admin approve dealer can access account **/
	public function not_approved(){
		$dealer = Auth::guard('dealer')->user();
		if($dealer->is_approved == 1){
			return redirect()->route('dealer.dashboard');
		}
		// if(!$dealer->bambora_id){
			// $admin = Admin::find(1);
			// $admin->email = 'developer.foremost@gmail.com';
			// $admin->notify(new ApproveDealer($dealer));
		// }
		return view('website.auth.not_approved');
	}

	public function plan_status(){
		$dealer = Auth::guard('dealer')->user();
				      $dealer->notify(new PlanExpired());

		if($dealer->plan_status == 1){
			// return redirect()->route('dealer.dashboard');
		}
		return view('website.plan_status',compact('dealer'));
	}
}
