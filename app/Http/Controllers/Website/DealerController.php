<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\v1\ShareVehicle;
use App\Models\v1\TimeZone;
use App\Models\v1\Vehicle;
use App\Models\v1\Make;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Notification;
use App\Notifications\ShareVehicleNotification;
use Validator;
use DB;
use App\Notifications\CurrentListingPostedNotification;

class DealerController extends Controller
{
	/** dealer dashboard page **/
	public function index(Request $request){
		$page   = $request->get('page9am');
		
		$dealer = Auth::guard('dealer')->user();
// 		$dealer->email = "shitalsavaliya1994@gmail.com";
                // $dealer->notify((new CurrentListingPostedNotification(5,'Canada/Mountain')));
		$dealer_id[]  = $dealer->id;
		if($dealer->parent_id == 0){
			$dealer_ids =  $dealer->sub_dealers()->pluck('id')->toArray();
		}else{
			$dealer_idmain[] =  $dealer->parent_id;
			$dealer_ids =  $dealer->main_dealers->sub_dealers()->pluck('id')->toArray();
			$dealer_ids = array_merge($dealer_idmain,$dealer_ids);
		}
		$dealer_id = array_merge($dealer_id,$dealer_ids);

		$data['timezone'] 		= $dealer->time_zone->timezone;
		$data['timezone_name'] 	= $dealer->time_zone->timezone_name;
		$appTiming = AppHelper::both_current_slot_time($dealer->time_zone->timezone);
		$appTimingCurrent = AppHelper::getCurrentSlotTiming($dealer->time_zone->timezone);
		$dealerMakes = $dealer->makes()->pluck('make_id')->toArray();

		// echo "<pre>";
		// print_r($appTiming);die();
		if($dealer->membership_plan && $dealer->membership_plan->plan_name == 'Free' && (Carbon::now()->diffInDays($dealer->created_at)) >= 30){
			return redirect()->route('website.dealer.membership_plan')->with('message', 'Your trial is expired. Please select membership plan to continue.');
		}

		$data['current_listing_9am_slot_starts_at'] 	= $appTiming['nine_am_slot_starts_at']->format('h:i A');
		$data['current_listing_9am_slot_ends_at'] 		= $appTiming['nine_am_slot_expired_at']->format('h:i A');
		$data['four_pm_slot_starts_at'] 	= $appTiming['nine_am_slot_starts_at']->format('h:i A');
		$data['four_pm_slot_expired_at'] 	= $appTiming['nine_am_slot_expired_at']->format('h:i A');
		
		$last_slot_time         			 = $appTimingCurrent['last_slot_start'];
		$data['nine_am_slot_time_remaining'] = $appTiming['nine_am_slot_time_remaining'];
		$data['four_pm_slot_time_remaining'] = $appTiming['four_pm_slot_time_remaining'];

		$lat  = $dealer->latitude;
		$long = $dealer->longitude;
		$data['last_wons_bids'] = $dealer->bids()
										->whereHas('vehicle',function($query) use ($last_slot_time){
											$query->where('starts_at',$last_slot_time);
										})
										->get()
										->filter(function ($wonBid) {
											return $wonBid->status == 1;
										})
										->count();

		$vehicleYears9am  = Vehicle::where('expired_at',$appTiming['expired_at_9am_utc'])
												->where('steps_submitted',8)
												->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
												->whereDoesntHave('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})
												->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
												->whereDoesntHave('seller', function ($query) {
															    $query->where('repo', 1);
															})
												->distinct('year')
												->orderBy('year','asc')
												->pluck('year');

		$vehicleBrands9am  = Vehicle::where('expired_at',$appTiming['expired_at_9am_utc'])
												->where('steps_submitted',8)
												->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
												->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
												->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
												->whereDoesntHave('seller', function ($query) {
															    $query->where('repo', 1);
															})
												->whereDoesntHave('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})->distinct('make')->orderBy('make','asc')->pluck('make');

		$vehicleYearsrepo  = Vehicle::where('expired_at',$appTiming['expired_at_9am_utc'])
												->where('steps_submitted',8)
												->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
												->whereDoesntHave('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})
												->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
												->whereHas('seller', function ($query) {
															    $query->where('repo', 1);
															})
												->distinct('year')
												->orderBy('year','asc')
												->pluck('year');

		$vehicleBrandsrepo  = Vehicle::where('expired_at',$appTiming['expired_at_9am_utc'])
												->where('steps_submitted',8)
												->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
												->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
												->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
												->whereHas('seller', function ($query) {
															    $query->where('repo', 1);
															})
												->whereHas('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})->distinct('make')->orderBy('make','asc')->pluck('make');


		$vehicleYears4pm  = Vehicle::where('expired_at',$appTiming['expired_at_4pm_utc'])
												->where('steps_submitted',8)
												->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
												->whereDoesntHave('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})
												->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
												->whereDoesntHave('seller', function ($query) {
															    $query->where('repo', 1);
															})
												->distinct('year')
												->orderBy('year','asc')
												->pluck('year');

		$vehicleBrands4pm  = Vehicle::where('expired_at',$appTiming['expired_at_4pm_utc'])
												->where('steps_submitted',8)
												->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
												->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
												->whereDoesntHave('seller', function ($query) {
															    $query->where('repo', 1);
															})
												->whereDoesntHave('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})->distinct('make')->orderBy('make','asc')->pluck('make');
	

		$data['current_listing_9am_slot'] = Vehicle::where('expired_at',$appTiming['expired_at_9am_utc'])
												->where('steps_submitted',8)
												->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
												->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
												->with(['vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type as type,image','vehicle_interior_photos:id,vehicle_id,vehicle_interior_type as type,image','vehicle_damage_photos:id,vehicle_id,image'])
												->whereDoesntHave('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})
												->whereDoesntHave('seller', function ($query) {
															    $query->where('repo', 1);
															})
												->withCount('bids');
		$year = $request->year;
				$brand = $request->brand;
$post_type = $request->post_type;
				if($year != ''){
					$data['current_listing_9am_slot']->where('year',$year);
				}
				if($brand != ''){
					$data['current_listing_9am_slot']->where('make',$brand);
				}
				if($brand != ''){
					$data['current_listing_9am_slot']->where('post_type',$post_type);
				}
				
												// ->groupBy('expired_at')
		$data['current_listing_9am_slot'] = $data['current_listing_9am_slot']->paginate(6, ['*'], 'page', $page);


		$page4pm   = $request->get('pagerepo');
		$data['current_listing_repo_slot'] = Vehicle::where('expired_at',$appTiming['expired_at_9am_utc'])
												->where('steps_submitted',8)
												->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
												->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
												->with(['vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type as type,image','vehicle_interior_photos:id,vehicle_id,vehicle_interior_type as type,image','vehicle_damage_photos:id,vehicle_id,image'])
												->whereDoesntHave('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})
												->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
												->whereHas('seller', function ($query) {
															    $query->where('repo', 1);
															})
												->withCount('bids');
		$year = $request->year;
				$brand = $request->brand;

				if($year != ''){
					$data['current_listing_repo_slot']->where('year',$year);
				}
				if($brand != ''){
					$data['current_listing_repo_slot']->where('make',$brand);
				}
				if($post_type != ''){
					$data['current_listing_repo_slot']->where('post_type',$post_type);
				}
				
												// ->groupBy('expired_at')
		$data['current_listing_repo_slot'] = $data['current_listing_repo_slot']->paginate(6, ['*'], 'page', $page);

		$page4pm   = $request->get('page4pm');
		$data['current_listing_4pm_slot'] = Vehicle::where('expired_at',$appTiming['expired_at_4pm_utc'])										
												->where('steps_submitted',8)
												->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$long.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
												->with(['vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type as type,image','vehicle_interior_photos:id,vehicle_id,vehicle_interior_type as type,image','vehicle_damage_photos:id,vehicle_id,image'])
												->whereDoesntHave('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})
												->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
												->whereDoesntHave('seller', function ($query) {
															    $query->where('repo', 1);
															})
												->orderBy('created_at','desc')
												->withCount('bids');
		// $year = $request->year;
				// $brand = $request->brand;

				if($year != ''){
					$data['current_listing_4pm_slot']->where('year',$year);
				}
				if($brand != ''){
					$data['current_listing_4pm_slot']->where('make',$brand);
				}
				if($post_type != ''){
					$data['current_listing_4pm_slot']->where('post_type',$post_type);
				}
				
												// ->groupBy('expired_at')
		$data['current_listing_4pm_slot'] = $data['current_listing_4pm_slot']->paginate(6, ['*'], 'page', $page4pm);
		$lastPage_9am_slot 	= $data['current_listing_9am_slot']->lastPage();        
		$total_9am_slot 	= $data['current_listing_9am_slot']->total(); 
		$lastPage_repo_slot 	= $data['current_listing_repo_slot']->lastPage();        
		$total_repo_slot 	= $data['current_listing_repo_slot']->total();
		$lastPage_4pm_slot 	= $data['current_listing_4pm_slot']->lastPage();        
		$total_4pm_slot 	= $data['current_listing_4pm_slot']->total(); 
		if ($request->ajax() && $request->has('page9am')) {
			if($request->page9am == 1){
		// print_r($data);die();
				// die('test');
				$view = view('website.vehicle_list_9amslot_filter',compact('data','lastPage_9am_slot','total_9am_slot','vehicleYears9am','vehicleBrands9am','year','brand','post_type'))->render();
				return response()->json(['html'=>$view]);
			}
				// die('test1');

			$view = view('website.vehicle_list_9amslot_results',compact('data'))->render();
			return response()->json(['html'=>$view]);
		}
		if ($request->ajax() && $request->has('pagerepo')) {
			if($request->pagerepo == 1){
		// print_r($data);die();
				// die('test');
				$view = view('website.vehicle_list_reposlot_filter',compact('data','lastPage_repo_slot','total_repo_slot','vehicleYearsrepo','vehicleBrandsrepo','year','brand','post_type'))->render();
				return response()->json(['html'=>$view]);
			}
				// die('test1');

			$view = view('website.vehicle_list_reposlot_filter',compact('data'))->render();
			return response()->json(['html'=>$view]);
		}
		if ($request->ajax() && $request->has('page4pm')) {
			if($request->page4pm == 1){
				$view = view('website.vehicle_list_9amslot_filter',compact('data','lastPage_4pm_slot','total_4pm_slot','vehicleYears4pm','vehicleBrands4pm','year','brand','post_type'))->render();
				return response()->json(['html'=>$view]);
			}
			$view = view('website.vehicle_list_4pmslot_results',compact('data'))->render();
			return response()->json(['html'=>$view]);
		}
		

		return view('website.dashboard',compact('data','lastPage_9am_slot','total_9am_slot','lastPage_repo_slot','total_repo_slot','lastPage_4pm_slot','total_4pm_slot','vehicleYears9am','vehicleBrands9am','vehicleYearsrepo','vehicleBrandsrepo','vehicleYears4pm','vehicleBrands4pm'));
	}
	/** benefits **/
	public function benefits(){
		return view('website.benefits');
	}

	/** edit profile page **/
	public function edit_profile(){
		$dealer = auth('dealer')->user();  
		$timezones = Timezone::all();
		$makes = Make::all();
		$dealerMakes = $dealer->makes()->pluck('make_id')->toArray();
		// print_r($dealerMakes);die();
		return view('website.edit_profile',compact('dealer','timezones','makes','dealerMakes'));
	}

	/** update profile info **/
	public function update_profile(Request $request){

		$dealer 	= auth('dealer')->user();  
		$rules =  [
			'first_name'    => 'required | string',
			'last_name'     => 'required | string',
			'email'         => 'required | string | email | unique:dealers,email,'.$dealer->id,
			'phone'         => 'required | unique:dealers,phone,'.$dealer->id,
			'makes'			=> 'required'
		];
		if($dealer->parent_id == 0){
			$rules['dealership_name'] = 'required';
			$rules['amvic_no'] = 'required';
			$rules['dealer_no'] = 'required';
			$rules['latitude'] = 'required';
			$rules['longitude'] = 'required';
			$rules['address'] = 'required';
		}
		$validator 	= Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$inputs = $request->all(); 
		if(isset($inputs['amvic_no']) && $inputs['amvic_no'] != $dealer->amvic_no){
			$inputs['is_approved'] = 0;
		}

		unset($inputs['_token']);
		$dealer->update($inputs);
		if($request->has('makes')){
				$dealerMakes = $dealer->makes()->pluck('make_id')->toArray();
			for($i=0;$i<count($inputs['makes']);$i++){
				if(!in_array($inputs['makes'][$i], $dealerMakes)){

					$dealer->makes()->attach($inputs['makes'][$i]);
				}

			}
			for($i=0;$i<count($dealerMakes);$i++){
				if(!in_array($dealerMakes[$i],$inputs['makes'])){

					$dealer->makes()->detach($dealerMakes[$i]);
				}
				
			}
		}
		return response()->json(['status' => true,'message' => 'Profile updated successfully']);
	}

	/** update password **/
	public function update_password(Request $request){

		$validator = Validator::make($request->all(), [
			'password'    	=> 'required',
			'newPassword'	=> 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$profile = auth('dealer')->user();

		if($request->get('password') != $request->get('newPassword')) {

			if (Hash::check($request->get('password'), $profile->password)) {
				$secret = bcrypt($request->newPassword);
				$updatePassword = $profile->update(['password'=>$secret]);

				if($updatePassword) {
					return response()->json(['status'=>true,'message'=>'Password change successfully.']);
				}

				return response()->json(['status'=>false,'message'=>'Something went wrong.']);
			}

			return response()->json(['status'=>false,'message'=>'Old Password Does not match!']);
		}

		return response()->json(['status'=>false,'message'=>'Both Password are Same!']);
	}

	/** update profile info **/
	public function shareVehicle(Request $request){

		$validator 	= Validator::make($request->all(),['email'         => 'required | email']);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}
		$dealer = auth('dealer')->user();
		$email = $request->get('email');
		$token = md5(time());
		$vehicle = $dealer->shared_vehicle()->create(['vehicle_id'=>$request->get('vehicle_id'),'email'=>$email,'token'=>$token]);
		$link = route('dealer.vehicle_detail',$token);
		$userName = $dealer->first_name.' '.$dealer->last_name;
		$vehicle->notify(new ShareVehicleNotification($link,$userName));

		return response()->json(['status'=>true,'message'=>'Vehicle shared successfully.']);
		// Mail::to($email)->send(new ShareVehicle($token));

	}

	public function vehicle_detail($token){
		$sharedvehicle = ShareVehicle::where('token',$token)->first();
		return view('website.vehicle',compact('sharedvehicle'));
	}
}
