<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v1\VehicleBid;
use App\Models\v1\Vehicle;
use Carbon\Carbon;
use Validator;
use Auth;
use PDF;

class DealerBidsController extends Controller
{
	/** submitted bids **/
	public function submitted_bids(Request $request){
		$page   = $request->get('page');
		$dealer = Auth::guard('dealer')->user();
		$dealer_id[]  = $dealer->id;
		if($dealer->parent_id == 0){
			$dealer_ids =  $dealer->sub_dealers()->pluck('id')->toArray();
		}else{
			$dealer_idmain[] =  $dealer->parent_id;
			$dealer_ids =  $dealer->main_dealers->sub_dealers()->pluck('id')->toArray();
			$dealer_ids = array_merge($dealer_idmain,$dealer_ids);
		}
		$dealer_id = array_merge($dealer_id,$dealer_ids);
		$vehicleYears  = Vehicle::whereHas('bids',function($query) use ($dealer_id){
							$query->whereIn('dealer_id',$dealer_id);
						})->distinct('year')->orderBy('year','asc')->pluck('year');
		$vehicleBrands  = Vehicle::whereHas('bids',function($query) use ($dealer_id){
							$query->whereIn('dealer_id',$dealer_id);
						})->distinct('make')->orderBy('make','asc')->pluck('make');
		$vehicleVins  = Vehicle::whereHas('bids',function($query) use ($dealer_id){
							$query->whereIn('dealer_id',$dealer_id);
						})->distinct('vin_no')->orderBy('vin_no','asc')->pluck('vin_no');

		$bids = VehicleBid::whereIn('dealer_id',$dealer_id)->orderBy('created_at','desc')->paginate(6);
		$lastPage = $bids->lastPage(); 
		if ($request->ajax()) {
			if($request->page != 1){
				$view = view('website.bids_list',compact('bids'))->render();
            	return response()->json(['html'=>$view]);
			}else{
				$year = $request->year;
				$brand = $request->brand;
				$vin_no = $request->vin_no;
				$post_type = $request->post_type;

				$bids = VehicleBid::whereIn('dealer_id',$dealer_id)
								->orderBy('created_at','desc');
				if($year != ''){
					$bids->whereHas('vehicle',function($query) use ($year){
						$query->where('year',$year);
					});
				}
				if($brand != ''){
					$bids->whereHas('vehicle',function($query) use ($brand){
						$query->where('make',$brand);
					});
				}
				if($vin_no != ''){
					$bids->whereHas('vehicle',function($query) use ($vin_no){
						$query->where('vin_no',$vin_no);
					});
				}
				if($post_type != ''){
					$bids->whereHas('vehicle',function($query) use ($post_type){
						$query->where('post_type',$post_type);
					});
				}
						
								
				$bids	= $bids->paginate(6);
				$total = $bids->total();

				$view = view('website.submitted_filter',compact('dealer','bids','lastPage','total','vehicleYears','vehicleBrands','vehicleVins','year','brand','vin_no','post_type'))->render();
            	return response()->json(['html'=>$view]);
			}
            
        }
        $total = $bids->total();
		return view('website.submitted_bids_list',compact('dealer','bids','lastPage','total','vehicleYears','vehicleBrands','vehicleVins'));
	}

	/** WON BIDS **/
	public function won_bids(Request $request){
		$page   	= $request->get('page');
		$skip 		= 0;
		$take = 2;
		if($page > 1){
			$skip = ($page-1) * $take;
		}
		$dealer     = Auth::guard('dealer')->user();
		$dealer_id[]  = $dealer->id;
		if($dealer->parent_id == 0){
			$dealer_ids =  $dealer->sub_dealers()->pluck('id')->toArray();
		}else{
			$dealer_idmain[] =  $dealer->parent_id;
			$dealer_ids =  $dealer->main_dealers->sub_dealers()->pluck('id')->toArray();
			$dealer_ids = array_merge($dealer_idmain,$dealer_ids);
		}
		$dealer_id  = array_merge($dealer_id,$dealer_ids);
		$wonBids 	= VehicleBid::whereIn('dealer_id',$dealer_id)
									->whereHas('vehicle',function($query){
										$query->where('expired_at','<=',Carbon::now());
									});

		$lastPage 	= ceil($wonBids->count() / $take); //$wonBids->lastPage(); 
        $total 	  	= $wonBids->get()
						    ->filter(function ($wonBid) {
							    return $wonBid->status == 1;
							})
							->count(); //$wonBids->total(); 

        $wonBids    = VehicleBid::whereIn('dealer_id',$dealer_id)
									->whereHas('vehicle',function($query){
										$query->where('expired_at','<=',Carbon::now());
									})//->skip($skip)
									//->take($take)
									// ->paginate(6);
									->orderBy('created_at','desc')
									->get()
									->filter(function ($wonBid) {
										return $wonBid->status == 1;
									});
		
		$wonBids->map(function($bids){
							$bids->created_date = Carbon::parse($bids->created_at)->format('d M Y, @ H:i a');
							return $bids;
						});

		$vehicleYears  = Vehicle::whereHas('bids',function($query) use ($dealer_id){
							$query->whereIn('dealer_id',$dealer_id);
						})->distinct('year')->orderBy('year','asc')->pluck('year');
		$vehicleBrands  = Vehicle::whereHas('bids',function($query) use ($dealer_id){
							$query->whereIn('dealer_id',$dealer_id);
						})->distinct('make')->orderBy('make','asc')->pluck('make');
		$vehicleVins  = Vehicle::whereHas('bids',function($query) use ($dealer_id){
							$query->whereIn('dealer_id',$dealer_id);
						})->distinct('vin_no')->orderBy('vin_no','asc')->pluck('vin_no');

		if ($request->ajax()) {
            // $view = view('website.won_bids',compact('wonBids'))->render();
            $year = $request->year;
				$brand = $request->brand;
				$vin_no = $request->vin_no;
								$post_type = $request->post_type;

				 $wonBids    = VehicleBid::whereIn('dealer_id',$dealer_id)
									->whereHas('vehicle',function($query){
										$query->where('expired_at','<=',Carbon::now());
									})//->skip($skip)
									//->take($take)
									// ->paginate(6);
									->orderBy('created_at','desc');
		
				if($year != ''){
					$wonBids->whereHas('vehicle',function($query) use ($year){
						$query->where('year',$year);
					});
				}
				if($brand != ''){
					$wonBids->whereHas('vehicle',function($query) use ($brand){
						$query->where('make',$brand);
					});
				}
				if($vin_no != ''){
					$wonBids->whereHas('vehicle',function($query) use ($vin_no){
						$query->where('vin_no',$vin_no);
					});
				}
				if($post_type != ''){
					$wonBids->whereHas('vehicle',function($query) use ($post_type){
						$query->where('post_type',$post_type);
					});
				}
						
				$wonBids = $wonBids->get()
									->filter(function ($wonBid) {
										return $wonBid->status == 1;
									});
									$total  = $wonBids->count();
            $view = view('website.won_bids_filter',compact('wonBids','lastPage','total','vehicleYears','vehicleBrands','vehicleVins','year','brand','vin_no','post_type'))->render();

            return response()->json(['html'=>$view]);
        }

		return view('website.won_bids_list',compact('wonBids','lastPage','total','vehicleYears','vehicleBrands','vehicleVins'));
	}

	/** view dealer bid detail **/
	public function view_bid_detail($id){
		
		$vehicle = Vehicle::where('id',$id)->first();
		return view('website.vehicle_details',compact('vehicle'));
	}

	/** submit a bid **/
	public function submitABid(Request $request){

		$validator = Validator::make($request->all(),[
			'vehicle_id' 	=> 'required | exists:vehicles,id',
			'amount'		=> 'required | numeric'
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$dealer = auth('dealer')->user(); 
		$inputs = $request->all(); 
		if(!$dealer->bids()->where('vehicle_id',$inputs['vehicle_id'])->first()){
			$dealer->bids()->create([ 'vehicle_id' => $inputs['vehicle_id'],'amount' => $inputs['amount'],'status' => 0 ]);
		}

		return response()->json(['status' => true,'message' => 'Bid submitted successfully']);

	}

	/** save as pdf **/
	public function downloadPdf($id){
		error_reporting(E_ALL ^ E_DEPRECATED);
		$vehicle = Vehicle::where('id',$id)
						->with(['bids'=>function($query){
							$query->where('status',1);
						}])
						->first();
		PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
		$pdf 	 = PDF::loadView('website.vehiclePdf',compact('vehicle'));
		return $pdf->download(date('Y-m-d').'-'.$vehicle->year.'_'.$vehicle->make.'_'.$vehicle->trim.'_'.$vehicle->style.'.pdf');
		// return view('website.vehiclePdf',compact('vehicle'));
	}
}
