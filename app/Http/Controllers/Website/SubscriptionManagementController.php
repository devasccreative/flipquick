<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Validator;
use Auth;

class SubscriptionManagementController extends Controller
{

	public function __construct()
	{
		$this->merchant_id 	= config('services.bambora.merchant_id'); 
		$this->api_key 		= config('services.bambora.api_key');
		$this->api_version 	= config('services.bambora.api_version');
		$this->platform 		= config('services.bambora.platform'); 
	}

	public function index(){
		$dealer = Auth::guard('dealer')->user();

		//Create Beanstream Gateway
		$beanstream = new \Beanstream\Gateway($this->merchant_id, $this->api_key, $this->platform, $this->api_version);
		$profile = $beanstream->profiles()->getProfile($dealer->bambora_id);
		/*
				$payment_passcode 	= config('services.bambora.payment_passcode'); 
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_URL => "https://api.na.bambora.com/v1/payments/".$dealer->transaction_response,
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "GET",
				  CURLOPT_HTTPHEADER => array(
				    "authorization: Passcode ".base64_encode($this->merchant_id.':'.$payment_passcode),
				    "content-type: application/json",
				  ),
				));

				$response = curl_exec($curl);
				$err = curl_error($curl);

				curl_close($curl);

				if ($err) {
				  echo "cURL Error #:" . $err;
				} else {
				  echo $response;
				}
		*/		
		$membership_plan = $dealer->membership_plan;
		// if($membership_plan->plan_name == 'Free'){
		// 	$membership_plan->expiry_date = $dealer->created_at->addDays(30);
		// } else if($membership_plan->plan_name == '$100'){
		// 	$membership_plan->expiry_date = $dealer->created_at->addMonth();
		// } else{
		// 	$membership_plan->expiry_date = $dealer->created_at->addYear();
		// }
		$transaction = $dealer->transactions()->orderBy('created_at','desc')->first();
		$membership_plan->expiry_date = Carbon::parse($transaction->billing_period_end)->format('d M, Y');
		return view('website.subscription_management.edit_profile',compact('profile','membership_plan'));
	}

	public function update_card(Request $request){

		$validator = Validator::make($request->all(),[
			'name' 			=> 'required',
			// 'number'		=> 'required',
			'expiry_date' 	=> 'required|date_format:m/y',
			'cvd'		  	=> 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		try {
			$dealer = Auth::guard('dealer')->user();
			$card_data["card"] = $request->all();
			$expiry_date    = explode('/',$request->get('expiry_date'));
			unset($card_data["card"]['_token']);
			unset($card_data["card"]['expiry_date']);
			$card_data["card"]['expiry_month'] = $expiry_date[0];
			$card_data["card"]['expiry_year'] = $expiry_date[1];

			$beanstream = new \Beanstream\Gateway($this->merchant_id, $this->api_key, $this->platform, $this->api_version);

			$profile = $beanstream->profiles()->getProfile($dealer->bambora_id);

			if($profile['card']['number'] != ''){
				$beanstream->profiles()->updateCard($dealer->bambora_id, 1, $card_data);
			}else{
				$beanstream->profiles()->addCard($dealer->bambora_id, $card_data);
			}

			return response()->json(['status'=>true,'message'=>'Card detail updated successfully']);

		} catch (\Beanstream\Exception $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}

	}

	public function delete_card(Request $request){
		try {
			$dealer = Auth::guard('dealer')->user();
			$beanstream = new \Beanstream\Gateway($this->merchant_id, $this->api_key, $this->platform, $this->api_version);
			$beanstream->profiles()->deleteCard($dealer->bambora_id,1);
			return response()->json(['status'=>true,'message'=>'Card deleted successfully']);
		} catch (\Beanstream\Exception $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}
	}

	public function update_subscription_profile(Request $request){

		$validator 	= Validator::make($request->all(), [
			'name'    			=> 'required',
			'email_address'     => 'required | email',
			'phone_number'      => 'required | numeric',
			'address_line1'     => 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		try {

			$dealer = Auth::guard('dealer')->user();
			$profile_data['billing'] = $request->all();
			unset($profile_data['billing']['_token']);
			$beanstream = new \Beanstream\Gateway($this->merchant_id, $this->api_key, $this->platform, $this->api_version);
			$profile = $beanstream->profiles()->updateProfile($dealer->bambora_id,$profile_data);
			return response()->json(['status'=>true,'message'=>'Profile updated successfully']);
		} catch (\Beanstream\Exception $e) {
			return response()->json(['status'=>false,'message'=>$e->getMessage()]);
		}

	}

	/** cancel subscription **/
	public function cancel_subscription(Request $request){

		$dealer = Auth::guard('dealer')->user();
		if($dealer->membership_plan_id != 1){
			$merchant_id 	= config('services.bambora.merchant_id'); 
			$recurring_passcode 	= config('services.bambora.recurring_passcode'); 
			$transaction_response   = explode('&',$dealer->transaction_response);
			$rbAccountId 			= explode('=',$transaction_response[11])[1];
			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://web.na.bambora.com/scripts/recurring_billing.asp?serviceVersion=1.0&operationType=C&merchantId=".$merchant_id."&passCode=".$recurring_passcode."&rbAccountId=".$rbAccountId,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_HTTPHEADER => array(
					"Content-Type: application/json",
					"Content-Length: 0"
				),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
			$trnResponse = explode('&',$response);
			if ($err) {
				return response()->json(['status'=>false,'message'=>$err]);
			} 
			
			// if($trnResponse[0] == '1'){
				$dealer->deleted_response = $response;
				$dealer->save();
			// }
			if($trnResponse[0] == '0'){
				return response()->json(['status'=>false,'message'=>explode('&',$trnResponse[0])[1]]);
			}
			$dealer->delete();
			return response()->json(['status'=>true,'message'=>"Membership plan cancelled successfully."]);

		}else{
			$dealer->delete();
			return response()->json(['status'=>true,'message'=>"Membership plan cancelled successfully."]);
		}
	}
}
