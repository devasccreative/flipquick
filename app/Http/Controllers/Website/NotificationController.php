<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Notification;
use Auth;

class NotificationController extends Controller
{
    public function index(Request $request){

    	$page 					= $request->get('page');
		$notifications 			= Auth::user()
									->notifications()
									->whereIn('type',[
										'App\Notifications\CurrentListingPostedNotification',
										'App\Notifications\WonsbidNotification'
									])
									->paginate(6);

		$notifications->getCollection()->transform(function($notification,$index){
			
			$data 	= $notification->data;

			switch($notification['type']) {

				case 'App\Notifications\CurrentListingPostedNotification':
				$notification['title']            = 'New Listing Posted';
				$notification['message']          = 'Hurry Up, '.$notification['data']['available_vehicle'].' new listings are posted today. Submit your bids before the listings goes away.';
				$notification['tag']              = 'CurrentListingPosted';
				break;

				case 'App\Notifications\WonsbidNotification':
				$notification['title']            = 'Won bid';
				$notification['message']          = 'You won a bid';
				$notification['tag']              = 'WonAbid';
				break;

			}

			return $notification;

		});

		$notificationsData 		= Auth::user()
									->notifications()
									->whereIn('type',[
										'App\Notifications\CurrentListingPostedNotification',
										'App\Notifications\WonsbidNotification'
									])
									->paginate(6);
		$notificationsData->getCollection()->transform(function($notificationsData,$index){
			$notificationsData->markAsRead();
		});

		if ($request->ajax()) {
            $view = view('website.notificationResults',compact('notifications'))->render();
            return response()->json(['html'=>$view]);
        }
		$lastPage = $notifications->lastPage();
		return view('website.notification',compact('notifications','lastPage'));
    }
}
