<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Models\v1\Dealer;
use Carbon\Carbon;
use DataTables;
use Validator;
use Notification;
use App\Notifications\GenerateDealerPassword;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('website.users.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::where('guard_name','dealer')->get();
        return view('website.users.add',compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dealer = auth('dealer')->user();  
        $input = $request->all();
        $validator = Validator::make($input,[
            'first_name'    => 'required | string',
            'last_name'     => 'required | string',
            'email'         => 'required | string | email | unique:dealers,email',
            'phone'         => 'required | unique:dealers,phone',
            // 'latitude'      => 'required | numeric | not_in:0',
            // 'longitude'     => 'required | numeric | not_in:0',
            // 'address'       => 'required',
            // 'permission'    => 'required | array',
            'role'          => 'required',
        ],[
            'email.unique'=>'Email already exist !'
        ]);

        if ($validator->fails()){ 
            return response()->json(['status'=>false,'message'=>$validator->messages()->first(),'maintenance_mode'  => false]);
        }

        $password = $request->email;
        $input['password']   = bcrypt($password); 
        $input['country_code']   = '+1';
        $input['parent_id']  = $dealer->id;
        $input['latitude']   = $dealer->latitude;
        $input['longitude']  = $dealer->longitude;
        $input['address']    = $dealer->address;
        $input['timezone_id']    = $dealer->timezone_id;
        $input['membership_plan_id']    = $dealer->membership_plan_id;
        $input['is_approved']    = $dealer->is_approved;
        $input['dealer_no']      = $dealer->dealer_no;
        $input['dealership_name'] = $dealer->dealership_name;
        $input['email_verified_at']  = Carbon::now()->toDateTimeString(); 
        
        $role = Role::updateOrCreate([
            'name'      =>  $input['role'],
            'guard_name'=> 'dealer'],[
                'name'      =>  $input['role'],
                'guard_name'=> 'dealer',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        unset($input['role']);
        unset($input['permission']);
        $dealerCreated  = Dealer::create($input);
        $dealerCreated->assignRole($role->name);
        $dealerCreated->givePermissionTo(['dashboard', 'submitted_bids','won_bids','submit_bid','notifications']);
        $token = app('auth.password.broker')->createToken($dealerCreated);
        $userName = $dealer->first_name.' '.$dealer->last_name;
        $dealerCreated->notify(new GenerateDealerPassword($token,$userName));
        return response()->json(['status'=>true,'message'=>$role->name.' added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $dealer = Dealer::find($id);
     $permissions = Permission::where('guard_name','dealer')->get();
     // $token = app('auth.password.broker')->createToken($dealer);
        // $userName = $dealer->first_name.' '.$dealer->last_name;
        // $dealer->notify(new GenerateDealerPassword($token,$userName));

     return view('website.users.edit',compact('dealer','permissions'));
 }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dealer = auth('dealer')->user();  
        $input = $request->all();
        $validator = Validator::make($input,[
            'first_name'    => 'required | string',
            'last_name'     => 'required | string',
            'email'         => 'required | string | email | unique:dealers,email,'.$id,
            'phone'         => 'required | unique:dealers,phone,'.$id,
            // 'latitude'      => 'required | numeric | not_in:0',
            // 'longitude'     => 'required | numeric | not_in:0',
            // 'address'       => 'required',
            // 'permission'    => 'required | array',
            'role'          => 'required',
        ],[
            'email.unique'=>'Email already exist !'
        ]);

        if ($validator->fails()){ 
            return response()->json(['status'=>false,'message'=>$validator->messages()->first(),'maintenance_mode'  => false]);
        }

        $password = $request->email;
        $input['password']   = bcrypt($password); 
        $input['country_code']   = '+1';
        $input['parent_id']   = $dealer->id;
        $input['email_verified_at']  = Carbon::now()->toDateTimeString(); 
        $role = Role::updateOrCreate([
            'name'      =>  $input['role'],
            'guard_name'=> 'dealer'],[
                'name'      =>  $input['role'],
                'guard_name'=> 'dealer',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        unset($input['role']);
        unset($input['permission']);
        $dealerCreated  = Dealer::find($id);
        $dealerCreated->update($input);
        // $dealerCreated->removeRole($dealerCreated->getRoleNames());
        $dealerCreated->assignRole($role->name);
        // $dealerCreated->syncPermissions($request->get('permission'));
        return response()->json(['status'=>true,'message'=>$role->name.' updated successfully']);
            //syncPermissions
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dealer  = Dealer::find($id);
        $dealer->delete();
        return response()->json(['status'=>true,'message'=>"User deleted successfully."]);
    }

    public function getUsersList(Request $request){
        $dealer = auth('dealer')->user();  
        $skip = (int)$request->get('start');
        $take = (int)$request->get('length');
        $search = $request->get('search')['value'];
        $dealers = Dealer::select('id','first_name','last_name','email','country_code','phone','is_approved')
        ->where('parent_id',$dealer->id);

        if($search != ''){
            $dealers->where(function($query) use ($search){
                $query->where('first_name','like','%'.$search.'%')
                ->orWhere('last_name','like','%'.$search.'%')
                ->orWhere('phone','like','%'.$search.'%')
                ->orWhere('email','like','%'.$search.'%');
            });
        }

        $dealers = $dealers->orderBy('created_at','DESC')->skip($skip)->take($take)->get();
        $count = Dealer::where('parent_id',$dealer->id)->count();
        if($search != ''){
            $count = Dealer::where(function($query) use ($search){
                $query->where('first_name','like','%'.$search.'%')
                ->orWhere('last_name','like','%'.$search.'%')
                ->orWhere('phone','like','%'.$search.'%')
                ->orWhere('email','like','%'.$search.'%');
            })->where('parent_id',$dealer->id)->count();
        }

        return DataTables::of($dealers)
        ->addIndexColumn()
        ->editColumn('created_at', function($dealer){
            return Carbon::parse($dealer->created_at)->format('d-M-Y @ h:i A');
        })
        ->editColumn('name', function($dealer){
            return $dealer->first_name.' '.$dealer->last_name;
        })
        ->editColumn('phone', function($dealer){
            return $dealer->country_code.'-'.$dealer->phone;
        })
        ->addColumn('action', function($dealer){
            // $data =  '<a href="'.route('website.dealer.dashboard',$dealer->id).'">View</a>';
            return '<a href="'.route("dealer.users.edit",$dealer->id).'" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;&nbsp;<a href="javascript:void(0)" class="delete_dealer" data-toggle="confirmation" data-title="Are you sure?" onclick="delete_dealer('.$dealer->id.')" data-toggle="tooltip" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>';
        })
        ->rawColumns(['action'])
        ->order(function ($query) {
            if (request()->has('created_at')) {
                $query->orderBy('created_at', 'DESC');
            }
        })
        ->setTotalRecords($count)
        ->setFilteredRecords($count)
        ->skipPaging()
        ->make(true);
    }
}
