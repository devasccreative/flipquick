<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class GeneralController extends Controller
{
    public function addDefaultRolePermissions(){

    	Role::insert([
				'name'		 => 'Dealer',
				'guard_name' => 'dealer',
				'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
			],[
				'name'		 => 'User',
				'guard_name' => 'dealer',
				'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
			]);

    	Permission::insert([
			[
				'name'		 => 'dashboard',
				'guard_name' => 'dealer',
				'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
			],[
				'name'		 => 'submitted_bids',
				'guard_name' => 'dealer',
				'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
			],[
				'name'		 => 'won_bids',
				'guard_name' => 'dealer',
				'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
			],[
				'name'		 => 'submit_bid',
				'guard_name' => 'dealer',
				'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
			],[
				'name'		 => 'subscription_management',
				'guard_name' => 'dealer',
				'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
			],[
				'name'		 => 'notifications',
				'guard_name' => 'dealer',
				'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
			],[
				'name'		 => 'users',
				'guard_name' => 'dealer',
				'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
			]
		]);

		return response()->json(array('status'=>true,'message'=> 'Permission Created'));
    }
}
