<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\v1\VehicleExteriorPlaceholderImage;
use App\Models\v1\VehicleInteriorPlaceholderImage;
use App\Models\v1\VehicleExteriorPhoto;
use App\Models\v1\VehicleInteriorPhoto;
use App\Models\v1\VehicleDamagePhoto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\v1\Vehicle;
use App\Models\v1\Dealer;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Validator;
use Storage;
use Image;
use Auth;
use File;
use DB;
use Notification;
use App\Notifications\WonsbidNotification;
use App\Notifications\CurrentListingPostedNotification;
use App\Notifications\SellerListingExpiredNotification;
use App\Helpers\NotificationHelper;

class SellerVehicleController extends Controller
{

	/** vin exist **/
	public function isVinExists(Request $request)
	{

		$seller = auth('api')->user();    
		$validator = Validator::make($request->all(),[
							'vin_no' => "required|unique:vehicles,vin_no",
						],[
							'vin_no.unique'=>'This VIN has already been used.'
						]);

		if ($validator->fails()) { 
			$vehicle = $seller->vehicles()->where('vin_no',$request->vin_no)->first();
			$current_time = Carbon::now();
			$data['vehicle_id'] = $vehicle->id;
			if($vehicle && $vehicle->steps_submitted != '8'){
				$data['paid_repost'] = false;
				$data['steps_submitted'] = $vehicle->steps_submitted;
				return response()->json(['status'=>true,'message'=>'This VIN has already been used.','data'=>$data]);            
			}
			$data['steps_submitted'] = 8;
			if($vehicle && $vehicle->steps_submitted == '8' && Carbon::parse($vehicle->expired_at)->gt($current_time) ){
				return response()->json(['status'=>false,'message'=>'You cann\'t repost before expiration time']);            
			}
			if($vehicle && $vehicle->steps_submitted == '8' && Carbon::parse($vehicle->expired_at)->lt($current_time) ){
				$data['paid_repost'] = true;
				if($current_time->diffInDays($vehicle->starts_at) >= 15){
					$data['paid_repost'] = false;
				}
				return response()->json(['status'=>true,'message'=>'Repost vehicle','data'=>$data]);            
			}

		
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		return response()->json(['status'=>true,'message'=>'You can use this vin number']);            
	}

	/** get Available Dealers Count **/
	public function getAvailableDealersCount(Request $request){

		$validator = Validator::make($request->all(),[
							'latitude' 		=> 'required | numeric',
							'longitude'		=> 'required | numeric',
							'distance'		=> 'required',
						]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$lat = $request->get('latitude');
		$lon = $request->get('longitude');
		$distance = $request->get('distance');
		$availableDealersCount = Dealer::select("id","latitude","longitude",DB::raw("6371 * acos(cos(radians(" . $lat . ")) * cos(radians(latitude))  * cos(radians(longitude) - radians(" . $lon . ")) + sin(radians(" .$lat. ")) * sin(radians(latitude))) AS distance"))
										->whereRaw(DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )').' < ?',[$distance]);
		$data['count'] = $availableDealersCount->count();

		$data['dealer'] = $availableDealersCount->get();

		return response()->json(['status'=>true,'message'=>'Available Dealers Count retrieved successfully','data'=>$data]);            

	}

	/** add seller vehicle detail step1 **/
	public function addVehicleStep1(Request $request)
	{

		$validator = Validator::make($request->all(),[
								'vin_no' 			=> 'required | unique:vehicles,vin_no,'.$request->get('vehicle_id'),
								'year' 				=> 'required',
								'make' 				=> 'required',
								'model' 			=> 'required',
								'trim' 				=> 'required',
								'milage' 			=> 'required | integer',
								'style' 			=> 'nullable',
								'body_shape' 		=> 'required',
								'exterior_color' 	=> 'required',
								'interior_color' 	=> 'required',
								'doors' 			=> 'required | integer',
								'passengers' 		=> 'required',
								'drive_train' 		=> 'required',
								'transmission'      => 'required',
								'vehicle_id'        => 'nullable',
								'engine'			=> 'required',
								'made_in'			=> 'required',
								'repost' 			=> 'required | boolean',
							]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$seller = auth('api')->user();    
		$inputs = $request->all();
		$inputs['seller_id'] = $seller->id;
		if(!$inputs['repost']){
			$inputs['steps_submitted'] = 1;
		}
		unset($inputs['repost']);
		if(!isset($inputs['vehicle_id'])){
			$vehicle = Vehicle::create($inputs);
		}else{
			unset($inputs['vehicle_id']);
			$vehicle = Vehicle::updateOrCreate(['id'=>(int)$request->get('vehicle_id')],$inputs);
		}
		return response()->json(['status' => true,'message' => 'Vehicle detail added successfully','data'=>$vehicle->id]);

	}

	/** add seller vehicle detail step2 **/
	public function addVehicleStep2(Request $request)
	{

		$validator = Validator::make($request->all(),[
									'vehicle_id'		=> 'required | exists:vehicles,id',
									'exterior_photos'	=> 'required | array',
									'exterior_photos.*'	=> 'image|mimes:jpeg,png,jpg,gif,svg',
									'repost' 			=> 'required | boolean',
								]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$seller = auth('api')->user();    
		$inputs = $request->all();
		if(!$inputs['repost']){
			Vehicle::where('id',$inputs['vehicle_id'])->update(['steps_submitted'=>2]);
		}
		$vehicle 		= Vehicle::find($inputs['vehicle_id']);
		$exterior_photos = $inputs['exterior_photos'];
		ini_set('upload_max_filesize', '50M');
		ini_set('post_max_size', '50M');
		ini_set('max_input_time', 300);
		ini_set('max_execution_time', 300);

		if($request->hasFile('exterior_photos')){
			foreach ($exterior_photos as $key => $exterior_photo) {

				switch ($key) {
					case 'front':
						$orderid = 1;
						break;

					case 'front_driver_corner':
						$orderid = 2;
						break;

					case 'driver_side':
						$orderid = 3;
						break;

					case 'rear_driver_corner':
						$orderid = 4;
						break;

					case 'rear':
						$orderid = 5;
						break;

					case 'rear_passanger':
						$orderid = 6;
						break;

					case 'passanger_side':
						$orderid = 7;
						break;

					case 'front_passanger_corner':
						$orderid = 8;
						break;
					
					default:
						$orderid = 0;
						break;
				}

				$exterior_photo_obj = [];
				$picName 		= $exterior_photo->hashName();

				$img 			= Image::make($exterior_photo);
				Storage::disk('public')->put('exterior_photos/'.$vehicle->id.'/',$exterior_photo);

				$exterior_photo_obj['vehicle_id']        		= $vehicle->id;
				$exterior_photo_obj['vehicle_exterior_type']    = is_numeric($key) ? 'other' : $key;
				$exterior_photo_obj['image']          = $picName;
				$exterior_photo_obj['orderid']        = $orderid;

				if(!is_numeric($key)){
					$vehicle->vehicle_exterior_photos()->updateOrCreate(['vehicle_exterior_type'=>$key],$exterior_photo_obj);
				}else{
					$vehicle->vehicle_exterior_photos()->create($exterior_photo_obj);
				}

			}
		}
		
		return response()->json(['status' => true,'message' => 'Exterior photos added successfully','data'=>$vehicle->id]);

	}

	/** add seller vehicle detail step3 **/
	public function addVehicleStep3(Request $request)
	{

		$validator = Validator::make($request->all(),[
									'vehicle_id'		=> 'required | exists:vehicles,id',
									'interior_photos'	=> 'required | array',
									'interior_photos.*'	=> 'image|mimes:jpeg,png,jpg,gif,svg',
									'repost' 			=> 'required | boolean',
								]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$inputs = $request->all();
		$vehicle 		= Vehicle::find($inputs['vehicle_id']);
		$interior_photos = $inputs['interior_photos'];
		if(!$inputs['repost']){
			Vehicle::where('id',$inputs['vehicle_id'])->update(['steps_submitted'=>3]);
		}
		ini_set('upload_max_filesize', '50M');
		ini_set('post_max_size', '50M');
		ini_set('max_input_time', 300);
		ini_set('max_execution_time', 300);
		if($request->hasFile('interior_photos')){
			foreach ($interior_photos as $key => $interior_photo) {

				$interior_photo_obj = [];
				$picName 		= $interior_photo->hashName();

				$img 			= Image::make($interior_photo);
				Storage::disk('public')->put('interior_photos/'.$vehicle->id.'/',$interior_photo);

				$interior_photo_obj['']        		= $vehicle->id;
				$interior_photo_obj['vehicle_interior_type']    = is_numeric($key) ? 'other' : $key;
				$interior_photo_obj['image']          = $picName;

				if(!is_numeric($key)){
					$vehicle->vehicle_interior_photos()->updateOrCreate(['vehicle_interior_type'=>$key],$interior_photo_obj);
				}else{
					$vehicle->vehicle_interior_photos()->create($interior_photo_obj);
				}

			}
		}

		return response()->json(['status' => true,'message' => 'Interior photos added successfully','data'=>$vehicle->id]);

	}

	/** add seller vehicle detail step4 **/
	public function addVehicleStep4(Request $request)
	{

		$validator = Validator::make($request->all(),[
									'vehicle_id'	=> 'required | exists:vehicles,id',
									'damage_photo'	=> 'required | array',
									'damage_photo.*'=> 'image|mimes:jpeg,png,jpg,gif,svg',
									'repost' 			=> 'required | boolean',
								]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$inputs = $request->all();
		$damage_photos   = $inputs['damage_photo'];
		$vehicle 		= Vehicle::find($inputs['vehicle_id']);
		if(!$inputs['repost']){
			Vehicle::where('id',$inputs['vehicle_id'])->update(['steps_submitted'=>4]);
		}
		ini_set('upload_max_filesize', '50M');
		ini_set('post_max_size', '50M');
		ini_set('max_input_time', 300);
		ini_set('max_execution_time', 300);
		if($request->hasFile('damage_photo')){
			foreach ($damage_photos as $key => $damage_photo) {

				$damage_photo_obj = [];
				$picName 		= $damage_photo->hashName();

				$img 			= Image::make($damage_photo);
				Storage::disk('public')->put('damage_photos/'.$vehicle->id.'/',$damage_photo);

				$damage_photo_obj['vehicle_id']     = $vehicle->id;
				$damage_photo_obj['image']          = $picName;

				$vehicle->vehicle_damage_photos()->create($damage_photo_obj);
			}

		}

		return response()->json(['status' => true,'message' => 'Damage photos added successfully','data'=>$vehicle->id]);

	}

	/** add seller vehicle detail step5 **/
	public function addVehicleStep5(Request $request)
	{

		$validator = Validator::make($request->all(),[
									'vehicle_id'		=> 'required | exists:vehicles,id',
									'air_conditioning' 	=> 'required | boolean',
									'navigation_system' => 'required | boolean',
									'leather_seats' 	=> 'required | boolean',
									'sun_roof' 			=> 'required | boolean',
									'panoramic_roof' 	=> 'required | boolean',
									'dvd' 				=> 'required | boolean',
									'heated_seats' 		=> 'required | boolean',
									'ac_seats' 			=> 'required | boolean',
									'360_camera' 		=> 'required | boolean',
									'repost' 			=> 'required | boolean',
								]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$seller = auth('api')->user();    
		$inputs = $request->all();
		$vehicle_id = $request->get('vehicle_id');
		unset($inputs['vehicle_id']);
		if(!$inputs['repost']){
			$inputs['steps_submitted'] = 5;
		}
		unset($inputs['repost']);
		Vehicle::where('id',$vehicle_id)->update($inputs);

		$vehicle = Vehicle::where('id',$vehicle_id)->select('air_conditioning','navigation_system','leather_seats','sun_roof','panoramic_roof','dvd','heated_seats','ac_seats','360_camera')->first();

		return response()->json(['status' => true,'message' => 'Vehicle detail added successfully','data'=>(int)$vehicle_id]);

	}

	/** add seller vehicle detail step6 **/
	public function addVehicleStep6(Request $request)
	{

		$validator = Validator::make($request->all(),[
								'vehicle_id'					=> 'required | exists:vehicles,id',
								'after_market_equipment' 		=> 'nullable',
								'original_owner' 				=> 'required',
								'still_making_monthly_payment' 	=> 'required | boolean',
								'still_own' 					=> 'required_if:still_making_monthly_payment,1',
								'have_any_accident_record' 		=> 'required | boolean',
								'how_much_records' 				=> 'required_if:have_any_accident_record,1',
								'modified_exhaust' 				=> 'required | boolean',
								'wantto_sell_or_tradein' 		=> 'required | in:sell,trade',
								'repost' 						=> 'required | boolean',
							],[
								'wantto_sell_or_tradein.required' => 'Trade/Sell Field Is Required'
							]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$seller = auth('api')->user();    
		$inputs = $request->all();

		$vehicle_id = $request->get('vehicle_id');
		unset($inputs['vehicle_id']);
		if(!$inputs['repost']){
			$inputs['steps_submitted'] = 6;
		}
		unset($inputs['repost']);
		Vehicle::where('id',$vehicle_id)->update($inputs);
		$vehicle = Vehicle::where('id',$vehicle_id)->select('after_market_equipment','original_owner','still_making_monthly_payment','still_own','have_any_accident_record','how_much_records')->first();

		return response()->json(['status' => true,'message' => 'Vehicle detail added successfully','data'=>(int)$vehicle_id]);

	}

	/** add seller vehicle detail step7 **/
	public function addVehicleStep7(Request $request)
	{

		$validator = Validator::make($request->all(),[
									'vehicle_id'				=> 'required | exists:vehicles,id',
									'smoked_in' 				=> 'required | boolean',
									'vehicle_condition' 		=> 'required | integer | min:0 | digits_between: 0,10',
									'front_tires_condition' 	=> 'required | in:bad,average,excellent',
									'back_tires_condition'  	=> 'required | in:bad,average,excellent',
									'warning_lights_in_cluster' => 'required | boolean',
									'warning_lights' 			=> 'nullable',
									'have_mechanical_issues' 	=> 'required | boolean',
									'mechanical_issues' 		=> 'nullable',
									'repost' 					=> 'required | boolean',
								]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$seller = auth('api')->user();    
		$inputs = $request->all();

		$vehicle_id = $request->get('vehicle_id');
		unset($inputs['vehicle_id']);
		if(!$inputs['repost']){
			$inputs['steps_submitted'] = 7;
		}
		unset($inputs['repost']);
		Vehicle::where('id',$vehicle_id)->update($inputs);
		$vehicle = Vehicle::where('id',$vehicle_id)
						->select('smoked_in','vehicle_condition','front_tires_condition','back_tires_condition','warning_lights','have_mechanical_issues','mechanical_issues')
						->first();

		return response()->json(['status' => true,'message' => 'Vehicle detail added successfully','data'=>(int)$vehicle_id]);

	}

	/** add seller vehicle detail step8 **/
	public function addVehicleStep8(Request $request)
	{

		$validator = Validator::make($request->all(),[
									'vehicle_id'	=> 'required | exists:vehicles,id',
									'latitude' 		=> 'required | numeric',
									'longitude'		=> 'required | numeric',
									'address'		=> 'required',
									'distance'		=> 'required',
									'repost' 		=> 'required | boolean',
								]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$seller = auth('api')->user();    
		$inputs = $request->all();
		if(!$inputs['repost']){
			$inputs['steps_submitted'] = 8;
		}
		$appTiming = AppHelper::getNextSlotTiming($seller->time_zone->timezone,$seller->time_zone->timezone_name);

		$inputs['starts_at'] 	= $appTiming['starts_at'];
		$inputs['expired_at'] 	= $appTiming['ends_at'];
		if((Carbon::now($seller->time_zone->timezone_name) > Carbon::now($seller->time_zone->timezone_name)->setTimeFromTimeString('09:00:00') && Carbon::now($seller->time_zone->timezone_name)->dayName == 'Saturday') || (Carbon::now($seller->time_zone->timezone_name) < Carbon::now($seller->time_zone->timezone_name)->setTimeFromTimeString('09:00:00') && Carbon::now($seller->time_zone->timezone_name)->dayName == 'Sunday')){
			$inputs['expired_at']->addHours(24);
		}
		$vehicle_id = $request->get('vehicle_id');
		unset($inputs['repost']);
		unset($inputs['vehicle_id']);

		Vehicle::where('id',$vehicle_id)->update($inputs);

		return response()->json(['status' => true,'message' => 'Vehicle detail added successfully','data'=>(int)$vehicle_id]);

	}

	/** delelete seller vehicle detail **/
	public function deleteVehicle(Request $request)
	{

		$validator = Validator::make($request->all(),[
			'vehicle_id'	=> 'required | exists:vehicles,id',
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$seller = auth('api')->user();    
		$vehicle = $seller->vehicles()->where('id',$request->get('vehicle_id'))->first();

		if(!$vehicle || $vehicle->bids->count() > 0){
			return response()->json(['status'=>false,'message'=>'You can\'t delete this vehicle.']);
		}
		Storage::disk('public')->deleteDirectory('damage_photos/'.$vehicle->id.'/');
		Storage::disk('public')->deleteDirectory('exterior_photos/'.$vehicle->id.'/');
		Storage::disk('public')->deleteDirectory('interior_photos/'.$vehicle->id.'/');
		$vehicle->vehicle_damage_photos()->delete();
		$vehicle->vehicle_exterior_photos()->delete();
		$vehicle->vehicle_interior_photos()->delete();
		$vehicle->delete();

		return response()->json(['status' => true,'message' => 'Vehicle detail added successfully']);

	}

	/** get Vehicle Information **/
	public function getVehicleInfo(Request $request){

		$validator = Validator::make($request->all(),[
									'listing_category '	=> 'in:current,past',
								]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}
		$seller = auth('api')->user();  
		// $current_time 	= Carbon::now($seller->time_zone->timezone);
	
		$appTiming = AppHelper::getNextSlotTiming($seller->time_zone->timezone,$seller->time_zone->timezone_name);
		$appCurrentTiming = AppHelper::getCurrentSlotTiming($seller->time_zone->timezone,$seller->time_zone->timezone_name);

		$data['timezone'] 		= $seller->time_zone->timezone;
		$data['timezone_name'] 	= $seller->time_zone->timezone_name;
		
		$vehicleDetails    	= Vehicle::select('id','vin_no','starts_at','expired_at','created_at','steps_submitted','repost_at','latitude','longitude','distance','model','year','make')
										->withCount('bids')
										// ->where('starts_at',$appTiming['starts_at'])
										->where('seller_id',$seller->id)
										->where('steps_submitted',8)
										->with(['latest_bid:id,vehicle_id,amount','vehicle_exterior_photos'=>function($query){
											$query->where('vehicle_exterior_type','front_driver_corner')->select('id','vehicle_id','vehicle_exterior_type','image');
										}]);

		if($request->has('listing_category')){
			if($request->get('listing_category') == 'current'){
				$vehicleDetails = $vehicleDetails->where('expired_at','>=',$appCurrentTiming['ends_at'])->orderBy('id','DESC')->get();
			}else{
				$vehicleDetails = $vehicleDetails->where('starts_at','<',$appCurrentTiming['starts_at'])->orderBy('id','DESC')->get();
			}
		}else{
			$vehicleDetails = $vehicleDetails->orderBy('id','DESC')->get();
		}
		// return Carbon::parse($vehicleDetail->starts_at)->setTimezone($seller->time_zone->timezone)->toDateTimeString();
		$vehicleDetails->map(function($vehicleDetail) use ($seller,$appTiming,$appCurrentTiming){
			$vehicleDetail->show_sat_sun_text = false;
			if((Carbon::parse($vehicleDetail->starts_at)->setTimezone($seller->time_zone->timezone)->toDateTimeString() == Carbon::parse($vehicleDetail->starts_at)->setTimeFromTimeString('09:00:00') && Carbon::parse($vehicleDetail->starts_at)->setTimezone($seller->time_zone->timezone)->dayName == 'Sunday') || (Carbon::parse($vehicleDetail->starts_at)->setTimezone($seller->time_zone->timezone)->toDateTimeString() == Carbon::parse($vehicleDetail->starts_at)->setTimeFromTimeString('16:00:00') && Carbon::parse($vehicleDetail->starts_at)->setTimezone($seller->time_zone->timezone)->dayName == 'Saturday')){
				$vehicleDetail->show_sat_sun_text = true;
			}
			$vehicleDetail->starts_at  = Carbon::parse($vehicleDetail->starts_at)->setTimeZone($seller->time_zone->timezone)->toDateTimeString();
			$vehicleDetail->expired_at  = Carbon::parse($vehicleDetail->expired_at)->setTimeZone($seller->time_zone->timezone)->toDateTimeString();

			$vehicleDetail->highest_bid  = (object)$vehicleDetail->bids()
														->select('id','dealer_id','vehicle_id','amount')
														->with('dealer:id,first_name,last_name,phone,country_code,email,address,latitude,longitude')
														->orderBy('amount', 'desc')
														->first();

			if($vehicleDetail->latest_bid == null){
				unset($vehicleDetail->latest_bid);
				$vehicleDetail->latest_bid = (object)(null);
			}

			$current_time 	= Carbon::now();
			$vehicleDetail->timezone 		= $seller->time_zone->timezone;
			$vehicleDetail->timezone_name 	= $seller->time_zone->timezone_name;
			
			$starts_at 	=  $appTiming['starts_at'];
			$vehicleDetail->repost_at = '';

			$repost_in = (int)AppHelper::repost_in();
			if($vehicleDetail->starts_at == $appTiming['starts_at_timezone'] && $vehicleDetail->steps_submitted == 8){
				$vehicleDetail->status = 'on-hold';
			}else if($vehicleDetail->expired_at >= $appCurrentTiming['ends_at_timezone'] && $vehicleDetail->steps_submitted == 8){
				$vehicleDetail->status = 'visible';
			}else{
				$vehicleDetail->repost_at 	= Carbon::parse($vehicleDetail->starts_at)->addDays($repost_in)->toDateTimeString();
				$vehicleDetail->status = 'expired';
			}

			if($current_time->diffInDays($vehicleDetail->starts_at) >= $repost_in){
				$vehicleDetail->status = 'repost';
			}

			$lat = $vehicleDetail->latitude;
			$lon = $vehicleDetail->longitude;
			$vehicleDetail->availDealers = Dealer::select("id","latitude","longitude",DB::raw("6371 * acos(cos(radians(" . $lat . ")) * cos(radians(latitude))  * cos(radians(longitude) - radians(" . $lon . ")) + sin(radians(" .$lat. ")) * sin(radians(latitude))) AS distance"))
													->whereRaw(DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )').' < ?',[$vehicleDetail->distance])
													->count();

			return	$vehicleDetail;	

		});
		$notCompletedVehicleDetail    	= Vehicle::select('id','vin_no','created_at','steps_submitted')
													->where('seller_id',$seller->id)
													->where('steps_submitted','!=',8)
													->orderBy('id','DESC')
													->first();
		
		return response()->json(['status'=>true,'message'=>'Vehicle information retrieved successfully','data'=>['vehicleDetails'=>(object)$vehicleDetails,'notCompletedVehicleDetail'=>(object)$notCompletedVehicleDetail]]);            
	}

	/** get Vehicle Information **/
	public function getVehicleDetail(Request $request){

		$validator = Validator::make($request->all(),[
								'vehicle_id' => 'required | exists:vehicles,id',
							]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$seller = auth('api')->user();  

		$vehicleDetail    	= Vehicle::select('id','vin_no','year','make','model','trim','milage','style','body_shape','exterior_color','interior_color','doors','passengers','drive_train','air_conditioning','navigation_system','leather_seats','sun_roof','panoramic_roof','dvd','heated_seats','ac_seats','360_camera','after_market_equipment','original_owner','still_making_monthly_payment','still_own','have_any_accident_record','how_much_records','smoked_in','vehicle_condition','front_tires_condition','back_tires_condition','warning_lights','have_mechanical_issues','mechanical_issues','latitude','longitude','address','distance','steps_submitted','transmission','engine','made_in','modified_exhaust','wantto_sell_or_tradein')
										->where('id',$request->get('vehicle_id'))
										->first();

		return response()->json(['status'=>true,'message'=>'Vehicle information retrieved successfully','data'=>$vehicleDetail]);
	}

	/** Get listing detail **/
	public function getCurListingDetail(Request $request){

		$vehicle = $current_listing    = Vehicle::where('id',62)->first();
		$dealer = Dealer::find(10);
		// $dealer->notify(new CurrentListingPostedNotification(5,'Asia/Kolkata'));
		// $additionalData['availableVehicle'] = 5;
		// NotificationHelper::sendPushNotification($dealer,'currentListingPosted',$additionalData);
		//   $token = md5(time());
	 // $dealer->shared_vehicle()->create(['vehicle_id'=>$vehicle->id,'email'=>$dealer->email,'token'=>$token,'type'=>'highest_bid']);
		//  $current_listing->link = route('dealer.vehicle_detail',$token);
		// $dealer->notify((new WonsbidNotification($current_listing,'Asia/Kolkata')));
		// $additionalData['vehicle'] = $current_listing->toArray();
		// NotificationHelper::sendPushNotification($dealer,'wonAbid',$additionalData);
		 // $highest_bid  = $vehicle->bids()
   //                      ->select('id','dealer_id','vehicle_id','amount')
   //                      ->with('dealer:id,first_name,last_name,phone,country_code,email,address,latitude,longitude')
   //                      ->orderBy('amount', 'desc')
   //                      ->first();

            $token = md5(time());
			$vehicle->seller->email = 'developer.foremost@gmail.com';
             $highest_bid->dealer->shared_vehicle()->create(['vehicle_id'=>$vehicle->id,'email'=>$vehicle->seller->email,'token'=>$token,'type'=>'highest_bid']);
            $vehicle->link = route('dealer.vehicle_detail',$token);
            // return $vehicle;
            if($highest_bid){
            	$vehicle->seller->email = 'developer.foremost@gmail.com';
                $vehicle->seller->notify((new SellerListingExpiredNotification($vehicle,$highest_bid,'Asia/Kolkata')));
                NotificationHelper::sendPushNotification($vehicle->seller,'gotHighestBid',$highest_bid->toArray());
            }

		// $dealer->notify((new CurrentListingPostedNotification(5,'Asia/Kolkata')));
		// $additionalData['availableVehicle'] = 5;
		// NotificationHelper::sendPushNotification($dealer,'currentListingPosted',$additionalData);
		// $current_listing->seller->notify((new SellerListingExpiredNotification($current_listing,'Asia/Kolkata')));
		// NotificationHelper::sendPushNotification($current_listing->seller,'sellerListingExpired',[]);

		return response()->json(['status'=>true,'message'=>'Vehicle information retrieved successfully','data'=>$current_listing]);
	}

	/** Get listing detail **/
	public function getCurrentListingDetail(Request $request){
		$validator = Validator::make($request->all(),[
								'vehicle_id' => 'required | exists:vehicles,id',
							]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}
		$current_listing    = Vehicle::select('id','vin_no','year','make','model','trim','milage','style','body_shape','exterior_color','interior_color','doors','passengers','drive_train','air_conditioning','navigation_system','leather_seats','sun_roof','panoramic_roof','dvd','heated_seats','ac_seats','360_camera','after_market_equipment','original_owner','still_making_monthly_payment','still_own','have_any_accident_record','how_much_records','smoked_in','vehicle_condition','front_tires_condition','back_tires_condition','warning_lights_in_cluster','warning_lights','have_mechanical_issues','mechanical_issues','latitude','longitude','steps_submitted','steps_submitted','transmission','engine','made_in','modified_exhaust','wantto_sell_or_tradein')
										->where('id',$request->get('vehicle_id'))
										->where('steps_submitted',8)
										->with(['vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type as type,image','vehicle_interior_photos:id,vehicle_id,vehicle_interior_type as type,image','vehicle_damage_photos:id,vehicle_id,image'])
										->withCount('bids')
										->first();

		return response()->json(['status'=>true,'message'=>'Vehicle information retrieved successfully','data'=>$current_listing]);
	}

	/** get vehicle exterior icons  **/
	public function getVehicleExteriorImage(Request $request){

		$validator = Validator::make($request->all(),[
								'vehicle_id' => 'required | exists:vehicles,id',
							]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$vehicle_id = $request->get('vehicle_id');
		$vehicle 	=	VehicleExteriorPlaceholderImage::select('vehicle_type','vehicle_key','image','layer_image','required')
													->get()
													->map(function($vehicleIcon) use ($vehicle_id){
														$vehicleImage = VehicleExteriorPhoto::where(['vehicle_id' => $vehicle_id,'vehicle_exterior_type' => $vehicleIcon->vehicle_key])->first();

														$vehicleIcon->uploaded = false;
														if($vehicleImage){
															$vehicleIcon->image = $vehicleImage->image;
															$vehicleIcon->uploaded = true;
														}
														return $vehicleIcon;
													});
		$otherImages    =  VehicleExteriorPhoto::where(['vehicle_id' => $vehicle_id,'vehicle_exterior_type' => 'other'])
													->select('vehicle_id','vehicle_exterior_type','image')
													->get();
													
		return response()->json(['status'=>true,'message'=>'Vehicle placeholder image retrieved successfully','data'=>$vehicle,'other'=>$otherImages]);
	}

	/** get vehicle interior icons  **/
	public function getVehicleInteriorImage(Request $request){

		$validator = Validator::make($request->all(),[
								'vehicle_id' => 'required | exists:vehicles,id',
							]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$vehicle_id = $request->get('vehicle_id');
		$vehicle 	=	VehicleInteriorPlaceholderImage::select('vehicle_type','vehicle_key','image','layer_image','required')
													->get()
													->map(function($vehicleIcon) use ($vehicle_id){
														$vehicleImage = VehicleInteriorPhoto::where(['vehicle_id' => $vehicle_id,'vehicle_interior_type' => $vehicleIcon->vehicle_key])->first();

														$vehicleIcon->uploaded = false;
														if($vehicleImage){
															$vehicleIcon->image = $vehicleImage->image;
															$vehicleIcon->uploaded = true;
														}
														// $vehicleIcon->layer_image = '';
														return $vehicleIcon;
													});

		$otherImages   	= VehicleInteriorPhoto::where(['vehicle_id' => $vehicle_id,'vehicle_interior_type' => 'other'])
												->select('vehicle_id','vehicle_interior_type','image')
												->get();

		return response()->json(['status'=>true,'message'=>'Vehicle placeholder image retrieved successfully','data'=>$vehicle,'other'=>$otherImages]);
	}

	/** get vehicle damage photo  **/
	public function getVehicleDamageImage(Request $request){

		$validator = Validator::make($request->all(),[
								'vehicle_id' => 'required | exists:vehicles,id',
							]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$vehicle_id = $request->get('vehicle_id');
		
		$damageImages   	= VehicleDamagePhoto::where(['vehicle_id' => $vehicle_id])
													->select('id','vehicle_id','image')
													->get();

		return response()->json(['status'=>true,'message'=>'Vehicle damage image retrieved successfully','data'=>$damageImages]);
	}

}
