<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v1\Maintenance;
use App\Models\v1\Seller;
use App\Models\v1\Dealer;
use App\Models\v1\EmailVerification;
use App\Models\v1\TimeZone;
use App\Models\v1\EmailVerificationDealer;
use Notification;
use App\Notifications\VerificationCode;
use App\Models\v1\Make;
use Carbon\Carbon;
use Auth;
use Hash;
use JWTAuth;
use Validator;

class AuthController extends Controller
{
  public function __construct()
  {
    $this->middleware('jwt-auth', ['except' => ['login','signup','isEmailExists','isPhoneExists','sendVerificationCode','emailVerification','refresh']]);
  }

  public function login(Request $request)
  {

    /** App under maintanence **/
    $maintenance    = Maintenance::select('android_version','ios_version','maintenance_mode')->first();

    if($maintenance != '' && $maintenance->maintenance_mode == '1'){
    	return response()->json([
    			'status'            => false,
    			'message'           => 'Application is under Maintainance.',
    			'maintenance_mode'  => true,
          'android_version'   => $maintenance->android_version,
          'ios_version'       => $maintenance->ios_version,    		
        ]);            
    }

	  $rules  = [
	        'email'     =>  'required | email',
	        'password'  =>  'required',
          'user_type' =>  'required | in:seller,dealer'
	     ];

    $validator = Validator::make($request->all(),$rules);

    if ($validator->fails()) { 
      return response()->json(['status'=>false,'message'=>$validator->messages()->first(),'maintenance_mode'  => false]);
    }

    $credentials = $request->only('email', 'password');

    if($request->user_type == 'dealer'){

      $credentials['is_approved'] = 1;
      $dealer = Dealer::where('email',$request->get('email'))->first();
      if($dealer && !$dealer->is_approved){
        return response()->json(['status'=>false,'message'=>'Your account is under review','maintenance_mode'  => false]);            
      }
      $token     = auth('api-dealer')->attempt($credentials);
      $user      = Auth::guard('api-dealer')->user();
      if($user){
        if(request('firebase_android_id')){
          $user->firebase_android_id  = request('firebase_android_id');
        }
        if(request('firebase_ios_id')){
          $user->firebase_ios_id    = request('firebase_ios_id');
        }
        $user->save();
      }
    $user->repo     = 0;
        $user->repo_charge     = 0;

    }else{

      $token     = auth('api')->attempt($credentials);
      $user      = Auth::guard('api')->user();
      if($user){
        if(request('firebase_android_id')){
          $user->firebase_android_id  = request('firebase_android_id');
        }
        if(request('firebase_ios_id')){
          $user->firebase_ios_id    = request('firebase_ios_id');
        }
        $user->save();
        $user->latitude  = 0;
        $user->longitude = 0;
        $user->address   = '';
        $user->dealership_name  = '';
        $user->amvic_no         = '';
      }
      if($user->bambora_id != ''){
        $user->repo_charge     = 0;
      }

    }


    if ($token) {
    $makes = Make::select('id','make')->where('id','!=',1)->get();

	    return response()->json([
          	         'status'    		=> true,
          	         'message'   		=> 'Login Successfully',
          	         'maintenance_mode'  => false,
                     'android_version'   => $maintenance->android_version,
                     'ios_version'   => $maintenance->ios_version,
          	         'token'     		=> $token,
                     'user_type'    => $request->user_type,
                     'makes'        => $makes,
                     'data'         => $user->only('id','first_name','last_name','email','country_code','phone','latitude','longitude','address','email_verified_at','dealership_name','amvic_no','repo_charge','repo'),
          	       ]); 

    }

	  return response()->json(['status' => false,'code' => 402,'message' => 'Your email or password does not match']);
  }


  /** Register api **/
  public function signup(Request $request) 
  { 

    $input = $request->all();

    /** App under maintanence **/
    $maintenance    = Maintenance::select('android_version','ios_version','maintenance_mode')->first();

    if($maintenance != '' && $maintenance->maintenance_mode == '1'){
    	return response()->json([
    			'status'            => false,
    			'message'           => 'Application is under Maintainance.',
    			'maintenance_mode'  => true,
          'android_version'   => $maintenance->android_version,
          'ios_version'   => $maintenance->ios_version,
    		]);            
    }

    $rules = [
			        'first_name' 	=> 'required | string',
			        'last_name'  	=> 'required | string',
			        'password'   	=> 'required | string|min:6',
//			        'country_code'  => 'required',
              'user_type'   => 'required | in:seller,dealer',
              'latitude'    => 'required_if:user_type,dealer',
              'longitude'   => 'required_if:user_type,dealer',
              'address'     => 'required_if:user_type,dealer',
              'dealership_name' => 'required_if:user_type,dealer',
              'amvic_no'        => 'required_if:user_type,dealer',
              'timezone'      => 'required',
              'timezone_name' => 'required'

			      ];

    if($input['user_type'] == 'dealer'){

      $rules['email'] = 'required | string | email | unique:dealers,email';
      $rules['phone'] = 'required | unique:dealers,phone';

    }else{

      $rules['email'] = 'required | string | email | unique:sellers,email';
      $rules['phone'] = 'required | unique:sellers,phone';

    }

	  $validator = Validator::make($input,$rules);

    if ($validator->fails()){ 
        return response()->json(['status'=>false,'message'=>$validator->messages()->first(),'maintenance_mode'  => false]);
    }

	  $credentials         = $request->only('email', 'password');
	  $input['password']   = bcrypt($input['password']); 
    $input['country_code']   = '+1';
    $input['email_verified_at']  = Carbon::now()->toDateTimeString(); 
    // if($input['user_type'] == 'dealer'){
        $input['timezone_id'] = TimeZone::updateOrCreate(['timezone' => $input['timezone'],'timezone_name' => $input['timezone_name']],['timezone' => $input['timezone'],'timezone_name' => $input['timezone_name']])->id;
    // }

    unset($input['timezone']);
    unset($input['timezone_name']);

    if($input['user_type'] == 'dealer'){
      $user            = Dealer::create($input);
      $token           = auth('api-dealer')->attempt($credentials);
    }else{
	    $user            = Seller::create($input);
      $token           = auth('api')->attempt($credentials);

      if($user){
        $user->latitude  = 0;
        $user->longitude = 0;
        $user->address   = '';
        $user->dealership_name  = '';
        $user->amvic_no         = '';
      }

    }
    $user->repo     = 0;
    $user->repo_charge     = 0;

	  if ($token) {
      $makes = Make::select('id','make')->where('id','!=',1)->get();

	    return response()->json([
  	         'status'    		=> true,
  	         'message'   		=> 'Register Successfully',
  	         'maintenance_mode'  => false,
             'android_version'   => $maintenance->android_version,
             'ios_version'  => $maintenance->ios_version,
  	         'token'     		=> $token,
             'user_type'    => $request->user_type,
             'makes'        => $makes,
  	         'data'      		=> $user->only('id','first_name','last_name','email','country_code','phone','latitude','longitude','address','email_verified_at','dealership_name','amvic_no','repo_charge','repo'),
  	       ]); 

	  }

	  return response()->json(['status'=>false,'message'=>'Something went wrong!','maintenance_mode'=>false]);            
  }

  /** is mobile exist **/
  public function isPhoneExists(Request $request)
  {

    $input = $request->all();
    $rules = [
      'user_type' => 'required | in:seller,dealer',
    ];

    $rules['phone'] = ($input['user_type'] == 'dealer') ? 'required | unique:dealers,phone' : 'required | unique:sellers,phone';

    $validator = Validator::make($input,$rules);

    if ($validator->fails()) { 
      return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
    }

    return response()->json(['status'=>true,'message'=>'You can use this phone number']);            
  }

  /** email exist **/
  public function isEmailExists(Request $request)
  {

    $input = $request->all();
    $rules = [
      'user_type' => 'required | in:seller,dealer',
    ];

    $rules['email'] = ($input['user_type'] == 'dealer') ? 'required | unique:dealers,email' : 'required | unique:sellers,email';

    $validator = Validator::make($input,$rules);

    if ($validator->fails()) { 
      return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
    }

    return response()->json(['status'=>true,'message'=>'You can use this email']);            
  }

  /** send email verification code **/
  public function sendVerificationCode(Request $request)
  {   

   $input = $request->all();
   $rules = [
      'user_type' => 'required | in:seller,dealer',
    ];

    $rules['email'] = ($input['user_type'] == 'dealer') ? 'required | unique:dealers,email' : 'required | unique:sellers,email';

    $validator = Validator::make($input,$rules);

    if ($validator->fails()) {

      return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
    }

    $vcode = rand(1000,9999);

    $EmailVerification = ($input['user_type'] == 'dealer') ? EmailVerificationDealer::UpdateOrCreate(['email' => $request->get('email')],['verification_code' => $vcode]) : EmailVerification::UpdateOrCreate(['email' => $request->get('email')],['verification_code' => $vcode]);

    $EmailVerification->notify(new VerificationCode($vcode));

    return response()->json(['status'=>true,'message'=>"Verification code has been sent to your email."]);
  }

  /** email verification **/
  public function emailVerification(Request $request)
  {

    $input = $request->all();
    $rules = [
                  'user_type' => 'required | in:seller,dealer',
                  'otp'   => 'required'
                ];

    $rules['email'] = ($input['user_type'] == 'dealer') ? 'required | unique:dealers,email' : 'required | unique:sellers,email';

    $validator = Validator::make($input,$rules);

	  if ($validator->fails()) {
	    return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
	  }

	  $email 		= $request->get('email');
	  $vcode 		= $request->get('otp');

	  $checkEmail = ($input['user_type'] == 'dealer') ? EmailVerificationDealer::where(['email'=>$email,'verification_code'=>(int)$vcode])->first() : EmailVerification::where(['email'=>$email,'verification_code'=>(int)$vcode])->first();

	  if ($checkEmail == "") {
	    return response()->json(['status'=>false,'message'=>"Please provide the valid OTP."]);
	  }

	  return response()->json(['status'=>true,'message'=>"Congrats! Thank you for Verification."]);
  }


  /**
    * Get the authenticated Seller
    *
    * @return \Illuminate\Http\JsonResponse
  */
  public function me()
  {
    return response()->json($this->guard()->user());
  }

  /**
    * Log the user out (Invalidate the token)
    *
    * @return \Illuminate\Http\JsonResponse
  */
  public function logout(Request $request)
  {
    $user = ($request->get('user_type') == 'dealer') ? auth('api-dealer')->user() : auth('api')->user();
    
    if(!$user){
      return response()->json(['status'=>false,'message' => 'Authentication Failed!']);
    }
    if ($request->get('device_type') == 'android') {
      $user->firebase_android_id = '';
    } else {
      $user->firebase_ios_id = '';
    }

    $user->save();
    ($request->get('user_type') == 'dealer') ? auth('api-dealer')->logout() : auth('api')->logout();

    return response()->json(['status'=>true,'message' => 'Successfully logged out']);
  }

  /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
  */
  public function refresh()
  {
    config([
     'jwt.blacklist_enabled' => false
    ]);
    $refreshed = JWTAuth::parseToken()->refresh(JWTAuth::getToken());
    $data['token'] = $refreshed;
    return response()->json($data);
  }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
  protected function respondWithToken($token)
  {
    return response()->json([
    		'token' => $token,
    	]);
  }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
  public function guard()
  {
    return Auth::guard('api');
  }
}
