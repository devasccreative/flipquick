<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\AppHelper;
use App\Models\v1\Vehicle;
use Carbon\Carbon;
use Validator;
use Auth;

class PaymentController extends Controller
{
	/** repost vehicle with payment **/
	public function savePaymentDetail(Request $request)
	{
		$validator = Validator::make($request->all(),[
			'name' 			=> 'required',
			'number'		=> 'required',
			'expiry_month' 	=> 'required|date_format:m',
			'expiry_year' 	=> 'required|date_format:y',
			'cvd'		  	=> 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}
		
		try {
			$seller = auth('api')->user();    
			$merchant_id 	= config('services.bambora.merchant_id'); 
			$api_key 		= config('services.bambora.api_key');
			$api_version 	= config('services.bambora.api_version');
			$platform 		= config('services.bambora.platform'); 
			$payment_passcode 	= config('services.bambora.payment_passcode'); 

			//Create Beanstream Gateway
			$beanstream = new \Beanstream\Gateway($merchant_id, $api_key, $platform, $api_version); 
			$amount = AppHelper::repost_charge();
			if($seller->bambora_id == ''){
				$profile_create = array(
					'billing' => array(
						'name' => $seller->first_name.' '.$seller->last_name,
						'email_address' => $seller->email,
						'phone_number' => $seller->phone,
						'address_line1' => 'Canada'
					),
					'card' => array(
						'name' => $request->get('name'),
						'number' => $request->get('number'),
						'expiry_month' => $request->get('expiry_month'),
						'expiry_year' => $request->get('expiry_year'),
						'cvd' => $request->get('cvd')
					)
				);

				$result = $beanstream->profiles()->createProfile($profile_create);
				$seller->bambora_id = $result;
				$seller->save();

			} else {
				$profile = $beanstream->profiles()->getProfile($seller->bambora_id);
				$card_data['card'] =  array(
					'name' => $request->get('name'),
					'expiry_month' => $request->get('expiry_month'),
					'expiry_year' => $request->get('expiry_year'),
					'cvd' => $request->get('cvd')
				);
				if($profile['card']['number'] != ''){
					$beanstream->profiles()->updateCard($seller->bambora_id, 1, $card_data);
				}else{
					$beanstream->profiles()->addCard($seller->bambora_id, $card_data);
				}
			}
			
			return response()->json(['status'=>true,'message'=>'Payment detail saved successfully']);
		} catch (\Beanstream\Exception $e) {
			return response()->json(['status'=>false,'message'=>$e->getCode().'--'.$e->getMessage()]);
		}
	}

	/** repost vehicle with payment **/
	public function repostVehicle(Request $request)
	{
		$validator = Validator::make($request->all(),[
			'vehicle_id'	=> 'required | exists:vehicles,id'
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}
		
		try {
			$seller = auth('api')->user();    
			if($seller->bambora_id == ''){
				return response()->json(['status'=>false,'message'=>'Please add payment detail first']);
			}
			$merchant_id 	= config('services.bambora.merchant_id'); 
			$api_key 		= config('services.bambora.api_key');
			$api_version 	= config('services.bambora.api_version');
			$platform 		= config('services.bambora.platform'); 
			$payment_passcode 	= config('services.bambora.payment_passcode'); 

			//Create Beanstream Gateway
			$beanstream = new \Beanstream\Gateway($merchant_id, $api_key, $platform, $api_version); 
			$amount = AppHelper::repost_charge();
			$pymentData = array(
				'amount'=> $amount,
				'payment_method'=> 'payment_profile',
				'payment_profile' => array(
					'customer_code' => $seller->bambora_id,
					'card_id' => 1,
					'complete' => true
				)
			);

			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://api.na.bambora.com/v1/payments",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => json_encode($pymentData),
				CURLOPT_HTTPHEADER => array(
					"authorization: Passcode ".base64_encode($merchant_id.':'.$payment_passcode),
					"Content-Type: application/json"
				),
			));
			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);

			$transactionResponse = json_decode($response);
			if($transactionResponse->approved == '1'){
				$appTiming 	= AppHelper::getNextSlotTiming($seller->time_zone->timezone,$seller->time_zone->timezone_name);
				$vehicle 	= Vehicle::where('id',$request->get('vehicle_id'))->first();

				$inputs['starts_at'] 	= $appTiming['starts_at'];
				$inputs['expired_at'] 	= $appTiming['ends_at'];
				$inputs['repost_at'] 	= $appTiming['starts_at'];
				$timezone = $seller->time_zone->timezone;
				$current_time 		= Carbon::now($timezone);


			if($seller->repo){
				$time_9to4 		= Carbon::now($timezone)->setTimeFromTimeString('09:00:00');
				$time_9 		= Carbon::now($timezone)->setTimeFromTimeString('09:00:00')->addDay();
				if($current_time->gt($time_9to4)){
					$time_9to4 = $time_9to4->addDay();
					$time_9    = $time_9->addDay();
				}
				$data['starts_at'] 	= Carbon::parse($time_9to4,$timezone)->setTimeZone('UTC');
				$data['ends_at'] 	= Carbon::parse($time_9,$timezone)->setTimeZone('UTC');

				if($time_9to4->format('I')){
					$data['starts_at'] 	= Carbon::parse($time_9to4,$timezone)->addHour()->setTimeZone('UTC');
					$data['ends_at'] 	= Carbon::parse($time_9,$timezone)->addHour()->setTimeZone('UTC');
				}		
				$inputs['repost_at'] 	= $data['starts_at'] ;

			}

				if($appTiming['starts_at_timezone']->format('I')){
					// $inputs['starts_at'] 	= $inputs['starts_at']->subHour();
					$inputs['expired_at']   = $inputs['expired_at']->subHour();
					$inputs['repost_at']   = $inputs['repost_at']->subHour();
				}
				if((Carbon::now($seller->time_zone->timezone_name)->gt(Carbon::now($seller->time_zone->timezone_name)->setTimeFromTimeString('09:00:00')) && Carbon::now($seller->time_zone->timezone_name)->dayName == 'Saturday') || (Carbon::now($seller->time_zone->timezone_name)->lt(Carbon::now($seller->time_zone->timezone_name)->setTimeFromTimeString('09:00:00')) && Carbon::now($seller->time_zone->timezone_name)->dayName == 'Sunday')){
					$inputs['expired_at']->addHours(24);
				}

				$vehicle->update($inputs);
				$vehicle->bids()->delete();
				$vehicle->repost()->create(['repost_at'=>$appTiming['starts_at']]);
				$seller->transactions()->create(['order_id'=>$transactionResponse->id, 'approved'=>$transactionResponse->approved, 'transaction_date'=>$transactionResponse->created,'amount'=>$amount,'vehicle_id'=>$request->get('vehicle_id')]);
				return response()->json(['status'=>true,'message'=>'Vehicle Reposted successfully']);
			}

			return response()->json(['status'=>false,'message'=>'Something went wrong ! Please, try letter']);
		} catch (\Beanstream\Exception $e) {
			return response()->json(['status'=>false,'message'=>$e->getCode().'--'.$e->getMessage()]);
		}
	}

	/** repost vehicle after 15 days **/
	public function repostVehicleAfter15Days(Request $request)
	{
		$validator = Validator::make($request->all(),[
			'vehicle_id'	=> 'required | exists:vehicles,id'
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$seller 		= auth('api')->user();    
		$current_time 	= Carbon::now($seller->time_zone->timezone);
		$appTiming 	= AppHelper::getNextSlotTiming($seller->time_zone->timezone,$seller->time_zone->timezone_name);
		$vehicle 	= Vehicle::where('id',$request->get('vehicle_id'))->first();
		$repost_in 	= (int)AppHelper::repost_in();

		// if($current_time->diffInDays($vehicle->starts_at) < $repost_in){
		// 	return response()->json(['status'=>false,'message'=>'You cann\'t repost vehicle before '.$repost_in.' days']);
		// }

		$inputs['starts_at'] 	= $appTiming['starts_at'];
		$inputs['expired_at'] 	= $appTiming['ends_at'];
		$inputs['repost_at'] 	= $appTiming['starts_at'];
		$timezone = $seller->time_zone->timezone;
		if($seller->repo){
			$time_9to4 		= Carbon::now($timezone)->setTimeFromTimeString('09:00:00');
			$time_9 		= Carbon::now($timezone)->setTimeFromTimeString('09:00:00')->addDay();
			if($current_time->gt($time_9to4)){
				$time_9to4 = $time_9to4->addDay();
				$time_9    = $time_9->addDay();
			}
			$data['starts_at'] 	= Carbon::parse($time_9to4,$timezone)->setTimeZone('UTC');
			$data['ends_at'] 	= Carbon::parse($time_9,$timezone)->setTimeZone('UTC');

			if($time_9to4->format('I')){
				$data['starts_at'] 	= Carbon::parse($time_9to4,$timezone)->addHour()->setTimeZone('UTC');
				$data['ends_at'] 	= Carbon::parse($time_9,$timezone)->addHour()->setTimeZone('UTC');
			}		
			$inputs['repost_at'] 	= $data['starts_at'] ;

		}

		
		if($appTiming['starts_at_timezone']->format('I')){
			// $inputs['starts_at'] 	= $inputs['starts_at']->subHour();
			$inputs['expired_at']   = $inputs['expired_at']->subHour();
			$inputs['repost_at']   = $inputs['repost_at']->subHour();
		}
		// print_r($inputs);die();

		if((Carbon::now($seller->time_zone->timezone_name) > Carbon::now($seller->time_zone->timezone_name)->setTimeFromTimeString('09:00:00') && Carbon::now($seller->time_zone->timezone_name)->dayName == 'Saturday') || (Carbon::now($seller->time_zone->timezone_name) < Carbon::now($seller->time_zone->timezone_name)->setTimeFromTimeString('09:00:00') && Carbon::now($seller->time_zone->timezone_name)->dayName == 'Sunday')){
			$inputs['expired_at']->addHours(24);
		}
		$vehicle->update($inputs);
		$vehicle->bids()->delete();
		$vehicle->repost()->create(['repost_at'=>$appTiming['starts_at']]);
		return response()->json(['status'=>true,'message'=>'Vehicle Reposted successfully']);

	}

	/** payment history **/
	public function paymentHistory(Request $request)
	{
		$seller	= auth('api')->user();    
		$data  	= $seller->transactions()
							->select('id','seller_id','vehicle_id','amount','created_at')
							->orderBy('created_at','DESC')
							->get()
							->map(function($transaction) use ($seller){
								$transaction->timezone 		= $seller->time_zone->timezone;
								$transaction->timezone_name = $seller->time_zone->timezone_name;
 								$transaction->created_at    = Carbon::parse($transaction->created_at)->setTimeZone($transaction->timezone);
 								$transaction->vin_no        = $transaction->vehicle->vin_no;
 								$transaction->payment_method  = 'Card';
 								unset($transaction->vehicle);
 								return $transaction;
							});    
		return response()->json(['status'=>true,'message'=>'Transaction history retrieved successfully','data'=>$data]);
	}
}
