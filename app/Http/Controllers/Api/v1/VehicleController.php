<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v1\VehicleList;
use Validator;

class VehicleController extends Controller
{
	/** get Vehicle Information **/
	public function getVehicleList(Request $request){

		$vehicleDetail    	= VehicleList::first();
		return response()->json(['status' => true,'message' => 'Vehicle list retrived successfully','data'=>$vehicleDetail]);

	}

	public function addVehicle(Request $request){
		$validator = Validator::make($request->all(),[
			'year' 				=> 'required',
			'make' 				=> 'required',
			'model' 			=> 'required',
			'trim' 				=> 'required',
			'milage' 			=> 'required | integer',
			'style' 			=> 'required',
			'body_shape' 		=> 'required',
			'exterior_color' 	=> 'required',
			'interior_color' 	=> 'required',
			'doors' 			=> 'required | integer',
			'passengers' 		=> 'required',
			'drive_train' 		=> 'required',
			'transmission'		=> 'required',
			'engine'			=> 'required',
			'made_in'			=> 'required'
			// 'air_conditioning' 	=> 'required | boolean',
			// 'navigation_system' => 'required | boolean',
			// 'leather_seats' 	=> 'required | boolean',
			// 'sun_roof' 			=> 'required | boolean',
			// 'panoramic_roof' 	=> 'required | boolean',
			// 'dvd' 				=> 'required | boolean',
			// 'heated_seats' 		=> 'required | boolean',
			// 'ac_seats' 			=> 'required | boolean',
			// '360_camera' 		=> 'required | boolean',
			// 'after_market_equipment' 		=> 'nullable',
			// 'original_owner' 				=> 'required',
			// 'still_making_monthly_payment' 	=> 'required | boolean',
			// 'still_own' 					=> 'required_if:still_making_monthly_payment,1',
			// 'have_any_accident_record' 		=> 'required | boolean',
			// 'how_much_records' 				=> 'required_if:have_any_accident_record,1',
			// 'smoked_in' 					=> 'required | boolean',
			// 'vehicle_condition' 		=> 'required | integer | min:1 | digits_between: 1,10',
			// 'front_tires_condition' 	=> 'required | in:bad,average,excellent',
			// 'back_tires_condition'  	=> 'required | in:bad,average,excellent',
			// 'warning_lights_in_cluster' => 'required | boolean',
			// 'warning_lights' 			=> 'nullable',
			// 'have_mechanical_issues' 	=> 'required | boolean',
			// 'mechanical_issues' 		=> 'nullable',
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$inputs = $request->all();
		$inputs['id'] = 1;

		$vehicle 		= VehicleList::updateOrCreate(['id' => 1],$inputs);
		return response()->json(['status' => true,'message' => 'Vehicle detail added successfully','data'=>$vehicle]);

	}
}
