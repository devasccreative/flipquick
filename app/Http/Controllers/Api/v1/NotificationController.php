<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v1\Dealer;
use Carbon\Carbon;
use Auth;

class NotificationController extends Controller
{
	public function getDealerNotifications(Request $request){

		$page 					= $request->get('page');
		$notifications 			= Auth::user()
									->notifications()
									->whereIn('type',[
										'App\Notifications\CurrentListingPostedNotification',
										'App\Notifications\WonsbidNotification'
									])
									->paginate(8);

		$notifications->getCollection()->transform(function($notification,$index){
			// $notification->markAsRead();
			$data 	= $notification->data;

			$notification['read_at']        = Carbon::parse($notification['read_at'])->setTimeZone($notification['data']['time_zone'])->toDateTimeString();
			$notification['created_at']     = Carbon::parse($notification['created_at'])->setTimeZone($notification['data']['time_zone'])->toDateTimeString();
			$notification['updated_at']     = Carbon::parse($notification['updated_at'])->setTimeZone($notification['data']['time_zone'])->toDateTimeString();

			switch($notification['type']) {

				case 'App\Notifications\CurrentListingPostedNotification':
				$notification['title']            = 'New Listing Posted';
				$notification['message']          = 'Hurry Up, '.$notification['data']['available_vehicle'].' new listings are posted today. Submit your bids before the listings goes away.';
				$notification['tag']              = 'CurrentListingPosted';
				break;

				case 'App\Notifications\WonsbidNotification':
				$notification['title']            = 'Won bid';
				$notification['message']          = 'You won a bid';
				$notification['tag']              = 'WonAbid';
				break;

			}

			return $notification;

		});

		$notificationsData 		= Auth::user()
									->notifications()
									->whereIn('type',[
										'App\Notifications\CurrentListingPostedNotification',
									])
									->paginate(6);

		$notificationsData->getCollection()->transform(function($notificationsData,$index){
			$notificationsData->markAsRead();
		});

		return response()->json(['status'=>true,'message'=>'Notification retrieved.','data'=>$notifications->all(),'last_page'=>$notifications->lastPage()]);
	}

	public function getSellerNotifications(Request $request){

		$page = $request->get('page');

		$notifications 	= Auth::user()
							->notifications()
							->whereIn('type',[
								'App\Notifications\CurrentListingPostedNotification',
							])
							->paginate(8,['*'],'page',$page);

		$notifications->getCollection()->transform(function($notification,$index) use ($preparedArrayForTrip,$trip_ids){

			switch($notification['type']) {

				case 'App\Notifications\BookingNotification':
				$notification['title']            = 'New Listing Posted';
				$notification['message']          = 'Hurry Up, '.$notification['data']['available_vehicle'].' new listings are posted today. Submit your bids before the listings goes away.';
				$notification['tag']              = 'CurrentListingPosted';
				break;
			}
			return $notification;

		});

		return response()->json(['status'=>true,'message'=>'Notification retrieved.','data'=>$notifications->all(),'last_page'=>$notifications->lastPage()]);
	}
}
