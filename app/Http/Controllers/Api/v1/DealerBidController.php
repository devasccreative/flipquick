<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v1\VehicleBid;
use App\Models\v1\Vehicle;
use App\Models\v1\Admin;
use App\Helpers\AppHelper;
use App\Models\v1\ReportIssue;
use Notifications;
use App\Notifications\ReportVehicle;
use App\Notifications\ReportIssue as RI;
use Carbon\Carbon;
use Validator;
use Auth;
use DB;
use Image;
use Storage;

class DealerBidController extends Controller
{
	/*clear bedge*/
	public function clearBedge(Request $request)
	{
		$dealer = auth('api-dealer')->user();  
		$dealer->bedge = 0;
		$dealer->save();
		return response()->json(['status'=>true,'message'=>'Bedge cleared successfully.']);            
	}
	/** get general Information **/
	public function getGeneralInfo(Request $request){

		$dealer = auth('api-dealer')->user();  
		$dealer_id[]  = $dealer->id;
		$dealerMakes = $dealer->makes()->pluck('make_id')->toArray();

		if($dealer->parent_id == 0){
			$dealer_ids =  $dealer->sub_dealers()->pluck('id')->toArray();
		}else{
			$dealer_idmain[] =  $dealer->parent_id;
			$dealer_ids =  $dealer->main_dealers->sub_dealers()->pluck('id')->toArray();
			$dealer_ids = array_merge($dealer_idmain,$dealer_ids);
		}

		$dealer_id = array_merge($dealer_id,$dealer_ids);
		// return $dealer_id;
		$appTiming = AppHelper::getCurrentSlotTiming($dealer->time_zone->timezone);

		$current_time 	= Carbon::now($dealer->time_zone->timezone);
		
		$data['timezone'] 		= $dealer->time_zone->timezone;
		$data['timezone_name'] 	= $dealer->time_zone->timezone_name;
	
		$data['starts_at'] 		= $appTiming['starts_at_timezone']->toDateTimeString();
		$data['expired_at'] 	= $appTiming['ends_at_timezone']->toDateTimeString();

		$lat = $dealer->latitude;
		$lon = $dealer->longitude;

		$current_time_utc 	= Carbon::now();
		$current_time 	= Carbon::now($dealer->time_zone->timezone);
		$current_9am_time = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00');
		$current_4pm_time = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00');
		if($current_9am_time > $current_time){
			$data['nine_am_slot_starts_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->subDay()->toDateTimeString();
			$data['nine_am_slot_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->toDateTimeString();
			$started_at_9am_utc2 = $started_at_9am_utc = Carbon::now($dealer->time_zone->timezone)->subDay()->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Tuesday'){
				$started_at_9am_utc2 = Carbon::now($dealer->time_zone->timezone)->subDays(2)->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Monday'){
				$data['nine_am_slot_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->addDay()->setTimeFromTimeString('9:00:00')->toDateTimeString();
			}
		}else{
			$data['nine_am_slot_starts_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->toDateTimeString();
			$data['nine_am_slot_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->addDay()->toDateTimeString();
			$started_at_9am_utc2 = $started_at_9am_utc = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Monday'){
				$started_at_9am_utc2 = Carbon::now($dealer->time_zone->timezone)->subDay()->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Sunday'){
			$data['nine_am_slot_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->addDays('2')->toDateTimeString();
			}
		}

		if($current_4pm_time > $current_time){
			$data['four_pm_slot_starts_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->subDay()->toDateTimeString();
			$data['four_pm_slot_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->toDateTimeString();
			$started_at_4pm_utc2 = $started_at_4pm_utc = Carbon::now($dealer->time_zone->timezone)->subDay()->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Monday'){
				$started_at_4pm_utc2 = Carbon::now($dealer->time_zone->timezone)->subDays(2)->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Sunday'){
				$data['four_pm_slot_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->addDay()->toDateTimeString();
			}
		}else{
			$data['four_pm_slot_starts_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->toDateTimeString();
			$data['four_pm_slot_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->addDay()->toDateTimeString();
			$started_at_4pm_utc2 = $started_at_4pm_utc = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Sunday'){
				$started_at_4pm_utc2 = Carbon::now($dealer->time_zone->timezone)->subDay()->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Saturday'){
				$data['four_pm_slot_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->addDays('2')->toDateTimeString();
			}
		}
		if($current_9am_time > $current_time){

			$expired_at_9am_utc2 = $expired_at_9am_utc = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Monday'){
				$expired_at_9am_utc2 = Carbon::now($dealer->time_zone->timezone)->addDay()->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
		}else{
			$expired_at_9am_utc2 = $expired_at_9am_utc = Carbon::now($dealer->time_zone->timezone)->addDay()->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Sunday'){
				$expired_at_9am_utc2 = Carbon::now($dealer->time_zone->timezone)->addDays(2)->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
		}

		if($current_4pm_time > $current_time){
			$expired_at_4pm_utc2 = $expired_at_4pm_utc = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Sunday'){
				$expired_at_4pm_utc2 = Carbon::now($dealer->time_zone->timezone)->addDay()->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
		}else{
			$expired_at_4pm_utc2 = $expired_at_4pm_utc = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->addDay()->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Saturday'){
				$expired_at_4pm_utc2 = Carbon::now($dealer->time_zone->timezone)->addDays(2)->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			}

		}
				// echo $started_at_9am_utc.'--'.$started_at_9am_utc2.'=='.$expired_at_4pm_utc.'--'.$expired_at_4pm_utc2;die();

		$current_listing    	= Vehicle::where(function($query) use ($started_at_9am_utc,$started_at_4pm_utc,$started_at_9am_utc2,$started_at_4pm_utc2){
											$query->where('starts_at',$started_at_9am_utc)
													->orWhere('starts_at',$started_at_4pm_utc)
													->orWhere('starts_at',$started_at_9am_utc2)
													->orWhere('starts_at',$started_at_4pm_utc2);
											})
											->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
											->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'));

		$data['current_listing_count']	= $current_listing->whereDoesntHave('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})->count();

		$data['current_listing_repo_count']	= Vehicle::where(function($query) use ($started_at_9am_utc,$started_at_4pm_utc,$started_at_9am_utc2,$started_at_4pm_utc2){
											$query->where('starts_at',$started_at_9am_utc)
													->orWhere('starts_at',$started_at_9am_utc2);
											})
											// ->where('expired_at','>=',$appTiming['ends_at'])
											->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
											->whereDoesntHave('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})
											->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
											->whereHas('seller', function ($query) {
											    $query->where('repo', 1);
											})
											->count();

		$data['current_listing_9am_slot_count']	= Vehicle::where(function($query) use ($started_at_9am_utc,$started_at_4pm_utc,$started_at_9am_utc2,$started_at_4pm_utc2){
											$query->where('starts_at',$started_at_9am_utc)
													->orWhere('starts_at',$started_at_9am_utc2);
											})
											// ->where('expired_at','>=',$appTiming['ends_at'])
											->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
											->whereDoesntHave('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})
											->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
											->whereDoesntHave('seller', function ($query) {
											    $query->where('repo', 1);
											})
											->count();

		$data['current_listing_4pm_slot_count']	= Vehicle::where(function($query) use ($started_at_9am_utc,$started_at_4pm_utc,$started_at_9am_utc2,$started_at_4pm_utc2){
											$query->where('starts_at',$started_at_4pm_utc)
													->orWhere('starts_at',$started_at_4pm_utc2);
											})
											// ->where('expired_at','>=',$appTiming['ends_at'])
											->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
											->whereDoesntHave('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})
											->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
											->whereDoesntHave('seller', function ($query) {
												    $query->where('repo', 1);
												})
											->count();
		$data['current_listing_submitted_bids_count']  	= Vehicle::where(function($query) use ($started_at_9am_utc,$started_at_4pm_utc,$started_at_9am_utc2,$started_at_4pm_utc2){
											$query->where('starts_at',$started_at_9am_utc)
													->orWhere('starts_at',$started_at_4pm_utc)
													->orWhere('starts_at',$started_at_9am_utc2)
													->orWhere('starts_at',$started_at_4pm_utc2);
											})
															// where('expired_at','>=',$appTiming['ends_at'])
														->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
															->whereHas('bids',function($query) use ($dealer_id){
																$query->whereIn('dealer_id',$dealer_id);
															})
															->count();

		$data['submitted_bids_repo_count']  	= Vehicle::where(function($query) use ($started_at_9am_utc,$started_at_4pm_utc,$started_at_9am_utc2,$started_at_4pm_utc2){
											$query->where('starts_at',$started_at_9am_utc)
													->orWhere('starts_at',$started_at_9am_utc2);
											})
															// where('expired_at','>=',$appTiming['ends_at'])
														->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
															->whereHas('bids',function($query) use ($dealer_id){
																$query->whereIn('dealer_id',$dealer_id);
															})
															->whereHas('seller', function ($query) {
															    $query->where('repo', 1);
															})
															->count();

		$data['submitted_bids_9am_slot_count']  	= Vehicle::where(function($query) use ($started_at_9am_utc,$started_at_4pm_utc,$started_at_9am_utc2,$started_at_4pm_utc2){
											$query->where('starts_at',$started_at_9am_utc)
													->orWhere('starts_at',$started_at_9am_utc2);
											})
															// where('expired_at','>=',$appTiming['ends_at'])
														->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
															->whereHas('bids',function($query) use ($dealer_id){
																$query->whereIn('dealer_id',$dealer_id);
															})
															->whereDoesntHave('seller', function ($query) {
															    $query->where('repo', 1);
															})
															->count();

		$data['submitted_bids_4pm_slot_count']  	= Vehicle::where(function($query) use ($started_at_9am_utc,$started_at_4pm_utc,$started_at_9am_utc2,$started_at_4pm_utc2){
											$query->where('starts_at',$started_at_4pm_utc)
													->orWhere('starts_at',$started_at_4pm_utc2);
											})
															// where('expired_at','>=',$appTiming['ends_at'])
														->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
															->whereHas('bids',function($query) use ($dealer_id){
																$query->whereIn('dealer_id',$dealer_id);
															})
															->whereDoesntHave('seller', function ($query) {
															    $query->where('repo', 1);
															})
															->count();

		$previous_won_bids    = VehicleBid::whereIn('dealer_id',$dealer_id);

		$data['lifetime_won_bids']  	= $previous_won_bids
											->whereHas('vehicle',function($query){
												$query->where('expired_at','<=',Carbon::now());
											})
											->get()
											->filter(function ($wonBid) {
											    return $wonBid->status == 1;
											})
											->count();
        $last_slot_time         		= $appTiming['last_slot_start'];
         $timeBefore48Hours = $appTiming['starts_at_timezone']->subDays(2);
        // if($appTiming['starts_at_timezone']->toTimeString() == '09:00:00' && Carbon::now($dealer->time_zone->timezone)->subDay()->dayName == 'Monday'){
        // 	$timeBefore48Hours = $appTiming['starts_at_timezone']->subDay(3);
        // }

        // if($appTiming['starts_at_timezone']->toTimeString() == '16:00:00' && Carbon::now($dealer->time_zone->timezone)->subDay()->dayName == 'Sunday'){
        // 	$timeBefore48Hours = $appTiming['starts_at_timezone']->subDay(3);
        // }
        $currentTime = Carbon::now();
		$data['previous_won_bids']  	= VehicleBid::whereIn('dealer_id',$dealer_id)
											->whereHas('vehicle',function($query) use ($timeBefore48Hours,$currentTime){
												$query->where('starts_at','>=',$timeBefore48Hours)->where('expired_at','<',$currentTime);
											})
											->get()
											->filter(function ($wonBid) {
											    return $wonBid->status == 1;
											})
											->count();

		$data['unread_notifications_count']   = $dealer->unreadNotifications->count();
		
		if($current_time->lt($appTiming['starts_at'])){
			$data['is_started'] = false;
		}else if($current_time->lt($appTiming['ends_at'])){
			$data['is_started'] = true;
		}

		return response()->json(['status' => true,'message' => 'General Information retrieved successfully','data'=>$data]);

	}

	/** get general Information **/
	public function prviouslyWonsBids(Request $request){
        $page   = $request->get('page');
        $skip  = 0;
        $take = 20;
        if($page > 1){
            $skip = ($page-1) * $take;
        }
		$dealer = auth('api-dealer')->user();  
		$dealer_id[]  = $dealer->id;
		if($dealer->parent_id == 0){
			$dealer_ids =  $dealer->sub_dealers()->pluck('id')->toArray();
		}else{
			$dealer_idmain[] =  $dealer->parent_id;
			$dealer_ids =  $dealer->main_dealers->sub_dealers()->pluck('id')->toArray();
			$dealer_ids = array_merge($dealer_idmain,$dealer_ids);
		}
		$dealer_id = array_merge($dealer_id,$dealer_ids);
		$appTiming = AppHelper::getCurrentSlotTiming($dealer->time_zone->timezone);
        $last_slot_time    = $appTiming['last_slot_start'];
        // $timeBefore48Hours = Carbon::now()->subHours(48);
        $timeBefore48Hours = $appTiming['starts_at_timezone']->subDays(2);
        // if($appTiming['starts_at_timezone']->toTimeString() == '09:00:00' && Carbon::now($dealer->time_zone->timezone)->subDay()->dayName == 'Monday'){
        // 	$timeBefore48Hours = $appTiming['starts_at_timezone']->subDay(3);
        // }

        // if($appTiming['starts_at_timezone']->toTimeString() == '16:00:00' && Carbon::now($dealer->time_zone->timezone)->subDay()->dayName == 'Sunday'){
        // 	$timeBefore48Hours = $appTiming['starts_at_timezone']->subDay(3);
        // }
        $currentTime = Carbon::now();
		$bids = $previous_won_bids  	= VehicleBid::whereIn('dealer_id',$dealer_id)
											->whereHas('vehicle',function($query) use ($last_slot_time,$timeBefore48Hours,$currentTime){
												$query->where('starts_at','>=',$timeBefore48Hours)->where('expired_at','<',$currentTime);
											})
											->with(['vehicle'=>function($query){
												$query->with(['vehicle_exterior_photo'=>function($query){
												$query->where('vehicle_exterior_type','front_driver_corner')->select('id','vehicle_id','vehicle_exterior_type','image');
											},'seller:id,email,first_name,last_name,phone,country_code'])->withCount('bids')->select('id','seller_id','vin_no','year','make','model','trim','milage','drive_train','transmission','post_type');
											},'dealer:id,first_name'])
											->select('id','vehicle_id','dealer_id','amount','status','created_at');
		$years = [];
		$brands = [];
		$vins   = [];
		$previous_won_bids->orderBy('id','desc')->get()
											->filter(function ($wonBid) {
											    return $wonBid->status == 1;
											})
											->map(function($query) use ($dealer,&$years,&$brands,&$vins){
											
												if(!in_array($query->vehicle->year, $years)){
													$years[] = $query->vehicle->year;
												}
												if(!in_array(strtoupper($query->vehicle->make), $brands)){
													$brands[] = strtoupper($query->vehicle->make);
												}
												$vins[] = $query->vehicle->vin_no;
												return $query;
											});
		if($request->has('brand') && $request->brand != ''){
				$previous_won_bids->whereHas('vehicle',function($query) use ($request){
												$query->where('make','like','%'.$request->brand.'%');
											});
		}
		if($request->has('year') && $request->year != ''){
				$previous_won_bids->whereHas('vehicle',function($query) use ($request){
												$query->where('year','like','%'.$request->year.'%');
											});
		}
		if($request->has('vin_no') && $request->vin_no != ''){
				$previous_won_bids->whereHas('vehicle',function($query) use ($request){
												$query->where('vin_no','like','%'.$request->vin_no.'%');
											});
		}
		if($request->has('post_type') && $request->post_type != ''){
				$previous_won_bids->whereHas('vehicle',function($query) use ($request){
												$query->where('post_type',$request->post_type);
											});
		}
		$totalRecords = $previous_won_bids->get()
											->filter(function ($wonBid) {
											    return $wonBid->status == 1;
											})->count();

				$lastPage   = ceil($totalRecords / $take); //$bids->lastPage(); 

		$previous_won_bids = $bids->get()
											->filter(function ($wonBid) {
											    return $wonBid->status == 1;
											})
											->map(function($query) use ($dealer){
												$query->timezone = $dealer->time_zone->timezone;
												$query->timezone_name = $dealer->time_zone->timezone_name;
												$query->created_at = Carbon::parse($query->created_at)->setTimeZone($query->timezone);
												$query->bids_count = $query->vehicle->bids()->count();
												return $query;
											});

		return response()->json(['status' => true,'message' => 'General Information retrieved successfully','data'=>$previous_won_bids->values()->all(),'lastPage'=>$lastPage,'totalRecords'=>$totalRecords,'years'=>array_reverse(array_sort($years)),'brands'=>collect($brands)->sort()->values(),'vins'=>$vins]);

	}

	/** get general Information **/
	public function lifetimeWonsBids(Request $request){
        $page   = $request->get('page');
        $skip   = 0;
        $take   = 20;
        if($page > 1){
            $skip = ($page-1) * $take;
        }
		$dealer = auth('api-dealer')->user();  
		$dealer_id[]  = $dealer->id;
		if($dealer->parent_id == 0){
			$dealer_ids =  $dealer->sub_dealers()->pluck('id')->toArray();
		}else{
			$dealer_idmain[] =  $dealer->parent_id;
			$dealer_ids =  $dealer->main_dealers->sub_dealers()->pluck('id')->toArray();
			$dealer_ids = array_merge($dealer_idmain,$dealer_ids);
		}
		$dealer_id = array_merge($dealer_id,$dealer_ids);
$years = [];
		$brands = [];
		$vins   = [];

		$bids = $lifetime_won_bids  	= VehicleBid::whereIn('dealer_id',$dealer_id)
											->with(['vehicle'=>function($query){
												$query->with(['vehicle_exterior_photo'=>function($query){
													$query->where('vehicle_exterior_type','front_driver_corner')->select('id','vehicle_id','vehicle_exterior_type','image');
											},'seller:id,email,first_name,last_name,phone,country_code'])
														->select('id','seller_id','vin_no','year','make','model','trim','milage','drive_train','transmission','post_type');
											},'dealer:id,first_name'])
											->whereHas('vehicle',function($query){
												$query->where('expired_at','<=',Carbon::now());
											})
											->select('id','vehicle_id','dealer_id','amount','status','created_at');
		$lifetime_won_bids->orderBy('id','desc')->get()
											->filter(function ($wonBid) {
											    return $wonBid->status == 1;
											})
											->map(function($query) use ($dealer,&$years,&$brands,&$vins){
												
												if(!in_array($query->vehicle->year, $years)){
													$years[] = $query->vehicle->year;
												}
												if(!in_array(strtoupper($query->vehicle->make), $brands)){
													$brands[] = strtoupper($query->vehicle->make);
												}
												$vins[] = $query->vehicle->vin_no;
												return $query;
											});
		if($request->has('brand') && $request->brand != ''){
				$lifetime_won_bids->whereHas('vehicle',function($query) use ($request){
												$query->where('make','like','%'.$request->brand.'%');
											});
		}
		if($request->has('year') && $request->year != ''){
				$lifetime_won_bids->whereHas('vehicle',function($query) use ($request){
												$query->where('year','like','%'.$request->year.'%');
											});
		}
		if($request->has('vin_no') && $request->vin_no != ''){
				$lifetime_won_bids->whereHas('vehicle',function($query) use ($request){
												$query->where('vin_no','like','%'.$request->vin_no.'%');
											});
		}
		if($request->has('post_type') && $request->post_type != ''){
				$lifetime_won_bids->whereHas('vehicle',function($query) use ($request){
												$query->where('post_type',$request->post_type);
											});
		}
		$totalRecords = $bids->get()
							->filter(function ($wonBid) {
								return $wonBid->status == 1;
							})
							->count();
		$lastPage   = ceil($totalRecords / $take); //$bids->lastPage(); 
		
		// if($request->has('brand') && $request->brand != ''){
		// 		$lifetime_won_bids->whereHas('vehicle',function($query) use ($request){
		// 										$query->where('make','like','%'.$request->brand.'%');
		// 									});
		// }
		// if($request->has('year') && $request->year != ''){
		// 		$lifetime_won_bids->whereHas('vehicle',function($query) use ($request){
		// 										$query->where('year','like','%'.$request->year.'%');
		// 									});
		// }
		// if($request->has('vin_no') && $request->vin_no != ''){
		// 		$lifetime_won_bids->whereHas('vehicle',function($query) use ($request){
		// 										$query->where('vin_no','like','%'.$request->vin_no.'%');
		// 									});
		// }
		
		$lifetime_won_bids = $lifetime_won_bids->orderBy('id','desc')->get()
											->filter(function ($wonBid) {
											    return $wonBid->status == 1;
											})
											->map(function($query) use ($dealer,&$years,&$brands,&$vins){
												$query->timezone = $dealer->time_zone->timezone;
												$query->timezone_name = $dealer->time_zone->timezone_name;
												$query->created_at = Carbon::parse($query->created_at)->setTimeZone($query->timezone);
												$query->bids_count = $query->vehicle->bids()->count();
												// if(!in_array($query->vehicle->year, $years)){
												// 	$years[] = $query->vehicle->year;
												// }
												// if(!in_array($query->vehicle->make, $brands)){
												// 	$brands[] = $query->vehicle->make;
												// }
												// $vins[] = $query->vehicle->vin_no;
												return $query;
											});



		return response()->json(['status' => true,'message' => 'General Information retrieved successfully','data'=>$lifetime_won_bids->values()->all(),'lastPage'=>$lastPage,'totalRecords'=>$totalRecords,'years'=>array_reverse(array_sort($years)),'brands'=>collect($brands)->sort()->values(),'vins'=>$vins]);

	}

	/** current listing **/
	public function current_listing(Request $request){
		$page 	= 1;
		$page 	= $request->get('page');
		$dealer = auth('api-dealer')->user();  
		$dealerMakes = $dealer->makes()->pluck('make_id')->toArray();

		$dealer_id[]  = $dealer->id;
        if($dealer->parent_id == 0){
            $dealer_ids =  $dealer->sub_dealers()->pluck('id')->toArray();
        }else{
            $dealer_idmain[] =  $dealer->parent_id;
            $dealer_ids =  $dealer->main_dealers->sub_dealers()->pluck('id')->toArray();
            $dealer_ids = array_merge($dealer_idmain,$dealer_ids);
        }
        $dealer_id = array_merge($dealer_id,$dealer_ids);
		$lat = $dealer->latitude;
		$lon = $dealer->longitude;
		// $appTiming = AppHelper::getCurrentSlotTiming($dealer->time_zone->timezone);
		$current_time_utc 	= Carbon::now();
		$current_time 	= Carbon::now($dealer->time_zone->timezone);
		$current_9am_time = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00');
		$current_4pm_time = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00');
		$data['timezone'] 		= $dealer->time_zone->timezone;
		$data['timezone_name'] 	= $dealer->time_zone->timezone_name;

		if($current_9am_time > $current_time){
			$data['nine_am_slot_starts_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->subDay()->toDateTimeString();
			$data['nine_am_slot_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->toDateTimeString();
			$started_at_9am_utc2 = $started_at_9am_utc = Carbon::now($dealer->time_zone->timezone)->subDay()->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Tuesday'){
				$started_at_9am_utc2 = Carbon::now($dealer->time_zone->timezone)->subDays(2)->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Monday'){
				$data['nine_am_slot_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->addDay()->setTimeFromTimeString('9:00:00')->toDateTimeString();
			}
		}else{
			$data['nine_am_slot_starts_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->toDateTimeString();
			$data['nine_am_slot_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->addDay()->toDateTimeString();
			$started_at_9am_utc2 = $started_at_9am_utc = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Monday'){
				$started_at_9am_utc2 = Carbon::now($dealer->time_zone->timezone)->subDay()->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Sunday'){
			$data['nine_am_slot_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->addDays('2')->toDateTimeString();
			}
		}

		if($current_4pm_time > $current_time){
			$data['four_pm_slot_starts_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->subDay()->toDateTimeString();
			$data['four_pm_slot_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->toDateTimeString();
			$started_at_4pm_utc2 = $started_at_4pm_utc = Carbon::now($dealer->time_zone->timezone)->subDay()->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Monday'){
				$started_at_4pm_utc2 = Carbon::now($dealer->time_zone->timezone)->subDays(2)->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Sunday'){
				$data['four_pm_slot_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->addDay()->toDateTimeString();
			}
		}else{
			$data['four_pm_slot_starts_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->toDateTimeString();
			$data['four_pm_slot_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->addDay()->toDateTimeString();
			$started_at_4pm_utc2 = $started_at_4pm_utc = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Sunday'){
				$started_at_4pm_utc2 = Carbon::now($dealer->time_zone->timezone)->subDay()->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Saturday'){
				$data['four_pm_slot_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->addDays('2')->toDateTimeString();
			}
		}

		$data['years'] =  Vehicle::where(function($query) use ($started_at_9am_utc,$started_at_9am_utc2,$started_at_4pm_utc,$started_at_4pm_utc2){
													$query->where('starts_at',$started_at_9am_utc)
														->orWhere('starts_at',$started_at_9am_utc2)
														->orWhere('starts_at',$started_at_4pm_utc)
														->orWhere('starts_at',$started_at_4pm_utc2);
												})
												->where('steps_submitted',8)
												->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
												->with(['vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type as type,image,orderid','vehicle_interior_photos:id,vehicle_id,vehicle_interior_type as type,image','vehicle_damage_photos:id,vehicle_id,image'])
												->whereDoesntHave('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})
												->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
												->distinct('year')
												->orderBy('year','desc')
												->pluck('year');
		$data['brands']  =  Vehicle::where(function($query) use ($started_at_9am_utc,$started_at_9am_utc2,$started_at_4pm_utc,$started_at_4pm_utc2){
													$query->where('starts_at',$started_at_9am_utc)
														->orWhere('starts_at',$started_at_9am_utc2)
														->orWhere('starts_at',$started_at_4pm_utc)
														->orWhere('starts_at',$started_at_4pm_utc2);
												})
												->where('steps_submitted',8)
												->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
												->with(['vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type as type,image,orderid','vehicle_interior_photos:id,vehicle_id,vehicle_interior_type as type,image','vehicle_damage_photos:id,vehicle_id,image'])
												->whereDoesntHave('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})
												->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
												->distinct('make')
												->orderBy('make','asc')
												->pluck('make');



// 		echo $started_at_9am_utc.''.$started_at_9am_utc2;die();
		$current_listing_9am_slot    	= Vehicle::select('id','vin_no','year','make','model','trim','milage','style','body_shape','exterior_color','interior_color','doors','passengers','drive_train','air_conditioning','navigation_system','leather_seats','sun_roof','panoramic_roof','dvd','heated_seats','ac_seats','360_camera','after_market_equipment','original_owner','still_making_monthly_payment','still_own','have_any_accident_record','how_much_records','smoked_in','vehicle_condition','front_tires_condition','back_tires_condition','warning_lights_in_cluster','warning_lights','have_mechanical_issues','mechanical_issues','latitude','longitude','steps_submitted','steps_submitted','starts_at','expired_at','transmission','engine','made_in','modified_exhaust','wantto_sell_or_tradein','vehicle_status','out_of_province','seller_id','post_type')
												->where(function($query) use ($started_at_9am_utc,$started_at_9am_utc2){
													$query->where('starts_at',$started_at_9am_utc)
														->orWhere('starts_at',$started_at_9am_utc2);
												})
												->where('steps_submitted',8)
												->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
												->with(['vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type as type,image,orderid','vehicle_interior_photos:id,vehicle_id,vehicle_interior_type as type,image','vehicle_damage_photos:id,vehicle_id,image'])
												->whereDoesntHave('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})
												->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
												->whereDoesntHave('seller', function ($query) {
															    $query->where('repo', 1);
															})
												->withCount('bids');
												// ->groupBy('expired_at')

			if($request->has('brand') && $request->brand != ''){
				$current_listing_9am_slot->where('make','like','%'.$request->brand.'%');
			}
			if($request->has('year') && $request->year != ''){
				$current_listing_9am_slot->where('year','like','%'.$request->year.'%');
			}

			if($request->has('post_type') && $request->post_type != ''){
				$current_listing_9am_slot->where('post_type',$request->post_type);
			}
			$current_listing_9am_slot    	= 	$current_listing_9am_slot->paginate(30);

			$current_listing_9am_slot->transform(function ($current_listing, $key) {
				$shared = $current_listing->shared_vehicle()->where('type','shared_app')->first();
				if(!$shared){

					$token = md5(time());
		            $shared = $current_listing->shared_vehicle()->create(['dealer_id'=>0,'vehicle_id'=>$current_listing->id,'email'=>$current_listing->seller->email,'token'=>$token,'type'=>'shared_app']);
				}
		        $current_listing->share_link = route('dealer.vehicle_detail',$shared->token);
		        unset($current_listing->seller);

				return $current_listing;
			});

			$page 	= 1;
			$page 	= $request->get('repo_page');
			$current_listing_repo_slot    	= Vehicle::select('id','vin_no','year','make','model','trim','milage','style','body_shape','exterior_color','interior_color','doors','passengers','drive_train','air_conditioning','navigation_system','leather_seats','sun_roof','panoramic_roof','dvd','heated_seats','ac_seats','360_camera','after_market_equipment','original_owner','still_making_monthly_payment','still_own','have_any_accident_record','how_much_records','smoked_in','vehicle_condition','front_tires_condition','back_tires_condition','warning_lights_in_cluster','warning_lights','have_mechanical_issues','mechanical_issues','latitude','longitude','steps_submitted','steps_submitted','starts_at','expired_at','transmission','engine','made_in','modified_exhaust','wantto_sell_or_tradein','vehicle_status','out_of_province','seller_id','post_type')
												->where(function($query) use ($started_at_9am_utc,$started_at_9am_utc2,$started_at_4pm_utc,$started_at_4pm_utc2){
													$query->where('starts_at',$started_at_9am_utc)
														->orWhere('starts_at',$started_at_9am_utc2)->orWhere('starts_at',$started_at_4pm_utc)
														->orWhere('starts_at',$started_at_4pm_utc2);
												})
												->where('steps_submitted',8)
												->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
												->with(['vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type as type,image,orderid','vehicle_interior_photos:id,vehicle_id,vehicle_interior_type as type,image','vehicle_damage_photos:id,vehicle_id,image'])
												->whereDoesntHave('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})
												->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
												->whereHas('seller', function ($query) {
															    $query->where('repo', 1);
															})
												->withCount('bids');
												// ->groupBy('expired_at')

			if($request->has('brand') && $request->brand != ''){
				$current_listing_repo_slot->where('make','like','%'.$request->brand.'%');
			}
			if($request->has('year') && $request->year != ''){
				$current_listing_repo_slot->where('year','like','%'.$request->year.'%');
			}
			if($request->has('post_type') && $request->post_type != ''){
				$current_listing_repo_slot->where('post_type',$request->post_type);
			}

			$current_listing_repo_slot    	= 	$current_listing_repo_slot->paginate(30,['*'], 'page', $page);

			$current_listing_repo_slot->transform(function ($current_listing, $key) {
				$shared = $current_listing->shared_vehicle()->where('type','shared_app')->first();
				if(!$shared){

					$token = md5(time());
		            $shared = $current_listing->shared_vehicle()->create(['dealer_id'=>0,'vehicle_id'=>$current_listing->id,'email'=>$current_listing->seller->email,'token'=>$token,'type'=>'shared_app']);
				}
		        $current_listing->share_link = route('dealer.vehicle_detail',$shared->token);
		        unset($current_listing->seller);

				return $current_listing;
			});


			$page 	= 1;
			$page 	= $request->get('four_pm_slot_page');
			$current_listing_4pm_slot    = Vehicle::select('id','vin_no','year','make','model','trim','milage','style','body_shape','exterior_color','interior_color','doors','passengers','drive_train','air_conditioning','navigation_system','leather_seats','sun_roof','panoramic_roof','dvd','heated_seats','ac_seats','360_camera','after_market_equipment','original_owner','still_making_monthly_payment','still_own','have_any_accident_record','how_much_records','smoked_in','vehicle_condition','front_tires_condition','back_tires_condition','warning_lights_in_cluster','warning_lights','have_mechanical_issues','mechanical_issues','latitude','longitude','steps_submitted','steps_submitted','starts_at','expired_at','transmission','engine','made_in','modified_exhaust','wantto_sell_or_tradein','vehicle_status','out_of_province','seller_id','post_type')
												->where(function($query) use ($started_at_4pm_utc,$started_at_4pm_utc2){
													$query->where('starts_at',$started_at_4pm_utc)
														->orWhere('starts_at',$started_at_4pm_utc2);
												})
												->where('steps_submitted',8)
												->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
												->with(['vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type as type,image,orderid','vehicle_interior_photos:id,vehicle_id,vehicle_interior_type as type,image','vehicle_damage_photos:id,vehicle_id,image'])
												->whereDoesntHave('bids',function($query) use ($dealer_id){
													$query->whereIn('dealer_id',$dealer_id);
												})
												->where(function($query) use ($dealerMakes){
													$query->where('post_type','!=','trade for new')
														->orWhereHas('makes',function($query) use ($dealerMakes){
															$query->where('makes.id',1)
																->orWhereIn('makes.id',$dealerMakes);
														});
												})
												->whereDoesntHave('seller', function ($query) {
															    $query->where('repo', 1);
															})
												->withCount('bids');
		if($request->has('brand') && $request->brand != ''){
			$current_listing_4pm_slot->where('make','like','%'.$request->brand.'%');
		}
		if($request->has('year') && $request->year != ''){
			$current_listing_4pm_slot->where('year','like','%'.$request->year.'%');
		}
		if($request->has('post_type') && $request->post_type != ''){
				$current_listing_4pm_slot->where('post_type',$request->post_type);
			}

												// ->groupBy('expired_at')
		$current_listing_4pm_slot = $current_listing_4pm_slot->paginate(30,['*'], 'page', $page);

		$current_listing_4pm_slot->transform(function ($current_listing, $key) {
				$shared = $current_listing->shared_vehicle()->where('type','shared_app')->first();
				if(!$shared){

					$token = md5(time());
		            $shared = $current_listing->shared_vehicle()->create(['dealer_id'=>0,'vehicle_id'=>$current_listing->id,'email'=>$current_listing->seller->email,'token'=>$token,'type'=>'shared_app']);
				}
		        $current_listing->share_link = route('dealer.vehicle_detail',$shared->token);
		        unset($current_listing->seller);

				return $current_listing;
			});

		$data['current_listing_9am_slot']   	= $current_listing_9am_slot->all();
		$data['current_listing_4pm_slot']   	= $current_listing_4pm_slot->all();
		$data['current_listing_repo']   	= $current_listing_repo_slot->all();

		$data['current_listing_nine_am__slot_lastPage'] = false;
		if($current_listing_9am_slot->lastPage() == $page){
			$data['current_listing_nine_am__slot_lastPage'] = true;
		}
		$data['current_listing_nine_am__slot_total']    = $current_listing_9am_slot->total();
		$data['current_listing_four_pm_slot_lastPage'] = false;
		if($current_listing_4pm_slot->lastPage() == $page){
			$data['current_listing_four_pm_slot_lastPage'] = true;
		}
		$data['current_listing_four_pm_slot_total']    = $current_listing_4pm_slot->total();

		$data['current_listing_repo_lastPage'] = false;
		if($current_listing_repo_slot->lastPage() == $page){
			$data['current_listing_repo_lastPage'] = true;
		}
		$data['current_listing_repo_total']    = $current_listing_repo_slot->total();

		return response()->json(['status' => true,'message' => 'Current Listing retrieved successfully','data'=>$data]);
	}

	/** current listing submitted bids **/
	public function current_listing_submitted_bids(Request $request){
		$page = $request->get('page');
		$dealer = auth('api-dealer')->user();  

		$dealer_id[]  = $dealer->id;
		if($dealer->parent_id == 0){
			$dealer_ids =  $dealer->sub_dealers()->pluck('id')->toArray();
		}else{
			$dealer_idmain[] =  $dealer->parent_id;
			$dealer_ids =  $dealer->main_dealers->sub_dealers()->pluck('id')->toArray();
			$dealer_ids = array_merge($dealer_idmain,$dealer_ids);
		}
		$dealer_id = array_merge($dealer_id,$dealer_ids);  
		$appTiming = AppHelper::getCurrentSlotTiming($dealer->time_zone->timezone);
		$lat = $dealer->latitude;
		$lon = $dealer->longitude;
		
		$data['timezone'] 		= $dealer->time_zone->timezone;
		$data['timezone_name'] 	= $dealer->time_zone->timezone_name;




		$current_time_utc 	= Carbon::now();
		$current_time 	= Carbon::now($dealer->time_zone->timezone);
		$current_9am_time = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00');
		$current_4pm_time = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00');
		if($current_9am_time > $current_time){
			$data['9am_starts_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->subDay()->toDateTimeString();
			$data['9am_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->toDateTimeString();
			$started_at_9am_utc2 = $started_at_9am_utc = Carbon::now($dealer->time_zone->timezone)->subDay()->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Tuesday'){
				$started_at_9am_utc2 = Carbon::now($dealer->time_zone->timezone)->subDays(2)->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Monday'){
				$data['9am_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->addDay()->setTimeFromTimeString('9:00:00')->toDateTimeString();
			}
		}else{
			$data['9am_starts_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->toDateTimeString();
			$data['9am_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->addDay()->toDateTimeString();
			$started_at_9am_utc2 = $started_at_9am_utc = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Monday'){
				$started_at_9am_utc2 = Carbon::now($dealer->time_zone->timezone)->subDay()->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Sunday'){
			$data['9am_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->addDays('2')->toDateTimeString();
			}
		}

		if($current_4pm_time > $current_time){
			$data['4pm_starts_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->subDay()->toDateTimeString();
			$data['4pm_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->toDateTimeString();
			$started_at_4pm_utc2 = $started_at_4pm_utc = Carbon::now($dealer->time_zone->timezone)->subDay()->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Monday'){
				$started_at_4pm_utc2 = Carbon::now($dealer->time_zone->timezone)->subDays(2)->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Sunday'){
				$data['4pm_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->addDay()->toDateTimeString();
			}
		}else{
			$data['4pm_starts_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->toDateTimeString();
			$data['4pm_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->addDay()->toDateTimeString();
			$started_at_4pm_utc2 = $started_at_4pm_utc = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Sunday'){
				$started_at_4pm_utc2 = Carbon::now($dealer->time_zone->timezone)->subDay()->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Saturday'){
				$data['4pm_expired_at'] 	= Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->addDays('2')->toDateTimeString();
			}
		}
		if($current_9am_time > $current_time){

			$expired_at_9am_utc2 = $expired_at_9am_utc = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Monday'){
				$expired_at_9am_utc2 = Carbon::now($dealer->time_zone->timezone)->addDay()->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
		}else{
			$expired_at_9am_utc2 = $expired_at_9am_utc = Carbon::now($dealer->time_zone->timezone)->addDay()->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Sunday'){
				$expired_at_9am_utc2 = Carbon::now($dealer->time_zone->timezone)->addDays(2)->setTimeFromTimeString('9:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
		}

		if($current_4pm_time > $current_time){
			$expired_at_4pm_utc2 = $expired_at_4pm_utc = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Sunday'){
				$expired_at_4pm_utc2 = Carbon::now($dealer->time_zone->timezone)->addDay()->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			}
		}else{
			$expired_at_4pm_utc2 = $expired_at_4pm_utc = Carbon::now($dealer->time_zone->timezone)->setTimeFromTimeString('16:00:00')->addDay()->setTimeZone('UTC')->toDateTimeString();
			if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Saturday'){
				$expired_at_4pm_utc2 = Carbon::now($dealer->time_zone->timezone)->addDays(2)->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
			}

		}












		// $data['starts_at'] 		= $appTiming['starts_at_timezone']->toDateTimeString();
		// $data['expired_at'] 	= $appTiming['ends_at_timezone']->toDateTimeString();
		// if($appTiming['starts_at_timezone']->toTimeString() == '09:00:00'){
		// 	$data['9am_starts_at'] 		= $appTiming['starts_at_timezone']->toDateTimeString();
		// 	$data['9am_expired_at'] 	= $appTiming['ends_at_timezone']->toDateTimeString();
		// 	$nineam_ends_at2 = $nineam_ends_at 	= $appTiming['ends_at']->toDateTimeString();
		// 	$data['4pm_starts_at'] 		= $appTiming['starts_at_timezone']->setTimeFromTimeString('16:00:00')->toDateTimeString();
		// 	$data['4pm_expired_at'] 	= $appTiming['ends_at_timezone']->setTimeFromTimeString('16:00:00')->toDateTimeString();
		// 	$fourpm_ends_at2 = $fourpm_ends_at = $appTiming['ends_at_timezone']->setTimeFromTimeString('16:00:00')->setTimeZone('UTC')->toDateTimeString();
		// 	if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Sunday'){
		// 		$data['9am_expired_at'] 	= $appTiming['ends_at_timezone']->setTimeFromTimeString('09:00:00')->addDay()->toDateTimeString();
		// 		$nineam_ends_at2 = $appTiming['ends_at']->addDay()->toDateTimeString();
		// 	}
		// 	if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Saturday'){
		// 		$data['4pm_expired_at'] 	= $appTiming['ends_at_timezone']->setTimeFromTimeString('16:00:00')->addDay()->toDateTimeString();
		// 		$fourpm_ends_at2 = $appTiming['ends_at_timezone']->setTimeFromTimeString('16:00:00')->addDay()->setTimeZone('UTC')->toDateTimeString();
		// 	}

		// }else{
		// 	$data['4pm_starts_at'] 		= $appTiming['starts_at_timezone']->toDateTimeString();
		// 	$data['4pm_expired_at'] 	= $appTiming['ends_at_timezone']->toDateTimeString();
		// 	$fourpm_ends_at2 = $fourpm_ends_at 	= $appTiming['ends_at']->toDateTimeString();
		// 	$data['9am_starts_at'] 		= $appTiming['starts_at_timezone']->addDay()->setTimeFromTimeString('09:00:00')->toDateTimeString();
		// 	$data['9am_expired_at'] 	= $appTiming['ends_at_timezone']->addDay()->setTimeFromTimeString('09:00:00')->toDateTimeString();
		// 	// print_r($nineam_ends_at2);
		// 	$nineam_ends_at2 = $nineam_ends_at 	= $appTiming['ends_at_timezone']->subDay()->setTimeFromTimeString('09:00:00')->setTimeZone('UTC')->toDateTimeString();
		// 	if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Sunday'){

		// 		$fourpm_ends_at2 = $appTiming['ends_at_timezone']->addDay()->setTimeZone('UTC')->toDateTimeString();
		// 		$data['4pm_expired_at'] 	= $appTiming['ends_at_timezone']->setTimeFromTimeString('16:00:00')->addDay()->toDateTimeString();
		// 	}
		// 	if(Carbon::now($dealer->time_zone->timezone)->dayName == 'Saturday'){
		// 		$nineam_ends_at2 	=  $appTiming['ends_at_timezone']->addDay()->setTimeFromTimeString('09:00:00')->setTimeZone('UTC')->toDateTimeString();
		// 		$data['9am_expired_at'] 	= $appTiming['ends_at_timezone']->setTimeFromTimeString('09:00:00')->addDay()->toDateTimeString();
		// 	}
		// }

		// echo $nineam_ends_at.'--'.$nineam_ends_at2.'=='.$fourpm_ends_at.'--'.$fourpm_ends_at2;die();

		$data['years']			 = Vehicle::where(function($query) use ($started_at_9am_utc,$started_at_9am_utc2,$started_at_4pm_utc,$started_at_4pm_utc2){
														$query->where('starts_at',$started_at_9am_utc)
															->orWhere('starts_at',$started_at_9am_utc2)
															->orWhere('starts_at',$started_at_4pm_utc)
																->orWhere('starts_at',$started_at_4pm_utc2);
													})
													->where('steps_submitted',8)
													->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
													->whereHas('bids',function($query) use ($dealer_id){
														$query->whereIn('dealer_id',$dealer_id);
													})
													->distinct('year')
													->orderBy('year','desc')
													->pluck('year');

		$data['brands']			 = Vehicle::where(function($query) use ($started_at_9am_utc,$started_at_9am_utc2,$started_at_4pm_utc,$started_at_4pm_utc2){
														$query->where('starts_at',$started_at_9am_utc)
															->orWhere('starts_at',$started_at_9am_utc2)
															->orWhere('starts_at',$started_at_4pm_utc)
																->orWhere('starts_at',$started_at_4pm_utc2);
													})
													->where('steps_submitted',8)
													->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
													->whereHas('bids',function($query) use ($dealer_id){
														$query->whereIn('dealer_id',$dealer_id);
													})
													->distinct('make')
													->orderBy('make','asc')
													->pluck('make');


		$data['vins']  =  		Vehicle::where(function($query) use ($started_at_9am_utc,$started_at_9am_utc2,$started_at_4pm_utc,$started_at_4pm_utc2){
														$query->where('starts_at',$started_at_9am_utc)
															->orWhere('starts_at',$started_at_9am_utc2)
															->orWhere('starts_at',$started_at_4pm_utc)
																->orWhere('starts_at',$started_at_4pm_utc2);
													})
													->where('steps_submitted',8)
													->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
													->whereHas('bids',function($query) use ($dealer_id){
														$query->whereIn('dealer_id',$dealer_id);
													})
												->distinct('vin_no')
												->orderBy('vin_no','asc')
												->pluck('vin_no');

		$data['9am_current_listing_submitted_bids']			 = Vehicle::select('id','vin_no','year','make','model','trim','milage','style','body_shape','exterior_color','interior_color','doors','passengers','drive_train','air_conditioning','navigation_system','leather_seats','sun_roof','panoramic_roof','dvd','heated_seats','ac_seats','360_camera','after_market_equipment','original_owner','still_making_monthly_payment','still_own','have_any_accident_record','how_much_records','smoked_in','vehicle_condition','front_tires_condition','back_tires_condition','warning_lights_in_cluster','warning_lights','have_mechanical_issues','mechanical_issues','latitude','longitude','steps_submitted','transmission','engine','made_in','modified_exhaust',DB::raw("6371 * acos(cos(radians(" . $lat . ")) * cos(radians(latitude))  * cos(radians(longitude) - radians(" . $lon . ")) + sin(radians(" .$lat. ")) * sin(radians(latitude))) AS dealer_distance"),'post_type')
													// ->where('starts_at','<',$appTiming['ends_at'])
													->where(function($query) use ($started_at_9am_utc,$started_at_9am_utc2){
														$query->where('starts_at',$started_at_9am_utc)
															->orWhere('starts_at',$started_at_9am_utc2);
													})
													->where('steps_submitted',8)
													->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
													->with(['vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type as type,image,orderid','vehicle_interior_photos:id,vehicle_id,vehicle_interior_type as type,image','vehicle_damage_photos:id,vehicle_id,image','bids.dealer:id,first_name','bids'=>function($query) use ($dealer_id){
														$query->whereIn('dealer_id',$dealer_id)->select('id','dealer_id','vehicle_id','amount');
													}])
													->withCount('bids')
													->whereHas('bids',function($query) use ($dealer_id){
														$query->whereIn('dealer_id',$dealer_id);
													})
													->whereDoesntHave('seller', function ($query) {
															    $query->where('repo', 1);
															});
		if($request->has('brand') && $request->brand != ''){
			$data['9am_current_listing_submitted_bids']->where('make','like','%'.$request->brand.'%');
		}
		if($request->has('year') && $request->year != ''){
			$data['9am_current_listing_submitted_bids']->where('year','like','%'.$request->year.'%');
		}
		if($request->has('vin_no') && $request->vin_no != ''){
			$data['9am_current_listing_submitted_bids']->where('vin_no','like','%'.$request->vin_no.'%');
		}
		if($request->has('post_type') && $request->post_type != ''){
				$data['9am_current_listing_submitted_bids']->where('post_type',$request->post_type);
			}
													// ->with('bids:vehicle_id,amount')
		$data['9am_current_listing_submitted_bids']	 = $data['9am_current_listing_submitted_bids']->orderBy('id','DESC')
													->paginate(20);
		$data['9am_lastPage'] = false;
		if($data['9am_current_listing_submitted_bids']->lastPage() == $page){
			$data['9am_lastPage'] = true;
		}
		$data['9am_total']    = $data['9am_current_listing_submitted_bids']->total();
		$data['9am_current_listing_submitted_bids']   	= $data['9am_current_listing_submitted_bids']->all();

		$page 	= 1;
		$page 	= $request->get('repo_page');

		$data['repo_current_listing_submitted_bids']			 = Vehicle::select('id','vin_no','year','make','model','trim','milage','style','body_shape','exterior_color','interior_color','doors','passengers','drive_train','air_conditioning','navigation_system','leather_seats','sun_roof','panoramic_roof','dvd','heated_seats','ac_seats','360_camera','after_market_equipment','original_owner','still_making_monthly_payment','still_own','have_any_accident_record','how_much_records','smoked_in','vehicle_condition','front_tires_condition','back_tires_condition','warning_lights_in_cluster','warning_lights','have_mechanical_issues','mechanical_issues','latitude','longitude','steps_submitted','transmission','engine','made_in','modified_exhaust',DB::raw("6371 * acos(cos(radians(" . $lat . ")) * cos(radians(latitude))  * cos(radians(longitude) - radians(" . $lon . ")) + sin(radians(" .$lat. ")) * sin(radians(latitude))) AS dealer_distance"),'post_type')
													// ->where('starts_at','<',$appTiming['ends_at'])
													->where(function($query) use ($started_at_9am_utc,$started_at_9am_utc2){
														$query->where('starts_at',$started_at_9am_utc)
															->orWhere('starts_at',$started_at_9am_utc2);
													})
													->where('steps_submitted',8)
													->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
													->with(['vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type as type,image,orderid','vehicle_interior_photos:id,vehicle_id,vehicle_interior_type as type,image','vehicle_damage_photos:id,vehicle_id,image','bids.dealer:id,first_name','bids'=>function($query) use ($dealer_id){
														$query->whereIn('dealer_id',$dealer_id)->select('id','dealer_id','vehicle_id','amount');
													}])
													->withCount('bids')
													->whereHas('bids',function($query) use ($dealer_id){
														$query->whereIn('dealer_id',$dealer_id);
													})
													->whereHas('seller', function ($query) {
															    $query->where('repo', 1);
															});
		if($request->has('brand') && $request->brand != ''){
			$data['repo_current_listing_submitted_bids']->where('make','like','%'.$request->brand.'%');
		}
		if($request->has('year') && $request->year != ''){
			$data['repo_current_listing_submitted_bids']->where('year','like','%'.$request->year.'%');
		}
		if($request->has('vin_no') && $request->vin_no != ''){
			$data['repo_current_listing_submitted_bids']->where('vin_no','like','%'.$request->vin_no.'%');
		}
		if($request->has('post_type') && $request->post_type != ''){
				$data['repo_current_listing_submitted_bids']->where('post_type',$request->post_type);
			}
													// ->with('bids:vehicle_id,amount')
		$data['repo_current_listing_submitted_bids']	 = $data['repo_current_listing_submitted_bids']->orderBy('id','DESC')
													->paginate(20,['*'], 'page', $page);
		$data['repo_lastPage'] = false;
		if($data['repo_current_listing_submitted_bids']->lastPage() == $page){
			$data['repo_lastPage'] = true;
		}
		$data['repo_total']    = $data['repo_current_listing_submitted_bids']->total();
		$data['repo_current_listing_submitted_bids']   	= $data['repo_current_listing_submitted_bids']->all();

		$page 	= 1;
		$page 	= $request->get('four_pm_slot_page');
		$data['4pm_current_listing_submitted_bids']  = Vehicle::select('id','vin_no','year','make','model','trim','milage','style','body_shape','exterior_color','interior_color','doors','passengers','drive_train','air_conditioning','navigation_system','leather_seats','sun_roof','panoramic_roof','dvd','heated_seats','ac_seats','360_camera','after_market_equipment','original_owner','still_making_monthly_payment','still_own','have_any_accident_record','how_much_records','smoked_in','vehicle_condition','front_tires_condition','back_tires_condition','warning_lights_in_cluster','warning_lights','have_mechanical_issues','mechanical_issues','latitude','longitude','steps_submitted','transmission','engine','made_in','modified_exhaust',DB::raw("6371 * acos(cos(radians(" . $lat . ")) * cos(radians(latitude))  * cos(radians(longitude) - radians(" . $lon . ")) + sin(radians(" .$lat. ")) * sin(radians(latitude))) AS dealer_distance"),'post_type')
													// ->where('starts_at','<',$appTiming['ends_at'])
													->where(function($query) use ($started_at_4pm_utc,$started_at_4pm_utc2){
															$query->where('starts_at',$started_at_4pm_utc)
																->orWhere('starts_at',$started_at_4pm_utc2);
																})
													->where('steps_submitted',8)
													->whereRaw('distance >= '.DB::raw('( 6367 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) )'))
													->with(['vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type as type,image,orderid','vehicle_interior_photos:id,vehicle_id,vehicle_interior_type as type,image','vehicle_damage_photos:id,vehicle_id,image','bids.dealer:id,first_name','bids'=>function($query) use ($dealer_id){
														$query->whereIn('dealer_id',$dealer_id)->select('id','dealer_id','vehicle_id','amount');
													}])
													->withCount('bids')
													->whereHas('bids',function($query) use ($dealer_id){
														$query->whereIn('dealer_id',$dealer_id);
													})
													->whereDoesntHave('seller', function ($query) {
															    $query->where('repo', 1);
															});
													// ->with('bids:vehicle_id,amount')
		if($request->has('brand') && $request->brand != ''){
			$data['4pm_current_listing_submitted_bids']->where('make','like','%'.$request->brand.'%');
		}
		if($request->has('year') && $request->year != ''){
			$data['4pm_current_listing_submitted_bids']->where('year','like','%'.$request->year.'%');
		}
		if($request->has('vin_no') && $request->vin_no != ''){
			$data['4pm_current_listing_submitted_bids']->where('vin_no','like','%'.$request->vin_no.'%');
		}
		if($request->has('post_type') && $request->post_type != ''){
				$data['4pm_current_listing_submitted_bids']->where('post_type',$request->post_type);
			}

												// ->groupBy('expired_at')
		$data['4pm_current_listing_submitted_bids'] = $data['4pm_current_listing_submitted_bids']->orderBy('id','DESC')
													->paginate(20,['*'], 'page', $page);

		$data['4pm_lastPage'] = false;
		if($data['4pm_current_listing_submitted_bids']->lastPage() == $page){
			$data['4pm_lastPage'] = true;
		}
		$data['4pm_total']    = $data['4pm_current_listing_submitted_bids']->total();
		$data['4pm_current_listing_submitted_bids']   	= $data['4pm_current_listing_submitted_bids']->all();
		return response()->json(['status' => true,'message' => 'Current Listing submitted bids retrieved successfully','data'=>$data]);
	}

	/** submit a bid **/
	public function submitABid(Request $request){

		$validator = Validator::make($request->all(),[
			'vehicle_id' 	=> 'required | exists:vehicles,id',
			'amount'		=> 'required | numeric'
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$dealer = auth('api-dealer')->user(); 
		$inputs = $request->all(); 
		if(!$dealer->bids()->where('vehicle_id',$inputs['vehicle_id'])->first()){
			$dealer->bids()->create([ 'vehicle_id' => $inputs['vehicle_id'],'amount' => $inputs['amount'],'status' => 0 ]);
		}else{
			return response()->json(['status' => false,'message' => 'Bid already submitted for this vehicle']);
		}

		return response()->json(['status' => true,'message' => 'Bid submitted successfully']);

	}

	/** get listing detail **/
	public function getListingDetail(Request $request){

		$validator = Validator::make($request->all(),[
			'vehicle_id' 	=> 'required | exists:vehicles,id',
		]);

		$dealer = auth('api-dealer')->user();  
		$lat = $dealer->latitude;
		$lon = $dealer->longitude;
		$vehicleDetail    	= Vehicle::select("*",DB::raw("6371 * acos(cos(radians(" . $lat . ")) * cos(radians(latitude))  * cos(radians(longitude) - radians(" . $lon . ")) + sin(radians(" .$lat. ")) * sin(radians(latitude))) AS dealer_distance"))
										->where('id',$request->get('vehicle_id'))
										->withCount('bids')
										->with('vehicle_exterior_photos:id,vehicle_id,vehicle_exterior_type,image,orderid','vehicle_interior_photos:id,vehicle_id,vehicle_interior_type,image','vehicle_damage_photos:id,vehicle_id,image')
										->get();

		return response()->json(['status' => true,'message' => 'Listing detail retrieved successfully','data'=>$vehicleDetail]);

	}

	/** submit a bid **/
	public function reportVehicle(Request $request){

		$validator = Validator::make($request->all(),[
			'vehicle_id' 	=> 'required | exists:vehicles,id',
			'title'			=> 'required',
			'image'			=> 'required|image|mimes:jpeg,png,jpg,gif,svg',
			'description'   => 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}

		$dealer = auth('api-dealer')->user(); 
		$vehicle = Vehicle::find($request->vehicle_id);
		$data = ['dealer_id'=>$dealer->id,'report'=>1];
		if($request->hasFile('image')){

			$exterior_photo = $request->image;
			$picName 		= $exterior_photo->hashName();

			$img 			= Image::make($exterior_photo);
			Storage::disk('public')->put('vehiclereports/',$exterior_photo);

			$data['image']        		= $picName;
		}
		$data['title'] = $request->title;
		$data['description'] = $request->description;

		$report = $vehicle->reports()->updateOrCreate(['dealer_id'=>$dealer->id,'report'=>1],$data);
		$admin = Admin::find(1);
		$admin->email = 'problem@flipquick.ca';
		$admin->notify(new ReportVehicle($report,$vehicle,$dealer));

		return response()->json(['status' => true,'message' => 'Vehicle reported successfully.']);

	}

	/** submit a bid **/
	public function reportIssue(Request $request){

		$validator = Validator::make($request->all(),[
			// 'vehicle_id' 	=> 'required | exists:vehicles,id',
			'title'			=> 'required',
			'image'			=> 'required|image|mimes:jpeg,png,jpg,gif,svg',
			'description'   => 'required',
			'user_type'		=> 'required|in:seller,dealer'
		]);

		if ($validator->fails()) {
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);
		}
		$user_type   = $request->get('user_type');
        if( $user_type == 'dealer' ){
        	$user = auth('api-dealer')->user();
			$data = ['dealer_id'=>$user->id,'report'=>1];
			$user_type = 'dealer';
        }else{
        	$user = auth('api')->user();
			$data = ['seller_id'=>$user->id,'report'=>1];
			$user_type = 'seller';
        }
		// $dealer = auth('api-dealer')->user(); 
		// $vehicle = Vehicle::find($request->vehicle_id);
		if($request->hasFile('image')){

			$exterior_photo = $request->image;
			$picName 		= $exterior_photo->hashName();

			$img 			= Image::make($exterior_photo);
			Storage::disk('public')->put('reports/',$exterior_photo);

			$data['image']        		= $picName;
		}
		$data['title'] = $request->title;
		$data['description'] = $request->description;

		$report = ReportIssue::create($data);
		$admin  = Admin::find(1);
		$admin->email = 'developer@flipquick.ca';
		$admin->notify(new RI($report,$user));

		return response()->json(['status' => true,'message' => 'Report submitted successfully.']);

	}

}
