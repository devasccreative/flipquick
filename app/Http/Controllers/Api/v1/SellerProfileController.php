<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use Hash;

class SellerProfileController extends Controller
{
	/** update profile **/
    public function updateProfile(Request $request)
    {
		$user 	    = Auth::guard('api')->user();
		$validator  = Validator::make($request->all(), [ 
					   		'first_name' 	=> "required|string",
					        'last_name'  	=> "required|string",
					        'email'      	=> "required|string|email|unique:sellers,email,".$user->id,
					      //  'country_code'  => "required",
					        'phone'			=> "required|unique:sellers,phone,".$user->id,
						]);

		if($validator->fails()){ 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		$inputs 	= $request->all();

		if($user->email != $inputs['email']){
			$inputs['email_verified_at']  = null; 
		}

		$user->update($inputs);
    	return response()->json(['status'=>true,'message'=>'Profile updated successfully'] ); 

	}

	/** get profile **/
	public function getProfile(Request $request)
    {
		$user = Auth::guard('api')->user();
		
		$data = $user->only('id','first_name','last_name','email','country_code','phone','email_verified_at');
    	return response()->json(['status'=>true,'message'=>'Profile information retrieved successfully','data'=>$data] ); 

	}

	/** change password seller **/
	public function changePassword(Request $request)
	{
		$data = $request->all();
        $validator = Validator::make($data,[
            'old_password' => "required",
            'new_password' => "required|min:6"
        ]);
        
        if ($validator->fails()) {
            $message = $validator->messages()->first();
            return response()->json(['status'=>false,'message'=>$message]);
        }else{

            $seller = Auth::guard('api')->user();
            $new_password = bcrypt($request->get('new_password'));
            $old_password = $request->get('old_password');

            $hashedPassword = $seller->password;
            if (Hash::check($old_password, $hashedPassword))
            {

                $data = array(
                    'password' => $new_password
                );

                $seller->update($data);

                return response()->json(['status'=>true,'message'=>"Password Change Successfully"]);
            }else{
                return response()->json(['status'=>false,'message'=>"Old password not match with your password"]);
            }    
        }
	}
}
