<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v1\Dealer;
use Validator;
use Notification;
use App\Notifications\PasswordResetRequest;

class DealerPasswordResetController extends Controller
{
    /** for email registered user **/
	public function forgotPassword(Request $request)
	{

		$validator = Validator::make($request->all(),['email'=>"required|email"]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		$seller = Dealer::where('email',$request->email)->first();
		if(!$seller){
			return response()->json(['status'=>false,'message'=>'Email not registered']);            
        }

		$otp  = rand(1000,9999);
		$seller->otp = $otp;
		$seller->save();
		$seller->notify(new PasswordResetRequest($otp));

		return response()->json(['status'=>true,'message' => 'We have e-mailed your password reset OTP!']);
	}

	/** match password reset otp and email **/
	public function matchOtp(Request $request)
	{   

		$validator = Validator::make($request->all(),['email'=>"required|email",'otp' => 'required']);
		
		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		$seller = Dealer::where('email',$request->email)->first();

		if(!$seller){
			return response()->json(['status'=>false,'message'=>'Email not registered']);            
        }

		$user   = Dealer::where('email', $request->get('email'))->where('otp', (int)$request->get('otp'))->first();

		if (!$user){
			return response()->json(['status'=>false,'message' => 'This password reset otp is invalid.']);
		}

		return response()->json(['status'=>true,'message' => 'OTP matched.']);
	}

	/** reset password **/
	public function resetPassword(Request $request)
	{   

		$validator = Validator::make($request->all(),['email'=>"required|email",'password'	 => 'required']);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		$seller = Dealer::where('email',$request->email)->first();

		if(!$seller){
			return response()->json(['status'=>false,'message'=>'Email not registered']);            
        }
		
		$seller->password = bcrypt($request->password);
		$seller->otp 		= 0;
		$seller->save();

		return response()->json(['status'=>true,'message' => 'Password Reset Successfully.']);
	}
}
