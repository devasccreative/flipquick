<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v1\TimeZone;
use Validator;
use Hash;
use Auth;

class DealerProfileController extends Controller
{
	/** update dealership info **/
    public function updateDealershipInfo(Request $request)
    {
		$dealer 	= Auth::guard('api-dealer')->user();
		$validator 	= Validator::make($request->all(), [ 
					   		'dealership_name' 	=> 'required|string',
					        'amvic_no'  	=> 'required|string',
							'address'       => 'required',
					        'latitude'      => 'required | numeric | not_in:0',
							'longitude'     => 'required | numeric | not_in:0',
					        'timezone'		=> 'required',
					        'timezone_name' => 'required'
						]);

		if($validator->fails()){ 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		$inputs = $request->all();
		
		$inputs['timezone_id'] = TimeZone::updateOrCreate(['timezone' => $inputs['timezone'],'timezone_name' => $inputs['timezone_name']],['timezone' => $inputs['timezone'],'timezone_name' => $inputs['timezone_name']])->id;
		unset($inputs['timezone']);
		unset($inputs['timezone_name']);

		$dealer->update($inputs);
    	return response()->json(['status'=>true,'message'=>'Dealership information updated successfully'] ); 

	}

	/** update profile **/
    public function updateProfile(Request $request)
    {
		$dealer 		= Auth::guard('api-dealer')->user();
		$validator 	= Validator::make($request->all(), [ 
					   		'first_name' 	=> "required|string",
					        'last_name'  	=> "required|string",
					        'email'      	=> "required|string|email|unique:dealers,email,".$dealer->id,
					        // 'country_code'  => "required",
					        'phone'			=> "required|unique:dealers,phone,".$dealer->id,
						]);

		if($validator->fails()){ 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		$inputs = $request->all();

		if($dealer->email != $inputs['email']){
			$inputs['email_verified_at']  = null; 
		}

		$dealer->update($inputs);
    	return response()->json(['status'=>true,'message'=>'Profile updated successfully'] ); 

	}

	/** get profile **/
	public function getProfile(Request $request)
    {
		$dealer = Auth::guard('api-dealer')->user();
		
		$data = $dealer->only('id','first_name','last_name','email','country_code','phone','email_verified_at','dealership_name','amvic_no','address','latitude','longitude');
		$data['timezone'] = $dealer->time_zone->timezone;
    	return response()->json(['status'=>true,'message'=>'Profile information retrieved successfully','data'=>$data] ); 

	}

	/** change password dealer **/
	public function changePassword(Request $request)
	{
		$data = $request->all();
        $validator = Validator::make($data,[
            'old_password' => "required",
            'new_password' => "required|min:6"
        ]);
        
        if ($validator->fails()) {
            $message = $validator->messages()->first();
            return response()->json(['status'=>false,'message'=>$message]);
        }else{

            $dealer  = Auth::guard('api-dealer')->user();
            $new_password = bcrypt($request->get('new_password'));
            $old_password = $request->get('old_password');

            $hashedPassword = $dealer->password;
            if (Hash::check($old_password, $hashedPassword))
            {

                $data = array(
                    'password' => $new_password
                );

                $dealer->update($data);

                return response()->json(['status'=>true,'message'=>"Password Change Successfully"]);
            }else{
                return response()->json(['status'=>false,'message'=>"Old password not match with your password"]);
            }    
        }
	}
}
