<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\v1\Dealer;
use App\Models\v1\Seller;
use Validator;
use Notification;
use App\Notifications\PasswordResetRequest;

class PasswordResetController extends Controller
{
    /** for email registered user **/
	public function forgotPassword(Request $request)
	{

		$validator = Validator::make($request->all(),
			['user_type' =>  'required | in:seller,dealer','email'=>"required|email"]);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}
        $user_type   = $request->get('user_type');
        if( $user_type == 'dealer' ){
        	$user = Dealer::where('email',$request->email)->first();
        }else{
        	$user = Seller::where('email',$request->email)->first();
        }
		if(!$user){
			return response()->json(['status'=>false,'message'=>'Email not registered']);            
        }

		$otp  = rand(1000,9999);
		$user->otp = $otp;
		$user->save();
		$user->notify(new PasswordResetRequest($otp));

		return response()->json(['status'=>true,'message' => 'We have e-mailed your password reset OTP!']);
	}

	/** match password reset otp and email **/
	public function matchOtp(Request $request)
	{   

		$validator = Validator::make($request->all(),['user_type' =>  'required | in:seller,dealer','email'=>"required|email",'otp' => 'required']);
		
		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		 $user_type   = $request->get('user_type');
        if( $user_type == 'dealer' ){
        	$user = Dealer::where('email',$request->email)->first();
        }else{
        	$user = Seller::where('email',$request->email)->first();
        }
		if(!$user){
			return response()->json(['status'=>false,'message'=>'Email not registered']);            
        }

		$userExist       = ($user_type == 'dealer') ? Dealer::where('email', $request->get('email'))->where('otp', (int)$request->get('otp'))->first() : Seller::where('email', $request->get('email'))->where('otp', (int)$request->get('otp'))->first();

		if (!$userExist){
			return response()->json(['status'=>false,'message' => 'This password reset otp is invalid.']);
		}

		return response()->json(['status'=>true,'message' => 'OTP matched.']);
	}

	/** reset password **/
	public function resetPassword(Request $request)
	{   

		$validator = Validator::make($request->all(),['user_type' =>  'required | in:seller,dealer',
													  'email'=>"required|email",
													   'password'	 => 'required']);

		if ($validator->fails()) { 
			return response()->json(['status'=>false,'message'=>$validator->messages()->first()]);            
		}

		$user_type   = $request->get('user_type');
        if( $user_type == 'dealer' ){
        	$user = Dealer::where('email',$request->email)->first();
        }else{
        	$user = Seller::where('email',$request->email)->first();
        }
		if(!$user){
			return response()->json(['status'=>false,'message'=>'Email not registered']);            
        }
		
		$user->password = bcrypt($request->password);
		$user->otp 		= 0;
		$user->save();

		return response()->json(['status'=>true,'message' => 'Password Reset Successfully.']);
	}
}
