<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CurrentListingPostedNotification extends Notification implements ShouldQueue
{
    use Queueable;
    public $availableVehicle;
    public $timezone;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($availableVehicle,$timezone)
    {
        $this->availableVehicle = $availableVehicle;
        $this->timezone = $timezone;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($this->availableVehicle <= 1){
        $message = $this->availableVehicle.' new listing has just been posted. Submit your bid before the listing expires.';

        }else{
        $message = $this->availableVehicle.' new listing have just been posted. Submit your bid before the listings expire.';

        }
        $title = "Current Listing Posted";
        return (new MailMessage)
                    ->markdown('notifications.commonTemplate',compact('message','title'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'available_vehicle'=>$this->availableVehicle,
            'time_zone'=>$this->timezone
        ];
    }
}
