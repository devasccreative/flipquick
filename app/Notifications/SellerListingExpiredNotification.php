<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SellerListingExpiredNotification extends Notification
{
    use Queueable;
    public $vehicle;
    public $highest_bid;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($vehicle,$highest_bid)
    {
        $this->vehicle = $vehicle;
        $this->highest_bid = $highest_bid;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $vehicle = $this->vehicle;
        $highest_bid = $this->highest_bid;
        return (new MailMessage)
                ->subject('You have recieved the highest bid of '.$highest_bid->amount.'$ for your vehicle From  '.$highest_bid->dealer->dealership_name )
                ->markdown('notifications.highestBid',compact('vehicle','highest_bid'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'vehicle_id' => $this->vehicle->id,
            'highest_bid'  => $this->highest_bid->id
        ];
    }
}
