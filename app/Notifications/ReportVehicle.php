<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ReportVehicle extends Notification
{
    use Queueable;
    public $report,$vehicle,$dealer;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($report,$vehicle,$dealer)
    {
                $this->report = $report;
        $this->vehicle = $vehicle;
        $this->dealer = $dealer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
                    ->markdown('notifications.reportVehicle',['report'=>$this->report,'vehicle'=>$this->vehicle,'dealer'=>$this->dealer]);

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
