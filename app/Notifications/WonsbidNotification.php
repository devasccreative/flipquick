<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class WonsbidNotification extends Notification
{
    use Queueable;
    public $vehicle;
    public $timezone;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($vehicle,$timezone)
    {
        $this->vehicle = $vehicle;
        $this->timezone = $timezone;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $vehicle = $this->vehicle;
        return (new MailMessage)
        ->subject('Bid Won! '.$sharedvehicle->vehicle->year.' '.$sharedvehicle->vehicle->make.' '.$sharedvehicle->vehicle->model.' '.$sharedvehicle->vehicle->trim.' '.substr($sharedvehicle->vehicle->vin_no, 0,5))
        ->markdown('notifications.wonAbid',compact('vehicle'));;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'vehicle_id' => $this->vehicle->id,
            'time_zone'  => $this->timezone
        ];
    }
}
