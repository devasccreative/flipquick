<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

class GenerateDealerPassword extends Notification
{
    use Queueable;
    public $token,$userName;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token,$userName)
    {
        $this->token = $token;
        $this->userName = $userName;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('password/reset', $this->token);
        return (new MailMessage)
        ->subject($this->userName.' has invited to join a dealer login')
        ->markdown('notifications.generatePassword',compact('url'));
        // ->subject(Lang::getFromJson('Generate Your Account Password'))
        // ->action(Lang::getFromJson('Generate Password'), url('password/reset', $this->token));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
