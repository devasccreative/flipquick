<?php
namespace App\Helpers;

use DB;
use Auth;

class NotificationHelper{

	public static function sendPushNotification($receiver,$tag,$additionalData=[],$is_broadcast=false){
		
		switch ($tag) {
			case 'currentListingPosted':
			$subject = "FlipQuick";
			if($additionalData['availableVehicle'] <= 1){
                $message = $additionalData['availableVehicle'].' new listing has just been posted. Submit your bid before the listing expires.';

			}else{
                $message = $additionalData['availableVehicle'].' new listing have just been posted. Submit your bid before the listings expire.';

			}
			break;

			case 'wonAbid':
			$subject = "FlipQuick";
			$message = 'You won a bid';
			break;

			case 'gotHighestBid':
			$subject = "FlipQuick";
			$message = 'You have recieved the highest bid of '.$additionalData['amount'].'$ for your vehicle From '.$additionalData['dealer']['dealership_name'];
			break;

			case 'sellerListingPosted':
			$subject = "FlipQuick";
			$message = 'Your vehicle is now live! In 24 hours you\'ll be matched with the highest bidder!';
			break;

		}

		$androidDeviceID = [];
		$iosDeviceID = [];

		if ($is_broadcast == true) {
			foreach ($receiver as $key => $user) {
				$androidDeviceID[] = $user['firebase_android_id'];
				$iosDeviceID[] = $user['firebase_ios_id'];
			}
			$androidDeviceID = array_values(array_filter($androidDeviceID));
			$iosDeviceID = array_values(array_filter($iosDeviceID));
		}else{
			$androidDeviceID[] = isset($receiver['firebase_android_id'])?$receiver['firebase_android_id']:"";
			$iosDeviceID[] 	 = isset($receiver['firebase_ios_id'])?$receiver['firebase_ios_id']:"";
		}
		$pushData 			  = [];
		$pushData['title'] 	  = $subject;
		$pushData['body'] 	  = $message;
		$pushData['priority'] = 10;
		$pushData['icon']  	  = "";
		$pushData['sound']    = 'mySound';
		// $pushData['bedge']    = $receiver['bedge'];
		$pushData['additional_data'] = [];

		$payLoad 			  = [];
		$payLoad['title'] 	  = $subject;
		$payLoad['body']  	  = $message;
		$payLoad['tag']  	  = $tag;
		$payLoad['vibrate']   = 1;
		$payLoad['sound']  	  = 1;
		// $pushData['bedge']    = $receiver['bedge'];
		$payLoad['additional_data'] = [];

		if($tag == 'gotHighestBid'){
			$payLoad['additional_data'] = ['vehicle_id'=>$additionalData['vehicle_id']];
		}
		
		if($tag == 'wonAbid'){
			$payLoad['additional_data'] = ['vehicle_id'=>$additionalData['vehicle']['id']];
		}

		if ($androidDeviceID != "") {
			$fields = array('registration_ids' => $androidDeviceID,'data'=>$payLoad);
			self::curlNotification($fields);
		}

		if ($iosDeviceID != "") {
			$fields = array('registration_ids' => $iosDeviceID,'notification'=> $pushData,'data'=>$payLoad);
			self::curlNotification($fields);
		}

// 		print_r($payLoad);die();
	}

	public static function curlNotification($fields){
		$headers = array('Authorization: key=AAAAqRJy8VM:APA91bGILRW0M5it0wlL31lHVgNnTZURQ9ISTK6Nfg27PZdvZXbLZgNawnSkTXK7KExqkxwycpldeDiDqyoyNXvlsb-p3dGc-0nvwHUj3Vk6fJFKrLOMpvhp6QWm7FwMHQ46D97bUWxA','Content-Type: application/json');
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch);
		curl_close( $ch );
		return $result;
	}
}