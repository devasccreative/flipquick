<?php
namespace App\Helpers;

use DB;
use Auth;
use App\Models\v1\AdminStatistic;
use Carbon\Carbon;

class AppHelper{
	public static function getCurrentSlotTiming($timezone = 'Canada/Mountain'){
		// $timezone = 'IST';
		$slots_timing_array = AdminStatistic::where('name','slots_timing')->first();
		$current_time 		= Carbon::now($timezone);//->setDateTimeFrom('2019-12-27 19:00:00')

		$slots_timing_array 	= json_decode($slots_timing_array->value);
		$data['current_time'] 	= $current_time;
		
		$starts_at_1slot 		= Carbon::now($timezone)->setTimeFromTimeString($slots_timing_array[0]->starts_at);
		$ends_at_1slot 		    = Carbon::now($timezone)->setTimeFromTimeString($slots_timing_array[0]->starts_at)->addHours($slots_timing_array[0]->hours);
		$ends_at_2slot 			= Carbon::now($timezone)->setTimeFromTimeString($slots_timing_array[1]->starts_at);
		
		if($current_time->lt($starts_at_1slot)){
			$data['starts_at_timezone'] 	= Carbon::now($timezone)->setTimeFromTimeString($slots_timing_array[0]->starts_at)->subHours($slots_timing_array[1]->hours);
			$data['ends_at_timezone']		= Carbon::now($timezone)->setTimeFromTimeString($slots_timing_array[0]->starts_at);
			$data['last_slot_start_timezone'] = Carbon::now($timezone)->setTimeFromTimeString($slots_timing_array[1]->starts_at)->subDay()->subHours($slots_timing_array[1]->hours);
		}else if($current_time->gt($ends_at_2slot)){
			$data['starts_at_timezone'] 	= $starts_at_1slot;
			$data['ends_at_timezone']		= $ends_at_1slot;
			$data['last_slot_start_timezone'] = $ends_at_2slot->subHours($slots_timing_array[0]->hours);
		}else{
			$data['starts_at_timezone'] 	= Carbon::now($timezone)->setTimeFromTimeString($slots_timing_array[1]->starts_at)->subday();
			$data['ends_at_timezone'] 		= Carbon::now($timezone)->setTimeFromTimeString($slots_timing_array[1]->starts_at)->addHours($slots_timing_array[1]->hours)->subday();
			$data['last_slot_start_timezone'] = $starts_at_1slot->subDay();
		}

		$data['starts_at'] 	= Carbon::parse($data['starts_at_timezone'],$timezone)->setTimeZone('UTC');
		$data['ends_at'] 	= Carbon::parse($data['ends_at_timezone'],$timezone)->setTimeZone('UTC');
		$data['last_slot_start'] 	= Carbon::parse($data['last_slot_start_timezone'],$timezone)->setTimeZone('UTC');
		if($data['starts_at']->format('I')){
			$data['starts_at'] 	= Carbon::parse($data['starts_at'],$timezone)->addHour()->setTimeZone('UTC');
			$data['ends_at'] 	= Carbon::parse($data['ends_at'],$timezone)->addHour()->setTimeZone('UTC');
			$data['last_slot_start'] 	= $data['last_slot_start']->addHour();
		}
		
		$data['time_remaining']  = $current_time->diffInSeconds($data['ends_at']);

		return $data;
	}

	public static function getNextSlotTiming($timezone = 'Canada/Mountain'){
		$slots_timing_array = AdminStatistic::where('name','slots_timing')->first();
		$slots_timing_array = json_decode($slots_timing_array->value);
		$current_time 		= Carbon::now($timezone);
		foreach ($slots_timing_array as $key => $slots_timings) {
			$starts_at 		= Carbon::now($timezone)->setTimeFromTimeString($slots_timings->starts_at);
			if($current_time->lt($starts_at)){
				$slots_timing = $slots_timings;
				break;
			}
		}
		if(!isset($slots_timing)){
			$slots_timing   = $slots_timing_array[0];
		}

		$time_9to4 		= Carbon::now($timezone)->setTimeFromTimeString($slots_timing->starts_at);
		$time_9 		= Carbon::now($timezone)->setTimeFromTimeString($slots_timing->starts_at)->addHours($slots_timing->hours);

		if($current_time->gt($time_9to4)){
			$time_9to4 = $time_9to4->addDay();
			$time_9    = $time_9->addDay();
		}

		$data['starts_at_timezone'] 	= $time_9to4;
		$data['ends_at_timezone'] 		= $time_9;

		$data['starts_at'] 	= Carbon::parse($time_9to4,$timezone)->setTimeZone('UTC');
		$data['ends_at'] 	= Carbon::parse($time_9,$timezone)->setTimeZone('UTC');
		if($time_9to4->format('I')){
			$data['starts_at'] 	= Carbon::parse($time_9to4,$timezone)->addHour()->setTimeZone('UTC');
			$data['ends_at'] 	= Carbon::parse($time_9,$timezone)->addHour()->setTimeZone('UTC');
		}
		return $data;
	}

	public static function both_current_slot_time($timezone = 'Canada/Mountain'){
		$data['current_time'] = $current_time_utc 	= Carbon::now();
		$data['current_time_timezone'] = $current_time 	= Carbon::now($timezone);
		$current_9am_time = Carbon::now($timezone)->setTimeFromTimeString('9:00:00');
		$current_4pm_time = Carbon::now($timezone)->setTimeFromTimeString('16:00:00');

		if($current_9am_time > $current_time){
			$data['nine_am_slot_starts_at'] 	= Carbon::now($timezone)->setTimeFromTimeString('9:00:00')->subDay();
			$data['nine_am_slot_expired_at'] 	= Carbon::now($timezone)->setTimeFromTimeString('9:00:00');
			$data['expired_at_9am_utc'] = Carbon::now($timezone)->setTimeFromTimeString('9:00:00')->setTimeZone('UTC');
		}else{
			$data['nine_am_slot_starts_at'] 	= Carbon::now($timezone)->setTimeFromTimeString('9:00:00');
			$data['nine_am_slot_expired_at'] 	= Carbon::now($timezone)->setTimeFromTimeString('9:00:00')->addDay();
			$data['expired_at_9am_utc'] = Carbon::now($timezone)->addDay()->setTimeFromTimeString('9:00:00')->setTimeZone('UTC');
		}
		$data['nine_am_slot_time_remaining']  = $current_time->diffInSeconds($data['nine_am_slot_expired_at']);


		if($current_4pm_time > $current_time){
			$data['four_pm_slot_starts_at'] 	= Carbon::now($timezone)->setTimeFromTimeString('16:00:00')->subDay();
			$data['four_pm_slot_expired_at'] 	= Carbon::now($timezone)->setTimeFromTimeString('16:00:00');
			$data['expired_at_4pm_utc'] = Carbon::now($timezone)->setTimeFromTimeString('16:00:00')->setTimeZone('UTC');
		}else{
			$data['four_pm_slot_starts_at'] 	= Carbon::now($timezone)->setTimeFromTimeString('16:00:00');
			$data['four_pm_slot_expired_at'] 	= Carbon::now($timezone)->setTimeFromTimeString('16:00:00')->addDay();
			$data['expired_at_4pm_utc'] = Carbon::now($timezone)->setTimeFromTimeString('16:00:00')->addDay()->setTimeZone('UTC');

		}
		$data['four_pm_slot_time_remaining']  = $current_time->diffInSeconds($data['four_pm_slot_expired_at']);
		return $data;
	}

	public static function repost_in(){
		return AdminStatistic::where('name','repost_in')->first()->value;
	}

	public static function repost_charge(){
		return AdminStatistic::where('name','repost_charge')->first()->value;
	}
}