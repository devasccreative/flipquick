<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dealers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name')->default('');
            $table->string('last_name')->default('');
            $table->string('email')->unique();
            $table->string('dealership_name')->default('');
            $table->string('amvic_no')->default('');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('country_code');
            $table->bigInteger('phone');
            $table->longText('firebase_android_id')->nullable();
            $table->longText('firebase_ios_id')->nullable();
            $table->integer('otp')->default(0);
            $table->integer('timezone_id')->unsigned()->default(1);
            $table->foreign('timezone_id')->references('id')->on('time_zones')->onDelete('cascade');
            $table->float('latitude',9,6)->default(0); 
            $table->float('longitude',9,6)->default(0); 
            $table->string('address')->default('');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dealers');
    }
}
