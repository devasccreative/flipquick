<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBamboraIdMembershipPlanIdColumnsDealersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dealers', function (Blueprint $table) {
            $table->string('bambora_id')->default('')->after('address');
            $table->bigInteger('membership_plan_id')->unsigned()->nullable()->after('bambora_id');
            // $table->foreign('membership_plan_id')->references('id')->on('membership_plans')->onDelete('cascade');
            $table->text('transaction_response')->nullable()->after('bambora_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dealers', function (Blueprint $table) {
            $table->dropColumn(['bambora_id','membership_plan_id','transaction_response']);
        });
    }
}
