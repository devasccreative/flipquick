<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealerTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dealer_transactions', function (Blueprint $table) {
           $table->bigIncrements('id');
           $table->bigInteger('dealer_id')->unsigned();
            // $table->foreign('dealer_id')->references('id')->on('dealers')->onDelete('cascade');
           $table->string('transaction_id')->nullable();
           $table->string('approved')->nullable();
           $table->float('amount',8,2)->default(0);
           $table->timestamp('transaction_date')->nullable();
           $table->string('recurring_account_id')->nullable();
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dealer_transactions');
    }
}
