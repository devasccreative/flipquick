<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleRepostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_reposts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('seller_id')->unsigned();
            // $table->foreign('seller_id')->references('id')->on('vehicles')->onDelete('cascade');
            $table->bigInteger('vehicle_id')->unsigned();
            // $table->foreign('vehicle_id')->references('id')->on('vehicles')->onDelete('cascade');           
            $table->timestamp('repost_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_reposts');
    }
}
