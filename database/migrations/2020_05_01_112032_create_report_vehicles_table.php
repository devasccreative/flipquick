<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('dealer_id')->unsigned();
            // $table->foreign('dealer_id')->references('id')->on('dealers')->onDelete('cascade');
            $table->integer('vehicle_id')->unsigned();
            // $table->foreign('vehicle_id')->references('id')->on('vehicles')->onDelete('cascade');
            $table->tinyInteger('report')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_vehicles');
    }
}
