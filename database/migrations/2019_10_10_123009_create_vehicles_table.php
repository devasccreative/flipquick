<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('seller_id')->unsigned();
            $table->foreign('seller_id')->references('id')->on('sellers')->onDelete('cascade');
            $table->string('vin_no')->default('');
            $table->integer('year')->default(0);
            $table->string('make')->default('');
            $table->string('model')->default('');
            $table->string('trim')->default('');
            $table->string('milage')->default('');
            $table->string('style')->default('');
            // $table->string('color')->default('');
            $table->string('body_shape')->default('');
            $table->string('exterior_color')->default('');
            $table->string('interior_color')->default('');
            $table->string('doors')->default('');
            $table->string('passengers')->default('');
            $table->string('drive_train')->default('');
            $table->boolean('air_conditioning')->default(false);
            $table->boolean('navigation_system')->default(false);
            $table->boolean('leather_seats')->default(false);
            $table->boolean('sun_roof')->default(false);
            $table->boolean('panoramic_roof')->default(false);
            $table->boolean('dvd')->default(false);
            $table->boolean('heated_seats')->default(false);
            $table->boolean('ac_seats')->default(false);
            $table->boolean('360_camera')->default(false);
            $table->text('after_market_equipment')->nullable();
            $table->boolean('original_owner')->default(false);
            $table->boolean('still_making_monthly_payment')->default(false);
            $table->float('still_own')->default(0);
            $table->boolean('have_any_accident_record')->default(false);
            $table->integer('how_much_records')->default(0);
            $table->boolean('smoked_in')->default(false);
            $table->integer('vehicle_condition')->default(0);
            $table->enum('front_tires_condition',['bad','average','excellent'])->default('bad');
            $table->enum('back_tires_condition',['bad','average','excellent'])->default('bad');
            $table->boolean('warning_lights_in_cluster')->default(false);
            $table->text('warning_lights')->nullable();
            $table->boolean('have_mechanical_issues')->default(false);
            $table->text('mechanical_issues')->nullable();
            $table->timestamp('starts_at')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->timestamp('repost_at')->nullable();
            $table->float('latitude',9,6)->default(0); 
            $table->float('longitude',9,6)->default(0); 
            $table->integer('distance')->default(10); 
            $table->string('address')->default('');
            $table->integer('steps_submitted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}