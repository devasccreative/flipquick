<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellerTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seller_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('seller_id')->unsigned();
            // $table->foreign('seller_id')->references('id')->on('sellers')->onDelete('cascade');
            $table->bigInteger('vehicle_id')->unsigned();
            $table->string('order_id')->nullable();
            $table->string('approved')->nullable();
            $table->float('amount',8,2)->default(0);
            $table->timestamp('transaction_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seller_transactions');
    }
}
