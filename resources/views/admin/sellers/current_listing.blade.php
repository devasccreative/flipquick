@section('title','Flipquick | Vehicle Listing')
@extends('admin.layouts.admin-app')

@section('content')
<div class="rightdetailpart dashbordscroll crtlistbox">
    <div class="boxandditboxcover" id="sidebarWrap">
        <div class="boxandditbox_left" id="sidebar">
                <!-- <div class="congrttextbox">
                    <h3>Congratulations !</h3>
                    <p>You won 2 bids in your previous bidding slot.</p>
                    <a href="javascript:void(0)">VIEW SELLER INFO</a>
                </div> -->
                @if($total > 0)
                <div class="hurryupbox">
                 <p>Current listings will be removed at {{$data['ends_at']}} {{$data['timezone_name']}}</p>
                 <ul>
                    <li>
                        <h4 id="hours">00<!-- {{ gmdate('h', $data['time_remaining']) }} --></h4>
                        <p>Hours</p>
                    </li>
                    <li>
                        <h4 id="minutes">00<!-- {{ gmdate('i', $data['time_remaining']) }} --></h4>
                        <p>Minutes</p>
                    </li>
                    <li>
                        <h4 id="seconds">00<!-- {{ gmdate('s', $data['time_remaining']) }} --></h4>
                        <p>Seconds</p>
                    </li>
                </ul>
            </div>
            @endif
            <div class="veiableboxset">
                <div class="veiablebox_title">
                    <h3>CURRENT LISTING - {{ ($total > 1)?$total.' VEHICLES':$total.' VEHICLE' }} AVAILABLE</h3>
                </div>
                <div class="vehiclesdetaboxcover"  id="vehicleData">
                    <div class="vehiclesdetaboxiner" id="vehicleResult" page="{{$lastPage}}">
                        @include('admin.sellers.vehicle_list_results')
                    </div>
                </div>
            </div>
            <div class="veiableboxset">
                <div class="veiablebox_title">
                    <h3>PAST LISTING - {{ ($pastTotal > 1)?$pastTotal.' VEHICLES':$pastTotal.' VEHICLE' }} AVAILABLE</h3>
                </div>
                <div class="vehiclesdetaboxcover"  id="vehicleData">
                    <div class="vehiclesdetaboxiner" id="vehicleResult" page="{{$lastPage}}">
                        @include('admin.sellers.pastvehicle_list_results')
                    </div>
                </div>
            </div>
        </div>
        <div class="boxandditbox_right" id="sidebar">
            <div class="selectcarboxiner">
                <?php  $allImage=[]; ?>
                @if($data['current_listing']->count() > 0)
                <?php $vehicle = $data['current_listing'][0]; ?>
                <div class="carsliderbox">
                    <div class="gallery-slider">

                        <div class="gallery-slider__images sldimgbox">
                            <?php
                            $vehicleExteriorPhotos = $vehicle->vehicle_exterior_photos->pluck('image')->toArray();
                            $vehicleDamagePhoto = $vehicle->vehicle_damage_photos->pluck('image')->toArray();
                            $vehicleInteriorPhoto = $vehicle->vehicle_interior_photos->pluck('image')->toArray();
                            $vehicleAllImages = array_merge($vehicleExteriorPhotos,$vehicleInteriorPhoto,$vehicleDamagePhoto);


                            $vehicleAllImage = array_merge($vehicleExteriorPhotos,$vehicleInteriorPhoto,$vehicleDamagePhoto); 

                            ?>
                            <div>
                                @foreach($vehicleAllImage as $key => $image)
                                <?php $allImage[] = ['src' => $image,'thumb'=>$image,'subHtml'=>'']; ?>

                                <div class="item">
                                  <div class="img-fill animated-thumbnials">
                                    <a href="{{ $image }}">
                                        <img src="{{ $image }}" alt="Kitten {{ $key }}">
                                    </a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>


                    <div class="gallery-slider__thumbnails sldthumbnailsbox">
                        <div>
                            @foreach($vehicleAllImage as $key => $image)

                            <div class="item">
                                <div class="img-fill"><img src="{{ $image }}" alt="Kitten {{ $key }}"></div>
                            </div>
                            @endforeach

                        </div>
                        @if(count($vehicleAllImage) > 0)
                        <div class="backnextbtn">
                            <button class="prev-arrow slick-arrow nextpribtnbox">
                                BACK
                            </button>
                            <button class="next-arrow slick-arrow nextpribtnbox">
                                NEXT
                            </button>
                        </div>   
                        @endif                           

                    </div>
                </div>
            </div>

            <div class="sldboxtitle">
                <h4>{{ $vehicle->year.' '.$vehicle->make.' '.$vehicle->trim.' '.$vehicle->style }}</h4>
                <p><span class="vehicleVIN">VIN : {{$vehicle->vin_no}}</span> </p>
                    <p><span class="vehicleVIN">{{$vehicle->post_type}}</span> </p>

                <p><span>Exterior : {{ $vehicle->exterior_color }}</span> <img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""> <span>Interior : {{ $vehicle->interior_color }}</span></p>
            </div>
            <div class="sedoseboxset">
                <ul>
                    <li>{{ $vehicle->body_shape }}</li>
                    <li>{{ $vehicle->doors }} Doors</li>
                    <li>{{ $vehicle->passangers }} Amount Of Seats</li>
                </ul>
            </div>
            <div class="midetrboxsetiner">
                <ul>
                    <li>
                        <img src="{{ asset('assets/svg/FlipQuick_milage.svg') }}" alt="">
                        <p>Milage <br>{{ $vehicle->milage }} kms</p>
                    </li>
                    <li>
                        <img src="{{ asset('assets/svg/FlipQuick_drive_train.svg') }}" alt="">
                        <p>Drive Train <br>{{ $vehicle->drive_train }}</p>
                    </li>
                    <li>
                        <img src="{{ asset('assets/svg/FlipQuick_transmission.svg') }}" alt="">
                        <p>Transmission <br>{{ ucfirst($vehicle->transmission) }}</p>
                    </li>
                    <li>
                        <img src="{{ asset('assets/svg/car-engine.svg') }}" alt="">
                        <p>Engine <br>{{ ucfirst($vehicle->engine) }}</p>
                    </li>
                </ul>
            </div>
            <div class="tabboxtsetcover">
                <div class="wrapper">
                    <ul class="tabs">
                        <li><a href="javascript:void(0);" rel="#tabcontent1" class="tab active">options</a></li>
                        <li><a href="javascript:void(0);" rel="#tabcontent2" class="tab">additional info</a></li>
                        <li><a href="javascript:void(0);" rel="#tabcontent3" class="tab">vehicle condition</a></li>
                        <li><a href="javascript:void(0);" rel="#tabcontent4" class="tab">Bids ({{$vehicle->bids->count()}})</a></li>
                    </ul>

                    <div class="tab_content_container">
                        <div class="tab_content tab_content_active" id="tabcontent1">                        
                            <p><label>Air Conditioning:</label> {{ ($vehicle->air_conditioning=="1")?'Yes':'No' }}</p>
                            <p><label>Navigation System:</label> {{ ($vehicle->navigation_system=="1")?'Yes':'No' }}</p>
                            <p><label>Leather Seats:</label> {{ ($vehicle->leather_seats=="1")?'Yes':'No' }}</p>
                            <p><label>Sun Roof:</label> {{ ($vehicle->sun_roof=="1")?'Yes':'No' }}</p>
                            <p><label>Panoramic Roof:</label> {{ ($vehicle->panoramic_roof=="1")?'Yes':'No' }}</p>
                            <p><label>DVD:</label> {{ ($vehicle->dvd=="1")?'Yes':'No' }}</p>
                            <p><label>Heated Seats:</label> {{ ($vehicle->heated_seats=="1")?'Yes':'No' }}</p>
                            <p><label>AC Seats:</label> {{ ($vehicle->ac_seats=="1")?'Yes':'No' }}</p>
                            <p><label>360 Camera:</label> {{ ($vehicle['360_camera']=="1")?'Yes':'No' }}</p>
                            <p><label>Engine :</label> {{ $vehicle->engine }}</p>
                            <p><label>Cab Type :</label> {{ ($vehicle->style) ? $vehicle->style : '-' }}</p>
                            <p><label>Fuel Type :</label> {{ $vehicle->made_in }}</p>
                        </div>

                        <div class="tab_content" id="tabcontent2">                                  
                            <p><label>After Market Equipment:</label> {{ ($vehicle->after_market_equipment)?$vehicle->after_market_equipment:'-' }}</p>
                            <p><label>Original Owner:</label> {{ ($vehicle->original_owner==1)?'Yes':'No' }}</p>
                            <p><label>Still Making Monthly Payments:</label> {{ ($vehicle->still_making_monthly_payment=="1")?'Yes':'No' }} </p>
                            <p><label>Still Owe:</label> {{ $vehicle->still_own }} </p>
                            <p><label>Have Any Accident Claims?:</label> {{ ($vehicle->have_any_accident_record=='1')?'Yes':'No' }} </p>
                            <p><label>How Much Are The Claims:</label> {{ ($vehicle->how_much_records > 1)?$vehicle->how_much_records:0 }}</p>
                            <p><label>Smoked In:</label> {{ ($vehicle->smoked_in=="1")?'Yes':'No' }}</p>
                            <p><label>Modified Exhaust :</label> {{ ($vehicle->modified_exhaust=="1")?'Yes':'No' }}</p>
                            <p><label>Sell / Trade :</label> {{ ($vehicle->wantto_sell_or_tradein)?$vehicle->wantto_sell_or_tradein:'-' }}</p>

                        </div>  
                        <div class="tab_content" id="tabcontent3">                                  
                            <p><label>Overall Vehicle Condition:</label> {{ $vehicle->vehicle_condition }}/10</p>
                            <p><label>Condition of Front Tires:</label> {{ $vehicle->front_tires_condition }}</p>
                            <p><label>Condition of Rear Tires:</label> {{ $vehicle->back_tires_condition }}</p>
                            <p><label>Warning Lights In Cluster:</label> {{ ($vehicle->warning_lights_in_clustor=="1")?$vehicle->warning_lights:'No' }}</p>
                            <p><label>Mechanical Issues:</label> {{ ($vehicle->mechanical_issues=="1")?'Yes':'No' }}</p>
                        </div>
                        <div class="tab_content" id="tabcontent4">                                  
                            <p><label>{{($vehicle->bids->count() > 1) ? $vehicle->bids->count().' bids' : $vehicle->bids->count().' bid' }}  Submitted</label></p>
                            <?php $i = 1; ?>
                            @forelse($vehicle->bids as $bid)
                            <p> {{ $bid->dealer->first_name.' '.$bid->dealer->last_name }} ({{'$'.$bid->amount}} @if($bid->status) <a href="javascript:void(0)" class="placebidbtn placeBid wonBid">WON</a> @endif)</p>
                            <?php $i++; ?>
                            @empty
                            <!-- <p><label>No Bids Found</label></p> -->
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
</div>

@endsection

@section('scripts')
<script>
    var allImage = {!! json_encode($allImage,JSON_UNESCAPED_SLASHES) !!};
    $(document).on('click','.animated-thumbnials', function(e) {
        e.preventDefault();
        $(this).lightGallery({
            dynamic: true,
            thumbnail:true,
            animateThumb: false,
            showThumbByDefault: false,
            dynamicEl:allImage,
        }); 
    });
    var page = 1;
    var loadMorePage = true;
    $(function() {
        $('#vehicleResult').scroll(function() {
            var $this = $(this);
            $results  = $('#vehicleResult');
            if ($this.scrollTop() + $this.height() >= $results.height()) {
                    // if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight){
                        page++;
                        if(page <= $("#vehicleResult").attr('page')){
                            loadMoreData(page);
                        }else{
                            $('.ajax-load').show();
                            $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                        }
                    }
                });
    });
    function loadMoreData(page) {
        if(loadMorePage){
            $.ajax({
                url: '?page=' + page,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data){
                if(data.html == " "){
                    $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    return;
                }
                $('.ajax-load').hide();
                $("#vehicleResult").append(data.html);
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                loadMorePage = false;
            });
        }
    }
    $(document).ready(function() {
        /* TIMER */
        var upgradeTime = parseInt("{{ $data['time_remaining']  }}");
        var seconds = upgradeTime;
        // function timer() {
            setInterval(function(){ 
                var days        = Math.floor(seconds/24/60/60);
                var hoursLeft   = Math.floor((seconds) - (days*86400));
                var hours       = Math.floor(hoursLeft/3600);
                var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
                var minutes     = Math.floor(minutesLeft/60);
                var remainingSeconds = seconds % 60;
                function pad(n) {
                    return (n < 10 ? "0" + n : n);
                }
                $('#hours').text(hours);
                $('#minutes').text(minutes);
                $('#seconds').text(remainingSeconds);
                if (seconds == 0) {
                    $('#hours').text(hours);
                    $('#minutes').text(minutes);
                    $('#seconds').text(remainingSeconds);
                } else {
                    seconds--;
                }
            },1000);
            /* END TIMER */

            $(document).on('click','.bids_list',function(){
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "Get",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ route("admin.seller.vehicle_detail","/") }}/'+id,
                    data: {id : id},
                    success: function(response)
                    {
                        $('.boxandditbox_right').html(response);
                        slider();
                        console.log(response);
                    }
                });
            }) 
        })
    

    function updateCaption(text) {
        // if empty, add a no breaking space
        if (text === '') {
            text = '&nbsp;';
        }
        $caption.html(text);
        $caption.removeClass('hide');
    }
</script>
<script>
    $(document).ready(function() {
       slider();
       $('.js-example-basic-single').select2();
   });
</script>
@endsection
