@section('title','Flipquick | Seller Edit Profile')
@extends('admin.layouts.admin-app')
@section('styles')

@endsection
@section('content')
<div class="rightdetailpart">
    <div class="boxandditboxcover">
        <div class="delspselimabox">
            <div class="wrapper">
                <ul class="tabs">
                    <li><a href="javascript:void(0);" rel="#tabcontent1" class=" active">Edit Profile</a></li>
                </ul>

                <div class="tab_content_container">
                    <div class="tab_content tab_content_active" id="tabcontent1">
                        <form id="updateProfileForm" method="post" >
                            @csrf
                            <div class="inputcover">
                                <div class="inputwidthsetbox">
                                    <div class="form-group">
                                        <label for="usr">First Name*</label>
                                        <input type="text" name="first_name" class="form-control" id="" placeholder="Enter First Name" value="{{ $seller->first_name }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="usr">Last Name*</label>
                                        <input type="text" name="last_name" class="form-control" id="" placeholder="Enter Last Name" value="{{ $seller->last_name }}">
                                    </div>
                                </div>
                                <div class="inputwidthsetbox">
                                    <div class="form-group">
                                        <label for="usr">Email Address*</label>
                                        <input type="text" name="email" class="form-control" id="" placeholder="Enter Email Address" value="{{ $seller->email }}">
                                        @error('email')
                                        <label class="error">{{ $message }}</label>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="usr">Phone Number*</label>
                                        <input type="text" name="phone" class="form-control" id="" placeholder="Enter Phone Number" value="{{ $seller->phone }}">
                                        @error('phone')
                                        <label class="error">{{ $message }}</label>
                                        @enderror
                                    </div>
                                </div>
                                <hr>
                                <div class="inputwidthsetbox">                                
                                    <div class="form-group">
                                        <label for="usr">Timezone*</label>
                                        <select name="timezone_id" class="form-control" id="timezone_id" value="{{ old('timezone') }}">
                                            <option>Select Timezone</option>
                                            @foreach($timezones as $timezone)
                                            <option value="{{$timezone->id}}" @if($timezone->id == $seller->timezone_id) selected @endif>{{$timezone->timezone.' ('.$timezone->timezone_name.')'}}</option>
                                            @endforeach
                                        </select>
                                        <label id="placeError" class="error"></label>
                                    </div>                 
                                </div>
                                <div class="cretactboxbtn">
                                    <button type="submit" >UPDATE PROFILE</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtVAl5jn7PIWC6YSBK-VkIaneaKLIFExM&libraries=places"></script>

<script type="text/javascript">
    $('#updateProfileForm').validate({
        focusInvalid: true,
        ignore: "",
        rules: {
            first_name:{
                required: true
            },
            last_name:{
                required: true
            },
            email:{
                required: true,
                email:true
            },
            phone:{
                required: true,
                number:true
            },
            timezone_id:{
                required: true
            },
        },
        messages: {
            first_name: "Please enter first name",
            last_name:"Please enter last name",
            email:{
                'required':"Please enter email",
                'email':"Invalid email format"
            },
            phone:{
                'required':"Please enter phone",
            },
            timezone_id:"Please select timezone",
        },
        errorPlacement: function(error, element) {
            $(element).css({ "border": "#ffad47 1px solid" });
            $(element).siblings('.help-block').remove();
            if( element.prop('type') == 'select-one' ) {
                $(element).parent().append(error);
            }else if(element.prop('type') == 'checkbox'){
                console.log($(element).parent().siblings('.field_error'));
                $(element).parent().siblings('.field_error').append(error);
            }else{
                error.insertAfter(element);
            }
            if (element.attr("name") == "latitude")
            {
                $('#placeError').html(error);
            }
            if (element.attr("name") == "longitude")
            {
                $('#placeError').html(error);
            }
        },
        submitHandler: function(form) {
            var formData = $(form).serialize();     

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:'PUT',
                url:"{{route('admin.seller.update',$id)}}",
                data:formData,
                beforeSend: function(){
                },
                success:function(data) {
                    switch (data.status) {
                        case true:
                        toastr.success(data.message);
                        location.replace("{{route('admin.seller.show',$id)}}");
                        break;
                        case false:
                        toastr.warning(data.message);
                        break;
                        default:
                        toastr.error("You are not Authorized to access this page");
                        break;
                    }
                },
                complete: function(){
                }
            });
        }
    });
</script>
@endsection
