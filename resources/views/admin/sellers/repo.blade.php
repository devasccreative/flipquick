<div class="modal-body">
        <div class="form-group">
            <label>Matk As Repo</label>
         <label class="container">Yes
          <input type="radio" name="repo" value="1" @if($seller->repo == 1) checked @endif>
          <span class="checkmark"></span>
        </label>
        <label class="container">No
          <input type="radio" name="repo" value="0" @if($seller->repo == 0) checked @endif>
          <span class="checkmark"></span>
        </label>
     </div>
     <div id="changeRepo" @if($seller->repo == 0) style="display: none;" @endif>
          <label>Is Repo Charged?</label>
         <label class="container">Yes
          <input type="radio" name="repo_charged" value="charged" @if($seller->repo_charged == 'charged') checked @endif>
          <span class="checkmark"></span>
        </label>
        <label class="container">No
          <input type="radio" name="repo_charged" value="free" @if($seller->repo_charged == 'free') checked @endif>
          <span class="checkmark"></span>
        </label>
     </div>
     <div class="form-group" id="repoCharge" @if($seller->repo_charged == 'Free') style="display: none;" @endif>
          <label>Repo Charge($)</label>
        <input type="text" name="repo_charge" id="repo_charge" class="form-control" placeholder="Add Repo Charge" value="{{$seller->repo_charge}}">
    </div>
    <input type="hidden" name="s_id" value="{{$seller->id}}">