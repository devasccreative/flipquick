@section('title','Flipquick | Seller Dashboard')
@extends('admin.layouts.admin-app')
@section('styles')

@endsection
@section('content')
<div class="rightdetailpart">
    <div class="cpbcboxcover">
        <div class="cpbcboxinrpat">
            <div class="textcountbox">
                <div id="shiva"><span class="count">{{$seller->vehicles->where('steps_submitted',8)->count()}}</span></div>            
                <p>Total Vehicles</p>
                <img src="{{ asset('assets/svg/total-vehicles.svg') }}">
            </div>
        </div>
        <div class="cpbcboxinrpat">
            <div class="textcountbox">
                <div id="shiva"><span class="count">@if($seller->earning_amount) {{$seller->earning_amount}} @else 0 @endif </span></div>
                <p>Earning Amount</p>
                <img src="{{ asset('assets/svg/total-income.svg') }}">
            </div>
        </div>
    </div>
    <div class="boxandditboxcover">
        <div class="delspselimabox">
            <div class="wrapper">
                <ul class="tabs">
                    <li><a href="javascript:void(0);" rel="#tabcontent1" class="active">Profile Information</a></li>
                    <li>
                        <div class="pedapclfil">
                            <a href="{{ route('admin.seller.edit',$id) }}" >Edit Profile</a>
                        </div>
                    </li>
                </ul>

                <div class="tab_content_container">
                    <div class="tab_content tab_content_active" id="tabcontent1">
                        <div class="inputcover">
                            <div class="inputwidthsetfull">
                                <div class="profplanboxdit">
                                    <div class="form-group">
                                        <label for="usr"><span>Full Name 
                                        :</span> {{ $seller->first_name.' '.$seller->last_name }} </label>
                                        
                                        <label for="usr"><span>Email Address 
                                        :</span> {{ $seller->email }} </label>
                                        <label for="usr"><span>Phone Number 
                                        :</span> {{ $seller->country_code.'-'.$seller->phone }} </label>

                                    </div>                              
                                </div>                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
</script>
@endsection
