@section('title','Flipquick | Sellers List')
@extends('admin.layouts.admin-app')
@section('styles')
<style>
/* The container */
.container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default radio button */
.container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
  border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.container input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the indicator (dot/circle) when checked */
.container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the indicator (dot/circle) */
.container .checkmark:after {
    top: 9px;
    left: 9px;
    width: 8px;
    height: 8px;
    border-radius: 50%;
    background: white;
}
</style>
@endsection
@section('content')
<div class="rightdetailpart">
    <div class="boxandditboxcover">
        <div class="tabledetacoverbox">
            <h3>Sellers</h3>
            <table id="usersTbl" class="table table-striped table-bordered" style="width:100%">
                <thead>
                   <tr>
                        <th>SR</th>
                        <th>Created Date</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Repo</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<!-- edit modal -->
<div class="modal fade" id="makeEditModal" make="dialog">
 <div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Repo</h4>
</div>
<form id="editMakeFrm">
    @csrf
    @method('PUT')
    <div id="repo"></div>
    <div class="form-group">
        <button type="submit" class="btn btn-default">Update</button>   
    </div>
</form>
</div>
<div class="modal-footer">

</div>
</div>

</div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function() {
            //GET DATA
            var table = $('#usersTbl').DataTable({
                pageLength: 10,
                processing: true,
                responsive: true,
                serverSide: true,
                order: [],
                lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],

                ajax: {
                    type: "get",
                    url: "{{ route('admin.getSellerList') }}",
                    data: function ( d ) {
                        d._token = "{{ csrf_token() }}";
                    },
                    complete:function(){
                        if( $('[data-toggle="tooltip"]').length > 0 )
                            $('[data-toggle="tooltip"]').tooltip();
                    }
                },
                columns:[
                { data:'DT_RowIndex',name:'id' },
                { data:'created_at',name:'created_at' },
                { data:'name',name:'name' },
                { data:'email',name:'email' },
                { data:'phone',name:'phone'},
                { data:'repo',name:'repo'},
                { data:'action',name:'action'},
                ],
                columnDefs: [
                {
                    orderable: false,
                        // targets: [0,5],
                        "bSortable": false
                    }
                    ],

                });
    });
    $(document).on('change','input[name="repo"]',function(e){
        if($(this).val() == 1){
          $('#changeRepo').show();
        }else{
          $('#changeRepo').hide();
        }
    });
     $(document).on('change','input[name="repo_charged"]',function(e){
        if($(this).val() == 'charged'){
          $('#repoCharge').show();
        }else{
          $('#repoCharge').hide();
        }
    });
    function changeRepo(id){
        var url = "{{route('admin.seller.repo',':id')}}";
        url = url.replace(':id',id);
        $.ajax({
            url:url,
            type:"GET",
            // data:{id:id},
            success:function(data){
                $('#repo').html(data);
                    $("#makeEditModal").modal('show');

                // if (data.status==true) {              
                //     $("#editMakeName").val(data.data.make);
                //     $("#Makeid").val(data.data.id);
                // } 
            }
        });
    }
$('#editMakeFrm').validate({
    focusInvalid: true,
    ignore: "",
    rules: {
        repoCharge:{
            required: function(element){
              if($('input[name="repo_charged"]').val() == 'charged'){
                return true;
              }
              return false;
            }
        }
    },
    messages: {
        repoCharge: "Please enter make"
    },
    errorPlacement: function(error, element) {
        $(element).css({ "border": "#ffad47 1px solid" });
        $(element).siblings('.help-block').remove();
        if( element.prop('type') == 'select-one' ) {
            $(element).parent().append(error);
        }else if(element.prop('type') == 'checkbox'){
            console.log($(element).parent().siblings('.field_error'));
            $(element).parent().siblings('.field_error').append(error);
        }else{
            error.insertAfter(element);
        }
    },
    submitHandler: function(form) {
        var formData = $(form).serialize();     
        var id      = $("#Makeid").val();

        var url = "{{route('admin.seller.updaterepo')}}";
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'PUT',
            url:url,
            data:formData,
            beforeSend: function(){
                // $('#updateDoctorBtn').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    // table.ajax.reload();
                    $("#makeEditModal").modal('hide');
                    toastr.success(data.message);
                    // table.ajax.reload();
                    break;
                    case false:
                    toastr.warning(data.message);
                    break;
                    default:
                    toastr.error("You are not Authorized to access this page");
                    break;
                }
            },
            complete: function(){
                // $('#updateDoctorBtn').button('reset');
            }
        });
    }
});
    </script>
    @endsection