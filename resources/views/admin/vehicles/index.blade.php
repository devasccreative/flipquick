@section('title','Flipquick | vehicles List')
@extends('admin.layouts.admin-app')
@section('styles')
    
@endsection
@section('content')
    
    <div class="rightdetailpart">
        <div class="boxandditboxcover">
            <div class="tabledetacoverbox">
                <h3>Vehicles</h3>
                <table id="dataTable" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                         <tr>
                            <th>Sr.</th>
                            <th>Seller Name</th>
                            <th>VIN no</th>
                            <th>Year</th>
                            <th>Model</th>
                            <th>Starts Date</th>
                            <th>Expired Date</th>
                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>
                        
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    
    <script type="text/javascript">
        $(document).ready(function() {
            //$('#dataTable').DataTable();

            //GET DATA
            var table = $('#dataTable').DataTable({
                pageLength: 10,
                processing: true,
                responsive: true,
                serverSide: true,
                order: [],
                lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],

                ajax: {
                    type: "get",
                    url: "{{ route('admin.getVehiclesList') }}",
                    data: function ( d ) {
                        d._token = "{{ csrf_token() }}";
                    },
                    complete:function(){
                        if( $('[data-toggle="tooltip"]').length > 0 )
                            $('[data-toggle="tooltip"]').tooltip();
                    }
                },
                columns:[
                    { data:'DT_RowIndex',name:'id' },
                    { data:'seller_name',name:'seller_name' },
                    { data:'vin_no',name:'vin_no' },
                    { data:'year',name:'year' },
                    { data:'model',name:'model' },
                    { data:'starts_at',name:'starts_at' },
                    { data:'expired_at',name:'expired_at' },
                    // { data:'action',name:'action' },
                ],
                columnDefs: [
                    {
                        orderable: false,
                        // targets: [0,4],
                        // "bSortable": false
                    }
                ],

            });
        });
    </script>
@endsection