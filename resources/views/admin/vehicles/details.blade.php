@section('title','Flipquick | vehicles List')
@extends('admin.layouts.admin-app')
@section('styles')
    
@endsection
@section('content')
    
<div class="users_detateblebox">
    <div class="addbtnallset">
        <a href="" class="backbtn">Back</a>    
    </div>
    <div class="profile_innerdetlis">
        <div class="profile_titaltext">
            <h3>Vehicles Details</h3>
        </div>
        <div class="namedetlis_boxpart">
            <div class="leftname_detlis">
                <p>VIN no</p>
            </div>
            <div class="rightname_detlis">
                <p>{{ $vehicle->vin_no }}</p>   
            </div>
        </div>
        <div class="namedetlis_boxpart">
            <div class="leftname_detlis">
                <p>Year</p>
            </div>
            <div class="rightname_detlis">
                <p>{{ $vehicle->year }}</p>     
            </div>
        </div>
        <div class="namedetlis_boxpart">
            <div class="leftname_detlis">
                <p>Make</p>
            </div>
            <div class="rightname_detlis">
                <p>{{ $vehicle->make }}</p>    
            </div>
        </div>
        <div class="namedetlis_boxpart">
            <div class="leftname_detlis">
                <p>Model</p>
            </div>
            <div class="rightname_detlis">
                <p>{{ $vehicle->model }}</p>     
            </div>
        </div>
    </div> 
</div>
@endsection

@section('scripts')
    
@endsection