@section('title','Flipquick | Settings')
@extends('admin.layouts.admin-app')
@section('styles')
<style type="text/css">

</style>
@endsection
@section('content')
<div class="rightdetailpart">
    <div class="boxandditboxcover">
      <div class="delspselimabox">
        <div class="wrapper">
            <ul class="tabs">
                <li><a href="javascript:void(0);" rel="#tabcontent1" class=" active">Settings</a></li>
            </ul>

            <div class="tab_content_container">
                <div class="tab_content tab_content_active" id="tabcontent1">
                    <form id="maintenanceForm" method="post" >
                        @csrf
                        <div class="inputcover">
                            <div class="inputwidthsetbox">
                                <div class="maintanacradio_button">
                                    <label class="maintanaclbl">Maintenance Mode</label>
                                    <div class="switch-field">                  
                                        <input type="radio" id="switch_left1" name="maintenance_mode" value="0" @if(isset($maintenance->maintenance_mode) && $maintenance->maintenance_mode=="0"){{'checked'}}@endif>
                                        <label for="switch_left1">OFF</label>

                                        <input type="radio" id="switch_right2" name="maintenance_mode" value="1" @if(isset($maintenance->maintenance_mode) && $maintenance->maintenance_mode=="1"){{'checked'}}@endif>
                                        <label for="switch_right2">ON</label>
                                    </div>                          
                                </div>
                            </div>
                            <div class="inputwidthsetbox">
                                <div class="form-group">
                                    <label for="usr">Android App Version</label>
                                    <input type="text" name="android_version" class="form-control" placeholder="0.0" value="@if($maintenance != ''){{$maintenance->android_version}}@endif" required="">
                                </div>
                                <div class="form-group">
                                    <label for="usr">IOS App Version</label>
                                    <input type="text" name="ios_version" class="form-control" placeholder="0.0" value="@if($maintenance != ''){{$maintenance->ios_version}}@endif" required="">
                                </div>
                            </div>
                            <div class="cretactboxbtn">
                                    <button type="submit" >SAVE</button>
                                </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"> </script>

<script type="text/javascript">
    $(document).ready(function() {
            //Settings 
            $('#maintenanceForm').submit(function(e) {
                e.preventDefault();
            })
            .validate({
                focusInvalid: false, 
                ignore: "",
                rules: {
                    //payment_gateway: { required: true },
                    android_version: { required: true },
                    ios_version: { required: true },
                    maintenance_mode: { required: true },
                },
                submitHandler: function (form) {
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: '{{ route("admin.settings.update") }}',
                        data: formData,
                        processData:false,
                        cache:false,
                        contentType: false,
                        success: function(response)
                        {
                            if (response.status == true) {
                                toastr.success(response.message);
                                $('#daylight_timeLbl').text($('#daylight_time').val());
                                $('#midnight_timeLbl').text($('#midnight_time').val());
                            } else {
                                toastr.warning(response.message);
                            }
                        }
                    });
                }
            });
        });
    </script>
    @endsection