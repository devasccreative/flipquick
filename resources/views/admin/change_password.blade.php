@section('title','Flipquick | Change Password')
@extends('admin.layouts.admin-app')

@section('content')
<div class="rightdetailpart dashbordscroll">
    <div class="boxandditboxcover">
        <div class="delspselimabox">
            <div class="wrapper">
                <ul class="tabs">
                    <li><a href="javascript:void(0);" rel="#changePassword" class="active">Change Password</a></li>
                </ul>
                <div class="tab_content_container">
                    <div class="tab_content tab_content_active" id="tabcontent1">
                        <form id="resetPasswordForm" method="post" >
                            @csrf
                            <div class="inputcover">
                                <div class="inputwidthsetfull">
                                    <div class="form-group">
                                        <label for="usr">Old Password*</label>
                                        <input type="password" name="password" class="form-control" id="" placeholder="Enter Old Password">
                                    </div>
                                </div>
                                <div class="inputwidthsetbox">
                                    <div class="form-group">
                                        <label for="usr">New Password*</label>
                                        <input type="password" name="newPassword" class="form-control" id="newPassword" placeholder="Enter New Password">
                                    </div>
                                    <div class="form-group">
                                        <label for="usr">Confirm Password*</label>
                                        <input type="password" name="confirm_password" class="form-control" id="" placeholder="Confirm Your Password">
                                    </div>
                                </div>
                                <div class="cretactboxbtn">
                                    <button type="submit" >CHANGE PASSWORD</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtVAl5jn7PIWC6YSBK-VkIaneaKLIFExM&libraries=places"></script>

<script type="text/javascript">

    $("form#resetPasswordForm").validate({
        rules : {
            "password":{
                required : true
            },
            "newPassword":{
                required : true,
            },
            confirm_password:{
                required: true,
                equalTo:'#newPassword'
            },
        },
        messages : {
            "password":{
                required : "Please provide old password first."
            },
            "newPassword":{
                required : "Please enter new password.",
            },
            confirm_password:{
                'required':"Please confirm your password"
            },
        },
        errorPlacement: function(error, element) {
            $(element).css({ "border": "#ffad47 1px solid" });
            $(element).siblings('.help-block').remove();
            if( element.prop('type') == 'select-one' ) {
                $(element).parent().append(error);
            }else if(element.prop('type') == 'checkbox'){
                console.log($(element).parent().siblings('.field_error'));
                $(element).parent().siblings('.field_error').append(error);
            }else{
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var input = $("form#resetPasswordForm").find('input');
            var form = $("form#resetPasswordForm").serialize();
            $.ajax({
                url: "{{ route('admin.update_password') }}",
                type: 'POST',
                data: form,
                success:function(data){
                    if(data.status == true){
                        $(input).val('');
                        $("input[name=_token]").val("{{ csrf_token() }}");
                        $("#editProfile").css('color','#3ab772').siblings('button#savePassword').attr('disabled',true);
                        toastr.success(data.message);
                    }else{
                        toastr.error(data.message);
                    }
                }
            });             
        }
    });

</script>
@endsection
