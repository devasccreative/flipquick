@section('title','Flipquick | Make List')
@extends('admin.layouts.admin-app')
@section('styles')
    
@endsection
@section('content')
    <div class="rightdetailpart">
  <div class="boxandditboxcover">
    <div class="tabledetacoverbox">
      <h3>Makes</h3>
      <div class="pedapclfil">
        <a href="javascript:void(0);" id="pendingDealers" data-toggle="modal" data-target="#addMakeModal">Add Make</a>
        <a href="javascript:void(0);" id="claerFilter" style="display:none;">Back</a>
        <input type="hidden" name="is_approved" id="is_approved">
      </div>
      <br/>
      <table id="usersTbl" class="table table-striped table-bordered responsive nowrap" style="width:100%">
        <thead>
          <tr>
                        <th>SR</th>
                        <th>MAKE</th>
                        <th>CREATED AT</th>
                        <th>MORE</th>
                    </tr>
      </thead>
    </table>
  </div>
</div>
</div>

    <!-- Modal -->
<div class="modal fade" id="addMakeModal" make="dialog">
 <div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Add Make</h4>
</div>
<div class="modal-body">
    <form id="addMakeFrm">
        <div class="form-group">
            <input type="text" name="make" id="makeName" class="form-control" placeholder="Add Make Name">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default">Add</button>  
        </div>
    </form>
</div>
<div class="modal-footer">

</div>
</div>

</div>
</div>


<!-- edit modal -->
<div class="modal fade" id="makeEditModal" make="dialog">
 <div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Edit Make</h4>
</div>
<form id="editMakeFrm">
    @csrf
    @method('PUT')
    <div class="modal-body">
        <div class="form-group">
         <input type="hidden" name="id" id="Makeid" class="form-control" >
     </div>
     <div class="form-group">
        <input type="text" name="make" id="editMakeName" class="form-control" placeholder="Add Make Name">
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-default">Update</button>   
    </div>
</form>
</div>
<div class="modal-footer">

</div>
</div>

</div>
</div>
@endsection

@section('scripts')
    
    <script type="text/javascript">
        //$(document).ready(function() {
            //$('#dataTable').DataTable();

            //GET DATA
            var table = $('#usersTbl').DataTable({
                pageLength:10,
                serverSide: true,
                // lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
                ajax: {
                    type: "get",
                    url: "{{ route('admin.makes.makeList') }}",
                    data: function ( d ) {
                        d._token = "{{ csrf_token() }}";
                    },
                    complete:function(){
                        if( $('[data-toggle="tooltip"]').length > 0 )
                            $('[data-toggle="tooltip"]').tooltip();
                    }
                },
                columns:[
                    { data:'DT_RowIndex',name:'id' },
                    { data:'make',name:'make' },
                    { data:'created_at',name:'created_at'},
                    { data:'action',name:'action', orderable:false, searchable:false },
                ],
                
            });
        //});
        $('#addMakeFrm').validate({
    focusInvalid: true,
    ignore: "",
    rules: {
        make:{
            required: true
        }
    },
    messages: {
        make: "Please enter make"
    },
    errorPlacement: function(error, element) {
        $(element).css({ "border": "#ffad47 1px solid" });
        $(element).siblings('.help-block').remove();
        if( element.prop('type') == 'select-one' ) {
            $(element).parent().append(error);
        }else if(element.prop('type') == 'checkbox'){
            console.log($(element).parent().siblings('.field_error'));
            $(element).parent().siblings('.field_error').append(error);
        }else{
            error.insertAfter(element);
        }
    },
    submitHandler: function(form) {
        var formData = $(form).serialize();     

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'POST',
            url:"{{route('admin.makes.store')}}",
            data:formData,
            beforeSend: function(){
                // $('#updateDoctorBtn').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    table.ajax.reload();
                    $("#addMakeModal").modal('hide');
                    toastr.success(data.message);
                    table.ajax.reload();
                    break;
                    case false:
                    toastr.warning(data.message);
                    break;
                    default:
                    toastr.error("You are not Authorized to access this page");
                    break;
                }
            },
            complete: function(){
                // $('#updateDoctorBtn').button('reset');
            }
        });
    }
});

  $(document).on('click','.deleteMake',function(e){
    $('.deleteMake').confirmation({
        container:"body",
        btnOkLabel:'OK',
        btnCancelLabel:"CANCEL",
        btnOkClass:"btn btn-sm btn-success",
        btnCancelClass:"btn btn-sm btn-danger",
        placement:'top',
        onConfirm:function(event, element) {
            event.preventDefault();
            var valid = element.data('id');
            var url = "{{route('admin.makes.destroy',':id')}}";
            url = url.replace(':id',valid);
            $.ajax({
                type: "DELETE",
                data:{'_token':'{{ csrf_token() }}'},
                url: url,
                success: function(response)
                {
                    if (response.status == true) {
                        table.ajax.reload();
                        toastr.success(response.message);
                    } else {
                        toastr.warning(response.message);
                    }
                }
            });
        }
    });
});


  function EditMake(id){
    var url = "{{route('admin.makes.edit',':id')}}";
    url = url.replace(':id',id);
    $.ajax({
        url:url,
        type:"GET",
        data:{id:id},
        success:function(data){

           if (data.status==true) {              
            $("#editMakeName").val(data.data.make);
            $("#Makeid").val(data.data.id);
            $("#makeEditModal").modal('show');

        } 
    }
});
}
$('#editMakeFrm').validate({
    focusInvalid: true,
    ignore: "",
    rules: {
        make:{
            required: true
        }
    },
    messages: {
        make: "Please enter make"
    },
    errorPlacement: function(error, element) {
        $(element).css({ "border": "#ffad47 1px solid" });
        $(element).siblings('.help-block').remove();
        if( element.prop('type') == 'select-one' ) {
            $(element).parent().append(error);
        }else if(element.prop('type') == 'checkbox'){
            console.log($(element).parent().siblings('.field_error'));
            $(element).parent().siblings('.field_error').append(error);
        }else{
            error.insertAfter(element);
        }
    },
    submitHandler: function(form) {
        var formData = $(form).serialize();     
        var id      = $("#Makeid").val();

        var url = "{{route('admin.makes.update',':id')}}";
        url = url.replace(':id',id);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'PUT',
            url:url.replace(':id', id),
            data:formData,
            beforeSend: function(){
                // $('#updateDoctorBtn').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    table.ajax.reload();
                    $("#makeEditModal").modal('hide');
                    toastr.success(data.message);
                    table.ajax.reload();
                    break;
                    case false:
                    toastr.warning(data.message);
                    break;
                    default:
                    toastr.error("You are not Authorized to access this page");
                    break;
                }
            },
            complete: function(){
                // $('#updateDoctorBtn').button('reset');
            }
        });
    }
});
    </script>
@endsection