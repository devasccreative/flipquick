<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title','Flipquick | Admin Panel')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=""/>
    <meta name="application-name" content="&nbsp;"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ asset('favicon/apple-touch-icon-57x57.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('favicon/apple-touch-icon-114x114.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('favicon/apple-touch-icon-72x72.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('favicon/apple-touch-icon-144x144.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{{ asset('favicon/apple-touch-icon-60x60.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{ asset('favicon/apple-touch-icon-120x120.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{ asset('favicon/apple-touch-icon-76x76.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ asset('favicon/apple-touch-icon-152x152.png') }}" />
    <link rel="icon" type="image/png" href="{{ asset('favicon/favicon-196x196.png') }}" sizes="196x196" />
    <link rel="icon" type="image/png" href="{{ asset('favicon/favicon-96x96.png') }}" sizes="96x96" />
    <link rel="icon" type="image/png" href="{{ asset('favicon/favicon-32x32.png') }}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{ asset('favicon/favicon-16x16.png') }}" sizes="16x16" />
    <link rel="icon" type="image/png" href="{{ asset('favicon/favicon-128.png') }}" sizes="128x128" />
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css" rel="Stylesheet"></link>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}">
    <link href="{{ asset('assets/css/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/lightgallery.css') }}">
    

    @yield('styles')
</head>
<body>

    @if(Auth::guard('admin')->check())
    @include('admin.layouts.header')
    <div class="leftbar_rightbarcover">
        @if(!Route::is('admin.dealer.edit_profile')) 
        @include('admin.layouts.left-sidebar')
        @endif
        @yield('content')
    </div>
    @else
    @yield('content')
    @endif


</body>

<script type="text/javascript" src="{{ asset('js/all.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lightgallery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="{{ asset('js/lg-thumbnail.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lg-fullscreen.min.js') }}"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://sachinchoolur.github.io/lightGallery/lightgallery/js/lg-zoom.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
@yield('scripts')
<script type="text/javascript">
    function slider(){
        $(".tabs li a").click(function() {

                // Active state for tabs
                $(".tabs li a").removeClass("active");
                $(this).addClass("active");
                
                // Active state for Tabs Content
                $(".tab_content_container > .tab_content_active").removeClass("tab_content_active").fadeOut(200);
                $(this.rel).fadeIn(500).addClass("tab_content_active");
                
            }); 
        var $imagesSlider = $(".gallery-slider .gallery-slider__images>div"),
        $thumbnailsSlider = $(".gallery-slider__thumbnails>div");

        // images options
        $imagesSlider.slick({
            speed:300,
            slidesToShow:1,
            slidesToScroll:1,
            cssEase:'linear',
            fade:true,
            draggable:false,
            asNavFor:".gallery-slider__thumbnails>div",
            prevArrow:'.gallery-slider__images .prev-arrow',
            nextArrow:'.gallery-slider__images .next-arrow'
        });

        // thumbnails options
        $thumbnailsSlider.slick({
            speed:300,
            slidesToShow:4,
            slidesToScroll:1,
            cssEase:'linear',
            centerMode:true,
            draggable:false,
            focusOnSelect:true,
            asNavFor:".gallery-slider .gallery-slider__images>div",
            prevArrow:'.gallery-slider__thumbnails .prev-arrow',
            nextArrow:'.gallery-slider__thumbnails .next-arrow',
            responsive: [
            {
                breakpoint: 720,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 350,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
            ]
        });

        /*captions*/

        var $caption = $('.gallery-slider .caption');

        // get the initial caption text
        var captionText = $('.gallery-slider__images .slick-current img').attr('alt');
        updateCaption(captionText);

        // hide the caption before the image is changed
        $imagesSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
            $caption.addClass('hide');
        });

        // update the caption after the image is changed
        $imagesSlider.on('afterChange', function(event, slick, currentSlide, nextSlide){
            captionText = $('.gallery-slider__images .slick-current img').attr('alt');
            updateCaption(captionText);
        });
        function updateCaption(text) {
            // if empty, add a no breaking space
            if (text === '') {
                text = '&nbsp;';
            }
            $caption.html(text);
            $caption.removeClass('hide');
        }
    }

</script>
</html>
<!-- {{ exit }} -->

