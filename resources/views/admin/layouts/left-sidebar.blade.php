<div class="leftsidebarbox">
	<div class="menuboxsidbar">
		<ul>
			@if(Route::is('admin.dashboard') || Route::is('admin.seller.index') || Route::is('admin.dealer.index') || Route::is('admin.vehicles.index') || Route::is('admin.vehicles.show') || Route::is('admin.sellerOrders') || Route::is('admin.changePassword') || Route::is('admin.settings.index') || Route::is('admin.reports') || Route::is('admin.report') || Route::is('admin.makes.index'))
			<li class="{{ Route::is('admin.dashboard') ? 'active' : '' }}">
				<a href="{{ route('admin.dashboard') }}">
					<img src="{{ asset('assets/svg/FlipQuick_dashboard_active.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/FlipQuick_dashboard.svg') }}" alt="" class="inactive_icon">
					<span>Dashboard</span>
				</a>
			</li>
			<li class="{{ Route::is('admin.seller.index') ? 'active' : '' }}">
				<a href="{{ route('admin.seller.index') }}">
					<img src="{{ asset('assets/svg/FlipQuick_seller_active.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/FlipQuick_seller.svg') }}" alt="" class="inactive_icon">
					<span>Sellers</span>
				</a>
			</li>
			<li class="{{ Route::is('admin.dealer.index') ? 'active' : '' }}">
				<a href="{{ route('admin.dealer.index') }}">
					<img src="{{ asset('assets/svg/FlipQuick_dealers_active.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/FlipQuick_dealers.svg') }}" alt="" class="inactive_icon">
					<span>Dealers</span>
				</a>
			</li>
			{{--<li class="{{ Route::is('admin.vehicles.index') ? 'active' : '' }}">
				<a href="{{ route('admin.vehicles.index') }}">
					<img src="{{ asset('assets/svg/FlipQuick_current_listings_active.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/FlipQuick_current_listings.svg') }}" alt="" class="inactive_icon">
					<span>Vehicles</span>
				</a>
			</li>--}}
			<li class="{{ Route::is('admin.sellerOrders') ? 'active' : '' }}">
				<a href="{{ route('admin.sellerOrders') }}">
					<img src="{{ asset('assets/svg/FlipQuick_subscription_management_active.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/FlipQuick_subscription_management.svg') }}" alt="" class="inactive_icon">
					<span>Payments</span>
				</a>
			</li>
				<li class="{{ Route::is('admin.settings.index') ? 'active' : '' }}">
				<a href="{{ route('admin.settings.index') }}">
					<img src="{{ asset('assets/svg/FlipQuick_account_settings_active.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/FlipQuick_account_settings.svg') }}" alt="" class="inactive_icon">
					<span>Settings</span>
				</a>
			</li>
			<li class="{{ Route::is('admin.reports') ? 'active' : '' }}">
				<a href="{{ route('admin.reports') }}">
					<img src="{{ asset('assets/svg/warning-svgrepo-com1.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/warning-svgrepo-com.svg') }}" alt="" class="inactive_icon">
					<span>Reports</span>
				</a>
			</li>
			<li class="{{ Route::is('admin.makes.index') ? 'active' : '' }}">
				<a href="{{ route('admin.makes.index') }}">
					<img src="{{ asset('assets/svg/FlipQuick_submitted_bids_active.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/FlipQuick_submitted_bids.svg') }}" alt="" class="inactive_icon">
					<span>Makes</span>
				</a>
			</li>
			@elseif(Route::is('admin.seller.show') || Route::is('admin.seller.current_listing') || Route::is('admin.seller.past_listing') || Route::is('admin.seller.edit'))
			<li class="{{ Route::is('admin.seller.show') ? 'active' : '' }}">
				<a href="{{ route('admin.seller.show',$id) }}">
					<img src="{{ asset('assets/svg/FlipQuick_dashboard_active.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/FlipQuick_dashboard.svg') }}" alt="" class="inactive_icon">
					<span>Seller Dashboard</span>
				</a>
			</li>
			<li class="{{ Route::is('admin.seller.current_listing') ? 'active' : '' }}">
				<a href="{{ route('admin.seller.current_listing',$id) }}">
					<img src="{{ asset('assets/svg/FlipQuick_current_listings_active.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/FlipQuick_current_listings.svg') }}" alt="" class="inactive_icon">
					<span>Vehicle Listing</span>
				</a>
			</li>
			
			{{--<li class="{{ Route::is('admin.seller.past_listing') ? 'active' : '' }}">
				<a href="{{ route('admin.seller.past_listing',$id) }}">
					<img src="{{ asset('assets/svg/FlipQuick_past_listings_active.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/FlipQuick_past_listings.svg') }}" alt="" class="inactive_icon">
					<span>Seller Past Listing</span>
				</a>
			</li>--}}
			<div class="backbtnboxset">
				<a href="@if(Route::is('admin.seller.edit')) {{ route('admin.seller.show',$id) }} @else {{ route('admin.seller.index') }} @endif" class="">
					<img src="{{ asset('assets/svg/FlipQuick_back_arrow.svg') }}" alt="">
					<span>Back</span>
				</a>
			</div>
			@else
			<li class="{{ Route::is('admin.dealer.dashboard') ? 'active' : '' }}">
				<a href="{{ route('admin.dealer.dashboard',$id) }}">
					<img src="{{ asset('assets/svg/FlipQuick_dashboard_active.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/FlipQuick_dashboard.svg') }}" alt="" class="inactive_icon">
					<span>Dealer Dashboard</span>
				</a>
			</li>
			<li class="{{ Route::is('admin.dealer.submitted_bids') ? 'active' : '' }}">
				<a href="{{ route('admin.dealer.submitted_bids',$id) }}">
					<img src="{{ asset('assets/svg/FlipQuick_submitted_bids_active.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/FlipQuick_submitted_bids.svg') }}" alt="" class="inactive_icon">
					<span>Dealer Submitted Bids</span>
				</a>
			</li>
			<li class="{{ Route::is('admin.dealer.won_bids') ? 'active' : '' }}">
				<a href="{{ route('admin.dealer.won_bids',$id)}}">
					<img src="{{ asset('assets/svg/FlipQuick_won_bids_active.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/FlipQuick_won_bids.svg') }}" alt="" class="inactive_icon">
					<span>Dealer Won Bids</span>
				</a>
			</li>
			<li class="{{ Route::is('admin.dealer.payment_detail') ? 'active' : '' }}">
				<a href="{{ route('admin.dealer.payment_detail',$id)}}">
					<img src="{{ asset('assets/svg/FlipQuick_dealers_active.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/FlipQuick_dealers.svg') }}" alt="" class="inactive_icon">
					<span>Account Information</span>
				</a>
			</li>
			<li class="{{ Route::is('admin.dealer.reported_post') ? 'active' : '' }}">
				<a href="{{ route('admin.dealer.reported_post',$id) }}">
					<img src="{{ asset('assets/svg/warning-svgrepo-com1.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/warning-svgrepo-com.svg') }}" alt="" class="inactive_icon">
					<span>Reported Posts</span>
				</a>
			</li>
			<li class="{{ Route::is('admin.dealer.edit_profile') ? 'active' : '' }}">
				<a href="{{ route('admin.dealer.edit_profile',$id) }}">
					<img src="{{ asset('assets/svg/FlipQuick_dealers_active.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/FlipQuick_dealers.svg') }}" alt="" class="inactive_icon">
					<span>Edit Profile</span>
				</a>
			</li>
			<li class="{{ Route::is('admin.dealer.login_as_dealer') ? 'active' : '' }}">
				<a href="{{ route('admin.dealer.login_as_dealer',$id)}}" target="_blank">
					<img src="{{ asset('assets/svg/FlipQuick_logout_active.svg') }}" alt="" class="active_icon">
					<img src="{{ asset('assets/svg/FlipQuick_logout.svg') }}" alt="" class="inactive_icon">
					<span>Login As Dealer</span>
				</a>
			</li>

			<div class="backbtnboxset">
				<a href="{{ route('admin.dealer.index') }}" class="">
					<img src="{{ asset('assets/svg/FlipQuick_back_arrow.svg') }}" alt="">
					<span>Back</span>
				</a>
			</div>
			@endif
		</ul>
	</div>
</div>


<!-- <div class="@if(Route::currentRouteName() == 'admin.users.show')  leftmenu_boxs @else leftmenu_boxs @endif">
	<div class="menuinnerbox_part">
		<ul>

			@if(Route::is('admin.dashboard') || Route::is('admin.seller.index') || Route::is('admin.dealer.index'))
			<li class="{{ Route::is('admin.dashboard') ? 'active' : '' }}">
				<a href="{{ route('admin.dashboard') }}" class="mainactbox">
					<span>Dashboard</span>
				</a>
			</li>
			<li class="{{ Route::is('admin.seller.index') ? 'active' : '' }}">
				<a href="{{ route('admin.seller.index') }}" class="mainactbox">
					<span>Sellers</span>
				</a>
			</li>
			<li class="{{ Route::is('admin.dealer.index') ? 'active' : '' }}">
				<a href="{{ route('admin.dealer.index') }}" class="mainactbox">
					<span>Dealers</span>
				</a>
			</li>
			<li class="{{ Route::is('admin.vehicles.index') ? 'active' : '' }}">
				<a href="{{ route('admin.vehicles.index') }}" class="mainactbox">
					<span>Vehicles</span>
				</a>
			</li>
			@else
				<li id="acountDetailli" class="active">
					<a  href="javascript:void(0);" id="acountDetailShow" class="mainactbox">
						<span>Account info</span>
					</a>
				</li>
			@endif
		</ul>
	</div>
</div> -->