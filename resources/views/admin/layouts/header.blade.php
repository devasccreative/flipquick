
<header>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="headercovercl">
            <div class="">              
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <!-- <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> -->
                        <img src="{{ asset('assets/svg/hamburger.svg') }}" alt="" class="hamburgerbtn">
                        <img src="{{ asset('assets/svg/hamburger-close.svg') }}" alt="" class="hamburger_closebtn">
                    </button>
                    <a class="navbar-brand" href="{{ route('admin.dashboard') }}"><img src="{{ asset('assets/svg/logo.svg') }}" alt=""></a>
                </div>
                
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="usermenuboxset">
                      <div>
                        <input type="checkbox" id="check">
                        <label for="check">{{ Auth::user()->name }} <img src="{{ asset('assets/svg/carat-icon.svg') }}"></label>
                        <div class="navbox">
                            <ul>
                                <li> 
                                    <a href="{{ route('admin.changePassword') }}" >Change Password</a>
                                </li>
                                <li> 
                                    <a href="{{ route('admin.logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">Log Out</a>
                                </li>
                            </ul>                       
                        </div>
                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                        @csrf
                        </form>
                        @yield('navHeader')
                      </div>
                    </div>
                </div>              
            </div>
        </div>
        
    </nav>
</header>

