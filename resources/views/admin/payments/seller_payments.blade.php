@section('title','Flipquick | Payment History')
@extends('admin.layouts.admin-app')
@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" /> 

@endsection
@section('content')

<div class="rightdetailpart dashbordscroll">
    <div class="cpbcboxcover">
     <div class="cpbcboxinrpat">
        <div class="textcountbox">
            <div id="shiva"><span class="count">{{$earning_amount_seller}}</span></div>
            <p>Total Earning From Seller</p>
            <img src="{{ asset('assets/svg/total-income.svg') }}">
        </div>
    </div>
    <div class="cpbcboxinrpat">
        <div class="textcountbox">
            <div id="shiva"><span class="count">{{$earning_amount_dealer}}</span></div>
            <p>Total Earning From Dealer</p>
            <img src="{{ asset('assets/svg/total-income.svg') }}">
        </div>
    </div>
</div>
<div class="boxandditboxcover pamtboxcover">
    <div class="tabledetacoverbox">
        <h3>Payments</h3>
        <form id="filterRecord" method="post" >
            @csrf
            <div class="inputcover filtboxcover">
                <div class="inputwidthsetbox">
                    <div class="form-group">
                        <input type="text" id="datefilter"  class="form-control" autocomplete="off" name="datefilter" value="" placeholder="DD/MM/YYYY - DD/MM/YYYY" />
                    </div>
                    <div class="form-group">
                     <div class="cretactboxbtn">
                        <button type="submit" >FILTER</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <table id="paymentHistory" class="table table-striped table-bordered" style="width:100%">
        <thead>
           <tr>
            <th>Sr.</th>
            <th>Transaction Id</th>
            <th>Amount</th>
            <th>Seller Name</th>
            <th>VIN no</th>
            <th>Tansaction Date</th>
            <!-- <th>Action</th> -->
        </tr>
    </thead>
</table>
</div>
</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script type="text/javascript">
   $(document).ready(function() {
            //$('#dataTable').DataTable();
            $(function() {

                $('input[name="datefilter"]').daterangepicker({
                    autoUpdateInput: false,
                    autoApply:true,
                    locale: {
                        cancelLabel: 'Clear'
                    }
                });

                $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
                    $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                });

                $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
                    $(this).val('');
                });

            });
            $('#filterRecord').submit(function(e){
                e.preventDefault();
                table.draw();
            });

            //GET DATA
            var table = $('#paymentHistory').DataTable({
                pageLength: 10,
                serverSide: true,
                lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                {
                  extend: 'excel',
                  className:'cexpobtn',
                  text: function ( dt, button, config ) {
                    return dt.i18n( 'buttons.excel', '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Report' );
                },
                exportOptions: {
                    columns: "thead th:not(.noExport)",
                    modifier: {
                      page: 'all',
                      search: 'none'   
                  }
              }
          }
          ],
          ajax: {
            type: "post",
            url: "{{ route('admin.seller_orders_list') }}",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.start_date=$('#datefilter').val();
            },
            complete:function(){
                if( $('[data-toggle="tooltip"]').length > 0 )
                    $('[data-toggle="tooltip"]').tooltip();
            }
        },
        columns:[
        { data:'DT_RowIndex',name:'id' },
        { data:'order_id',name:'order_id' },
        { data:'amount',name:'amount' },
        { data:'seller_name',name:'seller_name' },
        { data:'vin_no',name:'vin_no' },
        { data:'created_at',name:'created_at' },
        ],
        columnDefs: [
        {
            orderable: false,
        }
        ],

    });
        });
    </script>
    @endsection