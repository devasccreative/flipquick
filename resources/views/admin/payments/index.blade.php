@section('title','Flipquick | Earning')
@extends('admin.layouts.admin-app')
@section('styles')

@endsection
@section('content')
<div class="rightdetailpart">
    <div class="boxandditboxcover">
     <div class="addbtnallset">
        <a href="{{ route('admin.dashboard') }}" class="">Back</a>
    </div>
    <br/>
    <div class="boxandditbox_left">
        <div class="congrttextbox">
            <h3>Total Earnings</h3>
            <p>${{$earning_amount}} </p>
        </div>
        <!-- <div class="veiableboxset"></div> -->
    </div>
</div>
</div>
</div>

@endsection

@section('scripts')
<script>
</script>
@endsection
