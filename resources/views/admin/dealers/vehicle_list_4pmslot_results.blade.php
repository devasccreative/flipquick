@forelse($data['current_listing_4pm_slot'] as $vehicle)
<?php // echo "<pre>"; print_r($vehicle->bids);die(); ?>
<div class="userimgtextbox">
  @if($vehicle->vehicle_exterior_photos->count() > 0)
  <div class="userimgbox">
    <img src="{{ $vehicle->vehicle_exterior_photos[0]->image }}" alt="">
  </div>
  @endif
  <div class="usertextbox">
    <a href="javascript:void(0)" class="bids_list" data-id="{{ $vehicle->id }}"><h4>{{ $vehicle->year.' '.$vehicle->make.' '.$vehicle->trim.' '.$vehicle->style.' '.$vehicle->model }}</h4></a>
    <p><span class="vehicleVIN">VIN : {{$vehicle->vin_no}}</span> </p>
    <p><span>{{ $vehicle->milage }} kms </span>
      {{--<img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""><span class="bidsCount">{{ $vehicle->bids_count }} @if( $vehicle->bids_count>1) Bids @else Bid @endif</span>--}}
    </p>
    <p><span>{{ $vehicle->drive_train }} </span><img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""><span>{{ ucfirst($vehicle->transmission) }}</span></p>
  </div>
   @if($vehicle->bids->count() > 0)
    <div class="usertextinrbtnbox">
        <p><span>Bid Placed by <b>{{ $vehicle->bids[0]['dealer']['first_name'] }}</b> : </span> <span class="bidsCount">${{$vehicle->bids[0]['amount']}}</span> </p>
    </div>
    @endif
  <div class="listplacebtnbox">
   <a href="javascript:void(0)" class="listingditbtn bids_list" data-id="{{ $vehicle->id }}">LISTING DETAILS</a>
 </div>
 
</div>
@empty
<div class="userimgtextbox">
  <div class="usertextbox">
    <p>No Data Found</p>
  </div>
</div>
@endforelse