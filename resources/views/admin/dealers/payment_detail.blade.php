@section('title','Flipquick | Account Information')
@extends('admin.layouts.admin-app')
@section('styles')

@endsection
@section('content')
<div class="rightdetailpart">
    <div class="boxandditboxcover">
        <div class="delspselimabox">
            <div class="wrapper">
                <ul class="tabs">
                    <li><a href="javascript:void(0);" rel="#tabcontent1" class="active">Profile Information</a></li>
                    <li><a href="javascript:void(0);" rel="#planInfo" >Plan Information</a></li>
                </ul>

                <div class="tab_content_container">
                    <div class="tab_content tab_content_active" id="tabcontent1">
                        <div class="inputcover">
                            <div class="inputwidthsetfull">
                                <div class="profplanboxdit">
                                    <div class="form-group">
                                        <label for="usr"><span>Name:</span> {{ $dealer->first_name.' '.$dealer->last_name }}</label>
                                        <label for="usr"><span>Email Address:</span> {{ $dealer->email }}</label>
                                        <label for="usr"><span>Phone Number:</span> {{ $dealer->country_code.'-'.$dealer->phone }}</label>
                                        <label for="usr"><span>Address:</span> {{ $dealer->address }}</label>
                                        <label for="usr"><span>Dealership Name:</span> {{ $dealer->dealership_name }}</label>
                                        <label for="usr"><span>Dealer No.:</span> {{ $dealer->dealer_no }}</label>
                                        <label for="usr"><span>AMVIC No.:</span> {{ $dealer->amvic_no }}</label>

                                    </div>                              
                                </div>                              
                            </div>
                            
                        </div>
                    </div>
                    <div class="tab_content" id="planInfo"> 
                      <div class="inputcover">
                        <div class="inputwidthsetfull">
                            <div class="profplanboxdit">
                                <div class="form-group">
                                  <label for="usr"><span>Name:</span> @if($profile) {{ $profile['billing']['name'] }} @endif</label>
                                  <label for="usr"><span>Email Address:</span> @if($profile) {{ $profile['billing']['email_address'] }} @endif</label>
                                  <label for="usr"><span>Phone Number:</span> @if($profile) {{ $profile['billing']['phone_number'] }} @endif</label>
                                  <label for="usr"><span>Address:</span> @if($profile) {{ $profile['billing']['address_line1'] }} @endif</label>
                                  <label for="usr"><span>Membership Plan :</span> @if($membership_plan) {{ $membership_plan->plan_name }} - {{ $membership_plan->billing_cycle_name }} @if($membership_plan->discount != '') ({{ $membership_plan->discount }}) @endif @endif</label>
                                  <label><span>Next Billing Date:</span> @if($membership_plan) {{ $membership_plan->expiry_date }} @endif</label>
                              </div>
                          </div>
                      </div>
                      <div class="backnextbtnset">
                        <div class="subbtncancel"> <a href="javascript:void(0)" class="cancelSub" id="cancelPlan" data-toggle="confirmation" data-title="Are you sure?">CANCEL SUBSCRIPTION</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection

@section('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtVAl5jn7PIWC6YSBK-VkIaneaKLIFExM&libraries=places"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/imask/3.4.0/imask.min.js"></script>
<script src="{{ asset('assets/js/card.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });

    $(".tabs li a").click(function() {

                // Active state for tabs
                $(".tabs li a").removeClass("active");
                $(this).addClass("active");
                
                // Active state for Tabs Content
                $(".tab_content_container > .tab_content_active").removeClass("tab_content_active").fadeOut(200);
                $(this.rel).fadeIn(500).addClass("tab_content_active");
                
            }); 
    $('#cancelPlan').confirmation({
        container:"body",
        btnOkLabel:'OK',
        btnCancelLabel:"CANCEL",
        btnOkClass:"btn btn-sm btn-success",
        btnCancelClass:"btn btn-sm btn-danger",
        placement:'top',
        onConfirm:function(event, element) {
            event.preventDefault();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:'POST',
                url:"{{route('admin.dealer.cancel_subscription',$id)}}",
                beforeSend: function(){
                // $('#updateDoctorBtn').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    toastr.success(data.message);
                    location.replace("{{route('admin.dealer.index')}}");
                    break;
                    case false:
                    toastr.warning(data.message);
                    break;
                    default:
                    toastr.error("You are not Authorized to access this page");
                    break;
                }
            },
            complete: function(){
                // $('#updateDoctorBtn').button('reset');
            }
        });
        }
    });
</script>
@endsection
