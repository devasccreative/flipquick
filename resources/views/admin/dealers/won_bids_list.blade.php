@section('title','Flipquick | Dealer Wons Bids List')
@extends('admin.layouts.admin-app')
@section('styles')

@endsection
@section('content')
<div class="rightdetailpart">
    <div class="boxandditboxcover">
        <div class="boxandditbox_left">             
            <div class="hurryupbox">
                <h3>Won Bids</h3>
                <p>{{ $total }} {{($total > 1) ? 'bids' : 'bid'}} </p>                  
            </div>
            <div class="lisditboxset" id="vehicleResult" page="{{$lastPage}}">
                @include('admin.dealers.wonbids_list_results')
            </div>
        </div>
        <?php  $allImage=[]; ?>
        <div class="boxandditbox_right" id="sidebar">
            <div class="selectcarboxiner">
                <div class="nodetaboxset">
                    <img src="{{ asset('assets/svg/FlipQuick_install_our_mobile_app.svg') }}" alt="">
                    <h4>Select a vehicle <br>
                    listing to view details</h4>
                </div>                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    var allImage = {!! json_encode($allImage,JSON_UNESCAPED_SLASHES) !!};
    $(document).on('click','.animated-thumbnials', function(e) {
        e.preventDefault();
        $(this).lightGallery({
            dynamic: true,
            thumbnail:true,
            animateThumb: false,
            showThumbByDefault: false,
            dynamicEl:allImage,
        }); 
    });
    var page = 1;
    var loadMorePage = true;
    // $(function() {
    //     $(window).scroll(function() {
    //         var $this = $('#vehicleResult');
    //         $results  = $('#vehicleResult');
    //         // if ($this.scrollTop() + $this.height() >= $results.height()) {
    //             if ($(window).scrollTop() == $(document).height() - $(window).height()) {
    //                 page++;
    //                 if(page <= $("#vehicleResult").attr('page')){
    //                     loadMoreData(page);
    //                 }else{
    //                     $('.ajax-load').show();
    //                     $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
    //                 }
    //             }
    //         });
    // });
    function loadMoreData(page) {
        if(loadMorePage){
            $.ajax({
                url: '?page=' + page,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data){
                if(data.html == " "){
                    $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    return;
                }
                $('.ajax-load').hide();
                $("#vehicleResult").append(data.html);
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                loadMorePage = false;
            });
        }
    }
    $(document).ready(function() {
        $(document).on('click','.bids_list',function(){
            var id = $(this).attr('data-id');
            $.ajax({
                type: "Get",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route("admin.dealer.view_bid_detail",[$id,""]) }}/'+id,
                success: function(response)
                {
                    $('.boxandditbox_right').html(response);
                    slider();
                }
            });
        }); 
        slider();
    });
</script>
@endsection