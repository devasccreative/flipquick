@forelse($bids as $bid)
<div class="submittedbidsbox">
    <div class="submittedbids_userdit">
        @if(isset($bid->vehicle->vehicle_exterior_photos[0]))
        <div class="sbuserdeta_left">
            <img src="{{ $bid->vehicle->vehicle_exterior_photos[0]->image }}" alt="">
        </div>
        @endif

        <div class="sbuserdeta_right">
            <a href="javascript:void(0)" class="bids_list" data-id="{{ $bid->vehicle->id }}"><h4>{{ $bid->vehicle->year.' '.$bid->vehicle->make.' '.$bid->vehicle->trim.' '.$bid->vehicle->style.' '.$bid->vehicle->model }}</h4></a>
            <p><span class="vehicleVIN">VIN : {{$bid->vehicle->vin_no}}</span> </p>
            <p><span>{{ $bid->vehicle->milage }} kms </span>
            {{-- <img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""><span class="bidsCount">{{$bid->vehicle->bids->count()}} Bids</span> --}}
            </p>
            <p><span>{{ $bid->vehicle->drive_train }} </span><img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""><span>{{ $bid->vehicle->transmission }}</span></p>
        </div>
    </div>
    <div class="sbboxdatetime">
        <p>Bid submitted on {{$bid->created_date}} by {{$bid->dealer->first_name}}</p>
        <div class="form-group">
            <input type="text" class="form-control" id="" placeholder="" value="Your Bid Amount was ${{ $bid->amount }}" disabled>
        </div>  
        <div class="sellerinfobox">
            <p>Seller Info.</p>
            <h5>{{$bid->vehicle->seller->first_name.' '.$bid->vehicle->seller->last_name}}</h5>
            <h6><img src="{{ asset('assets/svg/FlipQuick_phone_icon.svg') }}" alt=""> <span>{{$bid->vehicle->seller->country_code.'-'.$bid->vehicle->seller->phone}}</span></h6>
            <h6><img src="{{ asset('assets/svg/FlipQuick_message_icon.svg') }}" alt=""> <span>{{$bid->vehicle->seller->email}}</span></h6>
        </div>
        <div class="listplacebtnbox">
            <a href="javascript:void(0)" class="listingditbtn bids_list" data-id="{{ $bid->vehicle->id }}">LISTING DETAILS</a>
            <!-- <a href="javascript:void(0)" class="placebidbtn">SAVE AS PDF</a> -->
        </div>
    </div>
</div>
@empty
<div class="submittedbidsbox">
    <div class="sbboxdatetime">
        <p>No Data Found</p>
    </div>
</div>
@endforelse