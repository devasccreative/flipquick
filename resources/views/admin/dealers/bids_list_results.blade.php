@forelse($bids as $bid)
<div class="userimgtextbox">
    @if(isset($bid->vehicle->vehicle_exterior_photos[0]))
    <div class="userimgbox">
        <img src="{{ $bid->vehicle->vehicle_exterior_photos[0]->image }}" alt="">
    </div>
    @endif
    <div class="usertextbox">
        <a href="javascript:void(0)" class="bids_list" data-id="{{ $bid->vehicle->id }}"><h4>{{ $bid->vehicle->year.' '.$bid->vehicle->make.' '.$bid->vehicle->trim.' '.$bid->vehicle->style.' '.$bid->vehicle->model }}</h4></a>
            <p><span class="vehicleVIN">VIN : {{$bid->vehicle->vin_no}}</span> </p>

        <p><span class="vehicleVIN">VIN : {{$bid->vehicle->vin_no}}</span> </p>
        <p><span>{{ $bid->vehicle->milage }} kms </span><img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""><span class="bidsAmount">${{ $bid->amount }}</span></p>
        <p><span>{{ $bid->vehicle->drive_train }} </span><img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""><span>{{ $bid->vehicle->transmission }}</span></p>
    </div>
    <div class="listplacebtnbox">
        <a href="javascript:void(0)" class="listingditbtn bids_list" data-id="{{ $bid->vehicle->id }}">LISTING DETAILS</a>
        @if($bid->count() > 0)
        <div class="usertextinrbtnbox">
            <p><span>Bid Placed by <b>{{$bid['dealer']['first_name']}}</b> : </span><span class="bidsCount">  ${{$bid['amount']}}</span> </p>
        </div>
        @endif
    </div>
</div>
@empty
<div class="userimgtextbox">
    <p>No data found</p>
</div>
@endforelse
