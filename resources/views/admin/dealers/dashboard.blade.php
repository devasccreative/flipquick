@section('title','Flipquick | Dealer Dashboard')
@extends('admin.layouts.admin-app')
@section('styles')

@endsection
@section('content')
<div class="rightdetailpart dashbordscroll crtlistbox">
   <div class="cpbcboxcover">
    <div class="cpbcboxinrpat">
        <div class="textcountbox">
            <div id="shiva"><span class="count">{{$data['total_income']}}</span></div>            
            <p>Total Amount</p>
            <img src="{{ asset('assets/svg/total-income.svg') }}">
        </div>
    </div>
    <div class="cpbcboxinrpat">
        <div class="textcountbox">
            <div id="shiva"><span class="count">{{$data['total_bids']}}</span></div>
            <p>Total Bid Submitted</p>
            <img src="{{ asset('assets/svg/total-dealers.svg') }}">
        </div>
    </div>
    <div class="cpbcboxinrpat">
        <div class="textcountbox">
            <div id="shiva"><span class="count">{{$data['lifetime_wons_bids']}}</span></div>
            <p>Total Won bids</p>
            <img src="{{ asset('assets/svg/total-sellers.svg') }}">
        </div>
    </div>
</div>
<div class="boxandditboxcover" id="sidebarWrap">
    <div class="boxandditbox_left" id="sidebar">
        @if($data['last_wons_bids'] > 0)
        <div class="congrttextbox">
            <h3>Congratulations !</h3>
            <p>You won {{$data['last_wons_bids']}} {{ ($data['last_wons_bids']>1) ? 'bids' : 'bid' }} in your previous bidding slot.</p>
            <a href="{{ route('dealer.won_bids') }}">VIEW SELLER INFO</a>
        </div>
        @endif
        
        <div class="timetabbox">
            <div class="wrapper">
                <ul class="tabs" id="tabs2">
                    <li><a href="javascript:void(0); return false;" rel="#tabcontent5" class="tab active">Expires at 9:00 AM</a></li>
                    <li><a href="javascript:void(0); return false;" rel="#tabcontent6" class="tab">Expires at 4:00 PM</a></li>
                    <li><a href="javascript:void(0); return false;" rel="#tabcontent7" class="tab">Repo</a></li>
                </ul>
                <div class="tab_content_container" id="tab_content2">
                    <div class="tab_content tab_content_active" id="tabcontent5">                        
                        <div class="hurryupbox" @if($total_9am_slot < 1) style="display:none;" @endif>
                            <ul>
                                <li>
                                    <h4 id="hours9am">00<!-- {{ gmdate('h', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Hours</p>
                                </li>
                                <li>
                                    <h4 id="minutes9am">00<!-- {{ gmdate('i', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Minutes</p>
                                </li>
                                <li>
                                    <h4 id="seconds9am">00<!-- {{ gmdate('s', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Seconds</p>
                                </li>
                            </ul>
                        </div>
                        <div class="veiableboxset">
                            <div class="veiablebox_title">
                                <?php
                                ?>
                                <h3>{{ $total_9am_slot }} {{ ($total_9am_slot > 1) ? 'VEHICLES' : 'VEHICLE' }}  AVAILABLE</h3>
                            </div>
                            <div class="vehiclesdetaboxcover">
                                <div class="vehiclesdetaboxiner" id="vehicleResult9am" page="{{$lastPage_9am_slot}}">
                                    @include('admin.dealers.vehicle_list_9amslot_results')
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab_content" id="tabcontent6">                                  
                        <div class="hurryupbox" @if($total_4pm_slot < 1) style="display:none;" @endif>
                            <ul>
                                <li>
                                    <h4 id="hours4pm">00<!-- {{ gmdate('h', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Hours</p>
                                </li>
                                <li>
                                    <h4 id="minutes4pm">00<!-- {{ gmdate('i', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Minutes</p>
                                </li>
                                <li>
                                    <h4 id="seconds4pm">00<!-- {{ gmdate('s', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Seconds</p>
                                </li>
                            </ul>
                        </div>
                        <div class="veiableboxset">
                            <div class="veiablebox_title">
                                <?php
                                ?>
                                <h3>{{ $total_4pm_slot }} {{ ($total_4pm_slot > 1) ? 'VEHICLES' : 'VEHICLE' }}  AVAILABLE</h3>
                            </div>
                            <div class="vehiclesdetaboxcover"  id="vehicleData">
                                <div class="vehiclesdetaboxiner" id="vehicleResult4pm" page="{{$lastPage_4pm_slot}}">
                                    @include('admin.dealers.vehicle_list_4pmslot_results')
                                </div>
                            </div>
                        </div>
                    </div>   
                    <div class="tab_content " id="tabcontent7">                        
                        <div class="hurryupbox" @if($total_repo_slot < 1) style="display:none;" @endif>
                            <ul>
                                <li>
                                    <h4 id="hoursrepo">00<!-- {{ gmdate('h', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Hours</p>
                                </li>
                                <li>
                                    <h4 id="minutesrepo">00<!-- {{ gmdate('i', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Minutes</p>
                                </li>
                                <li>
                                    <h4 id="secondsrepo">00<!-- {{ gmdate('s', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Seconds</p>
                                </li>
                            </ul>
                        </div>
                        <div class="veiableboxset">
                            <div class="veiablebox_title">
                                <?php
                                ?>
                                <h3>{{ $total_repo_slot }} {{ ($total_repo_slot > 1) ? 'VEHICLES' : 'VEHICLE' }}  AVAILABLE</h3>
                            </div>
                            <div class="vehiclesdetaboxcover">
                                <div class="vehiclesdetaboxiner" id="vehicleResultrepo" page="{{$lastPage_repo_slot}}">
                                    @include('admin.dealers.vehicle_list_repo_results')
                                </div>
                            </div>
                        </div>
                    </div>                 
                </div>
            </div>
        </div>

    </div>
    <div class="boxandditbox_right" id="sidebar">
        <div class="selectcarboxiner">
            <?php     $allImage=[]; ?>

            @if($total_9am_slot > 0)
            <?php $vehicle = $data['current_listing_9am_slot'][0]; ?>
            <div class="carsliderbox">
                <div class="gallery-slider">

                    <div class="gallery-slider__images sldimgbox">
                        <?php
                        $vehicleExteriorPhotos = $vehicle->vehicle_exterior_photos->pluck('image')->toArray();
                        $vehicleDamagePhoto = $vehicle->vehicle_damage_photos->pluck('image')->toArray();
                        $vehicleInteriorPhoto = $vehicle->vehicle_interior_photos->pluck('image')->toArray();
                        $vehicleAllImages = array_merge($vehicleExteriorPhotos,$vehicleInteriorPhoto,$vehicleDamagePhoto);
                        $vehicleAllImage = array_merge($vehicleExteriorPhotos,$vehicleInteriorPhoto,$vehicleDamagePhoto);

                        ?>
                        <div>
                            @foreach($vehicleAllImage as $key => $image)
                            <?php $allImage[] = ['src' => $image,'thumb'=>$image,'subHtml'=>'']; ?>
                            <div class="item">
                                <div class="img-fill animated-thumbnials">
                                    <a href="{{ $image }}">
                                        <img src="{{ $image }}" alt="Kitten {{ $key }}">
                                    </a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>


                    <div class="gallery-slider__thumbnails sldthumbnailsbox">
                        <div>
                            @foreach($vehicleAllImage as $key => $image)

                            <div class="item">
                                <div class="img-fill"><img src="{{ $image }}" alt="Kitten {{ $key }}"></div>
                            </div>
                            @endforeach

                        </div>
                        <div class="backnextbtn">
                            <button class="prev-arrow slick-arrow nextpribtnbox">
                                BACK
                            </button>
                            <button class="next-arrow slick-arrow nextpribtnbox">
                                NEXT
                            </button>
                        </div>                              

                    </div>
                </div>
            </div>

            <div class="sldboxtitle">
                <h4>{{ $vehicle->year.' '.$vehicle->make.' '.$vehicle->trim.' '.$vehicle->style }}</h4>
                <p><span>Exterior : {{ $vehicle->exterior_color }}</span> <img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""> <span>Interior : {{ $vehicle->interior_color }}</span></p>
            </div>
            <div class="sedoseboxset">
                <ul>
                    <li>{{ $vehicle->body_shape }}</li>
                    <li>{{ $vehicle->doors }} Doors</li>
                    <li>{{ $vehicle->passangers }} Amount Of Seats</li>
                </ul>
            </div>
            <div class="midetrboxsetiner">
                <ul>
                    <li>
                        <img src="{{ asset('assets/svg/FlipQuick_milage.svg') }}" alt="">
                        <p>Milage <br>{{ $vehicle->milage }} kms</p>
                    </li>
                    <li>
                        <img src="{{ asset('assets/svg/FlipQuick_drive_train.svg') }}" alt="">
                        <p>Drive Train <br>{{ $vehicle->drive_train }}</p>
                    </li>
                    <li>
                        <img src="{{ asset('assets/svg/FlipQuick_transmission.svg') }}" alt="">
                        <p>Transmission <br>{{ ucfirst($vehicle->transmission) }}</p>
                    </li>
                    <li>
                        <img src="{{ asset('assets/svg/car-engine.svg') }}" alt="">
                        <p>Engine <br>{{ ucfirst($vehicle->engine) }}</p>
                    </li>
                </ul>
            </div>
            <div class="tabboxtsetcover">
                <div class="wrapper">
                    <ul class="tabs" id="tabs1">
                        <li><a href="javascript:void(0); return false;" rel="#tabcontent1" class="tab active">options</a></li>
                        <li><a href="javascript:void(0); return false;" rel="#tabcontent2" class="tab">additional info</a></li>
                        <li><a href="javascript:void(0); return false;" rel="#tabcontent3" class="tab">vehicle condition</a></li>
                    </ul>

                    <div class="tab_content_container" id="tab_content1">
                        <div class="tab_content tab_content_active" id="tabcontent1">                        
                            <p><label>Air Conditioning:</label> {{ ($vehicle->air_conditioning=="1")?'Yes':'No' }}</p>
                            <p><label>Navigation System:</label> {{ ($vehicle->navigation_system=="1")?'Yes':'No' }}</p>
                            <p><label>Leather Seats:</label> {{ ($vehicle->leather_seats=="1")?'Yes':'No' }}</p>
                            <p><label>Sun Roof:</label> {{ ($vehicle->sun_roof=="1")?'Yes':'No' }}</p>
                            <p><label>Panoramic Roof:</label> {{ ($vehicle->panoramic_roof=="1")?'Yes':'No' }}</p>
                            <p><label>DVD:</label> {{ ($vehicle->dvd=="1")?'Yes':'No' }}</p>
                            <p><label>Heated Seats:</label> {{ ($vehicle->heated_seats=="1")?'Yes':'No' }}</p>
                            <p><label>AC Seats:</label> {{ ($vehicle->ac_seats=="1")?'Yes':'No' }}</p>
                            <p><label>360 Camera:</label> {{ ($vehicle['360_camera']=="1")?'Yes':'No' }}</p>
                            <p><label>Engine :</label> {{ $vehicle->engine }}</p>
                            <p><label>Cab Type :</label> {{ ($vehicle->style) ? $vehicle->style : '-' }}</p>
                            <p><label>Fuel Type :</label> {{ $vehicle->made_in }}</p>
                        </div>

                        <div class="tab_content" id="tabcontent2">                                  
                            <p><label>After Market Equipment:</label> {{ ($vehicle->after_market_equipment)?$vehicle->after_market_equipment:'-' }}</p>
                            <p><label>Original Owner:</label> {{ ($vehicle->original_owner==1)?'Yes':'No' }}</p>
                            <p><label>Still Making Monthly Payments:</label> {{ ($vehicle->still_making_monthly_payment=="1")?'Yes':'No' }} </p>
                            <p><label>Still Owe:</label> {{ $vehicle->still_own }} </p>
                            <p><label>Have Any Accident Claims?:</label> {{ ($vehicle->have_any_accident_record=='1')?'Yes':'No' }} </p>
                            <p><label>How Much Are The Claims:</label> {{ ($vehicle->how_much_records > 1)?$vehicle->how_much_records:0 }}</p>
                            <p><label>Smoked In:</label> {{ ($vehicle->smoked_in=="1")?'Yes':'No' }}</p>
                            <p><label>Modified Exhaust :</label> {{ ($vehicle->modified_exhaust=="1")?'Yes':'No' }}</p>
                            <p><label>Sell / Trade :</label> {{ ($vehicle->wantto_sell_or_tradein)?$vehicle->wantto_sell_or_tradein:'-' }}</p>

                        </div>  
                        <div class="tab_content" id="tabcontent3">                                  
                            <p><label>Overall Vehicle Condition:</label> {{ $vehicle->vehicle_condition }}/10</p>
                            <p><label>Condition of Front Tires:</label> {{ $vehicle->front_tires_condition }}</p>
                            <p><label>Condition of Rear Tires:</label> {{ $vehicle->back_tires_condition }}</p>
                            <p><label>Warning Lights:</label> {{ ($vehicle->warning_lights=="1")?'Yes':'No' }}</p>
                            <p><label>Mechanical Issues:</label> {{ ($vehicle->mechanical_issues=="1")?'Yes':'No' }}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
</div>

@endsection

@section('scripts')
<script>
    var allImage = {!! json_encode($allImage,JSON_UNESCAPED_SLASHES) !!};
    $(document).on('click','.animated-thumbnials', function(e) {
        e.preventDefault();
        $(this).lightGallery({
            dynamic: true,
            thumbnail:true,
            animateThumb: false,
            showThumbByDefault: false,
            dynamicEl:allImage,
        }); 
    });
//9am slot
var page = 1;
var loadMorePage = true;
$(function() {
    $('#vehicleResult9am').scroll(function() {
        var $this = $(this);
        $results  = $('#vehicleResult9am');
        if ($this.scrollTop() + $this.height() >= $results.height()) {
        // if ($(window).scrollTop() == $(document).height() - $(window).height()) {
         page++;
         if(page <= $("#vehicleResult9am").attr('page')){
            loadMoreData(page);
        }else{
            $('.ajax-load').show();
            $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
        }
    }
});
});
function loadMoreData(page) {
    if(loadMorePage){
        $.ajax({
            url: '?page9am=' + page,
            type: "get",
            beforeSend: function()
            {
                $('.ajax-load').show();
            }
        })
        .done(function(data){
            if(data.html == " "){
                $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                return;
            }
            $('.ajax-load').hide();
            $("#vehicleResult9am").append(data.html);
        })
        .fail(function(jqXHR, ajaxOptions, thrownError){
            loadMorePage = false;
        });
    }
}
 //4pm slot
 var page = 1;
 var loadMorePage = true;
 $(function() {
    $('#vehicleResult4pm').scroll(function() {
        var $this = $(this);
        $results  = $('#vehicleResult4pm');
        if ($this.scrollTop() + $this.height() >= $results.height()) {
        // if ($(window).scrollTop() == $(document).height() - $(window).height()) {
         page++;
         if(page <= $("#vehicleResult4pm").attr('page')){
            loadMoreData4pm(page);
        }else{
            $('.ajax-load').show();
            $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
        }
    }
});
});
 function loadMoreData4pm(page) {
    if(loadMorePage){
        $.ajax({
            url: '?page4pm=' + page,
            type: "get",
            beforeSend: function()
            {
                $('.ajax-load').show();
            }
        })
        .done(function(data){
            if(data.html == " "){
                $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                return;
            }
            $('.ajax-load').hide();
            $("#vehicleResult4pm").append(data.html);
        })
        .fail(function(jqXHR, ajaxOptions, thrownError){
            loadMorePage = false;
        });
    }
}
var page = 1;
 var loadMorePage = true;
 $(function() {
    $('#vehicleResultrepo').scroll(function() {
        var $this = $(this);
        $results  = $('#vehicleResultrepo');
        if ($this.scrollTop() + $this.height() >= $results.height()) {
        // if ($(window).scrollTop() == $(document).height() - $(window).height()) {
         page++;
         if(page <= $("#vehicleResultrepo").attr('page')){
            loadMoreDatarepo(page);
        }else{
            $('.ajax-load').show();
            $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
        }
    }
});
});
 function loadMoreDatarepo(page) {
    if(loadMorePage){
        $.ajax({
            url: '?page4pm=' + page,
            type: "get",
            beforeSend: function()
            {
                $('.ajax-load').show();
            }
        })
        .done(function(data){
            if(data.html == " "){
                $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                return;
            }
            $('.ajax-load').hide();
            $("#vehicleResultrepo").append(data.html);
        })
        .fail(function(jqXHR, ajaxOptions, thrownError){
            loadMorePage = false;
        });
    }
}
</script>
<script type="text/javascript">
    $(document).ready(function() {
        /* TIMER */
        var upgradeTime = parseInt("{{ $data['nine_am_slot_time_remaining']  }}");
        var seconds = upgradeTime;
        // function timer() {
            setInterval(function(){ 
                var days        = Math.floor(seconds/24/60/60);
                var hoursLeft   = Math.floor((seconds) - (days*86400));
                var hours       = Math.floor(hoursLeft/3600);
                var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
                var minutes     = Math.floor(minutesLeft/60);
                var remainingSeconds = seconds % 60;
                function pad(n) {
                    return (n < 10 ? "0" + n : n);
                }
                $('#hours9am').text(hours);
                $('#minutes9am').text(minutes);
                $('#seconds9am').text(remainingSeconds);
                if (seconds == 0) {
                    $('#hours9am').text(hours);
                    $('#minutes9am').text(minutes);
                    $('#seconds9am').text(remainingSeconds);
                } else {
                    seconds--;
                }
            },1000);

            var upgradeTime4pm = parseInt("{{ $data['four_pm_slot_time_remaining']  }}");
            var seconds4pm = upgradeTime4pm;
        // function timer() {
            setInterval(function(){ 
                var days        = Math.floor(seconds4pm/24/60/60);
                var hoursLeft   = Math.floor((seconds4pm) - (days*86400));
                var hours       = Math.floor(hoursLeft/3600);
                var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
                var minutes     = Math.floor(minutesLeft/60);
                var remainingSeconds = seconds4pm % 60;
                function pad(n) {
                    return (n < 10 ? "0" + n : n);
                }
                $('#hours4pm').text(hours);
                $('#minutes4pm').text(minutes);
                $('#seconds4pm').text(remainingSeconds);
                if (seconds4pm == 0) {
                    $('#hours4pm').text(hours);
                    $('#minutes4pm').text(minutes);
                    $('#seconds4pm').text(remainingSeconds);
                } else {
                    seconds4pm--;
                }
            },1000);
             var upgradeTimerepo = parseInt("{{ $data['nine_am_slot_time_remaining']  }}");
            var secondsrepo = upgradeTimerepo;
        // function timer() {
            setInterval(function(){ 
                var days        = Math.floor(secondsrepo/24/60/60);
                var hoursLeft   = Math.floor((secondsrepo) - (days*86400));
                var hours       = Math.floor(hoursLeft/3600);
                var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
                var minutes     = Math.floor(minutesLeft/60);
                var remainingSeconds = secondsrepo % 60;
                function pad(n) {
                    return (n < 10 ? "0" + n : n);
                }
                $('#hoursrepo').text(hours);
                $('#minutesrepo').text(minutes);
                $('#secondsrepo').text(remainingSeconds);
                if (secondsrepo == 0) {
                    $('#hoursrepo').text(hours);
                    $('#minutesrepo').text(minutes);
                    $('#secondsrepo').text(remainingSeconds);
                } else {
                    secondsrepo--;
                }
            },1000);
            /* END TIMER */
            $(document).on('click','.bids_list',function(e){
                e.preventDefault();
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "Get",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ route("dealer.view_bid_detail","/") }}/'+id,
                    success: function(response)
                    {
                        $('.boxandditbox_right').html(response);
                        slider();
                        console.log(response);
                    }
                });
            }) 
        });
    $(document).on('click','.placeBid',function(e){
       e.preventDefault();
       $('.modal-title').text('Place A Bid For - '+$(this).data('name'));
       $('#vehicle_id').val($(this).data('id'));
       $('#placeBidModal').modal('show');
   });
    $(document).on('click','.shareVehicle',function(e){
       e.preventDefault();
       $('.modal-title').text('Share - '+$(this).data('name'));
       $('#vehicleId').val($(this).data('id'));
       $('#shareVehicleModal').modal('show');
   });
    jQuery.validator.addMethod("rageIn50", function(value, element) {
        return (value%50 == 0);
    },"Please enter bid amount by $50 increments");
    $('#placeBidFrm').validate({
        focusInvalid: true,
        ignore: "",
        rules: {
            amount:{
                required: true,
                number:true,
                rageIn50:true
            },
            vehicle_id:{
                required: true
            },
        },
        messages: {
            amount: {
                required : "Please enter bid amount",
            }
        },
        submitHandler: function(form) {
            var formData = $(form).serialize();     

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:'POST',
                url:"{{route('dealer.submitABid')}}",
                data:formData,
                beforeSend: function(){
                // $('#updateDoctorBtn').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    toastr.success(data.message);
                    location.reload();
                    break;
                    case false:
                    toastr.warning(data.message);
                    break;
                    default:
                    toastr.error("You are not Authorized to access this page");
                    break;
                }
            },
            complete: function(){
                // $('#updateDoctorBtn').button('reset');
            }
        });
        }
    });

    $('#shareVehicleFrm').validate({
        focusInvalid: true,
        ignore: "",
        rules: {
            email:{
                required: true,
                email:true
            },
        },
        messages: {
            email: {
                required:"Please enter email",
            },
        },
        submitHandler: function(form) {
            var formData = $(form).serialize();     

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:'POST',
                url:"{{route('dealer.shareVehicle')}}",
                data:formData,
                beforeSend: function(){
                // $('#updateDoctorBtn').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    toastr.success(data.message);
                    location.reload();
                    break;
                    case false:
                    toastr.warning(data.message);
                    break;
                    default:
                    toastr.error("You are not Authorized to access this page");
                    break;
                }
            },
            complete: function(){
                // $('#updateDoctorBtn').button('reset');
            }
        });
        }
    });
</script>
<script>
    function slider(){
        $(".tabs li a").click(function() {

                    // Active state for tabs
                    $(".tabs li a").removeClass("active");
                    $(this).addClass("active");
                    
                    // Active state for Tabs Content
                    $(".tab_content_container > .tab_content_active").removeClass("tab_content_active").fadeOut(200);
                    $(this.rel).fadeIn(500).addClass("tab_content_active");
                    
                }); 
        var $imagesSlider = $(".gallery-slider .gallery-slider__images>div"),
        $thumbnailsSlider = $(".gallery-slider__thumbnails>div");

        // images options
        $imagesSlider.slick({
            speed:300,
            slidesToShow:1,
            slidesToScroll:1,
            cssEase:'linear',
            fade:true,
            draggable:false,
            asNavFor:".gallery-slider__thumbnails>div",
            prevArrow:'.gallery-slider__images .prev-arrow',
            nextArrow:'.gallery-slider__images .next-arrow'
        });

        // thumbnails options
        $thumbnailsSlider.slick({
            speed:300,
            slidesToShow:4,
            slidesToScroll:1,
            cssEase:'linear',
            centerMode:true,
            draggable:false,
            focusOnSelect:true,
            asNavFor:".gallery-slider .gallery-slider__images>div",
            prevArrow:'.gallery-slider__thumbnails .prev-arrow',
            nextArrow:'.gallery-slider__thumbnails .next-arrow',
            responsive: [
            {
                breakpoint: 720,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 350,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
            ]
        });

        /*captions*/

        var $caption = $('.gallery-slider .caption');

        // get the initial caption text
        var captionText = $('.gallery-slider__images .slick-current img').attr('alt');
        updateCaption(captionText);

        // hide the caption before the image is changed
        $imagesSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
            $caption.addClass('hide');
        });

        // update the caption after the image is changed
        $imagesSlider.on('afterChange', function(event, slick, currentSlide, nextSlide){
            captionText = $('.gallery-slider__images .slick-current img').attr('alt');
            updateCaption(captionText);
        });

        
    }
    slider();
    function updateCaption(text) {
            // if empty, add a no breaking space
            if (text === '') {
                text = '&nbsp;';
            }
            $caption.html(text);
            $caption.removeClass('hide');
        }
    </script>
    @endsection
