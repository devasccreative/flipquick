@forelse($bids as $bid)
<div class="submittedbidsbox" id="vehicle{{ $bid->vehicle_id }}">
    <div class="submittedbids_userdit">
        @if(isset($bid->image))
        <div class="sbuserdeta_left">
            <img src="{{ $bid->image }}" alt="">
        </div>
        @endif

        <div class="sbuserdeta_right">
            <a href="javascript:void(0)" class="bids_list" data-id="{{ $bid->vehicle->id }}"><h4>{{ $bid->vehicle->year.' '.$bid->vehicle->make.' '.$bid->vehicle->trim.' '.$bid->vehicle->style.' '.$bid->vehicle->model }}</h4></a>
            <p><span class="vehicleVIN">VIN : {{$bid->vehicle->vin_no}}</span> </p>
            <p><span>{{ $bid->vehicle->milage }} kms </span>
            </p>
            <p><span>{{ $bid->vehicle->drive_train }} </span><img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""><span>{{ $bid->vehicle->transmission }}</span></p>
        </div>
    </div>
    <div class="sbboxdatetime">
        <div class="sellerinfobox">
            <p><span>Problem </span>: <span>{{ $bid->title }}</span></p>
            <p><span>Description </span>: <span>{{ $bid->description }}</span></p>
            <p>Seller Info.</p>
            <h5>{{$bid->vehicle->seller->first_name.' '.$bid->vehicle->seller->last_name}}</h5>
            <h6><img src="{{ asset('assets/svg/FlipQuick_phone_icon.svg') }}" alt=""> <span>{{$bid->vehicle->seller->country_code.'-'.$bid->vehicle->seller->phone}}</span></h6>
            <h6><img src="{{ asset('assets/svg/FlipQuick_message_icon.svg') }}" alt=""> <span>{{$bid->vehicle->seller->email}}</span></h6>


        </div>
        <div class="listplacebtnbox">
            <a href="javascript:void(0)" class="placebidbtn bids_list" data-id="{{ $bid->vehicle_id }}">LISTING DETAILS</a>
            <a href="javascript:void(0)" class="listingditbtn rmvListing" data-id="{{ $bid->vehicle_id }}" style="background:#ff000;">REMOVE LISTING</a>
        </div>
    </div>
</div>
@empty
<div class="submittedbidsbox">
    <div class="sbboxdatetime">
        <p>No Data Found</p>
    </div>
</div>
@endforelse