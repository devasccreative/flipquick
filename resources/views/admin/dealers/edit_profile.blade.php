@section('title','FlipQuick | Account Settings')
@extends('admin.layouts.admin-app')
@section('styles')
<style type="text/css">
.customcheck.col-md-4 {
    width: 33%;
    padding-left: 5px;
}
.customcheck.col-md-4 {
    padding-bottom: 5px;
}
.tab_content_active span{
    width: 20px !important;
}
</style>

@endsection
@section('content')

<div class="boxandditboxcover">
    <div class="delspselimabox">
        <div class="wrapper">
            <div class="backbtnboxset">
                <a href="{{ route('admin.dealer.dashboard',$id) }}" class="">
                    <img src="{{ asset('assets/svg/FlipQuick_back_arrow.svg') }}" alt="">
                    <span>Back</span>
                </a>
            </div>
            <ul class="tabs" id="tabs">
                <li><a href="javascript:void(0);" rel="#tabcontent1" class=" active">Edit Profile</a></li>
            </ul>

            <div class="tab_content_container">
                <div class="tab_content tab_content_active" id="tabcontent1">
                    <form id="updateProfileForm" method="post" >
                        @csrf
                        <div class="inputcover">
                            <div class="inputwidthsetbox">
                                <div class="form-group">
                                    <label for="usr">First Name*</label>
                                    <input type="text" name="first_name" class="form-control" id="" placeholder="Enter First Name" value="{{ $dealer->first_name }}">
                                </div>
                                <div class="form-group">
                                    <label for="usr">Last Name*</label>
                                    <input type="text" name="last_name" class="form-control" id="" placeholder="Enter Last Name" value="{{ $dealer->last_name }}">
                                </div>
                            </div>
                            <div class="inputwidthsetbox">
                                <div class="form-group">
                                    <label for="usr">Email Address*</label>
                                    <input type="text" name="email" class="form-control" id="" placeholder="Enter Email Address" value="{{ $dealer->email }}">
                                    @error('email')
                                    <label class="error">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="usr">Phone Number*</label>
                                    <input type="text" name="phone" class="form-control" id="" placeholder="Enter Phone Number" value="{{ $dealer->phone }}">
                                    @error('phone')
                                    <label class="error">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                            @if($dealer->parent_id == 0)
                            <hr>
                            <div class="inputwidthsetbox">
                                <div class="form-group">
                                    <label for="usr">Dealership Name*</label>
                                    <input type="text" name="dealership_name" class="form-control" id="" placeholder="Enter Dealership Name" value="{{ $dealer->dealership_name }}">
                                </div>
                                <div class="form-group">
                                    <label for="usr">Dealership Phone Number*</label>
                                    <input type="text" name="dealer_no" class="form-control" id="" placeholder="Enter Dealership Phone Number" value="{{ $dealer->dealer_no }}">
                                </div>
                            </div>
                            <div class="inputwidthsetbox">                                
                                <div class="form-group">
                                    <label for="usr">AMVIC Number*</label>
                                    <input type="text" name="amvic_no" class="form-control" id="" placeholder="Enter AMVIC Number" value="{{ $dealer->amvic_no }}">
                                </div>
                                <div class="form-group">
                                    <label for="usr">Timezone*</label>
                                    <select name="timezone_id" class="form-control" id="timezone_id" value="{{ old('timezone') }}">
                                        <option>Select Timezone</option>
                                        @foreach($timezones as $timezone)
                                        <option value="{{$timezone->id}}" @if($timezone->id == $dealer->timezone_id) selected @endif>{{$timezone->timezone.' ('.$timezone->timezone_name.')'}}</option>
                                        @endforeach
                                    </select>
                                    <label id="placeError" class="error"></label>
                                </div>                 
                            </div>
                            <div class="inputwidthsetfull">
                                <div class="form-group">
                                    <label for="usr">Choose Makes*</label>
                                    @foreach($makes as $make)
                                    <div class="customcheck col-md-4">
                                       <label class="checkcontainer">{{$make->make}}
                                           <input type="checkbox" name="makes[]" value="{{$make->id}}" @if(in_array($make->id,$dealerMakes)) checked @endif class="@if($make->id == 1) reseller @else nonreseller @endif" @if(in_array(1,$dealerMakes) && $make->id != 1) disabled @endif @if(!in_array(1,$dealerMakes) && $make->id == 1) disabled @endif>
                                           <span class="checkmark"></span>
                                       </label>
                                   </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="inputwidthsetfull">
                                <div class="form-group">
                                    <label for="usr">Dealership Address*</label>
                                    <input type="text" name="address" class="form-control" id="placeSearch" autocomplete="false" placeholder="Enter Dealership Address" value="{{ $dealer->address }}">
                                    <label id="placeError" class="error"></label>
                                    <input type="hidden" name="latitude" id="latitude" value="{{ $dealer->latitude }}">
                                    <input type="hidden" name="longitude" id="longitude" value="{{ $dealer->longitude }}">
                                </div>                              
                            </div>
                            @endif
                            <div class="cretactboxbtn">
                                <button type="submit" >UPDATE PROFILE</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtVAl5jn7PIWC6YSBK-VkIaneaKLIFExM&libraries=places"></script>

<script type="text/javascript">
$('input[name="makes[]"').change(function(e){
        // if(){
            if($(this).val() == 1 && $(this).prop("checked") == true){
                $('.reseller').attr('disabled',false);
                $('.nonreseller').attr('disabled',true);
            }else if($(this).val() != 1 && $(this).prop("checked") == true){
                $('.reseller').attr('disabled',true);
                $('.nonreseller').attr('disabled',false);
            }else{
                $('.nonreseller').attr('disabled',false)
                $('.reseller').attr('disabled',false);
            }
            $('input:checkbox.nonreseller').each(function () {
                   var sThisVal = (this.checked ? $(this).val() : "");
                   if(sThisVal){
                        $('.reseller').attr('disabled',true);
                        return false;
                   }
              });
        // }
    })

    $('#updateProfileForm').validate({
        focusInvalid: true,
        ignore: "",
        rules: {
            first_name:{
                required: true
            },
            last_name:{
                required: true
            },
            email:{
                required: true,
                email:true
            },
            phone:{
                required: true,
                number:true
            },
            dealership_name:{
                required: true
            },
            dealer_no:{
                required: true
            },
            amvic_no:{
                required: true
            },
            address:{
                required: true
            },
            timezone_id:{
                required: true
            },
            latitude:{
                required : true
            },
            longitude:{
                required : true
            },
            password:{
                required: true
            },
            confirm_password:{
                required: true,
                equalTo:'#password'
            },
            'makes[]':{
                required: true
            },
        },
        messages: {
            first_name: "Please enter first name",
            last_name:"Please enter last name",
            email:{
                'required':"Please enter email",
                'email':"Invalid email format"
            },
            phone:{
                'required':"Please enter phone",
            },
            dealership_name:"Please enter first name",
            dealer_no:"Please enter dealership phone number",
            amvic_no:"Please enter AMVIC Number",
            address:"Please select address from place suggestion",
            timezone_id:"Please select timezone",
            latitude:{
                required : "Please select place from place suggestion."
            },
            longitude:{
                required : "Please select place from place suggestion."
            },
            password:"Please enter password",
            confirm_password:{
                'required':"Please confirm your password"
            },
            'makes[]':{
                required: "Please choose makes"
            },
        },
        errorPlacement: function(error, element) {
            $(element).css({ "border": "#ffad47 1px solid" });
            $(element).siblings('.help-block').remove();
            if( element.prop('type') == 'select-one' ) {
                $(element).parent().append(error);
            }else if(element.prop('type') == 'checkbox'){
                console.log($(element).parent().siblings('.field_error'));
                $(element).parent().siblings('.field_error').append(error);
            }else{
                error.insertAfter(element);
            }
            if (element.attr("name") == "latitude")
            {
                $('#placeError').html(error);
            }
            if (element.attr("name") == "longitude")
            {
                $('#placeError').html(error);
            }
        },
        submitHandler: function(form) {
            var formData = $(form).serialize();     

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:'POST',
                url:"{{route('admin.dealer.update_profile',$id)}}",
                data:formData,
                beforeSend: function(){
                },
                success:function(data) {
                    switch (data.status) {
                        case true:
                        toastr.success(data.message);
                        location.reload();
                        break;
                        case false:
                        toastr.warning(data.message);
                        break;
                        default:
                        toastr.error("You are not Authorized to access this page");
                        break;
                    }
                },
                complete: function(){
                }
            });
        }
    });

    $('#placeSearch').focus(function() {
        $(this).attr('autocomplete', 'new-placeSearch');
    });

    function initialize() {
        //static coordinates
        var coordinates = {lat: 42.345573, lng: -71.098326};
        
        //simple map instance
        var map = new google.maps.Map(document.getElementById('placeSearch'), {
            center: coordinates,
            zoom: 14,
            streetViewControl: false,   // it hides the street view control (i.e. person icon) from map
        });

        // Set up the markers on the map
        var marker = new google.maps.Marker({
            map: map,
            center: coordinates,
            draggable: false,
            animation: google.maps.Animation.DROP,
        });
        marker.setVisible(true);

        //set the autocomplete
        var input = document.getElementById('placeSearch');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']); // Set the data fields to return when the user selects a place.

        //change listener on each autocomplete action
        autocomplete.addListener('place_changed', function(){
            marker.setVisible(false);
            
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
            console.log(place);
            placeInfo = getPlaceInformation(place);
            $('#latitude').val(placeInfo['latitude']);
            $('#longitude').val(placeInfo['longitude']);
        });

        //update the street view on dragging of marker
        google.maps.event.addListener(marker, 'dragend', function (event) {
            var newPosition = marker.getPosition();
            setStreetViewMethod(map,newPosition);
            geocodePosition(newPosition);
        });
    }    

    function getPlaceInformation(place){
        console.log(place);
        placeInfo = [];
        placeInfo['latitude'] = "";
        placeInfo['longitude'] = "";

        placeInfo['name'] = place.name;
        placeInfo['latitude'] = place.geometry.location.lat();
        placeInfo['longitude'] = place.geometry.location.lng();
        return placeInfo;
    }

    $(window).load(function(){
        initialize();
        $('#placeSearch').attr('autocomplete', 'new-placeSearch');
    });

</script>
@endsection
