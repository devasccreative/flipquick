@section('title','Flipquick | Dealers List')
@extends('admin.layouts.admin-app')
@section('styles')
@endsection
@section('content')

<div class="rightdetailpart">
  <div class="boxandditboxcover">
    <div class="tabledetacoverbox">
      <h3>Dealers</h3>
      <div class="pedapclfil">
        <a href="javascript:void(0);" id="pendingDealers">Pending Approvals</a>
        <a href="javascript:void(0);" id="claerFilter" style="display:none;">Back</a>
        <input type="hidden" name="is_approved" id="is_approved">
      </div>
      <br/>
      <table id="usersTbl" class="table table-striped table-bordered responsive nowrap" style="width:100%">
        <thead>
         <tr>
          <th>SR</th>
          <th>Name</th>
          <th>Email</th>
          <th>Dealership Name</th>
          <th>Dealership Phone Number</th>
          <th>AMVIC Number (#)</th>
          <th>Created Date</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>
    </table>
  </div>
</div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
  var table = '';
  function is_approvedUpdate(e)
  {
    var id = $(e).attr('data-id');
    var is_approved = $("#is_approved_"+id).val(); 
    var token = "{{ csrf_token() }}" ;
        //is_approved
        if(is_approved == "Active"){
          is_approved = "Deactive";
          $("#is_approved_"+id).val("Deactive");
        }
        else
        {
          is_approved = "Active";
          $("#is_approved_"+id).val("Active");
        }   
        //end of is_approved
        swal({   
          title: "Are you sure?",   
          text: "You want to "+is_approved+" this Dealer!",   
          type: "warning",   
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Yes, I am sure!',
          cancelButtonText: "No, cancel it!",
        }, function(isConfirm){

          if (isConfirm) {
            $("button.confirm").attr("disabled",true);   
            $.ajax({
              url : "{{ route('admin.dealer.is_approved') }}",
              type : "POST",
              data : {is_approved:is_approved,id:id,_token:token},
              success : function(is_approvedUpdate)
              {
                if(is_approved == "Active"){
                            // swal("Success!", "Employes has been active.", "success");
                            swal({   
                              title: "Success!",   
                              text: "Dealer has been actived.",   
                              timer: 1000,   
                              showConfirmButton: false 
                            });
                          }
                          else
                          {
                         // swal("Success!", "Employes has been Deactive.", "success");
                         swal({   
                          title: "Success!",   
                          text: "Dealer has been Deactived.",   
                          timer: 1000,   
                          showConfirmButton: false 
                        });
                       }
                     }
                   });   
          } else {     
            table.draw();
          //  if(is_approved == "Active"){
          //   $("#is_approved_"+id).prop('checked',false);
          //   $("#is_approved_"+id).attr('checked',false);
          //   $("#is_approved_"+id).val("Deactive");
          // }
          // else {
          //   $("#is_approved_"+id).prop('checked',true);
          //   $("#is_approved_"+id).attr('checked',true);
          //   $("#is_approved_"+id).val("Active");
          // }
        } 
      });
      }
      $(document).ready(function() {
            //$('#dataTable').DataTable();
            $('#pendingDealers').click(function(){
              $('#is_approved').val('0');
              $('#claerFilter').show();
              $('#pendingDealers').hide();
              table.draw();
            });
            $('#claerFilter').click(function(){
              $('#is_approved').val('');
              $('#pendingDealers').show();
              $('#claerFilter').hide();
              table.draw();
            });
            //GET DATA
            table = $('#usersTbl').DataTable({
              pageLength: 10,
              processing: true,
              responsive: true,
              serverSide: true,
              order: [],
              lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],

              ajax: {
                type: "get",
                url: "{{ route('admin.getDealerList') }}",
                data: function ( d ) {
                  d._token = "{{ csrf_token() }}";
                  d.is_approved = $('#is_approved').val();
                },
                complete:function(){
                  if( $('[data-toggle="tooltip"]').length > 0 )
                    $('[data-toggle="tooltip"]').tooltip();
                }
              },
              columns:[
              { data:'DT_RowIndex',name:'id' },
              { data:'name',name:'name' },
              { data:'email',name:'email' },
              { data:'dealership_name',name:'dealership_name'},
              { data:'dealer_no',name:'dealer_no'},
              { data:'amvic_no',name:'amvic_no'},
              { data:'created_at',name:'created_at' },
              { data:'is_approved',name:'is_approved'},
              { data:'action',name:'action'},
              ],
              columnDefs: [
              {
                orderable: false,
                targets: [0,4],
              }
              ],

            });
          });
        </script>
        @endsection