@section('title','Flipquick | Expertise')
@extends('admin.layouts.admin-app')
@section('styles')

@endsection
@section('content')

<div class="users_detateblebox">
    <div class="users_textboxpart">
        <h3>Expertises</h3>

        <div style="float: right;">
           <a href="" class="btn btn-default"  data-toggle="modal" data-target="#addExpertiseModal">Add Expertise</a>
       </div> 
   </div>
   <div class="innerdatatable_detlis">
    <table id="expertiseTbl" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>SR</th>
                <th>Expertise</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>

</div>


<!-- Modal -->
<div class="modal fade" id="addExpertiseModal" expertise="dialog">
 <div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Add Expertise</h4>
</div>
<div class="modal-body">
    <form id="addExpertiseFrm">
        <div class="form-group">
            <input type="text" name="expertise" id="expertiseName" class="form-control" placeholder="Add Expertise Name">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default">Add</button>  
        </div>
    </form>
</div>
<div class="modal-footer">

</div>
</div>

</div>
</div>


<!-- edit modal -->
<div class="modal fade" id="expertiseEditModal" expertise="dialog">
 <div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Edit Expertise</h4>
</div>
<form id="editExpertiseFrm">
    @csrf
    @method('PUT')
    <div class="modal-body">
        <div class="form-group">
         <input type="hidden" name="id" id="Expertiseid" class="form-control" >
     </div>
     <div class="form-group">
        <input type="text" name="expertise" id="editExpertiseName" class="form-control" placeholder="Add Expertise Name">
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-default">Update</button>   
    </div>
</form>
</div>
<div class="modal-footer">

</div>
</div>

</div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
  var table = $('#expertiseTbl').DataTable({
    pageLength:10,
    // serverSide: true,
    lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
    ajax: {
        type: "get",
        url: "{{ route('admin.expertises.getExpertises') }}",
        data: function ( d ) {
            d._token = "{{ csrf_token() }}";
        },
        complete:function(){
            if( $('[data-toggle="tooltip"]').length > 0 )
                $('[data-toggle="tooltip"]').tooltip();
        }
    },
   // "order": [[ 0, 'desc' ]],
    columns:[
    { data:'DT_RowIndex',name:'id' },
    { data:'expertise',name:'expertise' },
    { data:'action',name:'action', orderable:false, searchable:false },
    ],

});

  $('#addExpertiseFrm').validate({
    focusInvalid: true,
    ignore: "",
    rules: {
        name:{
            required: true
        }
    },
    messages: {
        name: "Please enter expertise"
    },
    errorPlacement: function(error, element) {
        $(element).css({ "border": "#ffad47 1px solid" });
        $(element).siblings('.help-block').remove();
        if( element.prop('type') == 'select-one' ) {
            $(element).parent().append(error);
        }else if(element.prop('type') == 'checkbox'){
            console.log($(element).parent().siblings('.field_error'));
            $(element).parent().siblings('.field_error').append(error);
        }else{
            error.insertAfter(element);
        }
    },
    submitHandler: function(form) {
        var formData = $(form).serialize();     

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'POST',
            url:"{{route('admin.expertises.store')}}",
            data:formData,
            beforeSend: function(){
                // $('#updateDoctorBtn').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    table.ajax.reload();
                    $("#addExpertiseModal").modal('hide');
                    toastr.success(data.message);
                    table.ajax.reload();
                    break;
                    case false:
                    toastr.warning(data.message);
                    break;
                    default:
                    toastr.error("You are not Authorized to access this page");
                    break;
                }
            },
            complete: function(){
                // $('#updateDoctorBtn').button('reset');
            }
        });
    }
});

  $(document).on('click','.deleteExpertise',function(e){
    $('.deleteExpertise').confirmation({
        container:"body",
        btnOkLabel:'OK',
        btnCancelLabel:"CANCEL",
        btnOkClass:"btn btn-sm btn-success",
        btnCancelClass:"btn btn-sm btn-danger",
        placement:'top',
        onConfirm:function(event, element) {
            event.preventDefault();
            var valid = element.data('id');
            var url = "{{route('admin.expertises.destroy',':id')}}";
            url = url.replace(':id',valid);
            $.ajax({
                type: "DELETE",
                data:{'_token':'{{ csrf_token() }}'},
                url: url,
                success: function(response)
                {
                    if (response.status == true) {
                        table.ajax.reload();
                        toastr.success(response.message);
                    } else {
                        toastr.warning(response.message);
                    }
                }
            });
        }
    });
});


  function EditExpertise(id){
    var url = "{{route('admin.expertises.edit',':id')}}";
    url = url.replace(':id',id);
    $.ajax({
        url:url,
        type:"GET",
        data:{id:id},
        success:function(data){

           if (data.status==true) {              
            $("#editExpertiseName").val(data.data.expertise);
            $("#Expertiseid").val(data.data.id);
            $("#expertiseEditModal").modal('show');

        } 
    }
});
}
$('#editExpertiseFrm').validate({
    focusInvalid: true,
    ignore: "",
    rules: {
        name:{
            required: true
        }
    },
    messages: {
        name: "Please enter expertise"
    },
    errorPlacement: function(error, element) {
        $(element).css({ "border": "#ffad47 1px solid" });
        $(element).siblings('.help-block').remove();
        if( element.prop('type') == 'select-one' ) {
            $(element).parent().append(error);
        }else if(element.prop('type') == 'checkbox'){
            console.log($(element).parent().siblings('.field_error'));
            $(element).parent().siblings('.field_error').append(error);
        }else{
            error.insertAfter(element);
        }
    },
    submitHandler: function(form) {
        var formData = $(form).serialize();     
        var id      = $("#Expertiseid").val();

        var url = "{{route('admin.expertises.update',':id')}}";
        url = url.replace(':id',id);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'PUT',
            url:url.replace(':id', id),
            data:formData,
            beforeSend: function(){
                // $('#updateDoctorBtn').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    table.ajax.reload();
                    $("#expertiseEditModal").modal('hide');
                    toastr.success(data.message);
                    table.ajax.reload();
                    break;
                    case false:
                    toastr.warning(data.message);
                    break;
                    default:
                    toastr.error("You are not Authorized to access this page");
                    break;
                }
            },
            complete: function(){
                // $('#updateDoctorBtn').button('reset');
            }
        });
    }
});

</script>
@endsection