@section('title','Flipquick | Sellers List')
@extends('admin.layouts.admin-app')
@section('styles')

@endsection
@section('content')
<div class="rightdetailpart">
    <div class="boxandditboxcover">
        <div class="tabledetacoverbox">
            <h3>Sellers</h3>
            <table id="usersTbl" class="table table-striped table-bordered" style="width:100%">
                <thead>
                   <tr>
                        <th>SR</th>
                        <th>Subject</th>
                        <th>Created Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function() {
            //GET DATA
            var table = $('#usersTbl').DataTable({
                pageLength: 10,
                processing: true,
                responsive: true,
                serverSide: true,
                order: [],
                lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],

                ajax: {
                    type: "get",
                    url: "{{ route('admin.getreports') }}",
                    data: function ( d ) {
                        d._token = "{{ csrf_token() }}";
                    },
                    complete:function(){
                        if( $('[data-toggle="tooltip"]').length > 0 )
                            $('[data-toggle="tooltip"]').tooltip();
                    }
                },
                columns:[
                { data:'DT_RowIndex',name:'id' },
                { data:'title',name:'title' },
                { data:'created_at',name:'created_at' },
                { data:'action',name:'action'},
                ],
                columnDefs: [
                {
                    orderable: false,
                        // targets: [0,5],
                        "bSortable": false
                    }
                    ],

                });
        });
    </script>
    @endsection