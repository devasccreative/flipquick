<?php
$i = 0;
if($current_listing->currentPage() > 1){
    $i = 6*($current_listing->currentPage()-1);
}
?>
@forelse($current_listing as $vehicle)
<div class="userimgtextbox">
    @if($vehicle->vehicle_exterior_photos->count() > 0)
    <div class="userimgbox">
        <img src="{{ $vehicle->vehicle_exterior_photos[0]->image }}" alt="">
    </div>
    @endif
    <div class="usertextbox">
        <a href="javascript:void(0)" class="bids_list" data-id="{{ $vehicle->id }}"><h4>{{ $vehicle->year.' '.$vehicle->make.' '.$vehicle->trim.' '.$vehicle->style.' '.$vehicle->model }}</h4></a>
        <p><span class="vehicleVIN">VIN : {{$vehicle->vin_no}}</span> </p>
        <p><span>{{ $vehicle->milage }} kms </span>
            {{--   <img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""><span>{{ $vehicle->bids_count }} @if( $vehicle->bids_count>1) Bids @else Bid @endif</span> --}}
        </p>
        <p><span>{{ $vehicle->drive_train }} </span><img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""><span>{{ ucfirst($vehicle->transmission) }}</span></p>
    </div>
    <div class="hmsboxsetbotbox">
        <p>Current listings will be removed at {{ $vehicle->starts_at }} {{ $vehicle->timezone_name }}</p>
        <ul class="vehicleListTimer" value="{{$vehicle->time_remaining}}">
            <li>
                <h4 id="hours{{$i}}">00</h4>
                <p>Hours</p>
            </li>
            <li>
                <h4 id="minutes{{$i}}">00</h4>
                <p>Minutes</p>
            </li>
            <li>
                <h4 id="seconds{{$i}}">00</h4>
                <p>Seconds</p>
            </li>
        </ul>
    </div>
    <div class="listplacebtnbox">
     <a href="javascript:void(0)" class="listingditbtn bids_list" data-id="{{ $vehicle->id }}">LISTING DETAILS</a>
 </div>
</div>
<?php $i++; ?>
@empty
<div class="userimgtextbox">
    <div class="usertextbox">
        <p>No Data Found</p>
    </div>
</div>
@endif