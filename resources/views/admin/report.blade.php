@section('title','Flipquick | Account Information')
@extends('admin.layouts.admin-app')
@section('styles')

@endsection
@section('content')
<div class="rightdetailpart">
    <div class="boxandditboxcover">
        <div class="delspselimabox">
            <div class="wrapper">
                <ul class="tabs">
                    <li><a href="javascript:void(0);" rel="#tabcontent1" class="active">Report Information</a></li>
                </ul>

                <div class="tab_content_container">
                    <div class="tab_content tab_content_active" id="tabcontent1">
                        <div class="inputcover">
                            <div class="inputwidthsetfull">
                                <div class="profplanboxdit">
                                    <div class="form-group">
                                        <img src="{{$report->image}}" width="500">
                                        <label for="usr"><span>Subject:</span> {{ $report->title }}</label>
                                        <label for="usr"><span>Description:</span> {{ $report->description }}</label>
                                        <div class="sld boxtitle">
                                       {{-- <h2>Vehicle detail</h2>
                                        <h4>{{ $report->vehicle->year.' '.$report->vehicle->make.' '.$report->vehicle->trim.' '.$report->vehicle->style }}</h4>
                                        <p><span class="vehicleVIN">VIN : {{$report->vehicle->vin_no}}</span> </p>
                                    </div> --}}

                                    </div>                              
                                </div>                              
                            </div>
                            
                        </div>
                    </div>
                    
        </div>
    </div>
</div>
</div>
</div>
@endsection

@section('scripts')

@endsection
