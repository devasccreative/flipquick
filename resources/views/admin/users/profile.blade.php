@section('title','flipquick | Profile')
@extends('admin.layouts.admin-app')
@section('navHeader')
<!-- <div class="categoriesadd_detlis">
    <div class="leftcategories_text">
        <a href="{{ route('admin.users.index') }}">
            <img src="{{ asset('svg/header-icon.svg') }}">
            <p>Users</p>
        </a>
    </div>
</div> -->
@endsection
@section('content')
<!-- USER PROFILE -->
<div class="users_detateblebox">
<div class="addbtnallset">
    <a   href="{{ route('admin.users.index') }}" class="backbtn">Back</a>    
</div> 

   <div class="profile_innerdetlis">
    <div class="profile_titaltext">
        <h3>Profile</h3>
    </div>
    <div class="profilephoto_detlis">
        <div class="leftprofile_detlis">
            <p>PROFILE PHOTO</p>
        </div>
         @if($user->image)
        <div class="rightprofile_detlis">
            <img src="{{ $user->image }}">
        </div>
        @else
        <div class="rightprofile_detlis">
            <img src="{{ asset('/noimage.jpg') }}">
        </div>
        @endif
    </div>
    <div class="namedetlis_boxpart">
        <div class="leftname_detlis">
            <p>NAME</p>
        </div>
        <div class="rightname_detlis">
            <p>{{ $user->first_name.' '.$user->last_name }}</p>   
        </div>
    </div>
    <div class="namedetlis_boxpart">
        <div class="leftname_detlis">
            <p>EMAIL</p>
        </div>
        <div class="rightname_detlis">
            <p>{{ $user->email }}</p>     
        </div>
        <div class="verified_icondiv">
            <img src="{{asset('admin_assets/svg/verified_icon.svg')}}">
        </div>
    </div>
    @if(isset($user->phone) && !empty($user->phone))
    <div class="namedetlis_boxpart">
        <div class="leftname_detlis">
            <p>PHONE NUMBER</p>
        </div>
        <div class="rightname_detlis">
            <p>
                <span>@if(isset($user->dialing_code))
                {{$user->dialing_code}}
                @endif</span>

         <span>{{ $user->phone }}</span>
      </p>    
        </div>
    </div>
    @endif
    <!-- <div class="namedetlis_boxpart">
        <div class="leftname_detlis">
            <p>CITY</p>
        </div>
        <div class="rightname_detlis">
            <p>Edmonton, AB. CA</p>     
        </div>
    </div> -->
    <div class="namedetlis_boxpart">
        <div class="leftname_detlis">
            <p>GENDER</p>
        </div>
        <div class="rightname_detlis">
            <p>{{ $user->gender }}</p>     
        </div>
    </div>
    <div class="namedetlis_boxpart">
        <div class="leftname_detlis">
            <p>LAST CHANGE</p>
        </div>
        <div class="rightname_detlis">
            <p>Last changed {{ $user->updated_at->toDateTime()->format('M d , Y @ H:i A') }} MT</p>   
        </div>
    </div>
    @if(isset($user->device_type) && !empty($user->device_type))
    <div class="namedetlis_boxpart setborderclass">
        <div class="leftname_detlis">
            <p>DEVICE & OS</p>
        </div>
        <div class="rightname_detlis">
            <p>
                {{$user->device_type}}
               <!--    Honor 8X - Android version 2.9.0 
                <br>
                 iPhone X - iOS 10.3 -->
             </p>    
        </div>
    </div>
    @endif
</div> 
</div>

<!-- END USER PROFILE -->
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_add_subcategory").click(function(){
            var subcategory_id=$('#accordion .subcategy_id').length+1;
            $('#subcategory_content .subcategy_sqe_id').val(subcategory_id);
            $('#subcategory_content .subcategory_name').attr('name','subcategory_name['+subcategory_id+']');
            $('#subcategory_content .attribute_name').attr('name','attribute_name['+subcategory_id+'][]');

            $('#subcategory_content .subcategy_id').text(subcategory_id+'.')
            $('#subcategory_append').append($('#subcategory_content').html());
        });
        $("body").delegate(".btn_add_attr", "click", function() {
            var subcategory_id=$(this).parents('.panel-default').find('.subcategy_sqe_id').val();
            $(this).parent('.areaofexpetise_textbox').after('<div class="endurance_inputtext"><input type="text" name="attribute_name['+subcategory_id+'][]" placeholder="Endurance"><a href="javascript:void(0)">Remove</a></div>');
        });

            //SAVE SUBCATEGORY
            $("body").delegate(".btn_save_subcategory", "click", function() {
                var subcategory_name=$(this).parents('.panel-default').find('.subcategory_name').val();
                $(this).parents('.fitnesstrainertopdiv').find('.subcategy_value').text(subcategory_name);
            });
            

            //ADD CATEGORY
            $("#category_form").submit(function(e){
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type:'POST',
                    url:'{{ route("admin.dashboard") }}',
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    beforeSend:function(){},
                    success:function(data) {
                        switch (data.status) {
                            case 200:
                            swal({
                                title: "Variation Updated Successfully",
                                icon: "success"
                            })
                            .then((willDelete) => {
                                if (willDelete) {
                                //Turbolinks.visit("");
                                //window.location.href =  "";
                            }
                        });
                            break;
                            case 500:
                            //swal("Information Not Updated");
                            break;
                            default:
                            //swal("You are not Authorized to access this page");
                            break;
                        }
                    },
                    complete:function(){},
                    error:function(){}
                });
            });
        });
    </script>
    @endsection
