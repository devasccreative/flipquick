@section('title','Flipquick | User List')
@extends('admin.layouts.admin-app')
@section('styles')
    
@endsection
@section('content')
    
    <div class="users_detateblebox">
        <div class="users_textboxpart">
            <h3>Users</h3>
        </div>
        <div class="innerdatatable_detlis">
            <table id="usersTbl" class="table table-striped table-bordered" style="width:100%">
                <thead>
                     <tr>
                        <th>SR</th>
                        <th>NAME</th>
                        <th>GENDER</th>
                        <th>PHONE</th>
                        <th>VERIFIED INFO</th>
                        <th>MORE</th>
                    </tr>
                </thead>
                    
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    
    <script type="text/javascript">
        //$(document).ready(function() {
            //$('#dataTable').DataTable();

            //GET DATA
            var table = $('#usersTbl').DataTable({
                pageLength:10,
                serverSide: true,
                lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
                ajax: {
                    type: "get",
                    url: "{{ route('admin.users.getUsers') }}",
                    data: function ( d ) {
                        d._token = "{{ csrf_token() }}";
                    },
                    complete:function(){
                        if( $('[data-toggle="tooltip"]').length > 0 )
                            $('[data-toggle="tooltip"]').tooltip();
                    }
                },
                columns:[
                    { data:'DT_RowIndex',name:'id' },
                    { data:'name',name:'name' },
                    { data:'gender',name:'gender' },
                    { data:'phone',name:'phone' },
                    { data:'verifiedInfo',name:'verifiedInfo',orderable:false, searchable:false  },
                    { data:'more',name:'more', orderable:false, searchable:false },
                ],
                "columnDefs": [
                    { className: "userimages_class", "targets": [4] },
                    { className: "profiletext_colorset", "targets": [5] }
                ]

            });
        //});
    </script>
@endsection