    @if($dealer && $total > 0)
    <div class="boxandditbox_left" id="sidebar">
        <div class="veiableboxset">
            <div class="veiablebox_title">
                <h3>{{ $total }} {{ ($total > 1) ? 'VEHICLES' : 'VEHICLE' }}  AVAILABLE</h3>
                <div class="inputwidthsetbox">                                
                                <div class="form-group">
                                    <label for="usr">Year</label>
                                    <select name="year" class="form-control valid" id="year" value="">
                                        <option value="">All Year</option>
                                        @foreach($vehicleYears as $vehicleYear)
                                        <option value="{{$vehicleYear}}" @if(isset($year) && $year == $vehicleYear) selected @endif>{{$vehicleYear}}</option>
                                        @endforeach                                                                                
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Brand</label>
                                    <select name="brand" class="form-control valid" id="brand" value="">
                                        <option value="">All Brand</option>
                                        @foreach($vehicleBrands as $vehicleBrand)
                                        <option value="{{$vehicleBrand}}" @if(isset($brand) && $brand == $vehicleBrand) selected @endif>{{$vehicleBrand}}</option>
                                        @endforeach                                        
                                    </select>
                                    <label id="placeError" class="error"></label>
                                </div> 
                                 <div class="form-group">
                                    <label for="usr">Search by VIN Number</label>
                                    <select name="vin_no" class="form-control valid" id="vin_no" value="">
                                        <option value="">All VIN Number</option>
                                        @foreach($vehicleVins as $vehicleVin)
                                        <option value="{{$vehicleVin}}" @if(isset($vin_no) && $vin_no == $vehicleVin) selected @endif>{{$vehicleVin}}</option>
                                        @endforeach                                        
                                    </select>
                                    <label id="placeError" class="error"></label>
                                </div>  
                                 <div class="form-group">
                                    <label for="usr">Post type</label>
                                    <select name="post_type" class="form-control valid" id="post_type" value="">
                                        <option value="">All Post type</option>
                                         <option value="sell"  @if(isset($post_type) && $post_type == "sell") selected @endif>sell</option>
                                        <option value="trade for used"  @if(isset($post_type) && $post_type == 'trade for used') selected @endif>trade for used</option>
                                        <option value="trade for new"  @if(isset($post_type) && $post_type == 'trade for new') selected @endif>trade for new</option>

                                    </select>
                                    <label id="placeError" class="error"></label>
                                </div>                          
                            </div>
            </div>
            <div class="vehiclesdetaboxcover"  id="vehicleData">
                <div class="vehiclesdetaboxiner" id="vehicleResult" page="{{$lastPage}}">
                    @include('website.bids_list')
                </div>
            </div>
        </div>
    </div>
    
    <div class="boxandditbox_right" id="sidebar">
        <div class="selectcarboxiner">
            @if($total > 0)
            <div class="carsliderbox">
                <div class="gallery-slider">

                    <div class="gallery-slider__images sldimgbox">
                        <?php
                        $vehicleExteriorPhotos = $bids[0]->vehicle->vehicle_exterior_photos->pluck('image')->toArray();
                        $vehicleDamagePhoto = $bids[0]->vehicle->vehicle_damage_photos->pluck('image')->toArray();
                        $vehicleInteriorPhoto = $bids[0]->vehicle->vehicle_interior_photos->pluck('image')->toArray();
                        $vehicleAllImages = array_merge($vehicleExteriorPhotos,$vehicleInteriorPhoto,$vehicleDamagePhoto);
                        $vehicleAllImage = array_merge($vehicleExteriorPhotos,$vehicleInteriorPhoto,$vehicleDamagePhoto);
                        

                        ?>
                        <div>
                            @foreach($vehicleAllImage as $key => $image)
                            <?php $allImage[] = ['src' => $image,'thumb'=>$image,'subHtml'=>'']; ?>
                            <div class="item">
                             <div class="img-fill animated-thumbnials">
                                <a href="{{ $image }}">
                                    <img src="{{ $image }}" alt="Kitten {{ $key }}">
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>


                <div class="gallery-slider__thumbnails sldthumbnailsbox">
                    <div>
                        @foreach($vehicleAllImage as $key => $image)
                        <div class="item">
                            <div class="img-fill"><img src="{{ $image }}" alt="Kitten {{ $key }}"></div>
                        </div>
                        @endforeach

                    </div>
                    <div class="backnextbtn">
                        <button class="prev-arrow slick-arrow nextpribtnbox">
                            BACK
                        </button>
                        <button class="next-arrow slick-arrow nextpribtnbox">
                            NEXT
                        </button>
                    </div>                              

                </div>
            </div>
        </div>

        <div class="sldboxtitle">
            <?php
            $bid = $bids[0];
            ?>
            <h4>{{ $bid->vehicle->year.' '.$bid->vehicle->make.' '.$bid->vehicle->trim.' '.$bid->vehicle->style }}</h4>
              <p><span class="vehicleVIN">VIN : {{$bid->vehicle->vin_no}}</span> </p>
          <p><span class="vehicleVIN">{{$bid->vehicle->post_type}}</span> </p>
            <p><span>Exterior : {{ $bid->vehicle->exterior_color }}</span> <img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""> <span>Interior : {{ $bid->vehicle->interior_color }}</span></p>
        </div>
        <div class="sedoseboxset">
            <ul>
                <li>{{ $bid->vehicle->body_shape }}</li>
                <li>{{ $bid->vehicle->doors }} Doors</li>
                <li>{{ $bid->vehicle->passangers }} Seats</li>
            </ul>
        </div>
        <div class="midetrboxsetiner">
            <ul>
                <li>
                    <img src="{{ asset('assets/svg/FlipQuick_milage.svg') }}" alt="">
                    <p>Milage <br>{{ $bid->vehicle->milage }} kms</p>
                </li>
                <li>
                    <img src="{{ asset('assets/svg/FlipQuick_drive_train.svg') }}" alt="">
                    <p>Drive Train <br>{{ $bid->vehicle->drive_train }}</p>
                </li>
                <li>
                    <img src="{{ asset('assets/svg/FlipQuick_transmission.svg') }}" alt="">
                    <p>Transmission <br>{{ $bid->vehicle->transmission }}</p>
                </li>
                <li>
                    <img src="{{ asset('assets/svg/car-engine.svg') }}" alt="">
                    <p>Engine <br>{{ ucfirst($bid->vehicle->engine) }}</p>
                </li>
            </ul>
        </div>
        <div class="tabboxtsetcover">
            <div class="wrapper">
                <ul class="tabs" id="tabs1">
                    <li><a href="javascript:void(0); return false;" rel="#tabcontent1" class="tab active">options</a></li>
                    <li><a href="javascript:void(0); return false;" rel="#tabcontent2" class="tab">additional info</a></li>
                    <li><a href="javascript:void(0); return false;" rel="#tabcontent3" class="tab">vehicle condition</a></li>
                </ul>

                <div class="tab_content_container" id="tab_content1">
                    <div class="tab_content tab_content_active" id="tabcontent1">                        
                        <p><label>Air Conditioning:</label> {{ ($bid->vehicle->air_conditioning=="1")?'Yes':'No' }}</p>
                        <p><label>Navigation System:</label> {{ ($bid->vehicle->navigation_system=="1")?'Yes':'No' }}</p>
                        <p><label>Leather Seats:</label> {{ ($bid->vehicle->leather_seats=="1")?'Yes':'No' }}</p>
                        <p><label>Sun Roof:</label> {{ ($bid->vehicle->sun_roof=="1")?'Yes':'No' }}</p>
                        <p><label>Panoramic Roof:</label> {{ ($bid->vehicle->panoramic_roof=="1")?'Yes':'No' }}</p>
                        <p><label>DVD:</label> {{ ($bid->vehicle->dvd=="1")?'Yes':'No' }}</p>
                        <p><label>Heated Seats:</label> {{ ($bid->vehicle->heated_seats=="1")?'Yes':'No' }}</p>
                        <p><label>AC Seats:</label> {{ ($bid->vehicle->ac_seats=="1")?'Yes':'No' }}</p>
                        <p><label>360 Camera:</label> {{ ($bid->vehicle['360_camera']=="1")?'Yes':'No' }}</p>
                        <p><label>Engine :</label> {{ $bid->vehicle->engine }}</p>
                        <p><label>Cab Type :</label> {{ ($bid->vehicle->style) ? $bid->vehicle->style : '-' }}</p>
                        <p><label>Fuel Type :</label> {{ $bid->vehicle->made_in }}</p>
                    </div>
                    <div class="tab_content" id="tabcontent2">                                  
                        <p><label>After Market Equipment:</label> {{ ($bid->vehicle->after_market_equipment)?$bid->vehicle->after_market_equipment:'-' }}</p>
                        <p><label>Original Owner:</label> {{ ($bid->vehicle->original_owner==1)?'Yes':'No' }}</p>
                        <p><label>Still Making Monthly Payments:</label> {{ ($bid->vehicle->still_making_monthly_payment=="1")?'Yes':'No' }} </p>
                        <p><label>Still Owe:</label> {{ $bid->vehicle->still_own }} </p>
                        <p><label>Have Any Accident Claims?:</label> {{ ($bid->vehicle->have_any_accident_record=='1')?'Yes':'No' }} </p>
                        <p><label>How Much Are The Claims:</label> {{ ($bid->vehicle->how_much_records > 1)?$bid->vehicle->how_much_records:1 }}</p>
                        <p><label>Smoked In:</label> {{ ($bid->vehicle->smoked_in=="1")?'Yes':'No' }}</p>
                        <p><label>Modified Exhaust :</label> {{ ($bid->vehicle->modified_exhaust=="1")?'Yes':'No' }}</p>
                    </div>  
                    <div class="tab_content" id="tabcontent3">                                  
                        <p><label>Overall Vehicle Condition:</label> {{ $bid->vehicle->vehicle_condition }}/10</p>
                        <p><label>Condition of Front Tires:</label> {{ $bid->vehicle->front_tires_condition }}</p>
                        <p><label>Condition of Rear Tires:</label> {{ $bid->vehicle->back_tires_condition }}</p>
                        <p><label>Warning Lights In Cluster:</label> {{ ($bid->vehicle->warning_lights_in_clustor=="1")?$bid->vehicle->warning_lights:'No' }}</p>
                        <p><label>Mechanical Issues:</label> {{ ($bid->vehicle->mechanical_issues=="1")?'Yes':'No' }}</p>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@else
<div class="boxandditbox_left">
    <div class="veiableboxset">
        <div class="veiablebox_title">
            <h3>No Data Found</h3>
        </div>
    </div>
</div>
@endif