<div class="veiablebox_title">
                                <?php
                                ?>
                                <h3>{{ $total_repo_slot }} {{ ($total_repo_slot > 1) ? 'VEHICLES' : 'VEHICLE' }}  AVAILABLE</h3>
                            </div>
                            <div class="vehiclesdetaboxcover">

     <div class="inputwidthsetbox">                                
                                <div class="form-group">
                                    <label for="usr">Year</label>
                                    <select name="yearrepo" class="form-control valid" id="yearrepo" value="">
                                        <option value="">All Year</option>
                                        @foreach($vehicleYearsrepo as $vehicleYear)
                                        <option value="{{$vehicleYear}}" @if(isset($year) && $year == $vehicleYear) selected @endif>{{$vehicleYear}}</option>
                                        @endforeach 
                                        <option value="2010" @if(isset($year) && $year == $vehicleYear) selected @endif>2010</option>
                                                                        
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Brand</label>
                                    <select name="brandrepo" class="form-control valid" id="brandrepo" value="">
                                        <option value="">All Brand</option>
                                        @foreach($vehicleBrandsrepo as $vehicleBrand)
                                        <option value="{{$vehicleBrand}}" @if(isset($brand) && $brand == $vehicleBrand) selected @endif>{{$vehicleBrand}}</option>
                                        @endforeach                                        
                                    </select>
                                    <label id="placeError" class="error"></label>
                                </div> 
                                <div class="form-group">
                                    <label for="usr">Post type</label>
                                    <select name="posttyperepo" class="form-control valid" id="posttyperepo" value="">
                                        <option value="">All Post type</option>
                                        <option value="sell"  @if(isset($post_type) && $post_type == "sell") selected @endif>sell</option>
                                        <option value="trade for used"  @if(isset($post_type) && $post_type == 'trade for used') selected @endif>trade for used</option>
                                        <option value="trade for new"  @if(isset($post_type) && $post_type == 'trade for new') selected @endif>trade for new</option>

                                    </select>
                                    <label id="placeError" class="error"></label>
                                </div> 
                                                         
                            </div>
                                <div class="vehiclesdetaboxiner" id="vehicleResultrepo" page="{{$lastPage_repo_slot}}">
                                    @include('website.vehicle_list_reposlot_results')
                                </div>
                            </div>