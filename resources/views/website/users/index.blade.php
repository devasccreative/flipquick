@section('title','FlipQuick | User Roles')
@extends('website.layouts.app')
@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/datatables.min.css') }}">
@endsection
@section('content')

<div class="boxandditboxcover">

  <div class="tabledetacoverbox">
    <h3>Users</h3>
    <div class="addbtnallset">
        <a href="{{ route('dealer.users.create') }}">Add User</a>
    </div>
    <br/>
    <table id="usersTbl" class="table table-striped table-bordered" style="width:100%">
      <thead>
       <tr>
        <th>SR</th>
        <th>Created Date</th>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Action</th>
      </tr>
    </thead>
  </table>
</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('assets/js/datatables.min.js') }}"></script>
<script type="text/javascript">
  var delete_dealer;
  $(document).ready(function() {
            //$('#dataTable').DataTable();
            delete_dealer =   function delete_dealer(id){
             var url = "{{ route('dealer.users.destroy',':id') }}";
             url = url.replace(':id',id);
             $('.delete_dealer').confirmation({
              container:"body",
              btnOkLabel:'OK',
              btnCancelLabel:"CANCEL",
              btnOkClass:"btn btn-sm btn-success",
              btnCancelClass:"btn btn-sm btn-danger",
              placement:'top',
              onConfirm:function(event, element) {
                event.preventDefault();
                $.ajax({
                  type:'DELETE',
                  url:url,
                  data:{'_token':"{{ csrf_token() }}" },
                  success:function(data){
                    if(data.status == true){
                      toastr.success(data.message);
                      table.ajax.reload();
                    }   
                    else{
                      toastr.error(data.message);
                    }
                  }
                });
              }
            });

           }
            //GET DATA
            var table = $('#usersTbl').DataTable({
              pageLength: 10,
              processing: true,
              responsive: true,
              serverSide: true,
              order: [],
              lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],

              ajax: {
                type: "get",
                url: "{{ route('dealer.users.getUsersList') }}",
                data: function ( d ) {
                  d._token = "{{ csrf_token() }}";
                },
                complete:function(){
                  if( $('[data-toggle="tooltip"]').length > 0 )
                    $('[data-toggle="tooltip"]').tooltip();
                }
              },
              columns:[
              { data:'DT_RowIndex',name:'id' },
              { data:'created_at',name:'created_at' },
              { data:'name',name:'name' },
              { data:'email',name:'email' },
              { data:'phone',name:'phone'},
              { data:'action',name:'action'},
              ],
              columnDefs: [
              {
                orderable: false,
                targets: [0,4],
              }
              ],

            });
          });
        </script>
        @endsection