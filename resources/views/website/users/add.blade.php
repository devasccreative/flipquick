@section('title','FlipQuick | Add User')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="boxandditboxcover">
 <div class="addbtnallset">
    <a href="{{ route('dealer.users.index') }}" class="">Back</a>
</div>
<br/>
<div class="delspselimabox">
    <div class="wrapper">
        <ul class="tabs">
            <li><a href="javascript:void(0);" rel="#tabcontent1" class=" active">Add User</a></li>
        </ul>

        <div class="tab_content_container">
            <div class="tab_content tab_content_active" id="tabcontent1">
                <form id="addUserFrm" method="post" >
                    @csrf
                    <div class="inputcover">
                        <div class="inputwidthsetbox">
                            <div class="form-group">
                                <label for="usr">First Name*</label>
                                <input type="text" name="first_name" class="form-control" id="" placeholder="Enter First Name" value="">
                            </div>
                            <div class="form-group">
                                <label for="usr">Last Name*</label>
                                <input type="text" name="last_name" class="form-control" id="" placeholder="Enter Last Name" value="">
                            </div>
                        </div>
                        <div class="inputwidthsetbox">
                            <div class="form-group">
                                <label for="usr">Email Address*</label>
                                <input type="text" name="email" class="form-control" id="" placeholder="Enter Email Address" value="">
                                @error('email')
                                <label class="error">{{ $message }}</label>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="usr">Phone Number*</label>
                                <input type="text" name="phone" class="form-control" id="" placeholder="Enter Phone Number" value="">
                                @error('phone')
                                <label class="error">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <hr>
                       <!--  <div class="inputwidthsetfull">
                            <div class="form-group">
                                <label for="usr">Dealership Address*</label>
                                <input type="text" name="address" class="form-control" id="placeSearch" autocomplete="false" placeholder="Enter Dealership Address" value="">
                                <label id="placeError" class="error"></label>
                                <input type="hidden" name="latitude" id="latitude" value="">
                                <input type="hidden" name="longitude" id="longitude" value="">
                            </div>                              
                        </div> -->
                        <div class="inputwidthsetbox">
                            <div class="form-group">
                                <label for="usr">Role*</label>
                                <input type="text" name="role" class="form-control" id="" placeholder="Enter Role" >
                            </div>
                           {{--<div class="form-group">
                                <label for="usr">Module Access Permissions*</label>
                                @foreach($permissions as $permission)
                                <div class="customcheck">
                                 <label class="checkcontainer">{{ucfirst(str_replace('_',' ',$permission->name))}}
                                    <input type="checkbox" name="permission[]" class="" id="" placeholder="Enter AMVIC Number" value="{{$permission->name}}" id="{{$permission->name}}">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            @endforeach
                            <label for="permission" id="permissionErr"></label>
                        </div>--}} 
                    </div>
                    <div class="cretactboxbtn">
                        <button type="submit" >ADD USER</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>
@endsection

@section('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtVAl5jn7PIWC6YSBK-VkIaneaKLIFExM&libraries=places"></script>

<script type="text/javascript">
    $('#addUserFrm').validate({
        focusInvalid: true,
        ignore: "",
        rules: {
            first_name:{
                required: true
            },
            last_name:{
                required: true
            },
            email:{
                required: true,
                email:true
            },
            phone:{
                required: true,
                number:true
            },
            // address:{
            //     required: true
            // },
            // "permission[]":{
            //     required: true
            // },
            // latitude:{
            //     required : true
            // },
            // longitude:{
            //     required : true
            // },
            role:{
                required: true
            },
        },
        messages: {
            first_name: "Please enter first name",
            last_name:"Please enter last name",
            email:{
                'required':"Please enter email",
                'email':"Invalid email format"
            },
            phone:{
                'required':"Please enter phone",
            },
            // address:"Please select address from place suggestion",
            // "permission[]":"Please select access permissions",
            // latitude:{
            //     required : "Please select place from place suggestion."
            // },
            // longitude:{
            //     required : "Please select place from place suggestion."
            // },
            role:"Please enter role",
        },
        errorPlacement: function(error, element) {
            $(element).css({ "border": "#ffad47 1px solid" });
            $(element).siblings('.help-block').remove();
            if( element.prop('type') == 'select-one' ) {
                $(element).parent().append(error);
            }else if(element.prop('type') == 'checkbox'){
                $('#permissionErr').append(error);
            }else{
                error.insertAfter(element);
            }
            if (element.attr("name") == "latitude")
            {
                $('#placeError').html(error);
            }
            if (element.attr("name") == "longitude")
            {
                $('#placeError').html(error);
            }
        },
        submitHandler: function(form) {
            var formData = $(form).serialize();     

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:'POST',
                url:"{{route('dealer.users.store')}}",
                data:formData,
                beforeSend: function(){
                },
                success:function(data) {
                    switch (data.status) {
                        case true:
                        toastr.success(data.message);
                        location.replace("{{route('dealer.users.index')}}");
                        break;
                        case false:
                        toastr.warning(data.message);
                        break;
                        default:
                        toastr.error("You are not Authorized to access this page");
                        break;
                    }
                },
                complete: function(){
                }
            });
        }
    });

    $('#placeSearch').focus(function() {
        $(this).attr('autocomplete', 'new-placeSearch');
    });

    function initialize() {
        //static coordinates
        var coordinates = {lat: 42.345573, lng: -71.098326};
        
        //simple map instance
        var map = new google.maps.Map(document.getElementById('placeSearch'), {
            center: coordinates,
            zoom: 14,
            streetViewControl: false,   // it hides the street view control (i.e. person icon) from map
        });

        // Set up the markers on the map
        var marker = new google.maps.Marker({
            map: map,
            center: coordinates,
            draggable: false,
            animation: google.maps.Animation.DROP,
        });
        marker.setVisible(true);

        //set the autocomplete
        var input = document.getElementById('placeSearch');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']); // Set the data fields to return when the user selects a place.

        //change listener on each autocomplete action
        autocomplete.addListener('place_changed', function(){
            marker.setVisible(false);
            
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
            console.log(place);
            placeInfo = getPlaceInformation(place);
            $('#latitude').val(placeInfo['latitude']);
            $('#longitude').val(placeInfo['longitude']);
        });

        //update the street view on dragging of marker
        google.maps.event.addListener(marker, 'dragend', function (event) {
            var newPosition = marker.getPosition();
            setStreetViewMethod(map,newPosition);
            geocodePosition(newPosition);
        });
    }    

    function getPlaceInformation(place){
        console.log(place);
        placeInfo = [];
        placeInfo['latitude'] = "";
        placeInfo['longitude'] = "";

        placeInfo['name'] = place.name;
        placeInfo['latitude'] = place.geometry.location.lat();
        placeInfo['longitude'] = place.geometry.location.lng();
        return placeInfo;
    }

    $(window).load(function(){
        initialize();
        $('#placeSearch').attr('autocomplete', 'new-placeSearch');
    });

</script>
@endsection
