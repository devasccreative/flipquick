@section('title','FlipQuick | Signup')
@extends('website.layouts.app')
@section('styles')
<style type="text/css">
	.cretact_title {
    padding-bottom: 20px;
}
.pricingboxcover {
    max-width: 50%;
}
.box3 h3 {
    color: #01034A;
    min-height: 56px;
}
.box2 h3 {
    color: #FF6600;
    min-height: 31px;
}
</style>
@endsection
@section('content')
<div class="container">
	<div class="cretactbox pricingshowpage">

<div class="pricingboxcover">
	<!-- <div class="radio">
		<div class="pricingboxiner box1">
			<h3>FREE</h3>
			<p>30 days trial</p>
			<hr>
			<input id="radio-1" name="radio" type="radio" checked>
			<label for="radio-1" class="radio-label"></label>
		</div>
	</div> -->
<ul class="tabs">
					<li><a href="javascript:void(0);" rel="#tabcontent1" class="tab active">I'm a Dealership</a></li>
					<li><a href="javascript:void(0);" rel="#tabcontent2" class="tab">I'm a Seller</a></li>	       
				</ul>	    
				<div class="tab_content_container">
					<div class="tab_content tab_content_active" id="tabcontent1">
								<div class="cretact_title">
<!-- 			<h3>Hello</h3>
-->			<p>Here are the two types of membership plans to fit your needs!</p>
</div>
						<div class="radio">
							<div class="pricingboxiner box2">	
								<h3>$500 </h3>
								<h3>Unlimited Bid Winning Package. </h3>
								<p>Bid on, and win unlimited vehicles for one monthly fee (Recommended) </p>
								<hr>
								<!-- <span>FULL PRICE</span> -->
								<input id="radio-2" name="radio" type="radio">
								<label  for="radio-2" class="radio-label"></label>
							</div>
						</div>
						<div class="radio">
							<div class="pricingboxiner box3">
								<h3>$50 </h3>
								<h3>Pay per Win Package</h3>
								<p>Bid on unlimited vehicles but pay a fee for each vehicle you win.</p>
								<hr>
								<!-- <span>FULL PRICE</span> -->
								<input id="radio-3" name="radio" type="radio">
								<label  for="radio-3" class="radio-label"></label>
							</div>
						</div>
						<div class="veriemailbtn">
	<a href="{{route('website.dealer.signup')}}">COUNTINUE</a>
</div>
					</div>
					<div class="tab_content" id="tabcontent2">
						<div class="radio" style="margin-top: 50px;">
							<div class="pricingboxiner box2">	
								<p>Posting your vehicle with FlipQuick is absolutely free!</p>
								<hr>
								<!-- <span>FULL PRICE</span> -->
								<!-- <input id="radio-2" name="radio" type="radio">
								<label  for="radio-2" class="radio-label"></label> -->
							</div>
						</div>
					</div>
				</div>	
</div>

</div>
</div>


@include('website.layouts.footer')



@endsection

@section('scripts')
<script>
	$(document).ready(function(){
		$(".tabs li a").click(function() {
			
			// Active state for tabs
			$(".tabs li a").removeClass("active");
			$(this).addClass("active");
			
			// Active state for Tabs Content
			$(".tab_content_container > .tab_content_active").removeClass("tab_content_active").fadeOut(200);
			$(this.rel).fadeIn(500).addClass("tab_content_active");
			
		});	

	});

</script>
@endsection