@section('title','FlipQuick | Contact Us')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="container" id="contactustitle">
	<div class="pagetitlebg">
		<h2>Contact Us</h2>
	</div>
</div>

<div class="container" id="contactuspanel">
	<div class="contformcover">
		<div class="contforminer">
			<form id="contactusform">	
				<div class="form-group">
					<label for="usr">Name</label>
					<input type="text" class="form-control" name="name">
				</div>
				<div class="form-group">
					<label for="usr">Phone</label>
					<input type="text" class="form-control" name="phone">
				</div>
				<div class="form-group">
					<label for="usr">Email address</label>
					<input type="text" class="form-control" name="email">
				</div>
				<div class="form-group">
					<label for="usr">Subject</label>
					<input type="text" class="form-control" name="subject">
				</div>
				<div class="form-group">
					<label for="usr">How can we help?</label>
					<textarea class="form-control" rows="5" id="" name="help"></textarea>
				</div>
				<div class="sedbtnbox">
					<button type="submit" class="" id="contactsubmit">SEND</button>
				</div>
				<div id="mailstatus">
					
				</div>
			</form>
		</div>	
	</div>	
</div>

<div class="thnkpagecover" id="feedback" style="display: none;">
	<img src="{{ asset('assets/svg/thank-you-icon.svg') }}" alt="">
	<h3>Thank you !</h3>
	<p>We have received your application. Our team will <br>
		review your application & get back to assist you <br>
	with next steps.</p>
	<a href="{{route('home')}}">Back to homepage</a>
</div>

@include('website.layouts.footer')

@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js" integrity="sha256-sPB0F50YUDK0otDnsfNHawYmA5M0pjjUf4TvRJkGFrI=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.js" integrity="sha256-NdDw7k+fJewgwI1XmH9NMR6OILvTX+3arqb/OgFicoM=" crossorigin="anonymous"></script>
<script>
	$(document).ready(function(){
		$('#feedback').hide();
	});
	$('#contactsubmit').on( 'click', function( evt ) {
		evt.preventDefault();
		$('#contactusform').submit();
	});

	    $('#contactusform').validate({ // initialize the plugin
	    	rules: {
	    		name: {
	    			required: true,
	    		},
	    		phone: {
	    			required: true,
	    		},
	    		email: {
	    			required: true,
	    			email: true
	    		},
	    		subject: {
	    			required: true,
	    		},
	    		help:{
	    			required: true,
	    		},
	    		
	    	},
	    	submitHandler: function(form) {
	    		var serialized = $('#contactusform').serialize();
	    		$.ajax( {
	    			url: '{{route("contactus.mail")}}', 
	    			method: 'POST',
	    			data: serialized, 
	    		}).done( function( result ){
	    			if(result.status == "false"){
	    				
	                   	//	toastr.error(result.message);
	                   }
	                   else{
	                   	$('#contactusform')[0].reset();
	                   	$('#mailstatus').html(result.message);
	                   	$('#feedback').show();
	                   	$('#contactuspanel').hide();
	                   	$('#contactustitle').hide();
	                   }
	               });
	    	}
	    });	

	</script>
	@endsection