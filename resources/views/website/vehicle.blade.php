<!DOCTYPE html>
<html lang="en">
<head>
    <title>FlipQuick</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=""/>
<?php 
                            $vehicleExteriorPhotos = $sharedvehicle->vehicle->vehicle_exterior_photos->pluck('image')->toArray(); ?>
    <meta property="og:url" content="https://flipquick.ca/" />
    <meta property="og:type" content="type" />
    <meta property="og:title" content="FlipQuick" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="{{ $vehicleExteriorPhotos[0] }}" />

    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="{{ asset('favicon/mstile-144x144.png') }}" />
    <meta name="msapplication-square70x70logo" content="{{ asset('favicon/mstile-70x70.png') }}" />
    <meta name="msapplication-square150x150logo" content="{{ asset('favicon/mstile-150x150.png') }}" />
    <meta name="msapplication-wide310x150logo" content="{{ asset('favicon/mstile-310x150.png') }}" />
    <meta name="msapplication-square310x310logo" content="{{ asset('favicon/mstile-310x310.png') }}" />

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ asset('favicon/apple-touch-icon-57x57.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('favicon/apple-touch-icon-114x114.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('favicon/apple-touch-icon-72x72.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('favicon/apple-touch-icon-144x144.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{{ asset('favicon/apple-touch-icon-60x60.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{ asset('favicon/apple-touch-icon-120x120.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{ asset('favicon/apple-touch-icon-76x76.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ asset('favicon/apple-touch-icon-152x152.png') }}" />
    <link rel="icon" type="image/png" href="{{ asset('favicon/favicon-196x196.png') }}" sizes="196x196" />
    <link rel="icon" type="image/png" href="{{ asset('favicon/favicon-96x96.png') }}" sizes="96x96" />
    <link rel="icon" type="image/png" href="{{ asset('favicon/favicon-32x32.png') }}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{ asset('favicon/favicon-16x16.png') }}" sizes="16x16" />
    <link rel="icon" type="image/png" href="{{ asset('favicon/favicon-128.png') }}" sizes="128x128" />
    <meta name="_token" content="{!! csrf_token() !!}" />
    <link href="https://fonts.googleapis.com/css?family=Khula:300,400,600,700,800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/websiteInnerAssets.css') }}">
    <link rel="stylesheet" href="{{ asset('css/websiteAssets.css') }}">

    <link rel="stylesheet" href="{{ asset('css/lightgallery.css') }}">
    <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
    <!-- Global site
tag (gtag.js) - Google Ads: 656133024 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-656133024"></script>
<script> window.dataLayer = window.dataLayer || []; function
gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config',
'AW-656133024'); </script>
</head>
<body>
    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="headercovercl">
                <button class="sidebar-toggle is-closed">
                    <img class="menuiconset" src="{{ asset('assets/svg/menu-icon.svg') }}" alt="">
                    <img class="menuiconcloseset" src="{{ asset('assets/svg/menu-close-icon.svg') }}" alt="">
                </button>
                <div class="container">              
                    <div class="navbar-header page-scroll">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <!-- <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> -->
                        <img src="{{ asset('assets/svg/hamburger.svg') }}" alt="" class="hamburgerbtn">
                        <img src="{{ asset('assets/svg/hamburger-close.svg') }}" alt="" class="hamburger_closebtn">
                    </button>
                    {{-- <a class="navbar-brand" href="{{route('home')}}"><img src="{{ asset('assets/svg/logo.svg') }}" alt=""></a> --}}
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <ul class="nav navbar-nav">
                        <li class="hidden">
                            <a href="#page-top"></a>
                        </li>

                        <li class="page-scroll">
                            <a href="javascript:void(0)">About Us</a>
                        </li>
                        <li class="page-scroll">
                            <a href="javascript:void(0)">Pricing</a>
                        </li>
                    </ul>
                    <!-- <div class="rightlsubtn">
                        <a href="{{ route('website.dealer.login') }}">Login</a>
                        <a href="{{ route('website.dealer.signup') }}" class="signupbntbox">SIGN UP</a>
                    </div> -->
                </div>              
            </div>
        </div>
    </nav>
</header>
<div class="container">
    <div class="allvildetacover">
        <div class="alldetacoverhompg">
            <?php  $allImageInner=[]; ?>
            @if($sharedvehicle)

            <div class="selectcarboxiner">

                <div class="carsliderbox">
                    <div class="gallery-slider">

                        <div class="gallery-slider__images sldimgbox">
                            <?php
                            $vehicleExteriorPhotos = $sharedvehicle->vehicle->vehicle_exterior_photos->pluck('image')->toArray();
                            $vehicleDamagePhoto = $sharedvehicle->vehicle->vehicle_damage_photos->pluck('image')->toArray();
                            $vehicleInteriorPhoto = $sharedvehicle->vehicle->vehicle_interior_photos->pluck('image')->toArray();
                            $vehicleAllImages = array_merge($vehicleExteriorPhotos,$vehicleInteriorPhoto,$vehicleDamagePhoto);


                            $vehicleAllImage = array_merge($vehicleExteriorPhotos,$vehicleInteriorPhoto,$vehicleDamagePhoto);
                            $imageHtml = "";
                            ?>
                            <div>
                                @foreach($vehicleAllImage as $key => $image)
                                <?php $allImageInner[] = ['src' => $image,'thumb'=>$image,'subHtml'=>'']; ?>
                                <div class="item">
                                  <div class="img-fill animated-thumbnials">
                                    <a href="{{ $image }}">
                                        <img src="{{ $image }}" alt="Vehicle {{ $key }}">
                                    </a>
                                </div>
                            </div>
                            <?php 
                            $imageHtml .= '<div class="item">
                            <div class="img-fill"><img src="'.$image.'" alt="Vehicle '.$key.'"></div>
                            </div>'; 
                            ?>
                            @endforeach
                        </div>
                    </div>


                    <div class="gallery-slider__thumbnails sldthumbnailsbox">
                        <div>
                            {!! $imageHtml !!}
                        </div>
                        <div class="backnextbtn">
                            <button class="prev-arrow slick-arrow nextpribtnbox">
                                BACK
                            </button>
                            <button class="next-arrow slick-arrow nextpribtnbox">
                                NEXT
                            </button>
                        </div>                              

                    </div>
                </div>
            </div>

            <div class="sldboxtitle">

                <h4>{{ $sharedvehicle->vehicle->year.' '.$sharedvehicle->vehicle->make.' '.$sharedvehicle->vehicle->trim.' '.$sharedvehicle->vehicle->style }}</h4>
                <p><span class="vehicleVIN">VIN : {{$sharedvehicle->vehicle->vin_no}}</span> </p>
                <p><span class="vehicleVIN">{{$sharedvehicle->vehicle->post_type}}</span> </p>
                <p><span class="vehicleVIN">Model : {{$sharedvehicle->vehicle->model}}</span> </p>
                <p><span>Exterior : {{ $sharedvehicle->vehicle->exterior_color }}</span> <img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""> <span>Interior : {{ $sharedvehicle->vehicle->interior_color }}</span></p>
                @foreach($sharedvehicle->vehicle->bids as $bid)
                @if($bid->status == 1 && $sharedvehicle->vehicle->expired_at < \Carbon\Carbon::now())
                    <p><span class="vehicleVIN">Highest Bid: ${{$bid->amount}}</span></p>
                    @endif
                    @endforeach

            </div>
            @if($sharedvehicle->type == 'highest_bid')
            <div class="sldboxtitle">
             <h4>Seller Information:</h4>
             <p><span class="vehicleVIN">Email : {{$sharedvehicle->vehicle->seller->email}}</span> </p>
             <p><span class="vehicleVIN">Name : {{$sharedvehicle->vehicle->seller->first_name.' '.$sharedvehicle->vehicle->seller->last_name}}</span></p>
             <p><span class="vehicleVIN">Phone : :<a href="tel:{{ $sharedvehicle->vehicle->seller->country_code.''.$sharedvehicle->vehicle->seller->phone }}"> {{$sharedvehicle->vehicle->seller->country_code.'-('.substr($sharedvehicle->vehicle->seller->phone, 0, 3).') '.substr($sharedvehicle->vehicle->seller->phone, 3, 3).'-'.substr($sharedvehicle->vehicle->seller->phone, 6, 10)}}</a></span></p>
         </div>
         @endif
         @if($sharedvehicle->type == 'highest_bid')
         <div class="sldboxtitle">

            <h4>Dealership Information:</h4>
            <p><span class="vehicleVIN">Dealership Name : {{$sharedvehicle->dealer->dealership_name}}</span> </p>
            <p><span class="vehicleVIN">Email : {{$sharedvehicle->dealer->email}}</span> </p>
            <p><span class="vehicleVIN">Manager name : {{$sharedvehicle->dealer->first_name.' '.$sharedvehicle->dealer->last_name}}</span></p>
            <p><span class="vehicleVIN">Phone : {{$sharedvehicle->dealer->country_code.'-('.substr($sharedvehicle->dealer->phone, 0, 3).') '.substr($sharedvehicle->dealer->phone, 3, 3).'-'.substr($sharedvehicle->dealer->phone, 6, 10)}}</span></p>
            <p><span class="vehicleVIN">Address :<a href="https://www.google.com/maps/?q={{$sharedvehicle->dealer->latitude.','.$sharedvehicle->dealer->longitude}}" target="_blank"> {{$sharedvehicle->dealer->address}}</span></a></p>
        </div>
        @endif
        <div class="sedoseboxset">
            <ul>
                <li>{{ str_replace('/',' / ',$sharedvehicle->vehicle->body_shape) }}</li>
                <li>{{ $sharedvehicle->vehicle->doors }} Doors</li>
                <li>{{ $sharedvehicle->vehicle->passengers }} Seats</li>
                <li>{{ $sharedvehicle->vehicle->made_in  }}</li>
            </ul>
        </div>
        <div class="midetrboxsetiner">
            <ul>
                <li>
                    <img src="{{ asset('assets/svg/FlipQuick_milage.svg') }}" alt="">
                    <p>Milage <br>{{ $sharedvehicle->vehicle->milage }} kms</p>
                </li>
                <li>
                    <img src="{{ asset('assets/svg/FlipQuick_drive_train.svg') }}" alt="">
                    <p>Drive Train <br>{{ $sharedvehicle->vehicle->drive_train }}</p>
                </li>
                <li>
                    <img src="{{ asset('assets/svg/FlipQuick_transmission.svg') }}" alt="">
                    <p>Transmission <br>{{ $sharedvehicle->vehicle->transmission }}</p>
                </li>
                <li>
                    <img src="{{ asset('assets/svg/car-engine.svg') }}" alt="">
                    <p>Engine <br>{{ ucfirst($sharedvehicle->vehicle->engine) }}</p>
                </li>
            </ul>
        </div>
        <div class="tabboxtsetcover">
            <div class="wrapper">
                <ul class="tabs">
                    <li><a href="javascript:void(0);" rel="#tabcontent1" class="tab active">options</a></li>
                    <li><a href="javascript:void(0);" rel="#tabcontent2" class="tab">additional info</a></li>
                    <li><a href="javascript:void(0);" rel="#tabcontent3" class="tab">vehicle condition</a></li>
                </ul>

                <div class="tab_content_container">
                    <div class="tab_content tab_content_active" id="tabcontent1">                        
                        <p><label>Air Conditioning:</label> {{ ($sharedvehicle->vehicle->air_conditioning=="1")?'Yes':'No' }}</p>
                        <p><label>Navigation System:</label> {{ ($sharedvehicle->vehicle->navigation_system=="1")?'Yes':'No' }}</p>
                        <p><label>Leather Seats:</label> {{ ($sharedvehicle->vehicle->leather_seats=="1")?'Yes':'No' }}</p>
                        <p><label>Sun Roof:</label> {{ ($sharedvehicle->vehicle->sun_roof=="1")?'Yes':'No' }}</p>
                        <p><label>Panoramic Roof:</label> {{ ($sharedvehicle->vehicle->panoramic_roof=="1")?'Yes':'No' }}</p>
                        <p><label>DVD:</label> {{ ($sharedvehicle->vehicle->dvd=="1")?'Yes':'No' }}</p>
                        <p><label>Heated Seats:</label> {{ ($sharedvehicle->vehicle->heated_seats=="1")?'Yes':'No' }}</p>
                        <p><label>AC Seats:</label> {{ ($sharedvehicle->vehicle->ac_seats=="1")?'Yes':'No' }}</p>
                        <p><label>360 Camera:</label> {{ ($sharedvehicle->vehicle['360_camera']=="1")?'Yes':'No' }}</p>
                        <p><label>Engine :</label> {{ $sharedvehicle->vehicle->engine }}</p>
                        <p><label>Cab Type :</label> {{ ($sharedvehicle->vehicle->style) ? $sharedvehicle->vehicle->style : '-' }}</p>
                        <p><label>Fuel Type :</label> {{ $sharedvehicle->vehicle->made_in }}</p>
                    </div>

                    <div class="tab_content" id="tabcontent2">                                  
                        <p><label>After Market Equipment:</label> {{ ($sharedvehicle->vehicle->after_market_equipment)?$sharedvehicle->vehicle->after_market_equipment:'-' }}</p>
                        <p><label>Original Owner:</label> {{ ($sharedvehicle->vehicle->original_owner==1)?'Yes':'No' }}</p>
                        <p><label>Still Making Monthly Payments:</label> {{ ($sharedvehicle->vehicle->still_making_monthly_payment=="1")?'Yes':'No' }} </p>
                        <p><label>Still Owe:</label> {{ $sharedvehicle->vehicle->still_own }} </p>
                        <p><label>Have Any Accident Claims?:</label> {{ ($sharedvehicle->vehicle->have_any_accident_record=='1')?'Yes':'No' }} </p>
                        <p><label>How Much Are The Claims:</label> {{ ($sharedvehicle->vehicle->how_much_records > 1)?$sharedvehicle->vehicle->how_much_records:0 }}</p>
                        <p><label>Smoked In:</label> {{ ($sharedvehicle->vehicle->smoked_in=="1")?'Yes':'No' }}</p>
                        <p><label>Modified Exhaust :</label> {{ ($sharedvehicle->modified_exhaust=="1")?'Yes':'No' }}</p>

                    </div>  
                    <div class="tab_content" id="tabcontent3">                                  
                        <p><label>Overall Vehicle Condition:</label> {{ $sharedvehicle->vehicle->vehicle_condition }}/10</p>
                        <p><label>Condition of Front Tires:</label> {{ $sharedvehicle->vehicle->front_tires_condition }}</p>
                        <p><label>Condition of Rear Tires:</label> {{ $sharedvehicle->vehicle->back_tires_condition }}</p>
                        <p><label>Warning Lights In Cluster:</label> {{ ($sharedvehicle->vehicle->warning_lights_in_clustor=="1")?$sharedvehicle->vehicle->warning_lights:'No' }}</p>
                        <p><label>Mechanical Issues:</label> {{ ($sharedvehicle->vehicle->mechanical_issues=="1")?'Yes':'No' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="userimgtextbox">
      <div class="usertextbox">
        <p>Sorry ! Your link is invalid.</p>
    </div>
</div>
@endif

</div>
</div>
</div>
<script type="text/javascript" src="{{ asset('js/websiteAssets.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-confirmation.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/lightgallery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="{{ asset('js/lg-thumbnail.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lg-fullscreen.min.js') }}"></script>
<script type="text/javascript" src="https://sachinchoolur.github.io/lightGallery/lightgallery/js/lg-zoom.js"></script>

<script>
    var allImage = {!! json_encode($allImageInner,JSON_UNESCAPED_SLASHES) !!};
    $(document).on('click','.animated-thumbnials', function(e) {
        e.preventDefault();
        $(this).lightGallery({
            dynamic: true,
            thumbnail:true,
            animateThumb: false,
            showThumbByDefault: false,
            dynamicEl:allImage,
        }); 
    });
</script>


<script>
   function slider(){
    $(".tabs li a").click(function() {

                // Active state for tabs
                $(".tabs li a").removeClass("active");
                $(this).addClass("active");
                
                // Active state for Tabs Content
                $(".tab_content_container > .tab_content_active").removeClass("tab_content_active").fadeOut(200);
                $(this.rel).fadeIn(500).addClass("tab_content_active");
                
            }); 
    var $imagesSlider = $(".gallery-slider .gallery-slider__images>div"),
    $thumbnailsSlider = $(".gallery-slider__thumbnails>div");

        // images options
        $imagesSlider.slick({
            speed:300,
            slidesToShow:1,
            slidesToScroll:1,
            cssEase:'linear',
            fade:true,
            draggable:false,
            asNavFor:".gallery-slider__thumbnails>div",
            prevArrow:'.gallery-slider__images .prev-arrow',
            nextArrow:'.gallery-slider__images .next-arrow'
        });

        // thumbnails options
        $thumbnailsSlider.slick({
            speed:300,
            slidesToShow:4,
            slidesToScroll:1,
            cssEase:'linear',
            centerMode:true,
            draggable:false,
            focusOnSelect:true,
            asNavFor:".gallery-slider .gallery-slider__images>div",
            prevArrow:'.gallery-slider__thumbnails .prev-arrow',
            nextArrow:'.gallery-slider__thumbnails .next-arrow',
            responsive: [
            {
                breakpoint: 720,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 350,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
            ]
        });

        /*captions*/

        var $caption = $('.gallery-slider .caption');

        // get the initial caption text
        var captionText = $('.gallery-slider__images .slick-current img').attr('alt');
        updateCaption(captionText);

        // hide the caption before the image is changed
        $imagesSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
            $caption.addClass('hide');
        });

        // update the caption after the image is changed
        $imagesSlider.on('afterChange', function(event, slick, currentSlide, nextSlide){
            captionText = $('.gallery-slider__images .slick-current img').attr('alt');
            updateCaption(captionText);
        });

        function updateCaption(text) {
            // if empty, add a no breaking space
            if (text === '') {
                text = '&nbsp;';
            }
            $caption.html(text);
            $caption.removeClass('hide');
        }
    }

    slider();
</script>
<script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
</script>
<script>
    var toggleBtn = document.querySelector('.sidebar-toggle');
    var sidebar = document.querySelector('.sidebar');

    toggleBtn.addEventListener('click', function() {
        toggleBtn.classList.toggle('is-closed');
        sidebar.classList.toggle('is-closed');
    })

</script>
</body>
</html>
<!-- {{ exit }} -->