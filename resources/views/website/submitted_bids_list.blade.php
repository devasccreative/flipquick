@section('title','FlipQuick | Dealer Sumitted Bids')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="boxandditboxcover" id="sidebarWrap">
     <?php $allImage=[]; ?>

    @include('website.submitted_filter')
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    var allImage = {!! json_encode($allImage,JSON_UNESCAPED_SLASHES) !!};
    $(document).on('click','.animated-thumbnials', function(e) {
        e.preventDefault();
        $(this).lightGallery({
            dynamic: true,
            thumbnail:true,
            animateThumb: false,
            showThumbByDefault: false,
            dynamicEl:allImage,
        }); 
    });
      $(document).on('change','#vin_no',function(e){
        var year = $('#year').val();
        var brand = $('#brand').val();
        var vin_no = $('#vin_no').val();
        var post_type = $('#post_type').val();

         $.ajax({
                url: '?page=1&year='+year+'&brand='+brand+'&vin_no='+vin_no+'&post_type='+post_type,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data){
                if(data.html == " "){
                    $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    return;
                }
                $('.ajax-load').hide();
                $("#sidebarWrap").html(data.html);
                 slider();
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                loadMorePage = false;
            });
    });
    $(document).on('change','#year',function(e){
        var year = $('#year').val();
        var brand = $('#brand').val();
        var vin_no = $('#vin_no').val();
        var post_type = $('#post_type').val();

         $.ajax({
                url: '?page=1&year='+year+'&brand='+brand+'&vin_no='+vin_no+'&post_type='+post_type,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data){
                if(data.html == " "){
                    $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    return;
                }
                $('.ajax-load').hide();
                $("#sidebarWrap").html(data.html);
                 slider();
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                loadMorePage = false;
            });
    });
    $(document).on('change','#brand',function(e){
        var year = $('#year').val();
        var brand = $('#brand').val();
        var vin_no = $('#vin_no').val();
        var post_type = $('#post_type').val();

         $.ajax({
                url: '?page=1&year='+year+'&brand='+brand+'&vin_no='+vin_no+'&post_type='+post_type,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data){
                if(data.html == " "){
                    $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    return;
                }
                $('.ajax-load').hide();
                $("#sidebarWrap").html(data.html);
                 slider();
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                loadMorePage = false;
            });
    });
     $(document).on('change','#post_type',function(e){
        var year = $('#year').val();
        var brand = $('#brand').val();
        var vin_no = $('#vin_no').val();
        var post_type = $('#post_type').val();

         $.ajax({
                url: '?page=1&year='+year+'&brand='+brand+'&vin_no='+vin_no+'&post_type='+post_type,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data){
                if(data.html == " "){
                    $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    return;
                }
                $('.ajax-load').hide();
                $("#sidebarWrap").html(data.html);
                 slider();
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                loadMorePage = false;
            });
    });
    var page = 1;
    var loadMorePage = true;
    $(function() {
        $('#vehicleResult').scroll(function() {
            var $this = $(this);
            $results  = $('#vehicleResult');
            if ($this.scrollTop() + $this.height() >= $results.height()) {
                    // if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight)
                    page++;
                    if(page <= $("#vehicleResult").attr('page')){
                        loadMoreData(page);
                    }else{
                        $('.ajax-load').show();
                        $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    }
                }
            });
    });
    function loadMoreData(page) {
        var year = $('#year').val();
        var brand = $('#brand').val();
        var vin_no = $('#vin_no').val();
        if(loadMorePage){
            $.ajax({
                url: '?page=' + page+'&year='+year+'&brand='+brand+'&vin_no='+vin_no,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data){
                if(data.html == " "){
                    $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    return;
                }
                $('.ajax-load').hide();
                $("#vehicleResult").append(data.html);
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                loadMorePage = false;
            });
        }
    }
    $(document).ready(function() {
        $(document).on('click','.bids_list',function(){
            var id = $(this).attr('data-id');
            $.ajax({
                type: "Get",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route("dealer.view_bid_detail","/") }}/'+id,
                success: function(response)
                {
                    $('.boxandditbox_right').html(response);
                    slider();
                    console.log(response);
                }
            });
        }) 
    })
</script>
@endsection