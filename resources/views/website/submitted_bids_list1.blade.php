@section('title','Flipquick | Dealer Sumitted Bids')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="boxandditboxcover" id="sidebarWrap">
    <?php $allImage=[]; ?>
    @if($dealer && $total > 0)
    <div class="boxandditbox_left" id="sidebar">
        <div class="veiableboxset">
            <div class="veiablebox_title">
                <h3>{{ $total }} {{ ($total > 1) ? 'VEHICLES' : 'VEHICLE' }}  AVAILABLE</h3>
            </div>
            <div class="vehiclesdetaboxcover"  id="vehicleData">
                <div class="vehiclesdetaboxiner" id="vehicleResult" page="{{$lastPage}}">
                    @include('website.bids_list')
                </div>
            </div>
        </div>
    </div>
    <div class="boxandditbox_right" id="sidebar">
        <div class="selectcarboxiner">
            @if($total > 0)
            <div class="carsliderbox">
                <div class="gallery-slider">

                    <div class="gallery-slider__images sldimgbox">
                        <?php
                        $vehicleExteriorPhotos = $bids[0]->vehicle->vehicle_exterior_photos->pluck('image')->toArray();
                        $vehicleDamagePhoto = $bids[0]->vehicle->vehicle_damage_photos->pluck('image')->toArray();
                        $vehicleInteriorPhoto = $bids[0]->vehicle->vehicle_interior_photos->pluck('image')->toArray();
                        $vehicleAllImages = array_merge($vehicleExteriorPhotos,$vehicleInteriorPhoto,$vehicleDamagePhoto);
                        $vehicleAllImage = array_merge($vehicleExteriorPhotos,$vehicleInteriorPhoto,$vehicleDamagePhoto);
                        

                        ?>
                        <div>
                            @foreach($vehicleAllImage as $key => $image)
                            <?php $allImage[] = ['src' => $image,'thumb'=>$image,'subHtml'=>'']; ?>
                            <div class="item">
                               <div class="img-fill animated-thumbnials">
                                <a href="{{ $image }}">
                                    <img src="{{ $image }}" alt="Kitten {{ $key }}">
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>


                <div class="gallery-slider__thumbnails sldthumbnailsbox">
                    <div>
                        @foreach($vehicleAllImage as $key => $image)
                        <div class="item">
                            <div class="img-fill"><img src="{{ $image }}" alt="Kitten {{ $key }}"></div>
                        </div>
                        @endforeach

                    </div>
                    <div class="backnextbtn">
                        <button class="prev-arrow slick-arrow nextpribtnbox">
                            BACK
                        </button>
                        <button class="next-arrow slick-arrow nextpribtnbox">
                            NEXT
                        </button>
                    </div>                              

                </div>
            </div>
        </div>

        <div class="sldboxtitle">
            <?php
            $bid = $bids[0];
            ?>
            <h4>{{ $bid->vehicle->year.' '.$bid->vehicle->make.' '.$bid->vehicle->trim.' '.$bid->vehicle->style }}</h4>
            <p><span>Exterior : {{ $bid->vehicle->exterior_color }}</span> <img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""> <span>Interior : {{ $bid->vehicle->interior_color }}</span></p>
        </div>
        <div class="sedoseboxset">
            <ul>
                <li>{{ $bid->vehicle->body_shape }}</li>
                <li>{{ $bid->vehicle->doors }} Doors</li>
                <li>{{ $bid->vehicle->passangers }} Amount Of Seats</li>
            </ul>
        </div>
        <div class="midetrboxsetiner">
            <ul>
                <li>
                    <img src="{{ asset('assets/svg/FlipQuick_milage.svg') }}" alt="">
                    <p>Milage <br>{{ $bid->vehicle->milage }} kms</p>
                </li>
                <li>
                    <img src="{{ asset('assets/svg/FlipQuick_drive_train.svg') }}" alt="">
                    <p>Drive Train <br>{{ $bid->vehicle->drive_train }}</p>
                </li>
                <li>
                    <img src="{{ asset('assets/svg/FlipQuick_transmission.svg') }}" alt="">
                    <p>Transmission <br>{{ $bid->vehicle->transmission }}</p>
                </li>
                <li>
                    <img src="{{ asset('assets/svg/car-engine.svg') }}" alt="">
                    <p>Engine <br>{{ ucfirst($bid->vehicle->engine) }}</p>
                </li>
            </ul>
        </div>
        <div class="tabboxtsetcover">
            <div class="wrapper">
                <ul class="tabs">
                    <li><a href="javascript:void(0); return false;" rel="#tabcontent1" class="tab active">options</a></li>
                    <li><a href="javascript:void(0); return false;" rel="#tabcontent2" class="tab">additional info</a></li>
                    <li><a href="javascript:void(0); return false;" rel="#tabcontent3" class="tab">vehicle condition</a></li>
                </ul>

                <div class="tab_content_container">
                    <div class="tab_content tab_content_active" id="tabcontent1">                        
                        <p><label>Air Conditioning:</label> {{ ($bid->vehicle->air_conditioning=="1")?'Yes':'No' }}</p>
                        <p><label>Navigation System:</label> {{ ($bid->vehicle->navigation_system=="1")?'Yes':'No' }}</p>
                        <p><label>Leather Seats:</label> {{ ($bid->vehicle->leather_seats=="1")?'Yes':'No' }}</p>
                        <p><label>Sun Roof:</label> {{ ($bid->vehicle->sun_roof=="1")?'Yes':'No' }}</p>
                        <p><label>Panoramic Roof:</label> {{ ($bid->vehicle->panoramic_roof=="1")?'Yes':'No' }}</p>
                        <p><label>DVD:</label> {{ ($bid->vehicle->dvd=="1")?'Yes':'No' }}</p>
                        <p><label>Heated Seats:</label> {{ ($bid->vehicle->heated_seats=="1")?'Yes':'No' }}</p>
                        <p><label>AC Seats:</label> {{ ($bid->vehicle->ac_seats=="1")?'Yes':'No' }}</p>
                        <p><label>360 Camera:</label> {{ ($bid->vehicle['360_camera']=="1")?'Yes':'No' }}</p>
                    </div>
                    <div class="tab_content" id="tabcontent2">                                  
                        <p><label>After Market Equipment:</label> {{ ($bid->vehicle->after_market_equipment)?$bid->vehicle->after_market_equipment:'-' }}</p>
                        <p><label>Original Owner:</label> {{ ($bid->vehicle->original_owner==1)?'Yes':'No' }}</p>
                        <p><label>Still Making Monthly Payments:</label> {{ ($bid->vehicle->still_making_monthly_payment=="1")?'Yes':'No' }} </p>
                        <p><label>Still Owe:</label> {{ $bid->vehicle->still_own }} </p>
                        <p><label>Have Any Accident Claims?:</label> {{ ($bid->vehicle->have_any_accident_record=='1')?'Yes':'No' }} </p>
                        <p><label>How Much Are The Claims:</label> {{ ($bid->vehicle->how_much_records > 1)?$bid->vehicle->how_much_records:1 }}</p>
                        <p><label>Smoked In:</label> {{ ($bid->vehicle->smoked_in=="1")?'Yes':'No' }}</p>
                        <p><label>Modified Exhaust :</label> {{ ($bid->vehicle->modified_exhaust=="1")?'Yes':'No' }}</p>
                        <p><label>Sell / Trade :</label> {{ ($bid->vehicle->wantto_sell_or_tradein)?$bid->vehicle->wantto_sell_or_tradein:'-' }}</p>

                    </div>  
                    <div class="tab_content" id="tabcontent3">                                  
                        <p><label>Overall Vehicle Condition:</label> {{ $bid->vehicle->vehicle_condition }}/10</p>
                        <p><label>Condition of Front Tires:</label> {{ $bid->vehicle->front_tires_condition }}</p>
                        <p><label>Condition of Rear Tires:</label> {{ $bid->vehicle->back_tires_condition }}</p>
                        <p><label>Warning Lights In Cluster:</label> {{ ($bid->vehicle->warning_lights_in_clustor=="1")?$bid->vehicle->warning_lights:'No' }}</p>
                        <p><label>Mechanical Issues:</label> {{ ($bid->vehicle->mechanical_issues=="1")?'Yes':'No' }}</p>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@else
<div class="boxandditbox_left">
    <div class="veiableboxset">
        <div class="veiablebox_title">
            <h3>No Data Found</h3>
        </div>
    </div>
</div>
@endif
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    var allImage = {!! json_encode($allImage,JSON_UNESCAPED_SLASHES) !!};
    $(document).on('click','.animated-thumbnials', function(e) {
        e.preventDefault();
        $(this).lightGallery({
            dynamic: true,
            thumbnail:true,
            animateThumb: false,
            showThumbByDefault: false,
            dynamicEl:allImage,
        }); 
    });
    var page = 1;
    var loadMorePage = true;
    $(function() {
        $('#vehicleResult').scroll(function() {
            var $this = $(this);
            $results  = $('#vehicleResult');
            if ($this.scrollTop() + $this.height() >= $results.height()) {
                    // if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight)
                    page++;
                    if(page <= $("#vehicleResult").attr('page')){
                        loadMoreData(page);
                    }else{
                        $('.ajax-load').show();
                        $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    }
                }
            });
    });
    function loadMoreData(page) {
        if(loadMorePage){
            $.ajax({
                url: '?page=' + page,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data){
                if(data.html == " "){
                    $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    return;
                }
                $('.ajax-load').hide();
                $("#vehicleResult").append(data.html);
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                loadMorePage = false;
            });
        }
    }
    $(document).ready(function() {
        $(document).on('click','.bids_list',function(){
            var id = $(this).attr('data-id');
            $.ajax({
                type: "Get",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route("dealer.view_bid_detail","/") }}/'+id,
                success: function(response)
                {
                    $('.boxandditbox_right').html(response);
                    slider();
                    console.log(response);
                }
            });
        }) 
    })
</script>
@endsection