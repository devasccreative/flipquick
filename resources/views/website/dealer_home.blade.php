@section('title','FlipQuick | Signup')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="container">
	<div class="iamsellerboxset">
		<div class="imgaselldealbox">
			<div class="wrapper">
				<ul class="tabs">
					<li><a href="{{route('home')}}" rel="#tabcontent1" class="tab">I'm a Seller</a></li>
					<li><a href="javascript:void(0);" rel="#tabcontent2" class="tab active">I'm a Dealership</a></li>			    
				</ul>	    
				<div class="tab_content_container">
					<div class="tab_content " id="tabcontent1">
						<div class="iamseltextbox">
							<h3>Want the most for your vehicle?</h3>
							<p>Save time and download FlipQuick to get the maximum bid from dealerships in your area.</p>
							<div class="andiosboxbtn">
								<a href="javascript:void(0);">ANDROID</a>
								<a href="javascript:void(0);">IOS</a>
							</div>
							{{-- <a href="{{ route('about_us') }}" class="letnmorlink">Learn more about FlipQuick</a> --}}
						</div>
					</div>	        
					<div class="tab_content tab_content_active" id="tabcontent2">
						<div class="iamseltextbox">
							<h3>Are you a Dealership 
								looking to buy vehicles 
							from local sellers?</h3>
							<p>Get started with FlipQuick to view daily vehicle listings. Submit your best bid and start buying!</p>
							<div class="andiosboxbtn">
								<a href="javascript:void(0);">ANDROID</a>
								<a href="javascript:void(0);">IOS</a>
							</div>
							@if(!(auth()->guard('dealer')->check() && auth()->guard('dealer')->user()->email_verified_at && auth()->guard('dealer')->user()->is_approved))
							<a href="{{ route('website.dealer.signup') }}" class="letnmorlink">Sign Up!</a> 
							@endif
															{{-- <a href="{{ route('about_us') }}" class="letnmorlink">Learn more about FlipQuick</a> --}}
						</div>
					</div>	      
				</div>
			</div>
		</div>
	</div>

	<div class="doworkboxset">
		<h3>How does it work?</h3>
		<ul>
			<li class="divdiconset1">
				<img src="{{ asset('assets/svg/flipquick_get_best_offer.svg') }}" alt="">				
				<h4>Vehicles are listed</h4>
				<p>Vehicles are listed <br>at 9am and 4pm for 24 hours each.</p>
			</li>
			<li  class="divdiconset2">
				<img src="{{ asset('assets/svg/flipquick_receive_offers.svg') }}" alt="">
				<h4>Make offers</h4>
				<p>Dedicate a time in your day <br>to make offers on all posted vehicles. </p>
			</li>
			<li>
				<img src="{{ asset('assets/svg/flipquick_list_your_car.svg') }}" alt="">
				<h4>Win vehicles</h4>
				<p>Receive the information of the sellers <br>who's vehicles you offered the most for.</p>
			</li>
		</ul>
	</div>

	<!-- <div class="lokselboxcover"> -->
			<!-- <div class="lokselboxleft">
				<img src="{{ asset('assets/images/flipquick_looking_sell_your_car.png') }}" alt="">
			</div> -->
			<!-- <div class="lokselboxright">
				<h3>Looking to sell your car?</h3>
				<p>Install FlipQuick to list your vehicle &<br>
				start receiving offers from dealerships</p>
				<a href="javascript:void(0);">ANDROID</a>
				<a href="javascript:void(0);">IOS</a>
			</div> -->
			<!-- </div> -->

			<div class="midimgcoverbox">			
				<img src="{{ asset('assets/images/flipquick_looking_sell_your_car.png') }}" alt="">			
			</div>


	<!-- <div class="benflpboxcover">
		<h3>Benefits of FlipQuick</h3>
		<div class="benflpboxiner">
			<div class="benflpboxiner_icon">
				<img src="{{ asset('assets/svg/safety-icon.svg') }}" alt="">
			</div>
			<div class="benflpboxiner_text">
				<h4>Safety</h4>
				<p>You will only be in touch with a certified dealership. This decreases the chance of harm associated with meeting unknown people in unknown locations.</p>
			</div>
		</div>
		<div class="benflpboxiner">
			<div class="benflpboxiner_icon">
				<img src="{{ asset('assets/svg/time-icon.svg') }}" alt="">
			</div>
			<div class="benflpboxiner_text">
				<h4>Time</h4>
				<p>Posting your vehicle with FlipQuick saves both effort and time. It eliminates the need for several private showings as well as transportation between multiple dealerships. It is physically impossible to meet with every dealership that FlipQuick gives you access to, especially within 24 hours.</p>
			</div>
		</div>
		<div class="benflpboxiner">
			<div class="benflpboxiner_icon">
				<img src="{{ asset('assets/svg/value-icon.svg') }}" alt="">
			</div>
			<div class="benflpboxiner_text">
				<h4>Value</h4>
				<p>Dealerships are given the opportunity to place only one offer on your vehicle. Since they are competing with eachother, they will only give you their best offer. This saves you the process of negotiating, and connects you directly to the dealership willing to pay you the most for your vehicle.</p>
			</div>
		</div>
		<div class="benflpboxiner">
			<div class="benflpboxiner_icon">
				<img src="{{ asset('assets/svg/pro-icon.svg') }}" alt="">
			</div>
			<div class="benflpboxiner_text">
				<h4>Become a pro</h4>
				<p>Our app guides you through what information is required for dealerships to accurately evaluate your vehicle.</p>
			</div>
		</div>
	</div> -->
	

	<!-- <div class="aryodealcover">
		<div class="aryodealbox_left">
			<h3>Are you a Dealership 
			looking to buy vehicles 
			from local sellers?</h3>
			<p>Get started with FlipQuick to view daily vehicle listings. Submit your best bid and start buying!</p>
			<a href="{{ route('website.dealer.signup') }}">GET STARTED</a>
		</div>
		<div class="aryodealbox_right">
			<img src="{{ asset('assets/images/flipquick_dealership_car.png') }}" alt="">		
		</div>
	</div> -->
</div>

<footer>
	<div class="fotlogomenubox">
		<div class="fotlogomenu_left">
			<a href="{{route('home')}}"><img src="{{ asset('assets/svg/logo.svg') }}" alt=""></a>
		</div>
		<div class="fotlogomenu_right">
			<ul>                     
				<li><a href="javascript:void(0);">Download App <span></span></a></li>
				<li><a href="{{ route('about_us') }}">About us <span></span></a></li>
				<li><a href="{{ route('pricing') }}">Pricing <span></span></a></li>
				<li><a href="{{ route('website.dealer.login') }}">Login <span></span></a></li>
				<li><a href="{{ route('website.dealer.signup') }}">Sign up</a></li>
			</ul>
		</div>
	</div>
	<div class="didevforbox">
		<p>Designed & Developed by <a href="https://foremostdigital.com/">Foremost Digital</a></p>
	</div>
</footer>


@endsection

@section('scripts')

@endsection