@section('title','FlipQuick | Benifits')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="benflpboxcover">
		<h3>Benefits of FlipQuick</h3>
		<div class="benflpboxiner">
			<div class="benflpboxiner_icon">
				<img src="{{ asset('assets/svg/opportunity.svg') }}" alt="">
			</div>
			<div class="benflpboxiner_text">
				<h4>Opportunity</h4>
				<p>Customers loyal to the competitor will give you the opportunity to sell them on your brand.</p>
			</div>
		</div>
		<div class="benflpboxiner">
			<div class="benflpboxiner_icon">
				<img src="{{ asset('assets/svg/skip-search.svg') }}" alt="">
			</div>
			<div class="benflpboxiner_text">
				<h4>Skip the Search</h4>
				<p>No need to search through endless lists of private listings.  We bring the private listings to you at a time convenient to you.</p>
			</div>
		</div>
		<div class="benflpboxiner">
			<div class="benflpboxiner_icon">
				<img src="{{ asset('assets/svg/no-fees.svg') }}" alt="">
			</div>
			<div class="benflpboxiner_text">
				<h4>No fees</h4>
				<p>For the price of one auction buyer's fee, gain access to unlimited vehicles per month. No extra fees. </p>
			</div>
		</div>
		<div class="benflpboxiner">
			<div class="benflpboxiner_icon">
				<img src="{{ asset('assets/svg/missing-info.svg') }}" alt="">
			</div>
			<div class="benflpboxiner_text">
				<h4>No Missing Info</h4>
				<p>You will be given all the information required to make an accurate offer.</p>
			</div>
		</div>
		<div class="benflpboxiner">
			<div class="benflpboxiner_icon">
				<img src="{{ asset('assets/svg/customer-confidence.svg') }}" alt="">
			</div>
			<div class="benflpboxiner_text">
				<h4>Customer Confidence</h4>
				<p>Satisfy your in house customer by helping them post their trade-in on FlipQuick to give them full confidence in their vehicle appraisal. Focus on your sale instead of the trade</p>
			</div>
		</div>
		<div class="backbtnbox">
			<a href="{{route('dealer.dashboard')}}">Back</a>
		</div>
	</div>
	@endsection

@section('scripts')
@endsection