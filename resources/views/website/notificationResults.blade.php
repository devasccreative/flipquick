@if($notifications && $notifications->currentPage() == 1)
<h3>Notifications</h3>
@endif
@forelse($notifications as $notification)
<div class="notfisboxiner">
	<p>{{$notification->message}}</p>
	<h5>{{$notification->created_at->diffForHumans()}} ( {{$notification->created_at->parse($notification['created_at'])->setTimeZone($notification['data']['time_zone'])->format('d M,Y @ h:i A')}} )</h5>
	@if(!$notification->read_at)
	<h6>New</h6>
	@endif
</div>
@empty
<div class="notfisboxiner">
	<p>No result found.</p>
</div>
@endforelse