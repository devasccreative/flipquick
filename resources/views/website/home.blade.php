@section('title','FlipQuick | Home')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="container">
	<div class="iamsellerboxset">
		<div class="imgaselldealbox">
			<div class="wrapper">
				<ul class="tabs">
					<li><a href="javascript:void(0);" rel="#tabcontent1" class="tab active">I'm a Seller</a></li>
					<li><a href="{{route('dealer_home')}}" rel="#tabcontent2" class="tab">I'm a Dealership</a></li>	       
				</ul>	    
				<div class="tab_content_container">
					<div class="tab_content tab_content_active" id="tabcontent1">
						<div class="iamseltextbox">
							<h3>In 24 hours, FlipQuick will get you your vehicle's maximum value for free.</h3>
							<p>Download our app today and have dealerships in your area compete for your vehicle.</p>
							<div class="andiosboxbtn">
								<a href="javascript:void(0);">ANDROID</a>
								<a href="javascript:void(0);">IOS</a>
							</div>
							{{-- <a href="{{ route('about_us') }}" class="letnmorlink">Learn more about FlipQuick</a> --}}
						</div>
					</div>	        
					<div class="tab_content" id="tabcontent2">
						<div class="iamseltextbox">
							<h3>Are you a Dealership 
								looking to buy vehicles 
							from local sellers?</h3>
							<p>Get started with FlipQuick to view daily vehicle listings. Submit your best bid and start buying!</p>
							<div class="andiosboxbtn">
								<a href="javascript:void(0);">ANDROID</a>
								<a href="javascript:void(0);">IOS</a>
							</div>
							@if(!(auth()->guard('dealer')->check() && auth()->guard('dealer')->user()->email_verified_at && auth()->guard('dealer')->user()->is_approved))
							<a href="{{ route('website.dealer.signup') }}" class="letnmorlink">Sign Up!</a> 
							@endif
							{{-- <a href="{{ route('about_us') }}" class="letnmorlink">Learn more about FlipQuick</a> --}}
						</div>
					</div>	      
				</div>
			</div>
		</div>
	</div>
	<div class="doworkboxset">
		<h3>How does it work?</h3>
		<ul>
			<li class="divdiconset1">
				<img src="{{ asset('assets/svg/flipquick_get_best_offer.svg') }}" alt="">				
				<h4>List Your Car</h4>
				<p>List your vehicle in <br>a few easy steps.</p>
			</li>
			<li  class="divdiconset2">
				<img src="{{ asset('assets/svg/flipquick_receive_offers.svg') }}" alt="">
				<h4>Receive Offers</h4>
				<p>Each dealership will place one<br> offer (their best offer).</p>
			</li>
			<li>
				<img src="{{ asset('assets/svg/flipquick_list_your_car.svg') }}" alt="">
				<h4>Get Best Offer</h4>
				<p>You will be informed of the highest offer<br> for your vehicle and put in touch with the dealership.</p>
			</li>
		</ul>
	</div>

	<div class="lokselboxcover">
		<div class="lokselboxleft">
			<img src="{{ asset('assets/images/flipquick_seller_app.png') }}" alt="">
		</div>
		<div class="lokselboxright">
			<h3>Looking to sell your car?</h3>
			<p>Install FlipQuick to list your vehicle &<br>
			start receiving offers from dealerships</p>
			<a href="javascript:void(0);">ANDROID</a>
			<a href="javascript:void(0);">IOS</a>
		</div>
	</div>

	
	<div class="benflpboxcover">
		<h3>Benefits of FlipQuick</h3>
		<div class="benflpboxiner">
			<div class="benflpboxiner_icon">
				<img src="{{ asset('assets/svg/safety-icon.svg') }}" alt="">
			</div>
			<div class="benflpboxiner_text">
				<h4>Safety</h4>
				<p>You will only be in touch with a certified dealership. This decreases the chance of harm associated with meeting unknown people in unknown locations.</p>
			</div>
		</div>
		<div class="benflpboxiner">
			<div class="benflpboxiner_icon">
				<img src="{{ asset('assets/svg/time-icon.svg') }}" alt="">
			</div>
			<div class="benflpboxiner_text">
				<h4>Time</h4>
				<p>Posting your vehicle with FlipQuick saves both effort and time. It eliminates the need for several private showings as well as transportation between multiple dealerships. It is physically impossible to meet with every dealership that FlipQuick gives you access to, especially within 24 hours.</p>
			</div>
		</div>
		<div class="benflpboxiner">
			<div class="benflpboxiner_icon">
				<img src="{{ asset('assets/svg/value-icon.svg') }}" alt="">
			</div>
			<div class="benflpboxiner_text">
				<h4>Value</h4>
				<p>Dealerships are given the opportunity to place only one offer on your vehicle. Since they are competing with eachother, they will only give you their best offer. This saves you the process of negotiating, and connects you directly to the dealership willing to pay you the most for your vehicle.</p>
			</div>
		</div>
		<div class="benflpboxiner">
			<div class="benflpboxiner_icon">
				<img src="{{ asset('assets/svg/pro-icon.svg') }}" alt="">
			</div>
			<div class="benflpboxiner_text">
				<h4>Become a pro</h4>
				<p>Our app guides you through what information is required for dealerships to accurately evaluate your vehicle.</p>
			</div>
		</div>
	</div>
	

	<div class="aryodealcover">
		<div class="aryodealbox_left">
			<h3>Are you a Dealership 
				looking to buy vehicles 
			from local sellers?</h3>
			<p>Get started with FlipQuick to view daily vehicle listings. Submit your best bid and start buying!</p>
			<a href="{{ route('website.dealer.signup') }}">GET STARTED</a>
		</div>
		<div class="aryodealbox_right">
			<img src="{{ asset('assets/images/flipquick_dealership_car.png') }}" alt="">		
		</div>
	</div>
</div>

@include('website.layouts.footer')


@endsection

@section('scripts')

@endsection