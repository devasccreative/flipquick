<div class="veiablebox_title">
                                <?php
                                ?>
                                <h3>{{ $total_4pm_slot }} {{ ($total_4pm_slot > 1) ? 'VEHICLES' : 'VEHICLE' }}  AVAILABLE</h3>
                            </div>
                            <div class="vehiclesdetaboxcover"  id="vehicleData">

     <div class="inputwidthsetbox">                                
                                <div class="form-group">
                                    <label for="usr">Year</label>
                                    <select name="year4pm" class="form-control valid" id="year4pm" value="">
                                        <option value="">All Year</option>
                                        @foreach($vehicleYears4pm as $vehicleYear)
                                        <option value="{{$vehicleYear}}" @if(isset($year) && $year == $vehicleYear) selected @endif>{{$vehicleYear}}</option>
                                        @endforeach                                                                                
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Brand</label>
                                    <select name="brand4pm" class="form-control valid" id="brand4pm" value="">
                                        <option value="">All Brand</option>
                                        @foreach($vehicleBrands4pm as $vehicleBrand)
                                        <option value="{{$vehicleBrand}}" @if(isset($brand) && $brand == $vehicleBrand) selected @endif>{{$vehicleBrand}}</option>
                                        @endforeach                                        
                                    </select>
                                    <label id="placeError" class="error"></label>
                                </div> 
                                <div class="form-group">
                                    <label for="usr">Post type</label>
                                    <select name="posttype4pm" class="form-control valid" id="posttype4pm" value="">
                                        <option value="">All Post type</option>
                                         <option value="sell"  @if(isset($post_type) && $post_type == "sell") selected @endif>sell</option>
                                        <option value="trade for used"  @if(isset($post_type) && $post_type == 'trade for used') selected @endif>trade for used</option>
                                        <option value="trade for new"  @if(isset($post_type) && $post_type == 'trade for new') selected @endif>trade for new</option>

                                    </select>
                                    <label id="placeError" class="error"></label>
                                </div>
                                                        
                            </div>
                                <div class="vehiclesdetaboxiner" id="vehicleResult4pm" page="{{$lastPage_4pm_slot}}">
                                    @include('website.vehicle_list_4pmslot_results')
                                </div>
                            </div>