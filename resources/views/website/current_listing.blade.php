@section('title','FlipQuick | Listing')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="boxandditboxcover">
	<div class="boxandditbox_left">
		<div class="hurryupbox">
			<h3>Hurry up, {{auth('dealer')->user()->first_name.' '.auth('dealer')->user()->last_name}}!</h3>

			<p>Current listings will be removed at {{$data['ends_at']}} {{$data['timezone_name']}}</p>
			<ul>
				<li>
                    <h4 id="hours">00<!-- {{ gmdate('h', $data['time_remaining']) }} --></h4>
                    <p>Hours</p>
                </li>
                <li>
                    <h4 id="minutes">00<!-- {{ gmdate('i', $data['time_remaining']) }} --></h4>
                    <p>Minutes</p>
                </li>
                <li>
                    <h4 id="seconds">00<!-- {{ gmdate('s', $data['time_remaining']) }} --></h4>
                    <p>Seconds</p>
                </li>
            </ul>
        </div>
        <div class="veiableboxset">
         <div class="veiablebox_title">
            <h3>{{ ($data['current_listing']->count() > 1)?$data['current_listing']->count().'VEHICLES':$data['current_listing']->count().'VEHICLE' }} AVAILABLE</h3>
        </div>
        <div class="vehiclesdetaboxcover">
            <div class="vehiclesdetaboxiner">
               @forelse($data['current_listing'] as $vehicle)
               <?php // echo "<pre>"; print_r($vehicle->bids);die(); ?>
               <div class="userimgtextbox">
                  @if($vehicle->vehicle_exterior_photos->count() > 0)
                  <div class="userimgbox">
                    <img src="{{ $vehicle->vehicle_exterior_photos[0]->image }}" alt="">
                </div>
                @endif
                <div class="usertextbox">
                    <a href="javascript:void(0)" class="bids_list" data-id="{{ $vehicle->id }}"><h4>{{ $vehicle->year.' '.$vehicle->make.' '.$vehicle->trim.' '.$vehicle->style.' '.$vehicle->model }}</h4></a>
                    <p><span>{{ $vehicle->milage }} kms </span><img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""><span>{{ $vehicle->bids_count }} Bids</span></p>
                    <p><span>{{ $vehicle->drive_train }} </span><img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""><span>{{ ucfirst($vehicle->transmission) }}</span></p>
                </div>
                <div class="listplacebtnbox">
                 <a href="javascript:void(0)" class="listingditbtn bids_list" data-id="{{ $vehicle->id }}">LISTING DETAILS</a>
                 @if($vehicle->bids->count() == 0)
                 <a href="javascript:void(0)" class="placebidbtn placeBid" data-id="{{ $vehicle->id }}" data-name="{{ $vehicle->year.' '.$vehicle->make.' '.$vehicle->trim.' '.$vehicle->style }}">PLACE BID</a>
                 @endif
             </div>
         </div>
         @empty
         <div class="userimgtextbox">
            <div class="usertextbox">
                <p>No Data Found</p>
            </div>
        </div>
        @endif

    </div>
</div>
</div>
</div>
<div class="boxandditbox_right">
    @if($data['last_wons_bids'] > 0)
    <div class="congrttextbox">
        <h3>Congratulations !</h3>
        <p>You won {{$data['last_wons_bids']}} bids in your previous bidding slot.</p>
        <a href="javascript:void(0)">VIEW SELLER INFO</a>
    </div>
    @endif
    <div class="selectcarboxiner">
        @if($data['current_listing']->count() > 0)
        <?php $vehicle = $data['current_listing'][0]; ?>
        <div class="carsliderbox">
            <div class="gallery-slider">

                <div class="gallery-slider__images sldimgbox">
                    <?php
                    $vehicleExteriorPhotos = $vehicle->vehicle_exterior_photos->pluck('image')->toArray();
                    $vehicleDamagePhoto = $vehicle->vehicle_damage_photos->pluck('image')->toArray();
                    $vehicleInteriorPhoto = $vehicle->vehicle_interior_photos->pluck('image')->toArray();
                    $vehicleAllImages = array_merge($vehicleExteriorPhotos,$vehicleInteriorPhoto,$vehicleDamagePhoto);


                    $vehicleAllImage = array_merge($vehicleExteriorPhotos,$vehicleInteriorPhoto,$vehicleDamagePhoto);

                    ?>
                    <div>
                        @foreach($vehicleAllImage as $key => $image)

                        <div class="item">
                            <div class="img-fill"><img src="{{ $image }}" alt="Kitten {{ $key }}"></div>
                        </div>
                        @endforeach
                    </div>
                </div>


                <div class="gallery-slider__thumbnails sldthumbnailsbox">
                    <div>
                        @foreach($vehicleAllImage as $key => $image)

                        <div class="item">
                            <div class="img-fill"><img src="{{ $image }}" alt="Kitten {{ $key }}"></div>
                        </div>
                        @endforeach

                    </div>
                    <div class="backnextbtn">
                        <button class="prev-arrow slick-arrow nextpribtnbox">
                            BACK
                        </button>
                        <button class="next-arrow slick-arrow nextpribtnbox">
                            NEXT
                        </button>
                    </div>                              

                </div>
            </div>
        </div>

        <div class="sldboxtitle">
            <h4>{{ $vehicle->year.' '.$vehicle->make.' '.$vehicle->trim.' '.$vehicle->style }}</h4>
            <p><span>Exterior : {{ $vehicle->exterior_color }}</span> <img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""> <span>Interior : {{ $vehicle->interior_color }}</span></p>
        </div>
        <div class="sedoseboxset">
            <ul>
                <li>{{ $vehicle->body_shape }}</li>
                <li>{{ $vehicle->doors }} Doors</li>
                <li>{{ $vehicle->passangers }} Amount Of Seats</li>
            </ul>
        </div>
        <div class="midetrboxsetiner">
            <ul>
                <li>
                    <img src="{{ asset('assets/svg/FlipQuick_milage.svg') }}" alt="">
                    <p>Milage <br>{{ $vehicle->milage }} kms</p>
                </li>
                <li>
                    <img src="{{ asset('assets/svg/FlipQuick_drive_train.svg') }}" alt="">
                    <p>Drive Train <br>{{ $vehicle->drive_train }}</p>
                </li>
                <li>
                    <img src="{{ asset('assets/svg/FlipQuick_transmission.svg') }}" alt="">
                    <p>Transmission <br>{{ ucfirst($vehicle->transmission) }}</p>
                </li>
                <li>
                    <img src="{{ asset('assets/svg/car-engine.svg') }}" alt="">
                    <p>Engine <br>{{ ucfirst($vehicle->engine) }}</p>
                </li>
            </ul>
        </div>
        <div class="tabboxtsetcover">
            <div class="wrapper">
                <ul class="tabs">
                    <li><a href="javascript:void(0); return false;" rel="#tabcontent1" class="tab active">options</a></li>
                    <li><a href="javascript:void(0); return false;" rel="#tabcontent2" class="tab">additional info</a></li>
                    <li><a href="javascript:void(0); return false;" rel="#tabcontent3" class="tab">vehicle condition</a></li>
                </ul>

                <div class="tab_content_container">
                    <div class="tab_content tab_content_active" id="tabcontent1">                        
                        <p><label>Air Conditioning:</label> {{ ($vehicle->air_conditioning=="1")?'Yes':'No' }}</p>
                        <p><label>Navigation System:</label> {{ ($vehicle->navigation_system=="1")?'Yes':'No' }}</p>
                        <p><label>Leather Seats:</label> {{ ($vehicle->leather_seats=="1")?'Yes':'No' }}</p>
                        <p><label>Sun Roof:</label> {{ ($vehicle->sun_roof=="1")?'Yes':'No' }}</p>
                        <p><label>Panoramic Roof:</label> {{ ($vehicle->panoramic_roof=="1")?'Yes':'No' }}</p>
                        <p><label>DVD:</label> {{ ($vehicle->dvd=="1")?'Yes':'No' }}</p>
                        <p><label>Heated Seats:</label> {{ ($vehicle->heated_seats=="1")?'Yes':'No' }}</p>
                        <p><label>AC Seats:</label> {{ ($vehicle->ac_seats=="1")?'Yes':'No' }}</p>
                        <p><label>360 Camera:</label> {{ ($vehicle['360_camera']=="1")?'Yes':'No' }}</p>
                        <p><label>Engine :</label> {{ $vehicle->engine }}</p>
                        <p><label>Cab Type :</label> {{ ($vehicle->style) ? $vehicle->style : '-' }}</p>
                        <p><label>Fuel Type :</label> {{ $vehicle->made_in }}</p>
                    </div>

                    <div class="tab_content" id="tabcontent2">                                  
                        <p><label>After Market Equipment:</label> {{ ($vehicle->after_market_equipment)?$vehicle->after_market_equipment:'-' }}</p>
                        <p><label>Original Owner:</label> {{ ($vehicle->original_owner==1)?'Yes':'No' }}</p>
                        <p><label>Still Making Monthly Payments:</label> {{ ($vehicle->still_making_monthly_payment=="1")?'Yes':'No' }} </p>
                        <p><label>Still Owe:</label> {{ $vehicle->still_own }} </p>
                        <p><label>Have Any Accident Claims?:</label> {{ ($vehicle->have_any_accident_record=='1')?'Yes':'No' }} </p>
                        <p><label>How Much Are The Claims:</label> {{ ($vehicle->how_much_records > 1)?$vehicle->how_much_records:0 }}</p>
                        <p><label>Smoked In:</label> {{ ($vehicle->smoked_in=="1")?'Yes':'No' }}</p>
                        <p><label>Modified Exhaust :</label> {{ ($vehicle->modified_exhaust=="1")?'Yes':'No' }}</p>
                        <p><label>Sell / Trade :</label> {{ ($vehicle->wantto_sell_or_tradein)?$vehicle->wantto_sell_or_tradein:'-' }}</p>

                    </div>  
                    <div class="tab_content" id="tabcontent3">                                  
                        <p><label>Overall Vehicle Condition:</label> {{ $vehicle->vehicle_condition }}/10</p>
                        <p><label>Condition of Front Tires:</label> {{ $vehicle->front_tires_condition }}</p>
                        <p><label>Condition of Rear Tires:</label> {{ $vehicle->back_tires_condition }}</p>
                        <p><label>Warning Lights In Cluster:</label> {{ ($vehicle->warning_lights_in_clustor=="1")?$vehicle->warning_lights:'No' }}</p>
                        <p><label>Mechanical Issues:</label> {{ ($vehicle->mechanical_issues=="1")?'Yes':'No' }}</p>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
</div>
<!-- Modal -->
<div id="placeBidModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    	<form id="placeBidFrm">
    		@csrf
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title">Place A Bid</h4>
       </div>
       <div class="modal-body">
           <div class="inputwidthsetfull">
               <div class="form-group">
                   <label for="usr">Amount ($)*</label>
                   <input type="text" name="amount" class="form-control" id="amount" autocomplete="false" placeholder="Enter Bid Amount" required="">
               </div>                              
           </div>
       </div>
       <input type="hidden" name="vehicle_id" id="vehicle_id">
       <div class="modal-footer">
          <div class="listplacebtnbox">
           <button type="submit" class="placebidbtn">PLACE BID</button>
       </div>
   </div>
</form>
</div>

</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        /* TIMER */
        var upgradeTime = parseInt("{{ $data['time_remaining']  }}");
        var seconds = upgradeTime;
        // function timer() {
            setInterval(function(){ 
                var days        = Math.floor(seconds/24/60/60);
                var hoursLeft   = Math.floor((seconds) - (days*86400));
                var hours       = Math.floor(hoursLeft/3600);
                var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
                var minutes     = Math.floor(minutesLeft/60);
                var remainingSeconds = seconds % 60;
                function pad(n) {
                    return (n < 10 ? "0" + n : n);
                }
                $('#hours').text(hours);
                $('#minutes').text(minutes);
                $('#seconds').text(remainingSeconds);
                if (seconds == 0) {
                    $('#hours').text(hours);
                    $('#minutes').text(minutes);
                    $('#seconds').text(remainingSeconds);
                } else {
                    seconds--;
                }
            },1000);
            /* END TIMER */

            $(document).on('click','.bids_list',function(){
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "Get",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ route("dealer.view_bid_detail","/") }}/'+id,
                    success: function(response)
                    {
                        $('.boxandditbox_right').html(response);
                        slider();
                        console.log(response);
                    }
                });
            }) 
        });
    $('.placeBid').click(function(e){
    	e.preventDefault();
    	$('.modal-title').text('Place A Bid For - '+$(this).data('name'));
    	$('#vehicle_id').val($(this).data('id'));
    	$('#placeBidModal').modal('show');
    });
    $('#placeBidFrm').validate({
        focusInvalid: true,
        ignore: "",
        rules: {
            amount:{
                required: true,
                number:true
            },
            vehicle_id:{
                required: true
            },
        },
        messages: {
            amount: "Please enter bid amount",
        },
        // errorPlacement: function(error, element) {
        //     $(element).css({ "border": "#ffad47 1px solid" });
        //     $(element).siblings('.help-block').remove();
        //     if( element.prop('type') == 'select-one' ) {
        //         $(element).parent().append(error);
        //     }else if(element.prop('type') == 'checkbox'){
        //         console.log($(element).parent().siblings('.field_error'));
        //         $(element).parent().siblings('.field_error').append(error);
        //     }else{
        //         error.insertAfter(element);
        //     }
        // },
        submitHandler: function(form) {
            var formData = $(form).serialize();     

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:'POST',
                url:"{{route('dealer.submitABid')}}",
                data:formData,
                beforeSend: function(){
                // $('#updateDoctorBtn').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    toastr.success(data.message);
                    location.reload();
                    break;
                    case false:
                    toastr.warning(data.message);
                    break;
                    default:
                    toastr.error("You are not Authorized to access this page");
                    break;
                }
            },
            complete: function(){
                // $('#updateDoctorBtn').button('reset');
            }
        });
        }
    });
</script>
@endsection
