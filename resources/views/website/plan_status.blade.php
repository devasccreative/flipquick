@section('title','FlipQuick | Plan Expired')
@extends('website.layouts.app')

@section('scrpt')
  <!-- Global site tag (gtag.js) - Google Ads: 656133024 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-656133024"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-656133024'); </script>
@endsection
@section('content')
<div class="cretactbox">
    <!-- <div class="cretact_title">
        <h3>Please verify your email address</h3>
    </div> -->
    <div class="verifemlbox">
        <h5>Hello! {{$dealer->first_name.' '.$dealer->last_name}}<br>
            @if($dealer->membership_plan_id == 1) Your membership plan is expired. Please renew your plan. @else Your credit card has some issue. Please contact admin. @endif</h5>
        </div>
    </div>

    @endsection

    @section('scripts')
    <script type="text/javascript">
    </script>
    @endsection