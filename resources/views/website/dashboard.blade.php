@section('title','FlipQuick | Dashboard')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="boxandditboxcover" id="sidebarWrap">
	<div class="boxandditbox_left" id="sidebar">
		@if($data['last_wons_bids'] > 0)
		<div class="congrttextbox">
			<h3>Congratulations !</h3>
			<p>You won {{$data['last_wons_bids']}} {{ ($data['last_wons_bids']>1) ? 'bids' : 'bid' }} in your previous bidding slot.</p>
			<a href="{{ route('dealer.won_bids') }}">VIEW SELLER INFO</a>
		</div>
		@endif
		
        <div class="timetabbox">
            <div class="wrapper">
                <ul class="tabs" id="tabs2">
                    <li><a href="javascript:void(0); return false;" rel="#tabcontent5" class="tab active">Expires at 9:00 AM</a></li>
                    <li><a href="javascript:void(0); return false;" rel="#tabcontent6" class="tab">Expires at 4:00 PM</a></li>
                    <li><a href="javascript:void(0); return false;" rel="#tabcontent7" class="tab">Repo</a></li>
                </ul>
                <div class="tab_content_container" id="tab_content2">
                    <div class="tab_content tab_content_active" id="tabcontent5">                        
                        <div class="hurryupbox" @if($total_9am_slot < 1) style="display:none;" @endif>
                            <h3>Hurry up, {{auth('dealer')->user()->first_name.' '.auth('dealer')->user()->last_name}}!</h3>
                            <ul>
                                <li>
                                    <h4 id="hours9am">00<!-- {{ gmdate('h', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Hours</p>
                                </li>
                                <li>
                                    <h4 id="minutes9am">00<!-- {{ gmdate('i', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Minutes</p>
                                </li>
                                <li>
                                    <h4 id="seconds9am">00<!-- {{ gmdate('s', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Seconds</p>
                                </li>
                            </ul>
                        </div>
                        <div class="veiableboxset" id="nineamfilter">
                            @include('website.vehicle_list_9amslot_filter')
                        </div>
                    </div>
                    <div class="tab_content" id="tabcontent6">                                  
                        <div class="hurryupbox" @if($total_4pm_slot < 1) style="display:none;" @endif>
                            <h3>Hurry up, {{auth('dealer')->user()->first_name.' '.auth('dealer')->user()->last_name}}!</h3>
                            <ul>
                                <li>
                                    <h4 id="hours4pm">00<!-- {{ gmdate('h', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Hours</p>
                                </li>
                                <li>
                                    <h4 id="minutes4pm">00<!-- {{ gmdate('i', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Minutes</p>
                                </li>
                                <li>
                                    <h4 id="seconds4pm">00<!-- {{ gmdate('s', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Seconds</p>
                                </li>
                            </ul>
                        </div>
                        <div class="veiableboxset" id="fourpmfilter">
                            @include('website.vehicle_list_4pmslot_filter')
                        </div>

                    </div>  
                    <div class="tab_content" id="tabcontent7">                        
                        <div class="hurryupbox" @if($total_repo_slot < 1) style="display:none;" @endif>
                            <h3>Hurry up, {{auth('dealer')->user()->first_name.' '.auth('dealer')->user()->last_name}}!</h3>
                            <ul>
                                <li>
                                    <h4 id="hoursrepo">00<!-- {{ gmdate('h', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Hours</p>
                                </li>
                                <li>
                                    <h4 id="minutesrepo">00<!-- {{ gmdate('i', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Minutes</p>
                                </li>
                                <li>
                                    <h4 id="secondsrepo">00<!-- {{ gmdate('s', $data['nine_am_slot_time_remaining']) }} --></h4>
                                    <p>Seconds</p>
                                </li>
                            </ul>
                        </div>
                        <div class="veiableboxset" id="repofilter">
                            @include('website.vehicle_list_reposlot_filter')
                        </div>
                    </div>                  
                </div>
            </div>
        </div>

    </div>

    <div class="boxandditbox_right" id="sidebar">
        <div class="selectcarboxiner">
            <?php     $allImage=[]; ?>

            @if($total_9am_slot > 0)
            <?php $vehicle = $data['current_listing_9am_slot'][0]; ?>
            <div class="carsliderbox">
                <div class="gallery-slider">

                    <div class="gallery-slider__images sldimgbox">
                        <?php
                        $vehicleExteriorPhotos = $vehicle->vehicle_exterior_photos->pluck('image')->toArray();
                        $vehicleDamagePhoto = $vehicle->vehicle_damage_photos->pluck('image')->toArray();
                        $vehicleInteriorPhoto = $vehicle->vehicle_interior_photos->pluck('image')->toArray();
                        $vehicleAllImages = array_merge($vehicleExteriorPhotos,$vehicleInteriorPhoto,$vehicleDamagePhoto);
                        $vehicleAllImage = array_merge($vehicleExteriorPhotos,$vehicleInteriorPhoto,$vehicleDamagePhoto);

                        ?>
                        <div>
                            @foreach($vehicleAllImage as $key => $image)
                            <?php $allImage[] = ['src' => $image,'thumb'=>$image,'subHtml'=>'']; ?>
                            <div class="item">
                                <div class="img-fill animated-thumbnials">
                                    <a href="{{ $image }}">
                                        <img src="{{ $image }}" alt="Kitten {{ $key }}">
                                    </a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>


                    <div class="gallery-slider__thumbnails sldthumbnailsbox">
                        <div>
                            @foreach($vehicleAllImage as $key => $image)

                            <div class="item">
                                <div class="img-fill"><img src="{{ $image }}" alt="Kitten {{ $key }}"></div>
                            </div>
                            @endforeach

                        </div>
                        <div class="backnextbtn">
                            <button class="prev-arrow slick-arrow nextpribtnbox">
                                BACK
                            </button>
                            <button class="next-arrow slick-arrow nextpribtnbox">
                                NEXT
                            </button>
                        </div>                              

                    </div>
                </div>
            </div>

            <div class="sldboxtitle">
                <h4>{{ $vehicle->year.' '.$vehicle->make.' '.$vehicle->trim.' '.$vehicle->style }}</h4>
                <p><span class="vehicleVIN">VIN : {{$vehicle->vin_no}}</span> </p>
                <p><span class="vehicleVIN">{{$vehicle->post_type}}</span> </p>
                <p><span class="vehicleVIN">Model : {{$vehicle->model}}</span> </p>
                <p><span>Exterior : {{ $vehicle->exterior_color }}</span> <img src="{{ asset('assets/svg/FlipQuick_ellipse_icon.svg') }}" alt=""> <span>Interior : {{ $vehicle->interior_color }}</span></p>
            </div>
            <div class="sedoseboxset">
                <ul>
                    <li>{{ $vehicle->body_shape }}</li>
                    <li>{{ $vehicle->doors }} Doors</li>
                    <li>{{ $vehicle->passangers }} Seats</li>
                </ul>
            </div>
            <div class="midetrboxsetiner">
                <ul>
                    <li>
                        <img src="{{ asset('assets/svg/FlipQuick_milage.svg') }}" alt="">
                        <p>Milage <br>{{ $vehicle->milage }} kms</p>
                    </li>
                    <li>
                        <img src="{{ asset('assets/svg/FlipQuick_drive_train.svg') }}" alt="">
                        <p>Drive Train <br>{{ $vehicle->drive_train }}</p>
                    </li>
                    <li>
                        <img src="{{ asset('assets/svg/FlipQuick_transmission.svg') }}" alt="">
                        <p>Transmission <br>{{ ucfirst($vehicle->transmission) }}</p>
                    </li>
                    <li>
                        <img src="{{ asset('assets/svg/car-engine.svg') }}" alt="">
                        <p>Engine <br>{{ ucfirst($vehicle->engine) }}</p>
                    </li>
                </ul>
            </div>
            <div class="tabboxtsetcover">
                <div class="wrapper">
                    <ul class="tabs" id="tabs1">
                        <li><a href="javascript:void(0); return false;" rel="#tabcontent1" class="tab active">options</a></li>
                        <li><a href="javascript:void(0); return false;" rel="#tabcontent2" class="tab">additional info</a></li>
                        <li><a href="javascript:void(0); return false;" rel="#tabcontent3" class="tab">vehicle condition</a></li>
                    </ul>

                    <div class="tab_content_container" id="tab_content1">
                        <div class="tab_content tab_content_active" id="tabcontent1">                        
                            <p><label>Air Conditioning:</label> {{ ($vehicle->air_conditioning=="1")?'Yes':'No' }}</p>
                            <p><label>Navigation System:</label> {{ ($vehicle->navigation_system=="1")?'Yes':'No' }}</p>
                            <p><label>Leather Seats:</label> {{ ($vehicle->leather_seats=="1")?'Yes':'No' }}</p>
                            <p><label>Sun Roof:</label> {{ ($vehicle->sun_roof=="1")?'Yes':'No' }}</p>
                            <p><label>Panoramic Roof:</label> {{ ($vehicle->panoramic_roof=="1")?'Yes':'No' }}</p>
                            <p><label>DVD:</label> {{ ($vehicle->dvd=="1")?'Yes':'No' }}</p>
                            <p><label>Heated Seats:</label> {{ ($vehicle->heated_seats=="1")?'Yes':'No' }}</p>
                            <p><label>AC Seats:</label> {{ ($vehicle->ac_seats=="1")?'Yes':'No' }}</p>
                            <p><label>360 Camera:</label> {{ ($vehicle['360_camera']=="1")?'Yes':'No' }}</p>
                            <p><label>Engine :</label> {{ $vehicle->engine }}</p>
                            <p><label>Cab Type :</label> {{ ($vehicle->style) ? $vehicle->style : '-' }}</p>
                            <p><label>Fuel Type :</label> {{ $vehicle->made_in }}</p>
                        </div>

                        <div class="tab_content" id="tabcontent2">                                  
                            <p><label>After Market Equipment:</label> {{ ($vehicle->after_market_equipment)?$vehicle->after_market_equipment:'-' }}</p>
                            <p><label>Original Owner:</label> {{ ($vehicle->original_owner==1)?'Yes':'No' }}</p>
                            <p><label>Still Making Monthly Payments:</label> {{ ($vehicle->still_making_monthly_payment=="1")?'Yes':'No' }} </p>
                            <p><label>Still Owe:</label> {{ $vehicle->still_own }} </p>
                            <p><label>Have Any Accident Claims?:</label> {{ ($vehicle->have_any_accident_record=='1')?'Yes':'No' }} </p>
                            <p><label>How Much Are The Claims:</label> {{ ($vehicle->how_much_records > 1)?$vehicle->how_much_records:0 }}</p>
                            <p><label>Smoked In:</label> {{ ($vehicle->smoked_in=="1")?'Yes':'No' }}</p>
                            <p><label>Modified Exhaust :</label> {{ ($vehicle->modified_exhaust=="1")?'Yes':'No' }}</p>
                        </div>  
                        <div class="tab_content" id="tabcontent3">                                  
                            <p><label>Overall Vehicle Condition:</label> {{ $vehicle->vehicle_condition }}/10</p>
                            <p><label>Condition of Front Tires:</label> {{ $vehicle->front_tires_condition }}</p>
                            <p><label>Condition of Rear Tires:</label> {{ $vehicle->back_tires_condition }}</p>
                            <p><label>Warning Lights:</label> {{ ($vehicle->warning_lights=="1")?'Yes':'No' }}</p>
                            <p><label>Mechanical Issues:</label> {{ ($vehicle->mechanical_issues=="1")?'Yes':'No' }}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
<!-- Modal -->
<div id="placeBidModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    	<form id="placeBidFrm">
    		@csrf
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">&times;</button>
             <h4 class="modal-title">Place A Bid</h4>
         </div>
         <div class="modal-body">
             <div class="inputwidthsetfull">
                 <div class="form-group">
                    <label for="usr">Amount ($)*</label>
                    <input type="text" name="amount" class="form-control" id="amount" autocomplete="false" placeholder="Enter Bid Amount" required="">
                </div>                              
            </div>
        </div>
        <input type="hidden" name="vehicle_id" id="vehicle_id">
        <div class="modal-footer">
          <div class="listplacebtnbox">
             <button type="submit" class="placebidbtn">PLACE BID</button>
         </div>
     </div>
 </form>
</div>

</div>
</div>
<!-- Modal -->
<div id="shareVehicleModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <form id="shareVehicleFrm">
            @csrf
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">&times;</button>
             <h4 class="modal-title">Share Vehicle</h4>
         </div>
         <div class="modal-body">
             <div class="inputwidthsetfull">
                 <div class="form-group">
                     <label for="usr">Email *</label>
                     <input type="email" name="email" class="form-control" id="email" autocomplete="false" placeholder="Enter Email" required="">
                 </div>                              
             </div>
         </div>
         <input type="hidden" name="vehicle_id" id="vehicleId">
         <div class="modal-footer">
          <div class="listplacebtnbox">
             <button type="submit" class="placebidbtn">SHARE</button>
         </div>
     </div>
 </form>
</div>

</div>
</div>
@endsection

@section('scripts')

<script>
    var allImage = {!! json_encode($allImage,JSON_UNESCAPED_SLASHES) !!};
    $(document).on('click','.animated-thumbnials', function(e) {
        e.preventDefault();
        $(this).lightGallery({
            dynamic: true,
            thumbnail:true,
            animateThumb: false,
            showThumbByDefault: false,
            dynamicEl:allImage,
        }); 
    });
//9am slot
var page = 1;
var loadMorePage = true;
$(function() {
    $('#vehicleResult9am').scroll(function() {
        var $this = $(this);
        $results  = $('#vehicleResult9am');
        if ($this.scrollTop() + $this.height() >= $results.height()) {
        // if ($(window).scrollTop() == $(document).height() - $(window).height()) {
         page++;
         if(page <= $("#vehicleResult9am").attr('page')){
            loadMoreData(page);
        }else{
            $('.ajax-load').show();
            $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
        }
    }
});
});

 $(document).on('change','#year9am',function(e){
        var year = $('#year9am').val();
        var brand = $('#brand9am').val();
        var post_type = $('#posttype9am').val();
         $.ajax({
                url: '?page9am=1&year='+year+'&brand='+brand+'&post_type='+post_type,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data){
                if(data.html == " "){
                    $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    return;
                }
                $('.ajax-load').hide();
                $("#nineamfilter").html(data.html);
                 slider();
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                loadMorePage = false;
            });
    });

 $(document).on('change','#posttype9am',function(e){
        var year = $('#year9am').val();
        var brand = $('#brand9am').val();
        var post_type = $('#posttype9am').val();
         $.ajax({
                url: '?page9am=1&year='+year+'&brand='+brand+'&post_type='+post_type,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data){
                if(data.html == " "){
                    $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    return;
                }
                $('.ajax-load').hide();
                $("#nineamfilter").html(data.html);
                 slider();
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                loadMorePage = false;
            });
    });
    $(document).on('change','#brand9am',function(e){
        var year = $('#year9am').val();
        var brand = $('#brand9am').val();
        var post_type = $('#posttype9am').val();

         $.ajax({
                url: '?page9am=1&year='+year+'&brand='+bran+'&post_type='+post_type,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data){
                if(data.html == " "){
                    $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    return;
                }
                $('.ajax-load').hide();
                $("#nineamfilter").html(data.html);
                 slider();
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                loadMorePage = false;
            });
    });
    $(document).on('change','#yearrepo',function(e){
        var year = $('#yearrepo').val();
        var brand = $('#brandrepo').val();
        var post_type = $('#posttyperepo').val();

         $.ajax({
                url: '?pagerepo=1&year='+year+'&brand='+brand+'&post_type='+post_type,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data){
                if(data.html == " "){
                    $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    return;
                }
                $('.ajax-load').hide();
                $("#nineamfilter").html(data.html);
                 slider();
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                loadMorePage = false;
            });
    });
    $(document).on('change','#brandrepo',function(e){
        var year = $('#yearrepo').val();
        var brand = $('#brandrepo').val();
        var post_type = $('#posttyperepo').val();

         $.ajax({
                url: '?pagerepo=1&year='+year+'&brand='+brand+'&post_type='+post_type,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data){
                if(data.html == " "){
                    $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    return;
                }
                $('.ajax-load').hide();
                $("#repofilter").html(data.html);
                 slider();
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                loadMorePage = false;
            });
    });
    $(document).on('change','#posttyperepo',function(e){
        var year = $('#yearrepo').val();
        var brand = $('#brandrepo').val();
        var post_type = $('#posttyperepo').val();

         $.ajax({
                url: '?pagerepo=1&year='+year+'&brand='+brand+'&post_type='+post_type,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data){
                if(data.html == " "){
                    $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    return;
                }
                $('.ajax-load').hide();
                $("#repofilter").html(data.html);
                 slider();
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                loadMorePage = false;
            });
    });

    $(document).on('change','#year4pm',function(e){
        var year = $('#year4pm').val();
        var brand = $('#brand4pm').val();
        var post_type = $('#posttype4pm').val();

         $.ajax({
                url: '?page4pm=1&year='+year+'&brand='+brand+'&post_type='+post_type,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data){
                if(data.html == " "){
                    $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    return;
                }
                $('.ajax-load').hide();
                $("#fourpmfilter").html(data.html);
                 slider();
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                loadMorePage = false;
            });
    });
    $(document).on('change','#brand4pm',function(e){
        var year = $('#year4pm').val();
        var brand = $('#brand4pm').val();
        var post_type = $('#posttype4pm').val();

         $.ajax({
                url: '?page4pm=1&year='+year+'&brand='+brand+'&post_type='+post_type,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data){
                if(data.html == " "){
                    $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    return;
                }
                $('.ajax-load').hide();
                $("#fourpmfilter").html(data.html);
                 slider();
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                loadMorePage = false;
            });
    });
    $(document).on('change','#posttype4pm',function(e){
        var year = $('#year4pm').val();
        var brand = $('#brand4pm').val();
        var post_type = $('#posttype4pm').val();

         $.ajax({
                url: '?page4pm=1&year='+year+'&brand='+brand+'&post_type='+post_type,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data){
                if(data.html == " "){
                    $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                    return;
                }
                $('.ajax-load').hide();
                $("#fourpmfilter").html(data.html);
                 slider();
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                loadMorePage = false;
            });
    });
function loadMoreData(page) {
    if(loadMorePage){
        $.ajax({
            url: '?page9am=' + page,
            type: "get",
            beforeSend: function()
            {
                $('.ajax-load').show();
            }
        })
        .done(function(data){
            if(data.html == " "){
                $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                return;
            }
            $('.ajax-load').hide();
            $("#vehicleResult9am").append(data.html);
        })
        .fail(function(jqXHR, ajaxOptions, thrownError){
            loadMorePage = false;
        });
    }
}
 //4pm slot
 var page = 1;
 var loadMorePage = true;
 $(function() {
    $('#vehicleResult4pm').scroll(function() {
        var $this = $(this);
        $results  = $('#vehicleResult4pm');
        if ($this.scrollTop() + $this.height() >= $results.height()) {
        // if ($(window).scrollTop() == $(document).height() - $(window).height()) {
         page++;
         if(page <= $("#vehicleResult4pm").attr('page')){
            loadMoreData4pm(page);
        }else{
            $('.ajax-load').show();
            $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
        }
    }
});
});
 function loadMoreData4pm(page) {
    if(loadMorePage){
        $.ajax({
            url: '?page4pm=' + page,
            type: "get",
            beforeSend: function()
            {
                $('.ajax-load').show();
            }
        })
        .done(function(data){
            if(data.html == " "){
                $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                return;
            }
            $('.ajax-load').hide();
            $("#vehicleResult4pm").append(data.html);
        })
        .fail(function(jqXHR, ajaxOptions, thrownError){
            loadMorePage = false;
        });
    }
}
 //4pm slot
 var page = 1;
 var loadMorePage = true;
 $(function() {
    $('#vehicleResultrepo').scroll(function() {
        var $this = $(this);
        $results  = $('#vehicleResultrepo');
        if ($this.scrollTop() + $this.height() >= $results.height()) {
        // if ($(window).scrollTop() == $(document).height() - $(window).height()) {
         page++;
         if(page <= $("#vehicleResultrepo").attr('page')){
            loadMoreDatarepo(page);
        }else{
            $('.ajax-load').show();
            $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
        }
    }
});
});
 function loadMoreDatarepo(page) {
    if(loadMorePage){
        $.ajax({
            url: '?pagerepo=' + page,
            type: "get",
            beforeSend: function()
            {
                $('.ajax-load').show();
            }
        })
        .done(function(data){
            if(data.html == " "){
                $('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
                return;
            }
            $('.ajax-load').hide();
            $("#vehicleResultrepo").append(data.html);
        })
        .fail(function(jqXHR, ajaxOptions, thrownError){
            loadMorePage = false;
        });
    }
}
</script>
<script type="text/javascript">
    $(document).ready(function() {
        /* TIMER */
        var upgradeTime = parseInt("{{ $data['nine_am_slot_time_remaining']  }}");
        var seconds = upgradeTime;
        // function timer() {
            setInterval(function(){ 
                var days        = Math.floor(seconds/24/60/60);
                var hoursLeft   = Math.floor((seconds) - (days*86400));
                var hours       = Math.floor(hoursLeft/3600);
                var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
                var minutes     = Math.floor(minutesLeft/60);
                var remainingSeconds = seconds % 60;
                function pad(n) {
                    return (n < 10 ? "0" + n : n);
                }
                $('#hours9am').text(hours);
                $('#minutes9am').text(minutes);
                $('#seconds9am').text(remainingSeconds);
                if (seconds == 0) {
                    $('#hours9am').text(hours);
                    $('#minutes9am').text(minutes);
                    $('#seconds9am').text(remainingSeconds);
                } else {
                    seconds--;
                }
            },1000);

            var upgradeTime4pm = parseInt("{{ $data['four_pm_slot_time_remaining']  }}");
            var seconds4pm = upgradeTime4pm;
        // function timer() {
            setInterval(function(){ 
                var days        = Math.floor(seconds4pm/24/60/60);
                var hoursLeft   = Math.floor((seconds4pm) - (days*86400));
                var hours       = Math.floor(hoursLeft/3600);
                var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
                var minutes     = Math.floor(minutesLeft/60);
                var remainingSeconds = seconds4pm % 60;
                function pad(n) {
                    return (n < 10 ? "0" + n : n);
                }
                $('#hours4pm').text(hours);
                $('#minutes4pm').text(minutes);
                $('#seconds4pm').text(remainingSeconds);
                if (seconds4pm == 0) {
                    $('#hours4pm').text(hours);
                    $('#minutes4pm').text(minutes);
                    $('#seconds4pm').text(remainingSeconds);
                } else {
                    seconds4pm--;
                }
            },1000);
             var upgradeTimerepo = parseInt("{{ $data['nine_am_slot_time_remaining']  }}");
            var secondsrepo = upgradeTimerepo;
        // function timer() {
            setInterval(function(){ 
                var days        = Math.floor(secondsrepo/24/60/60);
                var hoursLeft   = Math.floor((secondsrepo) - (days*86400));
                var hours       = Math.floor(hoursLeft/3600);
                var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
                var minutes     = Math.floor(minutesLeft/60);
                var remainingSeconds = secondsrepo % 60;
                function pad(n) {
                    return (n < 10 ? "0" + n : n);
                }
                $('#hoursrepo').text(hours);
                $('#minutesrepo').text(minutes);
                $('#secondsrepo').text(remainingSeconds);
                if (secondsrepo == 0) {
                    $('#hoursrepo').text(hours);
                    $('#minutesrepo').text(minutes);
                    $('#secondsrepo').text(remainingSeconds);
                } else {
                    secondsrepo--;
                }
            },1000);
            /* END TIMER */
            $(document).on('click','.bids_list',function(e){
                e.preventDefault();
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "Get",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ route("dealer.view_bid_detail","/") }}/'+id,
                    success: function(response)
                    {
                        $('.boxandditbox_right').html(response);
                        slider();
                        console.log(response);
                    }
                });
            }) 
        });
    $(document).on('click','.placeBid',function(e){
       e.preventDefault();
       $('.modal-title').text('Place A Bid For - '+$(this).data('name'));
       $('#vehicle_id').val($(this).data('id'));
       $('#placeBidModal').modal('show');
   });
    $(document).on('click','.shareVehicle',function(e){
       e.preventDefault();
       $('.modal-title').text('Share - '+$(this).data('name'));
       $('#vehicleId').val($(this).data('id'));
       $('#shareVehicleModal').modal('show');
   });
    jQuery.validator.addMethod("rageIn50", function(value, element) {
        return (value%50 == 0);
    },"Please enter bid amount by $50 increments");
    $('#placeBidFrm').validate({
        focusInvalid: true,
        ignore: "",
        rules: {
            amount:{
                required: true,
                number:true,
                rageIn50:true
            },
            vehicle_id:{
                required: true
            },
        },
        messages: {
            amount: {
                required : "Please enter bid amount",
            }
        },
        submitHandler: function(form) {
            var formData = $(form).serialize();     

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:'POST',
                url:"{{route('dealer.submitABid')}}",
                data:formData,
                beforeSend: function(){
                // $('#updateDoctorBtn').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    toastr.success(data.message);
                    location.reload();
                    break;
                    case false:
                    toastr.warning(data.message);
                    break;
                    default:
                    toastr.error("You are not Authorized to access this page");
                    break;
                }
            },
            complete: function(){
                // $('#updateDoctorBtn').button('reset');
            }
        });
        }
    });

    $('#shareVehicleFrm').validate({
        focusInvalid: true,
        ignore: "",
        rules: {
            email:{
                required: true,
                email:true
            },
        },
        messages: {
            email: {
                required:"Please enter email",
            },
        },
        submitHandler: function(form) {
            var formData = $(form).serialize();     

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:'POST',
                url:"{{route('dealer.shareVehicle')}}",
                data:formData,
                beforeSend: function(){
                // $('#updateDoctorBtn').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    toastr.success(data.message);
                    location.reload();
                    break;
                    case false:
                    toastr.warning(data.message);
                    break;
                    default:
                    toastr.error("You are not Authorized to access this page");
                    break;
                }
            },
            complete: function(){
                // $('#updateDoctorBtn').button('reset');
            }
        });
        }
    });
</script>
@endsection
