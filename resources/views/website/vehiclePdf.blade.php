<!DOCTYPE html>
<html lang="en">
<head>
	<title></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">	
	<!-- 		<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
	-->
</head>
<body>
	<style>
	body{
		font-family: 'Poppins', sans-serif;
	}
	.clearfix::after {
		content: "";
		clear: both;
		display: table;
	}
	span.vehicleVIN {
		display: inline-block;
		width: 100%;
		margin: 0;
		color: #01034A;
		font-size: 16px;
		font-family: 'Poppins-Medium';
	}
</style>

<div style="width: 100%;text-align: center;">
	<div style="width: 528px;text-align: left;margin: 0 auto;">
		<div style="width: 100%;margin-top: 30px;">
			@if($vehicle->vehicle_exterior_photos->count() > 0)
			<img src="{{ $vehicle->vehicle_exterior_photos[0]->image }}" alt="" style="width: 180px;border-radius: 10px;max-height:130px;">
			@endif
			<div class="clearfix"></div>
			<h3 style="display: inline-block;width: 100%;color: #01034A;font-size: 20px;font-family: 'Poppins', sans-serif;font-weight: 700; margin-bottom: 0;">{{ $vehicle->year.' '.$vehicle->make.' '.$vehicle->trim.' '.$vehicle->style.' '.$vehicle->model }}</h3> 
			<div class="clearfix"></div>
			<p><span style="display: inline-block;width: 100%;color: #01034A;font-size:15px;font-family: 'Poppins', sans-serif;font-weight: 500; margin-bottom: 0;">VIN : {{ $vehicle->vin_no }}</span> </p>
			<div class="clearfix"></div>
			<p style=" display: inline-block;width: 100%;color: #929292;font-size: 15px;font-family: 'Poppins', sans-serif;font-weight: 400;">
				<span style="display: inline-block;float: left;margin-right: 5px;">Exterior : {{ $vehicle->exterior_color }}</span> 
				<span style="display: inline-block;float: left;width: 6px;height: 6px;border-radius: 50%;background: #929292;margin-left: 5px;margin-right: 10px;margin-top: 10px;"></span> 
				<span style="display: inline-block;float: left;margin-right: 5px;">Interior : {{ $vehicle->interior_color }}</span>
			</p>

		</div>
		<div class="clearfix"></div>
		<div style="width: 100%;margin-top: 0px;">
			<ul style="display: inline-block;width: 100%;padding-left: 0;border-top: 1px solid #E8E8E8;padding-top: 5px;padding-bottom: 0px;">
				<li style="display: inline-block;float: left;font-family: 'Poppins', sans-serif;font-weight: 500;color: #929292;font-size: 15px;width: 33.33%;">{{ $vehicle->body_shape }}</li>
				<li style="display: inline-block;float: left;font-family: 'Poppins', sans-serif;font-weight: 500;color: #929292;font-size: 15px;width: 33.33%;">{{ $vehicle->doors }} Doors</li>
				<li style="display: inline-block;float: left;font-family: 'Poppins', sans-serif;font-weight: 500;color: #929292;font-size: 15px;width: 33.33%;">{{ $vehicle->passengers }} Amount Of Seats</li>
			</ul>
		</div>
		<div class="clearfix"></div>
		<div style="width: 100%;margin-top: 8px;">
			<ul style="display: inline-block;width: 100%;padding-left: 0;border-top: 1px solid #E8E8E8;padding-top: 8px;padding-bottom: 0px;">
				<li style="display: inline-block;float: left;width: 25%;text-align: center;">
					<img src="{{ asset('assets/svg/FlipQuick_milage.png') }}" alt="" style="width: 38px;"><br>
					<p style="font-family: 'Poppins', sans-serif;font-weight: 500;color: #929292;display: inline-block;width: 100%;font-size: 13px;">Milage<br>{{ $vehicle->milage }} kms</p>
				</li>
				<li style="display: inline-block;float: left;width: 25%;text-align: center;">
					<img src="{{ asset('assets/svg/FlipQuick_drive_train.png') }}" alt="" style="width: 38px;"><br>
					<p style="font-family: 'Poppins', sans-serif;font-weight: 500;color: #929292;display: inline-block;width: 100%;font-size: 13px;">Drive Train<br>{{ $vehicle->drive_train }}</p>
				</li>
				<li style="display: inline-block;float: left;width: 25%;text-align: center;">
					<img src="{{ asset('assets/svg/FlipQuick_transmission.png') }}" alt="" style="width: 38px;"><br>
					<p style="font-family: 'Poppins', sans-serif;font-weight: 500;color: #929292;display: inline-block;width: 100%;font-size: 13px;">Transmission <br>{{ ucfirst($vehicle->transmission) }}</p>
				</li>
				<li style="display: inline-block;float: left;width: 25%;text-align: center;">
					<img src="{{ asset('assets/svg/FlipQuick_car_engine.png') }}" alt="" style="width: 38px;"><br>
					<p style="font-family: 'Poppins', sans-serif;font-weight: 500;color: #929292;display: inline-block;width: 100%;font-size: 13px;">Engine <br>{{ ucfirst($vehicle->engine) }}</p>
				</li>
			</ul>
		</div> 
		<div class="clearfix"></div>
		<div style="width: 100%;border-top: 1px solid #E8E8E8;margin-bottom: 8px;">
			<h4 style="width: 100%;margin-top: 6px;display: inline-block;color: #01034A;font-size: 16px;font-family: 'Poppins', sans-serif;font-weight: 700;margin-bottom: 4px;">OPTIONS</h4>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Air Conditioning: <span style="margin-left: 4px;font-family: 'Poppins', sans-serif;font-weight: 400;">{{ ($vehicle->air_conditioning=="1")?'Yes':'No' }}</span></p>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Navigation System: <span style="margin-left: 4px;font-family: 'Poppins', sans-serif;font-weight: 400;">{{ ($vehicle->navigation_system=="1")?'Yes':'No' }}</span></p>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Leather Seats: <span style="margin-left: 4px;font-family: 'Poppins', sans-serif;font-weight: 400;">{{ ($vehicle->leather_seats=="1")?'Yes':'No' }}</span></p>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Sun Roof: <span style="margin-left: 4px;font-family: 'Poppins', sans-serif;font-weight: 400;">{{ ($vehicle->sun_roof=="1")?'Yes':'No' }}</span></p>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Panoramic Roof: <span style="margin-left: 4px;font-family: 'Poppins', sans-serif;font-weight: 400;">{{ ($vehicle->panoramic_roof=="1")?'Yes':'No' }}</span></p>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">DVD: <span style="margin-left: 4px;font-family: 'Poppins', sans-serif;font-weight: 400;">{{ ($vehicle->dvd=="1")?'Yes':'No' }}</span></p>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Heated Seats: <span style="margin-left: 4px;font-family: 'Poppins', sans-serif;font-weight: 400;">{{ ($vehicle->heated_seats=="1")?'Yes':'No' }}</span></p>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">AC Seats: <span style="margin-left: 4px;font-family: 'Poppins', sans-serif;font-weight: 400;">{{ ($vehicle->ac_seats=="1")?'Yes':'No' }}</span></p>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">360 Camera: <span style="margin-left: 4px;font-family: 'Poppins', sans-serif;font-weight: 400;">{{ ($vehicle['360_camera']=="1")?'Yes':'No' }}</span></p>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Cab Type: <span style="margin-left: 4px;font-family: 'Poppins', sans-serif;font-weight: 400;">{{ ($vehicle->style) ? $vehicle->style : '-' }}</span></p>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Fuel Type: <span style="margin-left: 4px;font-family: 'Poppins', sans-serif;font-weight: 400;">{{ $vehicle->made_in }}</span></p>
		</div>

		<div style="width: 100%;border-top: 1px solid #E8E8E8;margin-bottom: 8px;">
			<h4 style="width: 100%;margin-top: 6px;display: inline-block;color: #01034A;font-size: 16px;font-family: 'Poppins', sans-serif;font-weight: 700;margin-bottom: 4px;">ADDITIONAL</h4>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">After Market Equipment: <span style="font-family: 'Poppins', sans-serif;font-weight: 400;margin-left: 4px;">{{ ($vehicle->after_market_equipment)?$vehicle->after_market_equipment:'-' }}</span></p>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Original Owner: <span style="font-family: 'Poppins', sans-serif;font-weight: 400;margin-left: 4px;">{{ ($vehicle->original_owner==1)?'Yes':'No' }}</span></p>			
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Still Making Monthly Payments: <span style="font-family: 'Poppins', sans-serif;font-weight: 400;margin-left: 4px;">{{ ($vehicle->still_making_monthly_payment==1)?'Yes':'No' }}</span></p>			
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Still Owe: <span style="font-family: 'Poppins', sans-serif;font-weight: 400;margin-left: 4px;">{{ $vehicle->still_own }}</span></p>			
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Have Any Accident Claims?: <span style="font-family: 'Poppins', sans-serif;font-weight: 400;margin-left: 4px;">{{ ($vehicle->have_any_accident_record==1)?'Yes':'No' }}</span></p>			
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">How Much Are The Claims: <span style="font-family: 'Poppins', sans-serif;font-weight: 400;margin-left: 4px;">{{ ($vehicle->how_much_records > 1)?$vehicle->how_much_records:0 }}</span></p>			
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Smoked In: <span style="font-family: 'Poppins', sans-serif;font-weight: 400;margin-left: 4px;">{{ ($vehicle->smoked_in=="1")?'Yes':'No' }}</span></p>			
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Modified Exhaust: <span style="font-family: 'Poppins', sans-serif;font-weight: 400;margin-left: 4px;">{{ ($vehicle->modified_exhaust=="1")?'Yes':'No' }}</span></p>	
			 <p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Sell / Trade : <span style="font-family: 'Poppins', sans-serif;font-weight: 400;margin-left: 4px;">{{ ($vehicle->wantto_sell_or_tradein)?$vehicle->wantto_sell_or_tradein:'-' }}</span></p>		

		</div>
		<hr style="border: 0;width: 100%;margin-top: 5px;margin-bottom: 5px;">
		<div class="clearfix"></div>
		<div style="width: 50%;border-top: 0 solid #E8E8E8;margin-bottom: 8px;float: left;">
			<h4 style="width: 100%;margin-top: 6px;display: inline-block;color: #01034A;font-size: 16px;font-family: 'Poppins', sans-serif;font-weight: 700;margin-bottom: 4px;">INFOVEHICLE CONDITION</h4>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Overall Vehicle Condition: <span style="font-family: 'Poppins', sans-serif;font-weight: 400;margin-left: 4px;">{{ $vehicle->vehicle_condition }}/10</span></p>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Condition of Front Tires: <span style="font-family: 'Poppins', sans-serif;font-weight: 400;margin-left: 4px;">{{ $vehicle->front_tires_condition }}</span></p>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Condition of Rear Tires: <span style="font-family: 'Poppins', sans-serif;font-weight: 400;margin-left: 4px;">{{ $vehicle->back_tires_condition }}</span></p>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Warning Lights In Clustor: <span style="font-family: 'Poppins', sans-serif;font-weight: 400;margin-left: 4px;">{{ ($vehicle->warning_lights_in_clustor=="1")?$vehicle->warning_lights:'No' }}</span></p>
			<p style="margin-top: 0;margin-bottom: 2px;color: #929292;font-size: 13px;width: 100%;font-family: 'Poppins', sans-serif;font-weight: 600;">Mechanical Issues: <span style="font-family: 'Poppins', sans-serif;font-weight: 400;margin-left: 4px;">{{ ($vehicle->mechanical_issues=="1")?'Yes':'No' }}</span></p>
		</div>

		<div style="width: 50%;border-top: 0 solid #E8E8E8;margin-top: 0;float: left;">
			<h4 style="display: inline-block;width: 100%;color: #01034A;font-size: 17px;font-family: 'Poppins', sans-serif;font-weight: 700;margin-top: 4px;">Seller Info.</h4>
			<br>
			<h5 style="display: inline-block;color: #929292;font-family: 'Poppins', sans-serif;font-weight: 600;font-size: 16px;margin-top: 0px;margin-bottom: 0px;float: left;margin-right: 10px;">{{$vehicle->seller->first_name.' '.$vehicle->seller->last_name}}</h5>
			<br>
			<p  style="display: inline-block;width: 100%;margin: 0;margin-right: 18px;color: #000000;margin-bottom: 0px;font-size: 15px;font-family: 'Poppins', sans-serif;font-weight: 500;"><img src="{{ asset('assets/svg/FlipQuick_phone_icon.svg') }}" style="width: 20px;margin-right: 6px;float: left;margin-top: 0px;"> <span style="margin-left: 25px;"> {{$vehicle->seller->country_code.'-'.$vehicle->seller->phone}}</span></p>
			<br>
			<p style="display: inline-block;width: 100%;margin: 0;margin-right: 18px;color: #000000;margin-bottom: 0px;font-size: 15px;font-family: 'Poppins', sans-serif;font-weight: 500;"><img src="{{ asset('assets/svg/FlipQuick_message_icon.svg') }}" style="width: 20px;margin-right: 6px;float: left;margin-top: 0px;"> <span style="margin-left: 25px;">  {{$vehicle->seller->email}}</span></p>

			<!-- <hr style="border: 0;border-top: 1px solid #E8E8E8;width: 100%;margin-top: 5px;margin-bottom: 5px;"> -->


		</div>

	</div>
</div>


<!-- <script type="text/javascript" src="js/jquery.js"></script> -->
<!-- <script src="js/bootstrap.min.js"></script> -->
</body>
</html>