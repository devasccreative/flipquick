@section('title','FlipQuick | Dealer Notifications')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="boxandditboxcover" id="notificationData">
	<div class="notfisboxcover" id="notificationResult"  page="{{$lastPage}}">
		@include('website.notificationResults')
		
		<!-- <div class="notfisboxiner">
			<p>Hurry Up, 35 new listings are posted today. Submit your bids 
			before the listings goes away.</p>
			<h5>Today @ 4:00 PM</h5>
			<h6>New</h6>
		</div>
		<div class="notfisboxiner">
			<p>Hurry Up, 35 new listings are posted today. Submit your bids 
			before the listings goes away.</p>
			<h5>Today @ 4:00 PM</h5>
		</div>
		<div class="notfisboxiner">
			<p>Hurry Up, 35 new listings are posted today. Submit your bids 
			before the listings goes away.</p>
			<h5>Today @ 4:00 PM</h5>
		</div>
		<div class="notfisboxiner">
			<p>Hurry Up, 35 new listings are posted today. Submit your bids 
			before the listings goes away.</p>
			<h5>Today @ 4:00 PM</h5>
		</div>
		<div class="notfisboxiner">
			<p>Hurry Up, 35 new listings are posted today. Submit your bids 
			before the listings goes away.</p>
			<h5>Today @ 4:00 PM</h5>
		</div> -->
	</div>
	<div class="ajax-load" style="display:none">
		<p><img src="{{ asset('assets/loader.gif') }}">Loading More Notifications</p>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	var page = 1;
	var loadMorePage = true;
	$(function() {
		$(window).scroll(function() {
			var $this = $('#notificationData');
			$results  = $('#notificationResult');

			if ($(window).scrollTop() == $(document).height() - $(window).height()) {
				page++;
				if(page <= $("#notificationResult").attr('page')){
					loadMoreData(page);
				}else{
					$('.ajax-load').show();
					$('.ajax-load').html('');//<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>
				}
			}
		});
	});
	function loadMoreData(page) {
		if(loadMorePage){
			$.ajax({
				url: '?page=' + page,
				type: "get",
				beforeSend: function()
				{
					$('.ajax-load').show();
				}
			})
			.done(function(data){
				if(data.html == " "){
					$('.ajax-load').html('<div class="submittedbidsbox"><div class="sbboxdatetime"><p>No more records found</p></div></div>');
					return;
				}
				$('.ajax-load').hide();
				$("#notificationResult").append(data.html);
			})
			.fail(function(jqXHR, ajaxOptions, thrownError){
				loadMorePage = false;
			});
		}
	}

</script>
@endsection