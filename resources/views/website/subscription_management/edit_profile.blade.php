@section('title','FlipQuick | Subscription Management')
@extends('website.layouts.app')
@section('styles')
<link href="{{ asset('assets/css/card.css') }}" rel="stylesheet" />  

@endsection
@section('content')

<div class="boxandditboxcover">
    <div class="listplacebtnbox">
        <a href="{{route('website.dealer.membership_plan')}}" class="listingditbtn">CHANGE PLAN</a>
    </div>
    <div class="delspselimabox">
        <div class="wrapper">
            <ul class="tabs" id="tabs">
                <li><a href="javascript:void(0);" rel="#tabcontent1" class="active">Profile Information</a></li>
                <li><a href="javascript:void(0);" rel="#planInfo" >Plan Information</a></li>
                <li><a href="javascript:void(0);" rel="#changeCardInfo" >Update Card</a></li>
            </ul>

            <div class="tab_content_container">
                <div class="tab_content tab_content_active" id="tabcontent1">
                    <form id="updateProfileForm" method="post" >
                        @csrf
                        <div class="inputcover">
                            <div class="inputwidthsetfull">
                                <div class="form-group">
                                    <label for="usr">Name*</label>
                                    <input type="text" name="name" class="form-control" id="" placeholder="Enter First Name" value="{{ $profile['billing']['name'] }}">
                                </div>
                            </div>
                            <div class="inputwidthsetbox">
                                <div class="form-group">
                                    <label for="usr">Email Address*</label>
                                    <input type="text" name="email_address" class="form-control" id="" placeholder="Enter Email Address" value="{{ $profile['billing']['email_address'] }}">
                                    @error('email_address')
                                    <label class="error">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="usr">Phone Number*</label>
                                    <input type="text" name="phone_number" class="form-control" id="" placeholder="Enter Phone Number" value="{{ $profile['billing']['phone_number'] }}">
                                    @error('phone_number')
                                    <label class="error">{{ $message }}</label>
                                    @enderror
                                </div>
                            </div>
                            <hr>
                            <div class="inputwidthsetfull">
                                <div class="form-group">
                                    <label for="usr">Address*</label>
                                    <input type="text" name="address_line1" class="form-control" id="placeSearch" autocomplete="false" placeholder="Enter Dealership Address" value="{{ $profile['billing']['address_line1'] }}">
                                    <label id="placeError" class="error"></label>
                                    <input type="hidden" name="city" id="city" value="{{ $profile['billing']['city'] }}">
                                    <input type="hidden" name="province" id="province" value="{{ $profile['billing']['province'] }}">
                                    <input type="hidden" name="country" id="country" value="{{ $profile['billing']['country'] }}">
                                    <input type="hidden" name="postal_code" id="postal_code" value="{{ $profile['billing']['postal_code'] }}">
                                </div>                              
                            </div>
                            <div class="backnextbtnset">
                                <button type="submit" >UPDATE PROFILE</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab_content" id="planInfo"> 
                  <div class="inputcover">
                    <div class="inputwidthsetfull">
                        <div class="form-group">
                            <label for="usr"> Membership Plan : {{ $membership_plan->plan_name }} - {{ $membership_plan->billing_cycle_name }} @if($membership_plan->discount != '') ({{ $membership_plan->discount }}) @endif</label>
                            <label>Next Billing Date: {{ $membership_plan->expiry_date }}</label>
                        </div>
                    </div>
                    <div class="backnextbtnset">
                        <div class="subbtncancel"> <a href="{{route('dealer.subscription')}}" class="cancelSub" id="cancelPlan" data-toggle="confirmation" data-title="Are you sure?">CANCEL SUBSCRIPTION</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab_content" id="changeCardInfo">
                <form method="post" id="updatepaymentInfo">
                    @csrf   
                    <div class="crdalldetacover">         
                        <div class="inputandcardcover">
                            <div class="inputandcard_right">
                                <div class="preload">
                                    <div class="creditcard">
                                        <div class="front">
                                            <div id="ccsingle"></div>
                                            <svg version="1.1" id="cardfront" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                            x="0px" y="0px" viewBox="0 0 750 471" style="enable-background:new 0 0 750 471;" xml:space="preserve">
                                            <g id="Front">
                                                <g id="CardBackground">
                                                    <g id="Page-1_1_">
                                                        <g id="amex_1_">
                                                            <path id="Rectangle-1_1_" class="lightcolor grey" d="M40,0h670c22.1,0,40,17.9,40,40v391c0,22.1-17.9,40-40,40H40c-22.1,0-40-17.9-40-40V40
                                                            C0,17.9,17.9,0,40,0z" />
                                                        </g>
                                                    </g>
                                                    <path class="darkcolor greydark" d="M750,431V193.2c-217.6-57.5-556.4-13.5-750,24.9V431c0,22.1,17.9,40,40,40h670C732.1,471,750,453.1,750,431z" />
                                                </g>
                                                <text transform="matrix(1 0 0 1 60.106 295.0121)" id="svgnumber" class="st2 st3 st4">@if($profile['card']) {{$profile['card']['number']}} @endif</text>
                                                <text transform="matrix(1 0 0 1 54.1064 428.1723)" id="svgname" class="st2 st5 st6">@if($profile['card']) {{$profile['card']['name']}} @endif</text>
                                                <text transform="matrix(1 0 0 1 54.1074 389.8793)" class="st7 st5 st8">cardholder name</text>
                                                <text transform="matrix(1 0 0 1 479.7754 388.8793)" class="st7 st5 st8">expiration</text>
                                                <text transform="matrix(1 0 0 1 65.1054 241.5)" class="st7 st5 st8">card number</text>
                                                <g>
                                                    <text transform="matrix(1 0 0 1 574.4219 433.8095)" id="svgexpire" class="st2 st5 st9">@if(isset($profile['card']['expiry_month'])) {{$profile['card']['expiry_month'].'/'.$profile['card']['expiry_year']}} @endif</text>
                                                    <text transform="matrix(1 0 0 1 479.3848 417.0097)" class="st2 st10 st11">VALID</text>
                                                    <text transform="matrix(1 0 0 1 479.3848 435.6762)" class="st2 st10 st11">THRU</text>
                                                    <polygon class="st2" points="554.5,421 540.4,414.2 540.4,427.9      " />
                                                </g>
                                                <g id="cchip">
                                                    <g>
                                                        <path class="st2" d="M168.1,143.6H82.9c-10.2,0-18.5-8.3-18.5-18.5V74.9c0-10.2,8.3-18.5,18.5-18.5h85.3
                                                        c10.2,0,18.5,8.3,18.5,18.5v50.2C186.6,135.3,178.3,143.6,168.1,143.6z" />
                                                    </g>
                                                    <g>
                                                        <g>
                                                            <rect x="82" y="70" class="st12" width="1.5" height="60" />
                                                        </g>
                                                        <g>
                                                            <rect x="167.4" y="70" class="st12" width="1.5" height="60" />
                                                        </g>
                                                        <g>
                                                            <path class="st12" d="M125.5,130.8c-10.2,0-18.5-8.3-18.5-18.5c0-4.6,1.7-8.9,4.7-12.3c-3-3.4-4.7-7.7-4.7-12.3
                                                            c0-10.2,8.3-18.5,18.5-18.5s18.5,8.3,18.5,18.5c0,4.6-1.7,8.9-4.7,12.3c3,3.4,4.7,7.7,4.7,12.3
                                                            C143.9,122.5,135.7,130.8,125.5,130.8z M125.5,70.8c-9.3,0-16.9,7.6-16.9,16.9c0,4.4,1.7,8.6,4.8,11.8l0.5,0.5l-0.5,0.5
                                                            c-3.1,3.2-4.8,7.4-4.8,11.8c0,9.3,7.6,16.9,16.9,16.9s16.9-7.6,16.9-16.9c0-4.4-1.7-8.6-4.8-11.8l-0.5-0.5l0.5-0.5
                                                            c3.1-3.2,4.8-7.4,4.8-11.8C142.4,78.4,134.8,70.8,125.5,70.8z" />
                                                        </g>
                                                        <g>
                                                            <rect x="82.8" y="82.1" class="st12" width="25.8" height="1.5" />
                                                        </g>
                                                        <g>
                                                            <rect x="82.8" y="117.9" class="st12" width="26.1" height="1.5" />
                                                        </g>
                                                        <g>
                                                            <rect x="142.4" y="82.1" class="st12" width="25.8" height="1.5" />
                                                        </g>
                                                        <g>
                                                            <rect x="142" y="117.9" class="st12" width="26.2" height="1.5" />
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                            <g id="Back">
                                            </g>
                                        </svg>
                                    </div>
                                    <div class="back">
                                        <svg version="1.1" id="cardback" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        x="0px" y="0px" viewBox="0 0 750 471" style="enable-background:new 0 0 750 471;" xml:space="preserve">
                                        <g id="Front">
                                            <line class="st0" x1="35.3" y1="10.4" x2="36.7" y2="11" />
                                        </g>
                                        <g id="Back">
                                            <g id="Page-1_2_">
                                                <g id="amex_2_">
                                                    <path id="Rectangle-1_2_" class="darkcolor greydark" d="M40,0h670c22.1,0,40,17.9,40,40v391c0,22.1-17.9,40-40,40H40c-22.1,0-40-17.9-40-40V40
                                                    C0,17.9,17.9,0,40,0z" />
                                                </g>
                                            </g>
                                            <rect y="61.6" class="st2" width="750" height="78" />
                                            <g>
                                                <path class="st3" d="M701.1,249.1H48.9c-3.3,0-6-2.7-6-6v-52.5c0-3.3,2.7-6,6-6h652.1c3.3,0,6,2.7,6,6v52.5
                                                C707.1,246.4,704.4,249.1,701.1,249.1z" />
                                                <rect x="42.9" y="198.6" class="st4" width="664.1" height="10.5" />
                                                <rect x="42.9" y="224.5" class="st4" width="664.1" height="10.5" />
                                                <path class="st5" d="M701.1,184.6H618h-8h-10v64.5h10h8h83.1c3.3,0,6-2.7,6-6v-52.5C707.1,187.3,704.4,184.6,701.1,184.6z" />
                                            </g>
                                            <text transform="matrix(1 0 0 1 621.999 227.2734)" id="svgsecurity" class="st6 st7">---</text>
                                            <g class="st8">
                                                <text transform="matrix(1 0 0 1 518.083 280.0879)" class="st9 st6 st10">security code</text>
                                            </g>
                                            <rect x="58.1" y="378.6" class="st11" width="375.5" height="13.5" />
                                            <rect x="58.1" y="405.6" class="st11" width="421.7" height="13.5" />
                                            <text transform="matrix(1 0 0 1 59.5073 228.6099)" id="svgnameback" class="st12 st13">@if($profile['card']) {{$profile['card']['name']}} @endif</text>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="inputandcard_left">
                        <div class="form-container">
                                        <!--<div class="custselectbox">
                                     <p>Card Type*</p>
                                    <select class="js-example-basic-single" name="name">
                                        <option value="AL">Alabama</option>
                                        <option value="WY">Wyoming</option>
                                    </select> 
                                </div>    -->                  
                                <div class="inputwidthsetfull">
                                    <div class="form-group">
                                        <label for="cardnumber">Card Number*</label>
                                        <span id="generatecard"></span>
                                        <input id="cardnumber" name="number" type="text" pattern="[0-9]*" inputmode="numeric" class="form-control" placeholder="@if($profile['card']) {{$profile['card']['number']}} @else XXXX-XXXX-XXXX-XXXX @endif" value="" @if(isset($profile['card']['expiry_month'])) disabled @endif>
                                        <svg id="ccicon" class="ccicon" width="750" height="471" viewBox="0 0 750 471" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink" style="display: none;">
                                    </svg>
                                </div>
                            </div>
                            <div class="inputwidthsetfull">
                                <div class="form-group">
                                    <label for="name">Cardholder Name*</label>
                                    <input id="name" name="name" maxlength="20" type="text" class="form-control" placeholder="Enter Cardholder Name" value="@if($profile['card']) {{$profile['card']['name']}} @endif">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="inputwidthsetbox">
                        <div class="form-group">
                            <label for="expirationdate">Expiry Date*</label>
                            <input id="expirationdate" type="text" pattern="[0-9]*" inputmode="numeric" name="expiry_date" class="form-control" placeholder="MM-YY" value="@if(isset($profile['card']['expiry_month'])) {{$profile['card']['expiry_month'].'/'.$profile['card']['expiry_year']}} @endif">
                        </div>
                        <div class="form-group">
                            <label for="securitycode">CVV*</label>
                            <input id="securitycode" type="text" pattern="[0-9]*" inputmode="numeric" name="cvd" class="form-control" placeholder="123" value="">
                        </div>
                    </div>
                </div>  
            </div>
            <div class="backnextbtnset">
                <a href="javascript:void(0);" style="background: #FF6600;" id="deleteCard"  data-toggle="confirmation" title="Delete" data-action="delete">DELETE CARD</a>
                <button type="submit">UPDATE CARD</button>
            </div>
        </form>
    </div>
</div>
</div>
</div>
</div>
@endsection

@section('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtVAl5jn7PIWC6YSBK-VkIaneaKLIFExM&libraries=places"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/imask/3.4.0/imask.min.js"></script>
<script src="{{ asset('assets/js/card.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
    $('#cancelPlan').confirmation({
        container:"body",
        btnOkLabel:'OK',
        btnCancelLabel:"CANCEL",
        btnOkClass:"btn btn-sm btn-success",
        btnCancelClass:"btn btn-sm btn-danger",
        placement:'top',
        onConfirm:function(event, element) {
            event.preventDefault();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:'POST',
                url:"{{route('dealer.cancel_subscription')}}",
                beforeSend: function(){
                // $('#updateDoctorBtn').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    toastr.success(data.message);
                    location.replace("{{route('website.dealer.login')}}");
                    break;
                    case false:
                    toastr.warning(data.message);
                    break;
                    default:
                    toastr.error("You are not Authorized to access this page");
                    break;
                }
            },
            complete: function(){
                // $('#updateDoctorBtn').button('reset');
            }
        });
        }
    });
    $('#deleteCard').confirmation({
        container:"body",
        btnOkLabel:'OK',
        btnCancelLabel:"CANCEL",
        btnOkClass:"btn btn-sm btn-success",
        btnCancelClass:"btn btn-sm btn-danger",
        placement:'top',
        onConfirm:function(event, element) {
            event.preventDefault();

            $.ajax({
                url:"{{route('dealer.delete_card')}}",
                type:"GET",
                success:function(data){
                    if (data.status==true) { 
                        toastr.success(data.message);             
                        location.reload();
                    } 
                }
            });
        }
    });

    $('#updateProfileForm').validate({
        focusInvalid: true,
        ignore: "",
        rules: {
            name:'required',
            email_address:{
                required:true,
                email:true
            },
            phone_number:'required',
            address_line1:'required'
        },
        messages: {
            name: "Please enter your name",
            email_address:{
                required:"Please enter email address",
            },
            phone_number:'Please enter phone number',
            address_line1:'Please enter address'
        },
        submitHandler: function(form) {
            var formData = $(form).serialize();     

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:'POST',
                url:"{{route('dealer.update_subscription_profile')}}",
                data:formData,
                beforeSend: function(){
                },
                success:function(data) {
                    switch (data.status) {
                        case true:
                        toastr.success(data.message);
                        location.reload();
                        // location.replace("{{route('dealer.dashboard')}}");
                        break;
                        case false:
                        toastr.warning(data.message);
                        break;
                        default:
                        toastr.error("You are not Authorized to access this page");
                        break;
                    }
                },
                complete: function(){
                }
            });
        }
    });


    $('#updatepaymentInfo').validate({
        focusInvalid: true,
        ignore: "",
        rules: {
            name:'required',
            number:{
                required:true,
                // number:true
            },
            expiry_date:'required',
            cvd:'required'
        },
        messages: {
            name: "Please enter card holder name",
            number:{
                required:"Please enter card number",
            },
            expiry_date:'Please enter expiry date',
            cvd:'Please enter cvd'
        },
        submitHandler: function(form) {
            var formData = $(form).serialize();     

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:'POST',
                url:"{{route('dealer.update_card')}}",
                data:formData,
                beforeSend: function(){
                },
                success:function(data) {
                    switch (data.status) {
                        case true:
                        toastr.success(data.message);
                        location.reload();
                        // location.replace("{{route('dealer.dashboard')}}");
                        break;
                        case false:
                        toastr.warning(data.message);
                        break;
                        default:
                        toastr.error("You are not Authorized to access this page");
                        break;
                    }
                },
                complete: function(){
                }
            });
        }
    });

    $('#placeSearch').focus(function() {
        $(this).attr('autocomplete', 'new-placeSearch');
    });

    function initialize() {
        //static coordinates
        var coordinates = {lat: 42.345573, lng: -71.098326};
        
        //simple map instance
        var map = new google.maps.Map(document.getElementById('placeSearch'), {
            center: coordinates,
            zoom: 14,
            streetViewControl: false,   // it hides the street view control (i.e. person icon) from map
        });

        // Set up the markers on the map
        var marker = new google.maps.Marker({
            map: map,
            center: coordinates,
            draggable: false,
            animation: google.maps.Animation.DROP,
        });
        marker.setVisible(true);

        //set the autocomplete
        var input = document.getElementById('placeSearch');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']); // Set the data fields to return when the user selects a place.

        //change listener on each autocomplete action
        autocomplete.addListener('place_changed', function(){
            marker.setVisible(false);
            
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
            console.log(place);
            placeInfo = getPlaceInformation(place);
            $('#city').val(placeInfo['city']);
            $('#country').val(placeInfo['country']);
            $('#postal_code').val(placeInfo['postal_code']);
            $('#province').val(placeInfo['province']);
        });

        //update the street view on dragging of marker
        google.maps.event.addListener(marker, 'dragend', function (event) {
            var newPosition = marker.getPosition();
            setStreetViewMethod(map,newPosition);
            geocodePosition(newPosition);
        });
    }    

    function getPlaceInformation(place){
        placeInfo = [];
        placeInfo['city'] = "";
        placeInfo['province'] = "";
        placeInfo['postal_code'] = "";
        placeInfo['country'] = "";

        placeInfo['name'] = place.name;
        $.each(place.address_components,function(index,value){
            if(value.types[0] == 'postal_code'){
                placeInfo['postal_code'] = value['long_name'];
            }else if(value.types[0] == 'locality' || value.types[0] == 'administrative_area_level_3'){
                placeInfo['city'] = value['long_name'];
            }else if(value.types[0] == 'administrative_area_level_1'){
                placeInfo['province'] = value['long_name'];
            }else if(value.types[0] == 'country'){
                placeInfo['country'] = value['long_name'];
            }
        });
        return placeInfo;
    }

    $(window).load(function(){
        initialize();
        $('#placeSearch').attr('autocomplete', 'new-placeSearch');
    });

</script>
@endsection
