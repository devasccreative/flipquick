<div class="boxandditbox_left" id="sidebar">             
        <div class="hurryupbox">
            <h3>Won Bids</h3>
            <p>{{ $total }} {{($total > 1) ? 'bids' : 'bid'}} </p>  
            <div class="inputwidthsetbox">                                
                                <div class="form-group">
                                    <label for="usr">Year</label>
                                    <select name="year" class="form-control valid" id="year" value="">
                                        <option value="">All Year</option>
                                        @foreach($vehicleYears as $vehicleYear)
                                        <option value="{{$vehicleYear}}" @if(isset($year) && $year == $vehicleYear) selected @endif>{{$vehicleYear}}</option>
                                        @endforeach                                                                                
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Brand</label>
                                    <select name="brand" class="form-control valid" id="brand" value="">
                                        <option value="">All Brand</option>
                                        @foreach($vehicleBrands as $vehicleBrand)
                                        <option value="{{$vehicleBrand}}" @if(isset($brand) && $brand == $vehicleBrand) selected @endif>{{$vehicleBrand}}</option>
                                        @endforeach                                        
                                    </select>
                                    <label id="placeError" class="error"></label>
                                </div> 
                                <div class="form-group">
                                    <label for="usr">Search by VIN Number</label>
                                    <select name="vin_no" class="form-control valid" id="vin_no" value="">
                                        <option value="">All VIN Number</option>
                                        @foreach($vehicleVins as $vehicleVin)
                                        <option value="{{$vehicleVin}}" @if(isset($vin_no) && $vin_no == $vehicleVin) selected @endif>{{$vehicleVin}}</option>
                                        @endforeach                                        
                                    </select>
                                    <label id="placeError" class="error"></label>
                                </div> 
                                <div class="form-group">
                                    <label for="usr">Post type</label>
                                    <select name="post_type" class="form-control valid" id="post_type" value="">
                                        <option value="">All Post type</option>
                                         <option value="sell"  @if(isset($post_type) && $post_type == "sell") selected @endif>sell</option>
                                        <option value="trade for used"  @if(isset($post_type) && $post_type == 'trade for used') selected @endif>trade for used</option>
                                        <option value="trade for new"  @if(isset($post_type) && $post_type == 'trade for new') selected @endif>trade for new</option>

                                    </select>
                                    <label id="placeError" class="error"></label>
                                </div>                 
                            </div>                
        </div>
        <div class="lisditboxset" id="vehicleResult" page="{{$lastPage}}">
            @include('website.won_bids')
        </div>
    </div>
    <div class="boxandditbox_right" id="sidebar">
        <div class="selectcarboxiner">
            <div class="nodetaboxset">
                <img src="{{ asset('assets/svg/FlipQuick_install_our_mobile_app.svg') }}" alt="">
                <h4>Select a vehicle <br>
                listing to view details</h4>
            </div>                  
        </div>
    </div>