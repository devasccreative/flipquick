<header>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="headercovercl">
			@if(auth()->guard('dealer')->check() && auth()->guard('dealer')->user()->email_verified_at && auth()->guard('dealer')->user()->is_approved && Route::currentRouteName() != 'home' && !\Request::is('membership_plan') && !\Request::is('not_approved') && !\Request::is('dealer') && !\Request::is('about_us') && !\Request::is('contact_us') && !\Request::is('pricing') && !Route::is('dealer.benefits'))
			<button class="sidebar-toggle is-closed">
				<img class="menuiconset" src="{{ asset('assets/svg/menu-icon.svg') }}" alt="">
				<img class="menuiconcloseset" src="{{ asset('assets/svg/menu-close-icon.svg') }}" alt="">
			</button>
			@endif
			<div class="@if (!auth()->guard('dealer')->check()) container @endif">		        
				<div class="navbar-header page-scroll">
					@if(Route::currentRouteName() == 'home' || \Request::is('membership_plan') || \Request::is('not_approved') || \Request::is('dealer') || \Request::is('about_us') || \Request::is('contact_us') || \Request::is('pricing'))

					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		                <!-- <span class="sr-only">Toggle navigation</span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span> -->
		                <img src="{{ asset('assets/svg/hamburger.svg') }}" alt="" class="hamburgerbtn">
		                <img src="{{ asset('assets/svg/hamburger-close.svg') }}" alt="" class="hamburger_closebtn">
		            </button>
		            @endif
		            <a class="navbar-brand @if(!(auth()->guard('dealer')->check() && auth()->guard('dealer')->user()->email_verified_at && auth()->guard('dealer')->user()->is_approved && Route::currentRouteName() != 'home' && !\Request::is('membership_plan') && !\Request::is('not_approved') && !\Request::is('dealer') && !\Request::is('about_us') && !\Request::is('contact_us') && !\Request::is('pricing') && !Route::is('dealer.benefits'))) sidbtnno @endif" href="@if(auth()->guard('dealer')->check() && auth()->guard('dealer')->user()->email_verified_at && auth()->guard('dealer')->user()->is_approved) {{route('dealer.dashboard')}} @else {{'https://flipquick.ca'}} @endif"><img src="{{ asset('assets/svg/logo.svg') }}" alt=""></a>
		        </div>

		        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		        	
		        	@if(Route::currentRouteName() == 'home' || \Request::is('membership_plan') || \Request::is('not_approved') || \Request::is('dealer') || \Request::is('about_us') || \Request::is('contact_us') || \Request::is('pricing') || Route::is('dealer.benefits'))
		        	<ul class="nav navbar-nav desktop_nav">
		        		<li class="hidden">
		        			<a href="#page-top"></a>
		        		</li>
		        		{{-- <li class="page-scroll {{ Route::is('about_us') ? 'active' : '' }}">
		        			<a href="https://flipquick.ca/about-us">About Us</a>
		        		</li>
		        		<li class="page-scroll {{ Route::is('pricing') ? 'active' : '' }}">
		        			<a href="{{ route('pricing') }}">Pricing</a>
		        		</li>
		        		<li class="page-scroll {{ Route::is('contact_us') ? 'active' : '' }}">
		        			<a href="{{ route('contact_us') }}">Contact Us</a>
		        		</li> 
		        		@if(!auth()->guard('dealer')->check())
		        		<li class="page-scroll {{ Route::is('dealer/benefits') ? 'active' : '' }}">
		        			<a href="{{ route('dealer.benefits') }}">Benefits</a>
		        		</li>
		        		@endif--}}
		        	</ul>
		        	@endif
		        	@if(Route::currentRouteName() == 'home' || \Request::is('membership_plan') || \Request::is('not_approved') || \Request::is('dealer') || \Request::is('about_us') || \Request::is('contact_us') || \Request::is('pricing') || Route::is('dealer.benefits'))
		        	<ul class="nav navbar-nav mobile_nav">
		        		<li class="hidden">
		        			<a href="#page-top"></a>
		        		</li>
		        		@if(!(auth()->guard('dealer')->check() && auth()->guard('dealer')->user()->email_verified_at && auth()->guard('dealer')->user()->is_approved))
		        		<li class="page-scroll {{ Route::is('login') ? 'active' : '' }}">
		        			<a href="{{ route('website.dealer.login') }}">Login</a>
		        		</li>
		        		<li class="page-scroll {{ Route::is('signup') ? 'active' : '' }}">
		        			<a href="{{ route('website.dealer.signup') }}" class="signupbntbox">Sign Up</a>
		        		</li>
		        		@endif
		        		{{-- <li class="page-scroll {{ Route::is('pricing') ? 'active' : '' }}">
		        			<a href="{{ route('pricing') }}">Pricing</a>
		        		</li>
		        		<li class="page-scroll">
		        			<a href="https://flipquick.ca/about-us">About Us</a>
		        		</li>		        		
		        		<li class="page-scroll {{ Route::is('contact_us') ? 'active' : '' }}">
		        			<a href="{{ route('contact_us') }}">Contact Us</a>
		        		</li>		 --}}        		
		        		@if(!auth()->guard('dealer')->check())
		        		{{--<li class="page-scroll {{ Route::is('dealer/benefits') ? 'active' : '' }}">
		        			<a href="{{ route('dealer.benefits') }}">Benefits</a>
		        		</li>--}}
		        		@endif
		        	</ul>
		        	@endif

		        	@if(!(auth()->guard('dealer')->check() && auth()->guard('dealer')->user()->email_verified_at && auth()->guard('dealer')->user()->is_approved))
		        	<div class="rightlsubtn desktop_nav">
		        		<a href="{{ route('website.dealer.login') }}">Login</a>
		        		<a href="{{ route('website.dealer.signup') }}" class="signupbntbox">SIGN UP</a>
		        	</div>
		        	@endif
		        	
		        </div>	
		        @if(auth()->guard('dealer')->check() && auth()->guard('dealer')->user()->email_verified_at && auth()->guard('dealer')->user()->is_approved)
		        <div class="usermenuboxset">
		        	<div>
		        		<input type="checkbox" id="check">
		        		<label for="check">{{auth()->guard('dealer')->user()->first_name.' '.auth()->guard('dealer')->user()->last_name}}<img src="{{ asset('assets/svg/carat-icon.svg') }}"></label>
		        		<div class="navbox">
		        			<ul>
		        				<li>
		        					<a href="{{ route('dealer.edit_profile') }}">Edit Profile</a>
		        				</li>
		        				<li> 
		        					<a href="{{ route('website.dealer.logout') }}" onclick="event.preventDefault();
		        					document.getElementById('logout-form').submit();">Log Out</a>
		        				</li>
		        				<form id="logout-form" action="{{ route('website.dealer.logout') }}" method="POST" style="display: none;">
		        					@csrf
		        				</form>
		        			</ul>
		        		</div>
		        	</div>
		        </div>
		        @endif

		    </div>
		</div>
	</nav>
</header>
