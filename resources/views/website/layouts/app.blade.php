<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title','FlipQuick')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=""/>

    <meta property="og:url" content="https://" />
    <meta property="og:type" content="type" />
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="{{ asset('images/Flipquick-OG-Image.png') }}" />

    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="{{ asset('favicon/mstile-144x144.png') }}" />
    <meta name="msapplication-square70x70logo" content="{{ asset('favicon/mstile-70x70.png') }}" />
    <meta name="msapplication-square150x150logo" content="{{ asset('favicon/mstile-150x150.png') }}" />
    <meta name="msapplication-wide310x150logo" content="{{ asset('favicon/mstile-310x150.png') }}" />
    <meta name="msapplication-square310x310logo" content="{{ asset('favicon/mstile-310x310.png') }}" />

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ asset('favicon/apple-touch-icon-57x57.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('favicon/apple-touch-icon-114x114.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('favicon/apple-touch-icon-72x72.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('favicon/apple-touch-icon-144x144.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{{ asset('favicon/apple-touch-icon-60x60.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{ asset('favicon/apple-touch-icon-120x120.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{ asset('favicon/apple-touch-icon-76x76.png') }}" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ asset('favicon/apple-touch-icon-152x152.png') }}" />
    <link rel="icon" type="image/png" href="{{ asset('favicon/favicon-196x196.png') }}" sizes="196x196" />
    <link rel="icon" type="image/png" href="{{ asset('favicon/favicon-96x96.png') }}" sizes="96x96" />
    <link rel="icon" type="image/png" href="{{ asset('favicon/favicon-32x32.png') }}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{ asset('favicon/favicon-16x16.png') }}" sizes="16x16" />
    <link rel="icon" type="image/png" href="{{ asset('favicon/favicon-128.png') }}" sizes="128x128" />
   <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-C52KLKF8M7"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-C52KLKF8M7');
</script>
  
    <meta name="_token" content="{!! csrf_token() !!}" />
    <link href="https://fonts.googleapis.com/css?family=Khula:300,400,600,700,800&display=swap" rel="stylesheet">
    @if (auth()->guard('dealer')->check())
    <link rel="stylesheet" href="{{ asset('css/websiteInnerAssets.css') }}">
    @endif
    <link rel="stylesheet" href="{{ asset('css/websiteAssets.css') }}">
     @yield('scrpt')
    @yield('styles')

    <link rel="stylesheet" href="{{ asset('css/lightgallery.css') }}">
    <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
</head>
<body>
	@if(!\Request::is('login') && !\Request::is('password/reset') && Route::currentRouteName() != 'website.dealer.password.update')
    @include('website.layouts.header')
    @endif
    @if (auth()->guard('dealer')->check() && auth()->guard('dealer')->user()->email_verified_at && Route::currentRouteName() != 'home' && !\Request::is('membership_plan') && !\Request::is('not_approved') && !\Request::is('dealer') && !\Request::is('about_us') && !\Request::is('contact_us') && !\Request::is('pricing') && !\Request::is('feedback/*') && !\Request::is('dealer/benefits') && !\Request::is('plan_status'))
    <div class="leftbar_rightbarcover">
      <div class="leftsidebarbox sidebar is-closed">
         @include('website.layouts.left-sidebar')
     </div>
     <div 
    class="rightdetailpart" 
    style="{{ (Route::is('dealer.subscription') || Route::is('dealer.edit_profile') || Route::is('dealer.getNotifications') || Route::is('dealer.users.create') || Route::is('dealer.users.edit'))? 'position: relative;': '' }}">
         @yield('content')
     </div>
 </div>
 @else
 <div class="alldetacoverhompg">
  @yield('content')
</div>
@endif
<script type="text/javascript" src="{{ asset('js/websiteAssets.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-confirmation.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/lightgallery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="{{ asset('js/lg-thumbnail.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lg-fullscreen.min.js') }}"></script>
<script type="text/javascript" src="https://sachinchoolur.github.io/lightGallery/lightgallery/js/lg-zoom.js"></script>


@yield('scripts')

<script>
 function slider(){
    $("#tabs li a").click(function() {
            
            // Active state for tabs
            $(".tabs li a").removeClass("active");
            $(this).addClass("active");
            
            // Active state for Tabs Content
            $(".tab_content_container > .tab_content_active").removeClass("tab_content_active").fadeOut(200);
            $(this.rel).fadeIn(500).addClass("tab_content_active");
            
        }); 
    $("#tabs1 li a").click(function() {
        // Active state for tabs
        $("#tabs1 li a").removeClass("active");
        $(this).addClass("active");
        
        // Active state for Tabs Content
        $("#tab_content1 > .tab_content_active").removeClass("tab_content_active").fadeOut(200);
        $(this.rel).fadeIn(500).addClass("tab_content_active");
        
    }); 
    $("#tabs2 li a").click(function() {
        // Active state for tabs
        $("#tabs2 li a").removeClass("active");
        $(this).addClass("active");
        
        // Active state for Tabs Content
        $("#tab_content2 > .tab_content_active").removeClass("tab_content_active").fadeOut(200);
        $(this.rel).fadeIn(500).addClass("tab_content_active");
        
    }); 
    var $imagesSlider = $(".gallery-slider .gallery-slider__images>div"),
    $thumbnailsSlider = $(".gallery-slider__thumbnails>div");

        // images options
        $imagesSlider.slick({
            speed:300,
            slidesToShow:1,
            slidesToScroll:1,
            cssEase:'linear',
            fade:true,
            draggable:false,
            asNavFor:".gallery-slider__thumbnails>div",
            prevArrow:'.gallery-slider__images .prev-arrow',
            nextArrow:'.gallery-slider__images .next-arrow'
        });

        // thumbnails options
        $thumbnailsSlider.slick({
            speed:300,
            slidesToShow:4,
            slidesToScroll:1,
            cssEase:'linear',
            centerMode:true,
            draggable:false,
            focusOnSelect:true,
            asNavFor:".gallery-slider .gallery-slider__images>div",
            prevArrow:'.gallery-slider__thumbnails .prev-arrow',
            nextArrow:'.gallery-slider__thumbnails .next-arrow',
            responsive: [
            {
                breakpoint: 720,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 350,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
            ]
        });

        /*captions*/

        var $caption = $('.gallery-slider .caption');

        // get the initial caption text
        var captionText = $('.gallery-slider__images .slick-current img').attr('alt');
        updateCaption(captionText);

        // hide the caption before the image is changed
        $imagesSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
            $caption.addClass('hide');
        });

        // update the caption after the image is changed
        $imagesSlider.on('afterChange', function(event, slick, currentSlide, nextSlide){
            captionText = $('.gallery-slider__images .slick-current img').attr('alt');
            updateCaption(captionText);
        });

        function updateCaption(text) {
            // if empty, add a no breaking space
            if (text === '') {
                text = '&nbsp;';
            }
            $caption.html(text);
            $caption.removeClass('hide');
        }
    }

    slider();
</script>
<script>
	$(document).ready(function() {
		$('.js-example-basic-single').select2();
	});
</script>
<script>
    var toggleBtn = document.querySelector('.sidebar-toggle');
    var sidebar = document.querySelector('.sidebar');

    // toggleBtn.addEventListener('click', function() {
    //     toggleBtn.classList.toggle('is-closed');
    //     sidebar.classList.toggle('is-closed');
    // })

</script>
<script>
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
</script>
</body>
</html>
<!-- {{ exit }} -->