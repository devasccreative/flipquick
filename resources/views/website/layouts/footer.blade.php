<footer>
	<div class="fotlogomenubox">
		<div class="fotlogomenu_left">
			<a href="https://flipquick.ca"><img src="{{ asset('assets/svg/logo.svg') }}" alt=""></a>
		</div>
		<div class="fotlogomenu_right">
			<ul>                     
				<li><a href="javascript:void(0);">Download App <span></span></a></li>
				<li><a href="https://flipquick.ca/about-us">About us <span></span></a></li>
				<li><a href="{{ route('pricing') }}">Pricing <span></span></a></li>
				<li><a href="{{ route('website.dealer.login') }}">Login <span></span></a></li>
				<li><a href="{{ route('website.dealer.signup') }}">Sign up</a></li>
			</ul>
			<div class="sociconlink">
				<a href="https://www.facebook.com/FlipQuickAuto/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
				<a href="https://www.instagram.com/flipquickauto/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
				<a href="https://twitter.com/FlipQuickAuto?s=08" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
				<a href="https://www.linkedin.com/company/flipquick" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
	<div class="didevforbox">
		<p>Designed & Developed by <a href="https://foremostdigital.com/">Foremost Digital</a></p>
	</div>
</footer>