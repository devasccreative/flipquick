
<div class="menuboxsidbar">
	<ul>
		@if(auth()->guard('dealer')->user()->hasPermissionTo('dashboard'))
		<li class="{{ Route::is('dealer.dashboard') ? 'active' : '' }}" >
			<a href="{{ route('dealer.dashboard') }}">
				<img src="{{ asset('assets/svg/FlipQuick_dashboard_active.svg') }}" alt="" class="active_icon">
				<img src="{{ asset('assets/svg/FlipQuick_dashboard.svg') }}" alt="" class="inactive_icon">
				<span>Dashboard</span>
			</a>
		</li>
		@endif
		{{--<li class="{{ Route::is('dealer.current_listing') ? 'active' : '' }}" >
			<a href="{{ route('dealer.current_listing') }}">
				<img src="{{ asset('assets/svg/FlipQuick_current_listings_active.svg') }}" alt="" class="active_icon">
				<img src="{{ asset('assets/svg/FlipQuick_current_listings.svg') }}" alt="" class="inactive_icon">
				<span>Current Listings</span>
			</a>
		</li>--}}
		@if(auth()->guard('dealer')->user()->hasPermissionTo('submitted_bids'))
		<li class="{{ Route::is('dealer.submitted_bids') ? 'active' : '' }}" >
			<a href="{{ route('dealer.submitted_bids') }}">
				<img src="{{ asset('assets/svg/FlipQuick_submitted_bids_active.svg') }}" alt="" class="active_icon">
				<img src="{{ asset('assets/svg/FlipQuick_submitted_bids.svg') }}" alt="" class="inactive_icon">
				<span>Submitted Bids</span>
			</a>
		</li>
		@endif
		@if(auth()->guard('dealer')->user()->hasPermissionTo('won_bids'))
		<li class="{{ Route::is('dealer.won_bids') ? 'active' : '' }}" >
			<a href="{{ route('dealer.won_bids') }}">
				<img src="{{ asset('assets/svg/FlipQuick_won_bids_active.svg') }}" alt="" class="active_icon">
				<img src="{{ asset('assets/svg/FlipQuick_won_bids.svg') }}" alt="" class="inactive_icon">
				<span>Won Bids</span>
			</a>
		</li>
		@endif
		@if(auth()->guard('dealer')->user()->hasPermissionTo('subscription_management'))
		<li class="{{ Route::is('dealer.subscription') ? 'active' : '' }}" >
			<a href="{{ route('dealer.subscription') }}">
				<img src="{{ asset('assets/svg/FlipQuick_subscription_management_active.svg') }}" alt="" class="active_icon">
				<img src="{{ asset('assets/svg/FlipQuick_subscription_management.svg') }}" alt="" class="inactive_icon">
				<span>Subscription Management</span>
			</a>
		</li>
		@endif
		<li class="{{ Route::is('dealer.edit_profile') ? 'active' : '' }}" >
			<a href="{{ route('dealer.edit_profile') }}">
				<img src="{{ asset('assets/svg/FlipQuick_account_settings_active.svg') }}" alt="" class="active_icon">
				<img src="{{ asset('assets/svg/FlipQuick_account_settings.svg') }}" alt="" class="inactive_icon">
				<span>Account Settings</span>
			</a>
		</li>
		<li class="{{ Route::is('dealer.getNotifications') ? 'active' : '' }}" >
			<a href="{{ route('dealer.getNotifications') }}">
				<img src="{{ asset('assets/svg/FlipQuick_notifications_active.svg') }}" alt="" class="active_icon">
				<img src="{{ asset('assets/svg/FlipQuick_notifications.svg') }}" alt="" class="inactive_icon">
				<span>Notifications</span>
			</a>
		</li>
		@if(auth()->guard('dealer')->user()->hasRole('Dealer'))
		<li class="{{ (Route::is('dealer.users.index') || Route::is('dealer.users.create') || Route::is('dealer.users.edit')) ? 'active' : '' }}" >
			<a href="{{ route('dealer.users.index') }}">
				<img src="{{ asset('assets/svg/FlipQuick_users_active.svg') }}" alt="" class="active_icon">
				<img src="{{ asset('assets/svg/FlipQuick_users.svg') }}" alt="" class="inactive_icon">
				<span>User Roles</span>
			</a>
		</li>
		@endif
		<li class="{{ Route::is('dealer.benefits') ? 'active' : '' }}" >
			<a href="{{ route('dealer.benefits') }}">
			<img src="{{ asset('assets/svg/FlipQuick_benefits_active.svg') }}" alt="" class="active_icon">
			<img src="{{ asset('assets/svg/FlipQuick_benefits.svg') }}" alt="" class="inactive_icon">
			<span>Benefits</span>
		</a>
		<li class="{{ Route::is('website.dealer.logout') ? 'active' : '' }}" >
			<a href="{{ route('website.dealer.logout') }}" onclick="event.preventDefault();
			document.getElementById('logout-form').submit();">
			<img src="{{ asset('assets/svg/FlipQuick_logout_active.svg') }}" alt="" class="active_icon">
			<img src="{{ asset('assets/svg/FlipQuick_logout.svg') }}" alt="" class="inactive_icon">
			<span>Logout</span>
		</a>
	</li>
</ul>
</div>