@section('title','FlipQuick | About Us')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="container">
	<div class="pagetitlebg">
		<h2>About Us</h2>
	</div>
</div>

<!-- <div class="container">
	<div class="benflpboxcover">
		<h3>Benefits of FlipQuick</h3>
		<div class="benflpboxiner">
			<div class="benflpboxiner_icon">
				<img src="{{ asset('assets/svg/safety-icon.svg') }}" alt="">
			</div>
			<div class="benflpboxiner_text">
				<h4>Safety</h4>
				<p>You will only be in touch with a certified dealership. This decreases the chance of harm associated with meeting unknown people in unknown locations.</p>
			</div>
		</div>
		<div class="benflpboxiner">
			<div class="benflpboxiner_icon">
				<img src="{{ asset('assets/svg/time-icon.svg') }}" alt="">
			</div>
			<div class="benflpboxiner_text">
				<h4>Time</h4>
				<p>Posting your vehicle with FlipQuick saves both effort and time. It eliminates the need for several private showings as well as transportation between multiple dealerships. It is physically impossible to meet with every dealership that FlipQuick gives you access to, especially within 24 hours.</p>
			</div>
		</div>
		<div class="benflpboxiner">
			<div class="benflpboxiner_icon">
				<img src="{{ asset('assets/svg/value-icon.svg') }}" alt="">
			</div>
			<div class="benflpboxiner_text">
				<h4>Value</h4>
				<p>Dealerships are given the opportunity to place only one offer on your vehicle. Since they are competing with eachother, they will only give you their best offer. This saves you the process of negotiating, and connects you directly to the dealership willing to pay you the most for your vehicle.</p>
			</div>
		</div>
		<div class="benflpboxiner">
			<div class="benflpboxiner_icon">
				<img src="{{ asset('assets/svg/pro-icon.svg') }}" alt="">
			</div>
			<div class="benflpboxiner_text">
				<h4>Become a pro</h4>
				<p>Our app guides you through what information is required for dealerships to accurately evaluate your vehicle.</p>
			</div>
		</div>
	</div>
</div> -->

<div class="container abtUs">
	<div class="aryodealcover mrgsetbox">
		<div class="aryodealbox_left">
			<!-- <h3>Are you a Dealership 
				looking to buy vehicles 
			from local sellers?</h3> -->
			<p>FlipQuick is Canada's first and only service that provides an interactive marketplace that links public sellers and certified dealerships. Our service was created to get you the most for your vehicle in the most convenient and efficient way possible. </p>		

			<p>If you need support from our customer service team, click below<br> <a href="{{ route('contact_us') }}">Contact Us!</a> </p>
		</div>
		<div class="aryodealbox_right">
			<img src="{{ asset('assets/images/page-title-4.png') }}" alt="">		
		</div>
	</div>
</div>
<!-- <div class="container">
	<div class="abtlextimgcover">
		<div class="abtlextimg_left">
			<h5>Conversation Together In One Place</h5>
			<h3>Redesign your Website into Modern Look and get started</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris purus arcu, sollicitudin quis malesuada ac, posuere et nunc. Maecenas a congue lacus. Nullam cursus eros dui vel rhoncus.</p>			
		</div>
		<div class="abtlextimg_right">
			<img src="{{ asset('assets/images/page-title-4.png') }}" alt="">
		</div>
	</div>
</div> -->




@include('website.layouts.footer')



@endsection

@section('scripts')

@endsection