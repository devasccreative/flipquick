@section('title','FlipQuick | Verify Email')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="cretactbox">
    <div class="cretact_title">
        <h3>Please verify your email address</h3>
    </div>
    <form method="post" id="verifyEmail">
        @csrf
        <div class="verifemlbox">
            <h5>We have sent a 4-digit verification code to<br>
                {{auth()->guard('dealer')->user()->email}}</h5>
                <div class="codinputbox">
                    <div class="form-group">
                        <input type="text" class="form-control verify" minlength="1" maxlength="1" name="otp1" placeholder="" value="">
                        <input type="text" class="form-control verify" minlength="1" maxlength="1" name="otp2" placeholder="" value="">
                        <input type="text" class="form-control verify" minlength="1" maxlength="1" name="otp3" placeholder="" value="">
                        <input type="text" class="form-control verify" minlength="1" maxlength="1" name="otp4" placeholder="" value="">
                    </div>
                    <div id="errordiv"></div>
                    <input type="hidden" name="email" value="{{auth()->guard('dealer')->user()->email}}">
                </div>
                <div class="resecodbtn">
                    <a href="javascript:void(0)" id="resendEmail">Resend Code</a>
                </div>
                
            </div>
            
            <div class="veriemailbtn">
                <button type="submit" href="javascript:void(0)">VERIFY EMAIL ADDRESS</button>
            </div>
        </form>
    </div>
    @endsection

    @section('scripts')
    <script type="text/javascript" src="{{ asset('assets/js/jquery.autotab.min.js') }}"></script>
    <script type="text/javascript">
        $.autotab({ tabOnSelect: true });
        $('.verify').autotab('filter', 'verify');

        $('#verifyEmail').validate({
        focusInvalid: true,
        ignore: "",
        rules: {
            otp1:{
                required: true,
                number:true
            },
            otp2:{
                required: true,
                number:true
            },
            otp3:{
                required: true,
                number:true
            },
            otp4:{
                required: true,
                number:true
            },
        },
        messages: {
            otp1: "Please enter OTP",
            otp2:"Please enter OTP",
            otp3:"Please enter OTP",
            otp4:"Please enter OTP",
        },
        errorPlacement: function(error, element) {
         error.html('#errordiv');
     },
        submitHandler: function(form) {
            var formData = $(form).serialize();     

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:'POST',
                url:"{{route('website.dealer.emailVerification')}}",
                data:formData,
                beforeSend: function(){
                // $('#updateDoctorBtn').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    toastr.success(data.message);
                    location.replace("{{route('dealer.dashboard')}}");
                    break;
                    case false:
                    toastr.warning(data.message);
                    break;
                    default:
                    toastr.error("You are not Authorized to access this page");
                    break;
                }
            },
            complete: function(){
                // $('#updateDoctorBtn').button('reset');
            }
        });
        }
    });



        /* RESEND OTP */
        $(document).on('click','#resendEmail',function(e){
            e.preventDefault();
            $.ajax({
                url: "{{ route('website.dealer.resendEmailOtp') }}",
                type:"POST",
                data:{email:$('[name="email"]').val()},
                beforeSend: function(msg){
                    $("body").css("opacity",'0.5');
                },
                success: function(data) {
                    $("body").css("opacity",'1');
                    if (data.status == true) {
                        toastr.success(data.message);
                    }
                }
            });
        });
</script>
@endsection