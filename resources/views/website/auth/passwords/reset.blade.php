@extends('website.layouts.app')

@section('content')
<div class="allloginformcover">
   <div class="signup_innerbox">
    <form method="POST" action="{{ route('website.dealer.password.update') }}">
        @csrf
        <div class="login_toplogobox">
           <img src="{{ asset('admin_assets/svg/admin-logo.svg') }}">
           <h3>{{ __('Reset Password') }}</h3>
       </div>
       <input type="hidden" name="token" value="{{ $token }}">
       <div class="logindetail_inputbox">
         <div class="form-group">
             <p>Email address</p>
             <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

             @if ($errors->has('email'))
             <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="logindetail_inputbox">
     <div class="form-group">
         <p>Password</p>
         <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

         @if ($errors->has('password'))
         <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
    </div>
</div>
<div class="logindetail_inputbox">


    <div class="form-group ">
        <p>{{ __('Confirm Password') }}</p>

        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
    </div>
</div>
<div class="signbtn_box">
        <button type="submit" class="btn btn-primary">
            {{ __('Reset Password') }}
        </button>
</div>
</form>
</div>
</div>
@endsection
