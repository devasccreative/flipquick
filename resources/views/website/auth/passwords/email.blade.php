@extends('website.layouts.app')

@section('content')
<div class="allloginformcover">
   <div class="signup_innerbox">
       <form method="POST" action="{{ route('website.dealer.password.email') }}" aria-label="{{ __('Admin Login') }}">
           @csrf
           <div class="login_toplogobox">
               <img src="{{ asset('admin_assets/svg/admin-logo.svg') }}">
               <h3>{{ __('Reset Password') }}</h3>
           </div>
           <div class="logindetail_inputbox">
               <div class="form-group">
                   <p>Email address</p>
                   <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                   @if ($errors->has('email'))
                   <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="signbtn_box">
           <button type="submit">{{ __('Send Password Reset Link') }}</button>
       </div>
   </form>
</div>
</div>

@endsection
