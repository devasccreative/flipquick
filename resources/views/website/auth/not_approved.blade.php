@section('title','FlipQuick | Verify Email')
@extends('website.layouts.app')

@section('scrpt')
  <!-- Global site tag (gtag.js) - Google Ads: 656133024 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-656133024"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-656133024'); </script>
@endsection
@section('content')
<div class="cretactbox">
    <!-- <div class="cretact_title">
        <h3>Please verify your email address</h3>
    </div> -->
    <div class="verifemlbox">
        <h5>Hello! {{auth()->guard('dealer')->user()->first_name.' '.auth()->guard('dealer')->user()->last_name}}<br>
            Your account is under review</h5>
        </div>
    </div>

    @endsection

    @section('scripts')
    <script type="text/javascript">
    </script>
    @endsection