@section('title','FlipQuick | Membership Plan')
@extends('website.layouts.app')
@section('styles')

@endsection
@section('content')
<div class="cretactbox">
    <div class="cretact_title">
        <h3>Hello {{auth()->guard('dealer')->user()->first_name}},</h3>
        <p>Here are the two types of membership plans to fit your needs!</p>
    </div>
    <form method="post" id="membershipPlan">
        @csrf
        <div class="pricingboxcover">
            <?php $i=1; ?>
            @foreach($membership_plans as $membership_plan)
            @if(!auth()->guard('dealer')->user()->membership_plan || (auth()->guard('dealer')->user()->membership_plan && $membership_plan->plan_name !='Free'))
            <div class="radio @if(auth()->guard('dealer')->user()->membership_plan_id == $membership_plan->id) active @endif">
                <div class="pricingboxiner box{{$i}}">
                    <h3>${{ $membership_plan->amount }}</h3>
                    <h3>{{ $membership_plan->plan_name }}</h3>
                    <p>{{ $membership_plan->billing_cycle_name }}</p>
                    <hr>
                    @if($membership_plan->discount != '')
                    <span>{{$membership_plan->discount}}</span>
                    @endif
                    <input id="radio-{{$i}}" name="membership_plan_id" type="radio" value="{{$membership_plan->id}}" @if(auth()->guard('dealer')->user()->membership_plan_id == $membership_plan->id) checked @endif>
                    <label for="radio-{{$i}}" class="radio-label"></label>
                </div>
            </div>
            <?php $i++; ?>
            @endif
            @endforeach

            <!-- <div class="radio">
                <div class="pricingboxiner box2">
                    <h3>$100</h3>
                    <p>billed monthly</p>
                    <hr>
                    <span>FULL PRICE</span>
                    <input id="radio-2" name="membership_name" type="radio">
                    <label  for="radio-2" class="radio-label"></label>
                </div>
            </div>
            <div class="radio">
                <div class="pricingboxiner box3">
                    <h3>$900</h3>
                    <p>billed annually</p>
                    <hr>
                    <span>30% OFF</span>
                    <input id="radio-3" name="membership_name" type="radio">
                    <label  for="radio-3" class="radio-label"></label>
                </div>
            </div> -->
        </div>
        <div class="backnextbtnset veriemailbtn">
            @if(auth()->guard('dealer')->user()->membership_plan_id != '' && (auth()->guard('dealer')->user()->membership_plan && \Carbon\Carbon::now()->diffInDays(auth()->guard('dealer')->user()->membership_plan->created_at) >= 30))
            <a href="{{route('dealer.subscription')}}" style="background: #FF6600;">GO BACK</a>
            <a href="javascript:void(0);" id="changePlan" data-toggle="confirmation" data-title="Are you sure?">SUBMIT</a>
            <input type="hidden" name="change_plan" value="true">
            <div class="subbtncancel"> <a href="{{route('dealer.subscription')}}" class="cancelSub" id="cancelPlan" data-toggle="confirmation" data-title="Are you sure?">CANCEL SUBSCRIPTION</a>
            </div>
            @else
            <button type="submit">NEXT</button>
            @endif

        </div>
    </form>
</div>
@endsection

@section('scripts')
@if(session('message') != '')
<script type="text/javascript">
    toastr.warning("{{session('message')}}");
</script>
@endif
<script type="text/javascript">
   $('#changePlan').confirmation({
    container:"body",
    btnOkLabel:'OK',
    btnCancelLabel:"CANCEL",
    btnOkClass:"btn btn-sm btn-success",
    btnCancelClass:"btn btn-sm btn-danger",
    placement:'top',
    onConfirm:function(event, element) {
        event.preventDefault();

        $('#membershipPlan').submit();
    }
});
   $('#cancelPlan').confirmation({
    container:"body",
    btnOkLabel:'OK',
    btnCancelLabel:"CANCEL",
    btnOkClass:"btn btn-sm btn-success",
    btnCancelClass:"btn btn-sm btn-danger",
    placement:'top',
    onConfirm:function(event, element) {
        event.preventDefault();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'POST',
            url:"{{route('dealer.cancel_subscription')}}",
            beforeSend: function(){
                // $('#updateDoctorBtn').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    toastr.success(data.message);
                    location.replace("{{route('website.dealer.login')}}");
                    break;
                    case false:
                    toastr.warning(data.message);
                    break;
                    default:
                    toastr.error("You are not Authorized to access this page");
                    break;
                }
            },
            complete: function(){
                // $('#updateDoctorBtn').button('reset');
            }
        });
    }
});
   $('#membershipPlan').validate({
    focusInvalid: true,
    ignore: "",
    rules: {
        membership_plan_id:{
            required: true,
        },
    },
    messages: {
        membership_plan_id: "Please select membership plan",
    },
    errorPlacement: function(error, element) {
     error.html('#errordiv');
 },
 submitHandler: function(form) {
    var formData = $(form).serialize();     

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method:'POST',
        url:"{{route('website.dealer.addMembershipPlan')}}",
        data:formData,
        beforeSend: function(){
                // $('#updateDoctorBtn').button('loading');
            },
            success:function(data) {
                switch (data.status) {
                    case true:
                    toastr.success(data.message);
                    location.replace(data.url);
                    break;
                    case false:
                    toastr.warning(data.message);
                    break;
                    default:
                    toastr.error("You are not Authorized to access this page");
                    break;
                }
            },
            complete: function(){
                // $('#updateDoctorBtn').button('reset');
            }
        });
}
});

</script>
@endsection