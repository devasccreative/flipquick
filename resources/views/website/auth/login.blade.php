@extends('website.layouts.app')


@section('content')
<div class="allloginformcover">

   <div class="signup_innerbox">
       <form method="POST" action="{{ route('website.dealer.login') }}">
           @csrf
           <div class="login_toplogobox">
               <img src="{{ asset('admin_assets/svg/admin-logo.svg') }}">
               <h3>Sign in to Dealer Account</h3>
               <p>Enter your details below</p>
           </div>
           <div class="logindetail_inputbox">
               <div class="form-group">
                   <p>Email address</p>
                   <input type="text" name="email" class="form-control" placeholder="Enter email" value="{{ old('email') }}" required autofocus>
                   @if ($errors->has('email'))
                   <div class="help-block with-errors text-left">
                       <ul class="list-unstyled">
                           <li>{{ $errors->first('email') }}</li>
                       </ul>
                   </div>
                   @endif
               </div>
           </div>
           <div class="logindetail_inputbox">
               <div class="form-group">
                   <p>Password</p>
                   <!-- <a href="javascript:void(0)">Forgot Password ?</a> -->
                   <input type="password" name="password" class="form-control" placeholder="Enter password" value="{{ old('password') }}" required autofocus>
                   @if ($errors->has('password'))
                   <div class="help-block with-errors text-left">
                       <ul class="list-unstyled">
                           <li>{{ $errors->first('password') }}</li>
                       </ul>
                   </div>
                   @endif
               </div>
           </div>
           <div class="customcheck">
               <label class="checkcontainer">Keep me logged in
                   <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                   <span class="checkmark"></span>
               </label>
           </div>
           <a class="forgotPassword" href="{{route('website.dealer.password.request')}}">Forgot Password?</a>
           <div class="signbtn_box">
               <button type="submit">Sign In</button>
           </div>
           <div class="actlogbntbox">
            <p>Create new account? <a href="{{route('website.dealer.signup')}}">Signup Now</a></p>
        </div>
    </form>
</div>
</div>
@endsection
@section('scripts')
@if(session('message'))
<script type="text/javascript">
 toastr.warning('{{ session('message') }}');
</script>
@endif
@endsection