@section('title','FlipQuick | Feedback')
@extends('website.layouts.app')
@section('styles')
<style type="text/css">
	/* The container */
.container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default radio button */
.container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
  border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.container input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the indicator (dot/circle) when checked */
.container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the indicator (dot/circle) */
.container .checkmark:after {
 	top: 9px;
	left: 9px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: white;
}
</style>
@endsection
@section('content')
<div class="container" id="contactustitle" @if($feedback->dealership) style="display: none;" @endif>
	<div class="pagetitlebg">
		<h2>FeedBack</h2>
	</div>
</div>

<div class="container" id="contactuspanel" @if($feedback->dealership) style="display: none;" @endif>
	<div class="contformcover">
		<div class="contforminer cretact_title">
			<p>Hi {{$feedback->seller->first_name.' '.$feedback->seller->last_name}}, thank you for trusting FlipQuick to help sell your vehicle. We want to make sure your experience was stellar! </p>
			<form id="contactusform">	
				<input type="hidden" name="token" value="{{$feedback->token}}">
				
				<div class="form-group">
					<label for="usr">Please let us know if {{$feedback->dealer->dealership_name}} followed through with their bid of {{$feedback->vehicle->bids->count()}}. </label>
					<label class="container">Yes
					  <input type="radio" checked="checked" id="radio-1" name="dealership"  value="Yes">
					  <span class="checkmark"></span>
					</label>
					<label class="container">No
					  <input type="radio" name="dealership" id="radio-2"  value="No">
					  <span class="checkmark"></span>
					</label>
				</div>
				<div class="form-group">
					<label for="usr">Were you happy with your experience using FlipQuick
					</label>
					<label class="container">Yes
					  <input type="radio" checked="checked" id="radio-3" name="experience"  value="Yes">
					  <span class="checkmark"></span>
					</label>
					<label class="container">No
					  <input type="radio" name="experience" id="radio-4"  value="No">
					  <span class="checkmark"></span>
					</label>
				</div>
				<div class="form-group">
					<label for="usr">Please give feedback below: </label>
					<textarea class="form-control" rows="5" id="" name="feedback"></textarea>
				</div>
				<div class="sedbtnbox">
					<button type="submit" class="" id="contactsubmit">SEND</button>
				</div>

				<div id="mailstatus">
					
				</div>
			</form>
		</div>	
	</div>	
</div>
@if($feedback->dealership)
<div class="thnkpagecover" id="feedback1" style="">
	<h3>Sorry !</h3>
	<p>Link Expired !</p>
	<a href="{{route('home')}}">Back to homepage</a>
</div>
@endif

<div class="thnkpagecover" id="feedback" style="display: none;">
	<img src="{{ asset('assets/svg/thank-you-icon.svg') }}" alt="">
	<h3>Thank you !</h3>
	<p>For Feedback.</p>
	<a href="{{route('home')}}">Back to homepage</a>
</div>

@include('website.layouts.footer')

@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js" integrity="sha256-sPB0F50YUDK0otDnsfNHawYmA5M0pjjUf4TvRJkGFrI=" crossorigin="anonymous"></script>
<script>
	$(document).ready(function(){
		$('#feedback').hide();
	});
	$('#contactsubmit').on( 'click', function( evt ) {
		evt.preventDefault();
		var serialized = $('#contactusform').serialize();
	    		$.ajax( {
	    			url: '{{route("submitFeedback")}}', 
	    			method: 'POST',
	    			data: serialized, 
	    		}).done( function( result ){
	    			if(result.status == "false"){
	    				
	                   	//	toastr.error(result.message);
	                   }
	                   else{
	                   	$('#contactusform')[0].reset();
	                   	$('#mailstatus').html(result.message);
	                   	$('#feedback').show();
	                   	$('#feedback1').hide();
	                   	$('#contactuspanel').hide();
	                   	$('#contactustitle').hide();
	                   }
	               });
	});

	    $('#contactusform').validate({ // initialize the plugin
	    	rules: {
	    		dealership: {
	    			required: true,
	    		},
	    		experience: {
	    			required: true,
	    		},
	    		feedback: {
	    			required: true,
	    		},
	    		
	    	},
	    	submitHandler: function(form) {
	    		var serialized = $('#contactusform').serialize();
	    		$.ajax( {
	    			url: '{{route("submitFeedback")}}', 
	    			method: 'POST',
	    			data: serialized, 
	    		}).done( function( result ){
	    			if(result.status == "false"){
	    				
	                   	//	toastr.error(result.message);
	                   }
	                   else{
	                   	$('#contactusform')[0].reset();
	                   	$('#mailstatus').html(result.message);
	                   	$('#feedback').show();
	                   	$('#contactuspanel').hide();
	                   	$('#contactustitle').hide();
	                   }
	               });
	    	}
	    });	

	</script>
	@endsection