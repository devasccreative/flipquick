<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{$title}}</title>
	<meta charset="utf-8">
<!-- 	<meta name="viewport" content="width=device-width, initial-scale=1">
 -->	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,600i,700,800|Roboto:400,400i,500,500i,700,900" rel="stylesheet">
</head>
<body style="background: #F1F1F1;">
	<div style="width: 100%; text-align: center; margin-top: 40px; display: inline-block; margin-bottom: 40px;">
		<div style="width: 600px;margin: 0 auto;text-align: left;text-align: center;background: #ffffff;padding-top: 30px;padding-bottom: 14px;padding-left: 14px;padding-right: 14px;">
			
			<p style="font-family: 'Open Sans', sans-serif; margin-bottom: 30px; font-size: 18px; color: #01034A; font-weight: 500;  line-height: 20px; text-align: center;">Hello! New Listing is posted.<span style="font-weight: 800;"> Submit your bids.</span> </p>
			<div style="margin:0 auto;padding-left: 0;margin-top: 0;float: left;width: 560px;">				
				<p style="font-family: 'Open Sans', sans-serif; margin-top: 0;margin-bottom: 4px; font-size: 15px; color: #01034A; font-weight: 600;  line-height: 20px; text-align: center;">{{$message}}</p>			
			</div>
			
			<div style="width: 560px; border-top: 1px solid #dedede; margin-top: 30px; display: inline-block;text-align: center;">
				<a href="JavaScript:Void(0)" style="display: inline-block;"><img src="{{asset('assets/images/logo.png')}}" style="width: 120px;margin-top: 20px;"></a>
			</div>
			<div style="width: 560px;display: inline-block; text-align: center; color: #01034A; font-family: 'Open Sans', sans-serif; font-weight: 600; margin-top: 3px; margin-bottom: 2px;">
				<p style="margin: 0px; margin-top: 5px; font-size: 12px; text-align: center;">Made by FlipQuick</p>
				<p style="margin-top: 5px; font-size: 12px; text-align: center;">3456 91 Street NW, Edmonton, AB T6E5R1 Canada</p>
			</div>
		</div>
	</div>
</body>
</html>