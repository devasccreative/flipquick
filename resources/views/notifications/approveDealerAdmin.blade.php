<!DOCTYPE html>
<html lang="en">
<head>
	<title>Approve Dealer </title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,600i,700,800|Roboto:400,400i,500,500i,700,900" rel="stylesheet">
</head>
<body>
<div style="width: 100%; text-align: center; margin-top: 50px;">
	<div style="width: 600px; margin: 0 auto; text-align: left;">
		
		<div style="margin-top: 9px;">
			<h3 style="margin: 0px; font-size: 28px; font-weight: 700; font-family: 'Roboto', Bold;">One new sign up from {{$dealer->dealership_name}}. Please approve this account.</h3>
		</div>

		{{-- <div style="text-align: center;">
			<a href="JavaScript:Void(0)" style="display: inline-block;"><img src="{{ asset('admin_assets/images/logo.png') }}" style="width: 50px;margin-top: 20px;"></a>
		</div> --}}
		<div style="text-align: center; color: #000000; font-family: 'Open Sans', sans-serif; font-weight: 700; margin-top: 13px; margin-bottom: 30px;">
			<p style="margin: 0px; margin-top: 10px; font-size: 14px;text-align: center;">Made by FlipQuick INC.</p>
			<!-- <p style="margin-top: 5px; font-size: 14px;text-align: center;">Canada</p> -->
		</div>
	</div>
</div>
</body>
</html>