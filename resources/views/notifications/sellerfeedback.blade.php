<!DOCTYPE html>
<html lang="en">
<head>
	<title>Feedback</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,600i,700,800|Roboto:400,400i,500,500i,700,900" rel="stylesheet">
</head>
<body>
<div style="width: 100%; text-align: center; margin-top: 50px;">
	<div style="width: 600px; margin: 0 auto; text-align: left;">
		
		<div style="margin-top: 9px;">
			<p style="font-family: 'Open Sans', sans-serif; font-weight: 700; line-height: 20px; font-size: 13px;">Hi {{$userName}}, thank you for trusting FlipQuick to help sell your vehicle. We want to make sure your experience was stellar! </p>
		</div>
		<div style="text-align: center; padding-top: 11px; padding-bottom: 10px; margin-top: 24px;">
			<p style="font-family: 'Open Sans', sans-serif; font-weight: 700; line-height: 20px; font-size: 13px;">Please give feedback by clicking below button:</p>
			<a href="{{route('feedback',$feedback->token)}}" style="background: #F14413;color: #ffffff;font-family: 'Poppins', sans-serif;font-weight: 600;text-decoration: none;padding: 8px 10px 8px;display: inline-block;border-radius: 4px;font-size: 15px;margin-bottom: 34px;" target="_blank">Submit Feedback</a>
		</div>
		{{--<div style="text-align: center;">
			<a href="JavaScript:Void(0)" style="display: inline-block;"><img src="{{ asset('admin_assets/images/logo.png') }}" style="width: 50px;margin-top: 20px;"></a>
		</div>--}}
		<div style="text-align: center; color: #000000; font-family: 'Open Sans', sans-serif; font-weight: 700; margin-top: 13px; margin-bottom: 30px;">
			<p style="margin: 0px; margin-top: 10px; font-size: 14px;text-align: center;">Made by FlipQuick INC.</p>
<!-- 			<p style="margin-top: 5px; font-size: 14px;text-align: center;">Canada</p>
 -->		</div>
	</div>
</div>
</body>
</html>
