<!DOCTYPE html>
<html lang="en">
<head>
	<title>Won Bid Payment Failed</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,600i,700,800|Roboto:400,400i,500,500i,700,900" rel="stylesheet">
</head>
<body style="background: #F1F1F1;">
	<div style="width: 100%; text-align: center; margin-top: 40px; display: inline-block; margin-bottom: 40px;">
		<div style="width: 600px;margin: 0 auto;text-align: left;text-align: center;background: #ffffff;padding-top: 30px;padding-bottom: 14px;">
			
			<p style="font-family: 'Open Sans', sans-serif; margin-bottom: 30px; font-size: 18px; color: #01034A; font-weight: 500;  line-height: 20px; text-align: center;">Payment Failed for won bid due to <b> {{$message}} </b>. Vehicle detail:</p>
			
			<div style="margin:0 auto;width: 560px;text-align: center;float: left;margin-bottom: 14px;">
				<a href="JavaScript:Void(0);"><img src="{{ $vehicle->vehicle_exterior_photos[0]->image }}" alt="" style="width: 160px; border-radius: 8px;"></a>
			</div>
			<div style="margin:0 auto;padding-left: 0;margin-top: 0;float: left;width: 560px;">		
			<p style="font-family: 'Open Sans', sans-serif; margin-top: 0;margin-bottom: 4px; font-size: 15px; color: #01034A; font-weight: 600;  line-height: 20px; text-align: center;">Bid Amount : ${{$vehicle->bid_ammount}}</p>		
				<p style="font-family: 'Open Sans', sans-serif; margin-top: 0;margin-bottom: 4px; font-size: 15px; color: #01034A; font-weight: 600;  line-height: 20px; text-align: center;">{{ $vehicle->year.' '.$vehicle->make.' '.$vehicle->trim.' '.$vehicle->style.' '.$vehicle->model }}</p>			
				<p style="font-family: 'Open Sans', sans-serif; margin-top: 0;margin-bottom: 4px; font-size: 15px; color: #01034A; font-weight: 500;  line-height: 20px; text-align: center;">
					<span style="font-weight: 600; text-align: center;">VIN </span>: {{ $vehicle->vin_no }}</p>
					<p style="font-family: 'Open Sans', sans-serif; margin-top: 0;margin-bottom: 4px; font-size: 15px; color: #01034A; font-weight: 500;  line-height: 20px; text-align: center;">
						<span style="font-weight: 600; text-align: center;">Seller Info. </span>: {{$vehicle->seller->first_name.' '.$vehicle->seller->last_name.' ( '.$vehicle->seller->country_code.'-('.substr($vehicle->seller->phone, 0, 3).') '.substr($vehicle->seller->phone, 3, 3).'-'.substr($vehicle->seller->phone, 6, 10)}})</p>
						<a href="{{ $vehicle->link }}" style="font-family: 'Open Sans', sans-serif;background: #FF6600;color: #ffffff;text-decoration: none;font-size: 17px;padding: 6px  12px 6px;display: inline-block;border-radius: 4px;margin-top: 12px; text-align: center;">View Details</a>

					</div>
					
					<div style="width: 560px; border-top: 1px solid #dedede; margin-top: 30px; display: inline-block;text-align: center;">
						<a href="JavaScript:Void(0)" style="display: inline-block;"><img src="{{asset('assets/images/logo.png')}}" style="width: 120px;margin-top: 20px;"></a>
					</div>
					<div style="width: 560px;display: inline-block; text-align: center; color: #01034A; font-family: 'Open Sans', sans-serif; font-weight: 600; margin-top: 3px; margin-bottom: 2px;">
						<p style="margin: 0px; margin-top: 5px; font-size: 12px; text-align: center;">Made by FlipQuick</p>
<!-- 						<p style="margin-top: 5px; font-size: 12px; text-align: center;">3456 91 Street NW, Edmonton, AB T6E5R1 Canada</p>
 -->					</div>
				</div>
			</div>
		</body>
		</html>