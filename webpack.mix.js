const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.js('resources/js/app.js', 'public/js')
 .sass('resources/sass/app.scss', 'public/css')
 .styles(['public/admin_assets/css/bootstrap.min.css',
 	'public/assets/font-awesome/css/font-awesome.min.css',
 	'public/assets/css/datatables.min.css',
 	'public/assets/css/toastr.min.css',
 	'public/assets/css/slick.min.css',
 	// 'public/assets/css/custom.css',
 	'public/assets/css/styles.css',
 	'public/assets/css/dashboard.css',
 	'public/assets/css/select2.min.css',
 	], 'public/css/all.css')
 .styles(['public/assets/css/bootstrap.min.css',
 	'public/assets/font-awesome/css/font-awesome.min.css',
 	// 'public/assets/css/slick.css',
 	'public/assets/css/styles.css',
 	// 'public/assets/css/dashboard.css',
 	'public/assets/css/custom.css',
 	], 'public/css/websiteAssets.css')
 .styles([
 	'public/assets/css/slick.css',
 	'public/assets/css/dashboard.css',
 	'public/assets/css/select2.min.css',
 	'public/assets/css/toastr.min.css',
 	], 'public/css/websiteInnerAssets.css')
 .scripts(['public/assets/js/jquery.js',
 	'public/assets/js/bootstrap.min.js',
 	'public/admin_assets/js/jquery.validate.min.js',
 	'public/assets/js/select2.min.js',
 	'public/assets/js/slick.js',
 	'public/assets/js/toastr.min.js',
 	], 'public/js/websiteAssets.js')
 .scripts(['public/assets/js/jquery.js',
 	'public/assets/js/bootstrap.min.js',
 	'public/assets/js/datatables.min.js',
 	'public/assets/js/jquery.validate.min.js',
 	'public/assets/js/toastr.min.js',
 	'public/assets/js/bootstrap-confirmation.min.js',
 	'public/assets/js/slick.js',
 	'public/assets/js/select2.min.js',
 	], 'public/js/all.js');

